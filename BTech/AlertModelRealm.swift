//
//  Specimen.swift
//  BTech
//
//  Created by Ahmed Akhtar on 26/10/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import Foundation
//import RealmSwift

class AlertModelRealm: NSObject {
    
    //dynamic var appearance:NSNumber? = nil
    //dynamic var day:String?
    //dynamic var eve_id:String?
    //dynamic var eve_sec:String?
    //dynamic var hour:String?
    //dynamic var striped_values:NSArray?
    //dynamic var tree:String?
    
    @objc dynamic var blocktype:String?
    @objc dynamic var alert_id:String?
    @objc dynamic var category:String?
    @objc dynamic var channel:String?
     @objc dynamic var desc:String?
    @objc dynamic var dest_ip:String?
    @objc dynamic var dest_port:String?
    @objc dynamic var device_id:String?
    @objc dynamic var hostname:String?
    @objc dynamic var ip:String?
    @objc dynamic var mac_address:String?
    @objc dynamic var input_src:String?
    @objc dynamic var device_category:String?
    @objc dynamic var msg:String?
    @objc dynamic var namespace:String?
    @objc dynamic var priority:String?
    @objc dynamic var timestamp:String?
    @objc dynamic var src_ip:String?
    @objc dynamic var src_port:String?
    @objc dynamic var postkey:String?
    @objc dynamic var test_id:String?
    @objc dynamic var direction:String?
    @objc dynamic var detail:String?
    
    
    
    @objc dynamic var eve_sec:String?
    @objc dynamic var internetOn:String?
    @objc dynamic var internetOff:String?
    @objc dynamic var tree:String!
    @objc dynamic var record_id:String?
    @objc dynamic var day:String?
    @objc dynamic var appearance:String?
    @objc dynamic var hour:String?
    @objc dynamic var type:String?
    @objc dynamic var reading:String?
    @objc dynamic var OS:String?
    @objc dynamic var seen:String?
    @objc dynamic var dateStored:Date?
    
    
    func populateDefaultAlerts(_ blockType : String, alert_id : String, category : String, channel : String, desc : String, dest_ip : String, dest_port : String, device_id : String, hostname : String, ip : String, mac_address : String, input_src : String, device_category : String, msg : String, namespace : String, priority : String, timestamp : String, src_ip : String, src_port : String, postkey : String, test_id : String, direction : String, detail : String, eve_sec : String, internetOn : String, internetOff : String, tree : String, record_id : String, day : String, appearance : String, hour : String, type : String, reading : String, OS : String) {
        
//        let realm = try! Realm()
        let newAlert = AlertModelRealm()
        newAlert.blocktype = blockType
        newAlert.alert_id = alert_id
        newAlert.category = category
        newAlert.channel = channel
        newAlert.desc = desc
        newAlert.dest_ip = dest_ip
        newAlert.dest_port = dest_port
        newAlert.device_id = device_id
        newAlert.hostname = hostname
        newAlert.ip = ip
        newAlert.mac_address = mac_address
        newAlert.input_src = input_src
        newAlert.device_category = device_category
        newAlert.msg = msg
        newAlert.namespace = namespace
        newAlert.priority = priority
        newAlert.timestamp = timestamp
        newAlert.src_ip = src_ip
        newAlert.src_port = src_port
        newAlert.postkey = postkey
        newAlert.test_id = test_id
        newAlert.direction = direction
        newAlert.detail = detail
        
        
        newAlert.eve_sec = eve_sec
        newAlert.internetOn = internetOn
        newAlert.internetOff = internetOff
        newAlert.tree = tree
        newAlert.record_id = record_id
        newAlert.day = day
        newAlert.appearance = appearance
        newAlert.hour = hour
        newAlert.type = type
        newAlert.reading = reading
        newAlert.OS = OS
        newAlert.seen = ""
        newAlert.dateStored = Date()
        
        
        
//        try! realm.write({
//            realm.add(newAlert)
//        })
    }
    
    
    static func updateAlertsWith(_ query_alert_id : String, query_record_id : String, blockType : String, alert_id : String, category : String, channel : String, desc : String, dest_ip : String, dest_port : String, device_id : String, hostname : String, ip : String, mac_address : String, input_src : String, device_category : String, msg : String, namespace : String, priority : String, timestamp : String, src_ip : String, src_port : String, postkey : String, test_id : String, direction : String, detail : String, eve_sec : String, internetOn : String, internetOff : String, tree : String, record_id : String, day : String, appearance : String, hour : String, type : String, reading : String, OS : String) {
        
        
//        let objects = retrieveAlerts().filter("record_id == %@ AND alert_id == %@",query_record_id,query_alert_id)
//        if objects.count == 0 {
//            return
//        }
//        let realm = try! Realm()
        
//        do{
//            try realm.write({
//
//
//
//                objects.first!.blocktype = blockType
//                objects.first!.alert_id = alert_id
//                objects.first!.category = category
//                objects.first!.channel = channel
//                objects.first!.desc = desc
//                objects.first!.dest_ip = dest_ip
//                objects.first!.dest_port = dest_port
//                objects.first!.device_id = device_id
//                objects.first!.hostname = hostname
//                objects.first!.ip = ip
//                objects.first!.mac_address = mac_address
//                objects.first!.input_src = input_src
//                objects.first!.device_category = device_category
//                objects.first!.msg = msg
//                objects.first!.namespace = namespace
//                objects.first!.priority = priority
//                objects.first!.timestamp = timestamp
//                objects.first!.src_ip = src_ip
//                objects.first!.src_port = src_port
//                objects.first!.postkey = postkey
//                objects.first!.test_id = test_id
//                objects.first!.direction = direction
//                objects.first!.detail = detail
//
//
//                objects.first!.eve_sec = eve_sec
//                objects.first!.internetOn = internetOn
//                objects.first!.internetOff = internetOff
//                objects.first!.tree = tree
//                objects.first!.record_id = record_id
//                objects.first!.day = day
//                objects.first!.appearance = appearance
//                objects.first!.hour = hour
//                objects.first!.type = type
//                objects.first!.reading = reading
//                objects.first!.OS = OS
//                objects.first!.seen = ""
//
//            })
//        }catch{
//
//        }

        
    }

    
    
    static func deleteRecordWith(_ recordID : String, alertID : String)
    {
//        let objects = retrieveAlerts().filter("record_id == %@ AND alert_id == %@",recordID,alertID)
//        let realm = try! Realm()
//
//        do{
//            try realm.write({
//                realm.delete(objects)
//            })
//        }catch{
//
//        }
        
    }
    
    
    
    
    static func deleteRecordOlderThanOneDay()
    {
//        let objects = retrieveAlerts()
//        let realm = try! Realm()
//
//        do{
//            try realm.write({
//                for object in objects{
//                    let timeInterval : TimeInterval = NSDate().timeIntervalSince(object.dateStored!)
//                    let ti = NSInteger(timeInterval)
//                    let hours = (ti / 3600)
//                    if(hours>=24)
//                    {
//                        realm.delete(object)
//                    }
//                }
//
//            })
//        }catch{
//
//        }
        
    }
    
    
    
    
    static func updateRecordWith(_ recordID : String, alertID : String, update:String)
    {
//        let objects = retrieveAlerts().filter("record_id == %@ AND alert_id == %@",recordID,alertID)
//        if objects.count == 0 {
//            return
//        }
//        let realm = try! Realm()
//
//        do{
//            try realm.write({
//                objects.first?.seen = update
//            })
//        }catch{
//
//        }
        
    }
    
    
    static func checkIfRecordExistsWith(_ recordID : String, alertID : String) -> Bool
    {
//        let objects = retrieveAlerts().filter("record_id == %@ AND alert_id == %@",recordID,alertID)
//        if objects.count > 0 {
//            return true
//        }
//        else{
//            return false
//        }
        return false
        
    }

    
    
    static func deleteAllRecords()
    {
//        let objects = retrieveAlerts()
//        let realm = try! Realm()
//
//        do{
//            try realm.write({
//                realm.delete(objects)
//            })
//        }catch{
//
//        }
        
    }

    
    
//    static func retrieveAlerts() -> Results<AlertModelRealm> {
//
////        let realm = try! Realm()
////        return realm.objects(AlertModelRealm.self)
//        return Any
//
//    }
    
    
    

    
    
}

