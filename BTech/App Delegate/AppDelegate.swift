//
//  AppDelegate.swift
//  BTech
//
//  Created by Ahmad Waqas on 22/07/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Reachability
//import ReachabilitySwift
import PubNub
//import SlideMenuControllerSwift
//import Fabric
//import Crashlytics
import UserNotifications
//import ZDCChat
import WatchConnectivity
import Firebase
import UserNotifications
import AppTrackingTransparency
import FirebaseFirestore
import FirebaseMessaging

typealias UnixTime = Double
extension UnixTime {
    fileprivate func formatType(_ form: String) -> DateFormatter {
        
        //let localTimeZoneAbbreviation: String = NSTimeZone.localTimeZone().abbreviation!
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.dateFormat = form
        return dateFormatter
    }
    var dateFull: Date {
        return Date(timeIntervalSince1970: Double(self))
    }
    var toHour: String {
        return formatType("h:mm").string(from: dateFull)
    }
    var toDay: String {
        return formatType("MMM-dd-yyyy h:mm:a").string(from: dateFull)
    }
}
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PNObjectEventListener, MessagingDelegate,  UNUserNotificationCenterDelegate, WCSessionDelegate {
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("hello")
    }
    #if os(iOS)
    public func sessionDidBecomeInactive(_ session: WCSession) { }
    public func sessionDidDeactivate(_ session: WCSession) {
        session.activate()
    }
    #endif
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "receivedwatchMessage"), object: self, userInfo: message)
        
    }
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    var reachability: Reachability?
    // Instance property
    var client: PubNub?
    var countSensorData : Int = 0
    let defaults = UserDefaults.standard
    //    var deviceToken : String = ""
    
    override init() {
        super.init()
        FirebaseApp.configure()
        
        let settings = FirestoreSettings()
        settings.isPersistenceEnabled = false
        // Enable offline data persistence
        let db = Firestore.firestore()
        db.settings = settings
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //get app id from db
        let appID = NetworkHelper.sharedInstance.getCurrentAppID()
//        ZDCChat.initialize(withAccountKey: "5luXmZ1u6TcE3yW7s9iIHMwUZkEMHTuk")
        
        NetworkHelper.sharedInstance.checkAppRole()
        
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        
        if(appID == "")
        {
            NetworkHelper.sharedInstance.app_ID = UIDevice.current.identifierForVendor?.uuidString
        }
        NetworkHelper.sharedInstance.app_ID = appID
        UIApplication.shared.statusBarStyle = .lightContent
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Book", size: 20)!]
        IQKeyboardManager.shared.enable = true
        self.initialiseNetwokManager()
        
        //        if let option = launchOptions {
        //            let info = option[UIApplication.LaunchOptionsKey.remoteNotification]
        //            if (info != nil) {
        //                //                goToAlertsView()
        //            }}
        // Override point for customization after application launch.
        //os Check
        
        // Override point for customization after application launch.
        //os Check
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        Messaging.messaging().delegate = self
        
     //   Messaging.messaging().shouldEstablishDirectChannel = true
        
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
                //you got permission to track
                print("got permissions for ios14+")
            })
        } else {
            //you got permission to track, iOS 14 is not yet installed
            print("got permission below ios14")
        }
        
        return true
    }
    
    func goToAlertsView() {
        //call view alerts screen nav
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let apptVC = storyboard.instantiateViewController(withIdentifier: "ViewAlerts") as! ViewAlerts
        //        let apptVC = storyboard.instantiateViewController(withIdentifier: "MainMenu") as! MainMenu
        let navigationController = UINavigationController.init(rootViewController: apptVC)
        //        UIApplication.shared.statusBarStyle = .lightContent
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }
    func applicationWillResignActive(_ application: UIApplication) {
        
        NetworkHelper.sharedInstance.isNotificationLocked = true
        NetworkHelper.sharedInstance.viewAlertsPushedInNavigationStack = false
        NotificationCenter.default.post(name: Notification.Name(rawValue: "enterBackgroundNotification"), object: nil);
        if(NetworkHelper.sharedInstance.deviceToken != nil)
        {
            (UIApplication.shared.delegate as! AppDelegate).UnmuteNotifications()
        }
        //NetworkHelper.sharedInstance.pushNotification = true
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        //NetworkHelper.sharedInstance.viewAlertsPushedInNavigationStack = false
        //Logging out of chat
        //        ServicesManager.instance().chatService.disconnect(completionBlock: nil)
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        if(NetworkHelper.sharedInstance.deviceToken != nil)
        {
            print("unmuting at applicationWillEnterForeground AppDelegate.swift")
            (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
        }
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        //NSNotificationCenter.defaultCenter().postNotificationName("enterForegroundNotification", object: nil);
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("applicationDidBecomeActive151 called")
        NetworkHelper.sharedInstance.isNotificationLocked =  true
        if(NetworkHelper.sharedInstance.deviceToken != nil)
        {
            print("unmuting at applicationDidBecomeActive AppDelegate.swift")
            (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
        }
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func checkTopControllerViewAlerts()->Bool
    {
        let navigationController = window?.rootViewController as! UINavigationController
        let vc = navigationController.topViewController
        if(vc!.isKind(of: ViewAlerts.self))
        {
            return true
        }
        else{
            return false
        }
    }
    
    
    //MARK:- Slider Methods
    
    
    func setSliderOptions() -> Void {
        
//        SlideMenuOptions.contentViewDrag = true
//        SlideMenuOptions.hideStatusBar = false
//        SlideMenuOptions.leftViewWidth = UIScreen.main.bounds.size.width - 80
//        SlideMenuOptions.simultaneousGestureRecognizers = true
    }
    
    func setRootViewController() -> Void {
        print("root view controller called")
        let frontController = MainMenu.MainMenuVC()
        //let frontController = Home.homeVC()
        let mainViewController = UINavigationController(rootViewController: frontController)
        self.window?.rootViewController = mainViewController
        self.window?.makeKeyAndVisible()
        mainViewController.navigationBar.tintColor = UIColor.white
        mainViewController.navigationBar.barTintColor = CozyLoadingActivity.Settings.CLAppThemeColor
        mainViewController.navigationBar.topItem?.title = ""
        mainViewController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
    }
    
    
    //MARK:- Network Avalibility Manager
    
    func initialiseNetwokManager() {
        //declare this inside of viewWillAppear
//        do {
//            reachability =  Reachability()
//        } catch {
//            print("Unable to get Reachability")
//            return
//        }
//
//        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.reachabilityChanged(_:)),name: Notification.Name.reachabilityChanged,object: reachability)
//        do{
//            try reachability?.startNotifier()
//        }catch{
//            print("could not start reachability notifier")
//        }
    }
    
    @objc func reachabilityChanged(_ note: Notification) {
        
        let reachability = note.object as! Reachability
        NetworkHelper.sharedInstance.isNetworkAvailable = reachability.isReachable
        if reachability.isReachable {
            
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            print("Network not reachable")
        }
    }
    
    
    
    //MARK:- APNS & PUBNUB
    
    //19-jan    func registerForPushNotifications(_ application: UIApplication) {
    //
    //        print("registerForPushNotifications248 called")
    //        let notificationSettings = UIUserNotificationSettings(
    //            types: [.badge, .sound, .alert], categories: nil)
    //        application.registerUserNotificationSettings(notificationSettings)
    //
    //        print("registerForPushNotifications251 called")
    //        //for ios10 register for push notifications only
    //
    //        if #available(iOS 10, *) {
    //            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
    //
    //                switch settings.authorizationStatus {
    //                case .notDetermined:
    //                    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: { (granted, error) in
    //                        // You might want to remove this, or handle errors differently in production
    //                        assert(error == nil)
    //                        if granted {
    //
    //                            application.registerUserNotificationSettings(notificationSettings)
    ////                            UIApplication.shared.registerForRemoteNotifications()
    //                        }
    //                    })
    //                case .authorized:
    //
    //                    DispatchQueue.main.async {
    //                        application.registerUserNotificationSettings(notificationSettings)
    //                    }
    ////                    UIApplication.shared.registerForRemoteNotifications()
    //                case .denied:
    //                    let useNotificationsAlertController = UIAlertController(title: "Turn on notifications", message: "This app needs notifications turned on for the best user experience", preferredStyle: .alert)
    //                    let goToSettingsAction = UIAlertAction(title: "Go to settings", style: .default, handler: { (action) in
    //
    //                    })
    //                    let cancelAction = UIAlertAction(title: "Cancel", style: .default)
    //                    useNotificationsAlertController.addAction(goToSettingsAction)
    //                    useNotificationsAlertController.addAction(cancelAction)
    //                    self.window?.rootViewController?.present(useNotificationsAlertController, animated: true)
    //                default :
    //                    print("nothing happens for ios 10, *")
    //                }
    //            }
    //        } else if #available(iOS 8, *) {
    //            let settings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)
    //            UIApplication.shared.registerUserNotificationSettings(settings)
    //        } else {
    //            print("We cannot handle iOS 7 or lower in this example. Contact support@pubnub.com")
    //        }
    //        //end of APNS for ios10
    //    }
    
    
    //A11221
    
    
    
    func muteNotifications()
    {
        //19-jan        print("muteNotifications310 called")
        //        client?.removePushNotificationsFromChannels(getViewAlertChannels(), withDevicePushToken: NetworkHelper.sharedInstance.deviceToken! as Data, andCompletion: { acknowledgementStatus in
        //            if !acknowledgementStatus.isError {
        //                acknowledgementStatus.clientRequest?.url?.absoluteString
        //                print("successfully Unsubscribed.", acknowledgementStatus.clientRequest?.url!.absoluteString as Any)
        //            }
        //        })
        
    }
    
    func UnsubUserForNotifications() {
        //19-jan        print("UnsubUserForNotifications320 called")
        ////        let token: Data = NetworkHelper.sharedInstance.uuidToken!.data(using: .utf8)! // non-nil
        ////andCompletion: <#T##PNPushNotificationsStateModificationCompletionBlock?##PNPushNotificationsStateModificationCompletionBlock?##(PNAcknowledgmentStatus) -> Void#>) removeAllPushNotificationsFromDevice(withPushToken:  NetworkHelper.sharedInstance.deviceToken!,
        ////        print("DeviceToken after parsing from string at UnsubUserForNotifications== \(token)")
        //        if NetworkHelper.sharedInstance.deviceToken != nil {
        //            self.client?.removeAllPushNotificationsFromDeviceWithPushToken(NetworkHelper.sharedInstance.deviceToken!,  andCompletion: { (status) in
        //
        //            NetworkHelper.sharedInstance.uuidToken = "";
        //            if !status.isError {
        //                print("apns unsubscribed")
        ////                self.client!.add(self)
        ////                self.client?.unsubscribe(fromChannels: self.getViewAlertChannels(), withPresence: false)
        //                self.client?.addListener(self)
        //                self.client?.unsubscribeFromChannels(self.getViewAlertChannels(), withPresence: false)
        //
        //                self.client?.unsubscribeFromAll()
        //                /**
        //                 Handle successful push notification disabling for all channels associated with
        //                 specified device push token.
        //                 */
        //            }
        //            else {
        //
        //                /**
        //                 Handle modification error. Check 'category' property
        //                 to find out possible reason because of which request did fail.
        //                 Review 'errorData' property (which has PNErrorData data type) of status
        //                 object to get additional information about issue.
        //
        //                 Request can be resent using: status.retry();
        //                 */
        //            }
        //        })
        //        }
        
    }
    
    func UnmuteNotifications()
    {
        //19-jan        print("UnmuteNotifications359 called")
        //            self.client?.addPushNotificationsOnChannels(getViewAlertChannels(),
        //                                                       withDevicePushToken: NetworkHelper.sharedInstance.deviceToken!,
        //                                                       andCompletion: { (status) in
        //
        //            if !status.isError {
        //                status.clientRequest?.url?.absoluteString
        //                print("successfully subscribed.", status.clientRequest?.url!.absoluteString)
        //            }
        //        })
        
    }
    
    //A11221
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        print("didRegister notificationSettings297 called")
        if notificationSettings.types != UIUserNotificationType() {
            application.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("didRegisterForRemoteNotificationsWithDeviceToken377 called")
        Messaging.messaging().apnsToken = deviceToken
        //register for d.moat pubnub notifications
        print("devicetoken in didRegisterForRemoteNotificationsWithDeviceToken == \(deviceToken)")
        NetworkHelper.sharedInstance.deviceToken = deviceToken
        let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var tokenString = ""
        for i in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        print("Device Token:", tokenString)
        print("Device Token received:", NetworkHelper.sharedInstance.deviceToken)
        
        defaults.set(deviceToken, forKey: "deviceToken")
        defaults.synchronize()
        NetworkHelper.sharedInstance.uuidToken = tokenString
        
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("didFailToRegisterForRemoteNotificationsWithError306 called")
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print("444")
        print("push notification received didReceiveRemoteNotification448 called")
        
        NetworkHelper.sharedInstance.pushNotification = true
        //            goToAlertsView()
        NotificationCenter.default.post(name: Notification.Name(rawValue: "pushNotification"), object: nil, userInfo : userInfo);
        
        print("push notification received 425 \(userInfo)")
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        setRootViewController()
        //coordinateToSomeVC
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        print("446")
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)
        setRootViewController()
        completionHandler(UIBackgroundFetchResult.newData)
    }
    //
    //A11221
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
    print("Firebase registration token: \(fcmToken)")
    
    UserDefaults.standard.set(fcmToken, forKey: "deviceToken")
    
    let dataDict: [String: String] = ["token": fcmToken!]
    NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    
//        InstanceID.instanceID().instanceID { (result, error) in
//            if let error = error {
//                print("Error fetching remote instance ID: \(error)")
//            } else if let result = result {
//                print("Remote instance ID token: \(result.token)")
//                UserDefaults.standard.set(result.token, forKey: "deviceToken")
//                NetworkHelper.sharedInstance.FCM_DEVICE_TOKEN = result.token
//            }
//        }
    
        NetworkHelper.sharedInstance.FCM_DEVICE_TOKEN = fcmToken
    
    
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}
    
    
    
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//
//        print("504")
//        print("Received data message 84: \(remoteMessage.appData)")
//        //        coordinateToSomeVC(userInfo: remoteMessage.appData)
//
//        setRootViewController()
//
//        //        let userInfo = remoteMessage.
//    }
    
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingDelegate) {
        
        print("504")
        print("Received data message 84: \(remoteMessage.description)")
        //        coordinateToSomeVC(userInfo: remoteMessage.appData)
        
        setRootViewController()
        
        //        let userInfo = remoteMessage.
    }
    
    
    // This method will be called when app received push notifications in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        print("499")
        if self.window?.rootViewController?.foregroundViewController is ChatViewController {
            completionHandler([])
        } else {
            completionHandler([.alert, .badge, .sound])
        }
//        completionHandler([.alert, .badge, .sound])
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        print("523")
        // Do whatever you want when the user tapped on a notification
        // If you are waiting for a specific value from the notification
        // (e.g., associated with key "valueKey"),
        // you can capture it as follows then do the navigation:
        
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        print(userInfo["notification_type"] as? String ?? "")
        //        if userInfo["notification_type"] as! String == "chat_notification" {
        //            //navigate to chat VC
        //        }
        let keyExists = userInfo["notification_type"] != nil
        
        if keyExists{
            if userInfo["notification_type"] as! String == "chat_notification" {
                print("key is present in the userinfor, navigate to specific window")
                print(userInfo["notification_type"] as? String ?? "")
                let id = UserDefaults.standard.value(forKey: "channelFirebaseID") as! String? ?? ""
                print("firebaseID== \(id)")
                if id != "" {
                    
                    
                }
            }
        } else {
            print("key is not present in the userinfo")
        }
        
        //        setRootViewController()
        completionHandler()
    }
    
    func openChatViewController() {
        let vc = Profile.ProfileVC()
        let mainViewController = UINavigationController(rootViewController: vc)
        self.window?.rootViewController = mainViewController
        self.window?.makeKeyAndVisible()
        mainViewController.navigationBar.tintColor = UIColor.white
        mainViewController.navigationBar.barTintColor = CozyLoadingActivity.Settings.CLAppThemeColor
        mainViewController.navigationBar.topItem?.title = ""
        mainViewController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    

    func subscribeToChannelsList(_ list: [String])
    {
        //19-jan        print("subscribeToChannelsList484 called")
        //        // Instantiate configuration instance.
        //        let configuration = PNConfiguration(publishKey: "pub-c-eca16a80-264f-4971-9dad-60ce16cf6707", subscribeKey: "sub-c-6a9b76f8-cb99-11e7-9319-62175e58f2c1")
        //
        //        if UserDefaults.standard.object(forKey: "deviceToken") != nil {
        //            NetworkHelper.sharedInstance.uuidToken = UserDefaults.standard.value(forKey: "deviceToken") as? String ?? ""
        //
        //        }
        //
        //                if NetworkHelper.sharedInstance.uuidToken == "" {
        //                NetworkHelper.sharedInstance.uuidToken = NetworkHelper.sharedInstance.getRandomUUIDString()
        //                }else {
        //                    }
        //        // Instantiate PubNub client.
        //        configuration.uuid = NetworkHelper.sharedInstance.uuidToken!
        //        configuration.TLSEnabled = true
        ////        configuration.stripMobilePayload = false
        //        self.client = PubNub.clientWithConfiguration(configuration)
        //        self.client!.addListener(self)
        //        self.client!.subscribeToChannels(list, withPresence: false)
        ////        }
    }
    
    
    func updateViewAlertArrayWith(_ alertID : String, recordID : String, alertModel : AlertModel)
    {
        //19-jan        for object in NetworkHelper.sharedInstance.arrayViewAlerts
        //        {
        //            if(object.alert_id == alertID && object.record_id == recordID)
        //            {
        //                let index : Int = NetworkHelper.sharedInstance.arrayViewAlerts.firstIndex(of: object)!
        //                NetworkHelper.sharedInstance.arrayViewAlerts[index] = alertModel
        //            }
        //        }
    }
    
    
    func getViewAlertChannels() -> [String]
    {
        let channelDmoatInfo : String = "info_f" + NetworkHelper.sharedInstance.getCurrentUserNamespace()
        let channelNetConfig : String = "netconfig_f" + NetworkHelper.sharedInstance.getCurrentUserNamespace()
        let channelResult : String = "result_f" + NetworkHelper.sharedInstance.getCurrentUserNamespace()
        
        NetworkHelper.sharedInstance.arrayChannelListViewAlert.append(channelDmoatInfo)
        NetworkHelper.sharedInstance.arrayChannelListViewAlert.append(channelNetConfig)
        NetworkHelper.sharedInstance.arrayChannelListViewAlert.append(channelResult)
        
        
        return NetworkHelper.sharedInstance.arrayChannelListViewAlert
        
    }
    
    func calculateDaysFromWeight(_ weight: Int) -> [Int]
    {
        var weight = weight
        var indexes = [Int]()
        indexes = [0,0,0,0,0,0,0]
        var j : Int = 0
        var i : Int = 6
        while(i >= 0)
        {
            let result : Int = Int(2^i)
            if(weight>=result)
            {
                weight = weight - result
                indexes[j] = 1
            }
            j = j + 1
            i = i - 1
        }
        
        return indexes
    }
    
    func calculateDaysStringFromIndexexArray(_ indexes : [Int]) -> String
    {
        var daysString : String = ""
        
        if(indexes[0] == 0)
        {
            
        }
        else{
            daysString = daysString + "Mon,"
        }
        
        if(indexes[1] == 0)
        {
            
        }
        else{
            daysString = daysString + "Tue,"
            
        }
        
        if(indexes[2] == 0)
        {
            
        }
        else{
            daysString = daysString + "Wed,"
            
        }
        
        if(indexes[3] == 0)
        {
            
        }
        else{
            daysString = daysString + "Thu,"
            
        }
        
        if(indexes[4] == 0)
        {
            
        }
        else{
            daysString = daysString + "Fri,"
            
        }
        
        if(indexes[5] == 0)
        {
            
        }
        else{
            daysString = daysString + "Sat,"
            
        }
        
        if(indexes[6] == 0)
        {
            
        }
        else{
            daysString = daysString + "Sun,"
            
        }
        
        daysString = String(daysString.dropLast())
        return daysString
        
    }
    //
    
    func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {
        print("1122pubnub didReceiveMessage called \(message)")
        CozyLoadingActivity.hide(nil, animated: true)
        
        let channelNetConfig : String = "netconfig_f" + NetworkHelper.sharedInstance.getCurrentUserNamespace()
        
        //check if channel is that of netconfig
        if(message.data.channel == channelNetConfig)
        {
            let  dict = message.data.message as! NSDictionary
            print("NetConfigDict== \(dict)")
            let appID = dict["app_id"] as? String
            if(appID != NetworkHelper.sharedInstance.app_ID)
            {
                return
            }
            
            let broadcast = dict["broadcast"]! as AnyObject?
            let default_lease_time = dict["default_lease_time"]! as AnyObject?
            let end_ip_range = dict["end_ip_range"]! as AnyObject?
            let gateway = dict["gateway"]! as AnyObject?
            //let iface = dict["iface"]! as AnyObject?
            let ip = dict["ip"]! as AnyObject?
            //let mac_address = dict["mac_address"]! as AnyObject?
            let max_lease_time = dict["max_lease_time"]! as AnyObject?
            let netmask = dict["netmask"]! as AnyObject?
            let router_ip = dict["router_ip"]! as AnyObject?
            let start_ip_range = dict["start_ip_range"]! as AnyObject?
            let subnet = dict["subnet"]! as AnyObject?
            
            let broadcastString:String = broadcast as! String
            let defaultLeaseTimeString:String = default_lease_time as! String
            let endIpRangeString:String = end_ip_range as! String
            let gatewayString:String = gateway as! String
            //let ifaceString:String = iface as! String
            let ipString:String = ip as! String
            //let macAddressString = mac_address as! String
            let maxLeaseTimeString = max_lease_time as! String
            let netmaskString = netmask as! String
            let routerIpString = router_ip as! String
            let startIpRangeString = start_ip_range as! String
            let subnetString = subnet as! String
            
            let advanceMode = AdvanceModeModel()
            advanceMode.broadcastString = broadcastString
            advanceMode.defaultLeaseTimeString = defaultLeaseTimeString
            advanceMode.endIpRangeString = endIpRangeString
            advanceMode.gatewayString = gatewayString
            //advanceMode.ifaceString = ifaceString
            advanceMode.ipString = ipString
            //advanceMode.macAddressString = macAddressString
            advanceMode.maxLeaseTimeString = maxLeaseTimeString
            advanceMode.netmaskString = netmaskString
            advanceMode.routerIpString = routerIpString
            advanceMode.startIpRangeString = startIpRangeString
            advanceMode.subnetString = subnetString
            
            NetworkHelper.sharedInstance.advanceModeModel = advanceMode
            NotificationCenter.default.post(name: Notification.Name(rawValue: "AdvanceModeNotification"), object: nil);
            
        }
        
    }
    
    
    func convertTimeStamp(_ eve_sec : String, model : AlertModel)
    {
        if(eve_sec != "")
        {
            let valueAfterMultiply = Double(eve_sec)!
            let ms : UnixTime = valueAfterMultiply
            model.eve_sec = ms.toDay
        }
    }
    
    
    // Handle subscription status change.
    func client(_ client: PubNub, didReceive status: PNStatus) {
        print("1122didreceive status for pubnub called")
        if status.operation == .subscribeOperation {
            
            // Check whether received information about successful subscription or restore.
            if status.category == .PNConnectedCategory || status.category == .PNReconnectedCategory {
                
                let subscribeStatus: PNSubscribeStatus = status as! PNSubscribeStatus
                if subscribeStatus.category == .PNConnectedCategory {
                    
                    // This is expected for a subscribe, this means there is no error or issue whatsoever.
                }
                else {
                    
                    /**
                     This usually occurs if subscribe temporarily fails but reconnects. This means there was
                     an error but there is no longer any issue.
                     */
                }
            }
            // Looks like some kind of issues happened while client tried to subscribe or disconnected from
            // network.
            else {
                
                let errorStatus: PNErrorStatus = status as! PNErrorStatus
                if errorStatus.category == .PNAccessDeniedCategory {
                    
                    /**
                     This means that PAM does allow this client to subscribe to this channel and channel group
                     configuration. This is another explicit error.
                     */
                }
                else if errorStatus.category == .PNUnexpectedDisconnectCategory {
                    
                    /**
                     This is usually an issue with the internet connection, this is an error, handle
                     appropriately retry will be called automatically.
                     */
                }
                else {
                    
                    /**
                     More errors can be directly specified by creating explicit cases for other error categories
                     of `PNStatusCategory` such as `PNTimeoutCategory` or `PNMalformedFilterExpressionCategory` or
                     `PNDecryptionErrorCategory`
                     */
                }
            }
        }
        else if status.operation == .unsubscribeOperation {
            print("pubnub unsubscribeOperation called")
            if status.category == .PNDisconnectedCategory {
                
                /**
                 This is the expected category for an unsubscribe. This means there was no error in
                 unsubscribing from everything.
                 */
            }
        }
        else if status.operation == .heartbeatOperation {
            
            /**
             Heartbeat operations can in fact have errors, so it is important to check first for an error.
             For more information on how to configure heartbeat notifications through the status
             PNObjectEventListener callback, consult http://www.pubnub.com/docs/ios-objective-c/api-reference-sdk-v4#configuration_basic_usage
             */
            
            if !status.isError { /* Heartbeat operation was successful. */ }
            else { /* There was an error with the heartbeat operation, handle here. */ }
        }
    }
    
    // New presence event handling.
    func client(_ client: PubNub, didReceivePresenceEvent event: PNPresenceEventResult) {
        
        // Handle presence event event.data.presenceEvent (one of: join, leave, timeout, state-change).
        if event.data.channel != nil {
            
            // Presence event has been received on channel group stored in event.data.subscribedChannel.
        }
        else {
            
            // Presence event has been received on channel stored in event.data.subscribedChannel.
        }
        
        if event.data.presenceEvent != "state-change" {
            
            print("\(String(describing: event.data.presence.uuid)) \"\(event.data.presenceEvent)'ed\"\n" +
                    "at: \(event.data.presence.timetoken) " +
                    "on \((event.data.channel ?? event.data.channel)!) " +
                    "(Occupancy: \(event.data.presence.occupancy))");
        }
        else {
            
            print("\(String(describing: event.data.presence.uuid)) changed state at: " +
                    "\(event.data.presence.timetoken) " +
                    "on \((event.data.channel)) to:\n" +
                    "\(String(describing: event.data.presence.state))");
        }
    }
    
    
    func unsubPubnubPushNotifications() {
        //// 19-jan       let d = NSData(data: NetworkHelper.sharedInstance.deviceToken!)aaa
        //        print("unsubPubnubPushNotifications814 called")
        //        if NetworkHelper.sharedInstance.deviceToken != nil {
        //            client?.removePushNotificationsFromChannels(getViewAlertChannels(),
        //                                        withDevicePushToken: NetworkHelper.sharedInstance.deviceToken! as Data,
        //                                        andCompletion: { (status) in
        //
        //                                            if !status.isError {
        //                                                print("successfully unsubPubnubPushNotifications")
        //                                                // Handle successful push notification disabling on passed channels.
        ////                                                self.client?.unsubscribe(fromChannels: self.getViewAlertChannels(), withPresence: false)
        ////                                                self.client?.unsubscribeFromAll()
        //
        //                                            }
        //                                            else {
        //
        //                                                print("unable to unsubPubnubPushNotifications")
        //                                                /**
        //                                                 Handle modification error. Check 'category' property
        //                                                 to find out possible reason because of which request did fail.
        //                                                 Review 'errorData' property (which has PNErrorData data type) of status
        //                                                 object to get additional information about issue.
        //
        //                                                 Request can be resent using: status.retry();
        //                                                 */
        //                                            }
        //        })
        //        } else {
        //            print("device token is nil in unsubPubnubPushNotifications()")
        //        }
    }
    
    func unsubscribeFromAll()
    {
        //19-jan        print("unsubscribeFromAll847 called")
        //        client?.unsubscribeFromAll()
    }
    
    func showAdvanceSuccessAlert() {
        
        let alertController = UIAlertController(title: "d.moat mode", message: ConstantStrings.advance_done_guide, preferredStyle: .actionSheet)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        
        alertController.addAction(okAction)
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
    
}
extension UIApplication {
    func _handleNonLaunchSpecificActions(arg1: AnyObject, forScene arg2: AnyObject, withTransitionContext arg3: AnyObject, completion completionHandler: () -> Void) {
        //catching handleNonLaunchSpecificActions:forScene exception on app close
        print("push noti exception")
    }
}

extension UIViewController {
    var foregroundViewController: UIViewController? {
        return self.foregroundViewController(currentViewController: self)
    }

    private func foregroundViewController(currentViewController: UIViewController) -> UIViewController {
        if let tabBarController = currentViewController as? UITabBarController,
            let selectedViewController = tabBarController.selectedViewController {
            return self.foregroundViewController(currentViewController: selectedViewController)
        } else if let navigationController = currentViewController as? UINavigationController,
            let visibleViewController = navigationController.visibleViewController {
            return self.foregroundViewController(currentViewController: visibleViewController)
       } else if let presentedViewController = currentViewController.presentedViewController {
            return self.foregroundViewController(currentViewController: presentedViewController)
       } else {
            return currentViewController
        }
    }
}



//extension AppDelegate: WCSessionDelegate {
//
//    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
//        print("Message received: ",message)
//    }
//
//    //below 3 functions are needed to be able to connect to several Watches
//    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {}
//
//    func sessionDidDeactivate(_ session: WCSession) {}
//
//    func sessionDidBecomeInactive(_ session: WCSession) {}
//}

