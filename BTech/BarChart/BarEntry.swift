//
//  BarEntry.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 02/10/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit


struct BarEntry {
    let color: UIColor
    
    /// Ranged from 0.0 to 1.0
    let height: Float
    
    /// To be shown on top of the bar
    let textValue: String
    
    /// To be shown at the bottom of the bar
    let title: String
    
}
