//
//  BasicBarChart.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 02/10/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit

class BasicBarChart: UIView {
    
    /// the width of each bar
    let barWidth: CGFloat = 40.0
    
    /// space between each bar
    let space: CGFloat = 20.0
    
    /// space at the bottom of the bar to show the title
    private let bottomSpace: CGFloat = 60.0
    
    /// space at the top of each bar to show the value
    private let topSpace: CGFloat = 40.0
    
    /// contain all layers of the chart
    private let mainLayer: CALayer = CALayer()
    
    /// contain mainLayer to support scrolling
    private let scrollView: UIScrollView = UIScrollView()
    
    var dataEntries: [BarEntry]? = nil {
        didSet {
            mainLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
            
            if let dataEntries = dataEntries {
                scrollView.contentSize = CGSize(width: (barWidth + space)*CGFloat(dataEntries.count), height: self.frame.size.height)
                mainLayer.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
                
                drawHorizontalLines()
                
                for i in 0..<dataEntries.count {
                    showEntry(index: i, entry: dataEntries[i])
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        scrollView.layer.addSublayer(mainLayer)
        self.addSubview(scrollView)
    }
    
    override func layoutSubviews() {
        scrollView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
    }
    
    private func showEntry(index: Int, entry: BarEntry) {
        /// Starting x postion of the bar
        let xPos: CGFloat = space + CGFloat(index) * (barWidth + space)
        
        /// Starting y postion of the bar
        let yPos: CGFloat = translateHeightValueToYPosition(value: entry.height)
        
        drawBar(xPos: xPos, yPos: yPos, color: entry.color)
        
        /// Draw text above the bar
//        drawTextValue(xPos: xPos - space/2, yPos: yPos - 30, textValue: entry.textValue, color: entry.color)
      
//        print("yPos== \(yPos)")
        var heigt = yPos
        if yPos < 40 {
            heigt = mainLayer.frame.height - 525
        }else if yPos > 450{
            heigt = yPos + 10
            }
        else{
            heigt = yPos + 10
        }
        drawTextValue(xPos: xPos - space/2, yPos: heigt, textValue: entry.textValue, color: UIColor.white)
        
        /// Draw text below the bar
        drawTitle(xPos: xPos - space/2, yPos: mainLayer.frame.height - bottomSpace + 10, title: entry.title, color: entry.color, index: index)
//        drawTitle(xPos: xPos - space/2, yPos: heigt, title: entry.title, color: UIColor.red, index: index)

    }
    
    private func drawBar(xPos: CGFloat, yPos: CGFloat, color: UIColor) {
        let barLayer = CALayer()
        barLayer.frame = CGRect(x: xPos, y: yPos, width: barWidth, height: mainLayer.frame.height - bottomSpace - yPos)
        barLayer.backgroundColor = color.cgColor
        mainLayer.addSublayer(barLayer)
    }
    
    private func drawHorizontalLines() {
        self.layer.sublayers?.forEach({
            if $0 is CAShapeLayer {
                $0.removeFromSuperlayer()
            }
        })
        let horizontalLineInfos = [["value": Float(0.0), "dashed": false], ["value": Float(0.5), "dashed": true], ["value": Float(1.0), "dashed": false]]
        for lineInfo in horizontalLineInfos {
            let xPos = CGFloat(0.0)
            let yPos = translateHeightValueToYPosition(value: (lineInfo["value"] as! Float))
            let path = UIBezierPath()
            path.move(to: CGPoint(x: xPos, y: yPos))
            path.addLine(to: CGPoint(x: scrollView.frame.size.width, y: yPos))
            let lineLayer = CAShapeLayer()
            lineLayer.path = path.cgPath
            lineLayer.lineWidth = 0.5
            if lineInfo["dashed"] as! Bool {
                lineLayer.lineDashPattern = [4, 4]
            }
            lineLayer.strokeColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).cgColor
            self.layer.insertSublayer(lineLayer, at: 0)
        }
    }
    
    private func drawTextValue(xPos: CGFloat, yPos: CGFloat, textValue: String, color: UIColor) {
        let textLayer = CATextLayer()
        textLayer.frame = CGRect(x: xPos, y: yPos, width: barWidth+space, height: yPos)
        textLayer.foregroundColor = color.cgColor
        textLayer.backgroundColor = UIColor.clear.cgColor
        textLayer.alignmentMode = CATextLayerAlignmentMode.center
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
        textLayer.fontSize = 8
        textLayer.string = textValue
        
//        textLayer.string = textValue  provide "" to hide top label value
        mainLayer.addSublayer(textLayer)
    }
    
    private func drawTitle(xPos: CGFloat, yPos: CGFloat, title: String, color: UIColor, index: Int) {
        let textLayer = CATextLayer()
        textLayer.frame = CGRect(x: xPos, y: yPos, width: barWidth+space, height: yPos)
        textLayer.foregroundColor = color.cgColor
        textLayer.backgroundColor = UIColor.clear.cgColor
        textLayer.alignmentMode = CATextLayerAlignmentMode.center
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
        textLayer.fontSize = 8
        textLayer.string = title

//        let degrees = 30.0
//        let radians = CGFloat(degrees * Double.pi / 180)
//        
//        // translate
//        var transform = CATransform3DMakeTranslation(90, 50, 0)
//        
//        // rotate
//        transform = CATransform3DRotate(transform, radians, 0.0, 0.0, 1.0)
//        
//        // scale
//        transform = CATransform3DScale(transform, 0.5, 3.0, 1.0)
//        
//        // apply the transforms
//        textLayer.transform = transform
        
        //        textLayer.string = textValue  provide "" to hide top label value
        mainLayer.addSublayer(textLayer)
        
//        let textLayer = CATextLayer()
//            if (index % 2 == 0){
//                print("index % 2 \(index)")
//                textLayer.frame = CGRect(x: xPos, y: yPos, width: 150, height: 50)
//            }else{
//                print("index not % 2 \(index)")
//                textLayer.frame = CGRect(x: xPos, y: yPos + 20, width: 150, height: 50)
//            }
//        textLayer.frame = CGRect(x: xPos, y: yPos, width: barWidth + space, height: 22)
//        textLayer.foregroundColor = color.cgColor
//        textLayer.backgroundColor = UIColor.clear.cgColor
//        textLayer.alignmentMode = kCAAlignmentCenter
//        textLayer.contentsScale = UIScreen.main.scale
//        textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
//        textLayer.fontSize = 12
////        textLayer.lineBreakMode = .ByWordWrapping // or NSLineBreakMode.ByWordWrapping
////        textLayer.numberOfLines = 2
//        textLayer.string = title
//        mainLayer.addSublayer(textLayer)
    }
    
    
    private func translateHeightValueToYPosition(value: Float) -> CGFloat {
        let height: CGFloat = CGFloat(value) * (mainLayer.frame.height - bottomSpace - topSpace)
        return mainLayer.frame.height - bottomSpace - height
    }
}

