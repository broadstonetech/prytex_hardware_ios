//
//  AdvanceModeModel.swift
//  BTech
//
//  Created by Ahmed Akhtar on 28/12/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class AdvanceModeModel: NSObject {

    var broadcastString:String?
    var defaultLeaseTimeString:String?
    var endIpRangeString:String?
    var gatewayString:String?
    var ifaceString:String?
    var ipString:String?
    var macAddressString:String?
    var maxLeaseTimeString:String?
    var netmaskString:String?
    var routerIpString:String?
    var startIpRangeString:String?
    var subnetString:String?



override init() {
    
    self.broadcastString=""
    self.defaultLeaseTimeString=""
    self.endIpRangeString=""
    self.gatewayString=""
    self.ifaceString=""
    self.ipString=""
    self.macAddressString=""
    self.maxLeaseTimeString=""
    self.netmaskString=""
    self.routerIpString=""
    self.startIpRangeString=""
    self.subnetString=""
    
}




}
