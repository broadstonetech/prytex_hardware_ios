//
//  AlertModel.swift
//  BTech
//
//  Created by Talha Ejaz on 30/08/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class AlertModel: BaseModel {

    
    var alert_id:String?
    var appearance:String?
    var blocktype:String?
    var category:String?
    var channel:String?
    var day:String?
    var desc:String?
    var dest_ip:String?
    var dest_port:String?
    var device_id:String?
    var eve_id:String?
    var eve_sec:String?
    var internetOn:String?
    var internetOff:String?
    var hostname:String?
    var hour:String?
    var ip:String?
    var macaddress:String?
    var input_src:String?
    var device_category:String?
    var msg:String?
    var namespace:String?
    var priority:String?
    var detail:String?
    var timestamp:String?
    var src_ip:String?
    var src_port:String?
    var postkey:String?
    var test_id:String?
    var direction:String?
    var tree:String!
    var record_id:String?
    var OS:String?
    var date:String?
    var type:String?
    var reading:String?
    var seen:String?
    var checked:Bool?
    
    var title:String?
    var risk:String?
    var attack_complexity:String?
    var attack_vector:String?
    
    var arrayVulProducts = [String]()
    var arrayVulReferences = [String]()
    var arrayVulImpact = [String]()
    
    
    override init() {
        alert_id=""
        appearance=""
        blocktype=""
        category=""
        channel=""
        day=""
        desc=""
        dest_ip=""
        dest_port=""
        device_id=""
        eve_id=""
        eve_sec=""
        internetOn=""
        internetOff=""
        hostname=""
        hour=""
        ip=""
        macaddress=""
        input_src=""
        device_category = ""
        msg=""
        namespace=""
        priority=""
        detail=""
        timestamp=""
        src_ip=""
        src_port=""
        postkey=""
        test_id=""
        direction=""
        tree=""
        record_id=""
        OS=""
        date=""
        type=""
        reading=""
        seen=""
        risk=""
        attack_complexity=""
        attack_vector=""
        title=""
        
        
        checked=false

    }
    
    class func initWithDict(_ dictionary:NSDictionary)-> AlertModel {
       let aModel = AlertModel.autoInitWithDict(dictionary)  as! AlertModel
        if(dictionary["description"] != nil) {
            aModel.desc = dictionary["description"] as? String
        }
        
        aModel.eve_sec = "1519294482"
        aModel.record_id = "12111111111343434"
//        if(dictionary["alert_id"] != nil)
//        {
//            let alertID = dictionary["alert_id"] as? NSNumber
//            aModel.alert_id = alertID?.stringValue
//            
//            if(aModel.alert_id==nil)
//            {
//                 aModel.alert_id = dictionary["alert_id"] as? String
//            }
//        }else{
//            
//        }
//        
//        if(dictionary["detail"] != nil) {
//            aModel.detail = dictionary["detail"] as? String
//        }
//        
//        if(dictionary["timestamp"] != nil)
//        {
//            let timeStamp = dictionary["timestamp"] as? NSNumber
//            aModel.timestamp = timeStamp?.stringValue
//            
//            if(aModel.timestamp==nil)
//            {
//                aModel.timestamp = dictionary["timestamp"] as? String
//            }
//        }
//        
//        if(dictionary["reading"] != nil)
//        {
//            let reading = dictionary["reading"] as? NSNumber
//            aModel.reading = reading?.stringValue
//            
//            if(aModel.reading==nil)
//            {
//                aModel.reading = dictionary["reading"] as? String
//            }
//        }
//        
//        if(dictionary["eve_sec"] != nil)
//        {
//            let eve_sec = dictionary["eve_sec"] as? NSNumber
//            aModel.eve_sec = eve_sec?.stringValue
//            
//            if(aModel.eve_sec==nil)
//            {
//                aModel.eve_sec = dictionary["eve_sec"] as? String
//            }
//        }
        
//        if(dictionary["on"] != nil)
//        {
//            let internetOn = dictionary["on"] as? NSNumber
//            aModel.internetOn = internetOn?.stringValue
//            
//            if(aModel.internetOn==nil)
//            {
//                aModel.internetOn = dictionary["on"] as? String
//            }
//        }
//        if(dictionary["off"] != nil)
//        {
//            let internetoff = dictionary["off"] as? NSNumber
//            aModel.internetOff = internetoff?.stringValue
//            
//            if(aModel.internetOff==nil)
//            {
//                aModel.internetOff = dictionary["off"] as? String
//            }
//        }
        
        //if(dictionary["striped_values"] != nil) {
        //    aModel.striped_values = dictionary["striped_values"] as? NSArray
        //}
//        if(dictionary["tree"] != nil) {
//            let tree = dictionary["tree"] as! String
//            aModel.tree = String.convertStringToDictionary(tree)
//        }
        return aModel
    }
//    func primaryKey()->NSNumber{
//    
//    return self.alert_id
//    }
}
