//
//  BandwidthModel.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 13/07/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class BandwidthModel {
    
    fileprivate var hostIP:String!
    fileprivate var hostID:String!
    fileprivate var hostName:String!
    fileprivate var totalUsage:Float!
    fileprivate var protocols:NSDictionary!
    //protocol detail page vars
    fileprivate var protocolName:String!
    fileprivate var protocolUsage:Int!
    fileprivate var parameters:String
    fileprivate var isCurrent:String
    
    init(){
        hostID=""
        hostIP=""
        hostName=""
        protocols = NSDictionary()
        totalUsage = 0.000
        protocolName=""
        protocolUsage=0
        parameters = ""
        isCurrent = ""
    }
    
    func getprotocolName()->String {
        return protocolName;
    }
    
    func setprotocolName(_ protocolName : String) {
        self.protocolName = protocolName
    }
    func getIsCurrent()->String {
        return isCurrent;
    }
    
    func setIsCurrent(_ isCurrent : String) {
        self.isCurrent = isCurrent
    }
    func getprotocolUsage()->Int {
        return protocolUsage;
    }
    
    func setprotocolUsage(_ protocolUsage : Int) {
        self.protocolUsage = protocolUsage
    }
    
    
    func getHostID()->String {
        return hostID;
    }
    
    func setHostID(_ hId : String) {
        self.hostID = hId
    }
    
    func getHostIP()->String {
        return hostIP;
    }
    
    func setHostIP(_ hIp : String) {
        self.hostIP = hIp
    }
    
    func getHostName()->String {
        return hostName;
    }
    
    func setHostName(_ hName : String) {
        self.hostName = hName
    }
    
    func getUsage()->Float {
        return totalUsage;
    }
    
    func setUsgae(_ usage : Float) {
        self.totalUsage = usage;
    }
    
    func getProtocols()->NSDictionary {
        return protocols;
    }
    
    func setProtocols(_ protocl : NSDictionary) {
        self.protocols = protocl
    }
    func getParameters()->String {
        return parameters;
    }
        func setParameters(_ param : String) {
        self.parameters = param
    }
    
}
