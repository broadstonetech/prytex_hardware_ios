//
//  BaseModel.swift
//  BTech
//
//  Created by Talha Ejaz on 30/08/2016.
//  Copyright © 2016 Talha Ejaz. All rights reserved.
//

import UIKit
import CoreData

class BaseModel: NSObject {

    var primaryKey:NSString?
    var entity:NSManagedObject.Type!

    // MARK: Parsing Methods
    class func autoInitWithDict(_ dictionary:NSDictionary)-> AnyObject {
        
        let Class: NSObject.Type = self
        let obj : NSObject = Class.init()
        
        let list:NSArray = getPropertyList()
        for i in 0  ..< list.count  {
            let dict = list[i] as! NSDictionary
            let propName = dict["name"] as! String
            let propType = dict["class"] as! String
            let dictObj = dictionary[propName]
            if propType == NSStringFromClass(NSDate.self){
                var  date:Date? = nil
                if(dictObj != nil) {
                    if(propName == "dob") {
                      date = Date.dobFromServer(dictObj as! String)
                    }else {
                     date = Date.dateFromServer(dictObj as! String)
                    }
                }
                if(date != nil) {
                    obj.setValue(date, forKey: propName)
                }
            }
            if(propType == NSStringFromClass(NSMutableArray.self)) {
                continue
            }
            if (dictObj != nil && (dictObj! as AnyObject).isKind(of: NSClassFromString(propType as String)!)) {
                obj.setValue(dictObj, forKey: propName)
            }
            
        }
        return obj
    }
    func getDictFromObject() -> NSMutableDictionary {
        
        let propDict = NSMutableDictionary()
        let Class: BaseModel.Type = type(of: self)
        let list:NSArray = Class.getPropertyList()
        for i in 0  ..< list.count  {
            let dict = list[i] as! NSDictionary
            let propName = dict["name"] as! String
            let value = self.value(forKey: propName)
            propDict[propName] = value
            
        }
        
        return propDict
        
    }
    func autoEncodeWithCoder(_ coder: NSCoder) {
        let Class: BaseModel.Type = type(of: self)
        let allProp:NSArray =  Class.getPropertyList()
        for i in 0 ..< allProp.count {
            let dict = allProp[i] as! NSDictionary
            let propName = dict["name"] as! String
            coder.encode(self .value(forKey: propName), forKey: propName)
        }
    }
    func autoInitWithCoder(_ decoder: NSCoder) {
        
        let Class: BaseModel.Type = type(of: self)
        let allProp:NSArray =  Class.getPropertyList()
        for i in 0 ..< allProp.count {
            let dict = allProp[i] as! NSDictionary
            let propName = dict["name"] as! String
            self .setValue(decoder.decodeObject(forKey: propName), forKey: propName)
        }
    }
    class func nullFreeDictForDict(_ dictionary:NSDictionary) ->NSDictionary {
        let nullFreeDict:NSMutableDictionary  = NSMutableDictionary()
        if(dictionary.isKind(of: NSDictionary.self)){
            dictionary.enumerateKeysAndObjects({ (key, obj, stop) in
                if (obj as AnyObject).isKind(of: NSNull.self){
                    nullFreeDict.setObject("", forKey: key as! String as NSCopying)
                }else{
                    nullFreeDict.setObject(obj, forKey: key as! String as NSCopying)
                }
            })
        }
        return nullFreeDict
    }
    class func property_getTypeString(_ property:objc_property_t) -> String {
        let propAttr:UnsafePointer<Int8> =  property_getAttributes(property)!;
        let strProp:String = String(cString: propAttr)
        var componentArr = strProp.components(separatedBy: ",")
        var type:String = componentArr[0]
        type = type.replacingOccurrences(of: "T@", with: "")
        type = type.replacingOccurrences(of: "\\", with: "")
        type = type.replacingOccurrences(of: "\"", with: "")
        return type
    }
    class func classOfProperty(_ name:String) -> AnyClass {
        let Class: NSObject.Type = self
        let unsafePointerOfN = (name as NSString).utf8String
        let unsafeMutablePointerOfN: UnsafeMutablePointer<Int8> = UnsafeMutablePointer(mutating: unsafePointerOfN!)
        let property : objc_property_t = class_getProperty(Class,unsafeMutablePointerOfN)!
        let kind:String = property_getTypeString(property)
        return NSClassFromString(kind)!
    }
    class func getPropertyList() -> NSArray {
        
        var count = UInt32()
        let properties : UnsafeMutablePointer <objc_property_t> = class_copyPropertyList(self, &count)!
//        let properties : UnsafeMutablePointer <objc_property_t?> = class_copyPropertyList(self, &count)
        
        var propertyNames = [NSDictionary]()
        let intCount = Int(count)
        for i in 0 ..< intCount {
            let property : objc_property_t = properties[i]
            let kind:String = property_getTypeString(property)
            guard let propertyName = NSString(utf8String: property_getName(property)) as? String else {
                debugPrint("Couldn't unwrap property name for \(property)")
                break
            }
            propertyNames.append(["name":propertyName, "class":kind])
        }
        return propertyNames as NSArray
    }
    class func getPropertyNameList() -> NSArray {
        
        var count = UInt32()
        let properties : UnsafeMutablePointer <objc_property_t> = class_copyPropertyList(self, &count)!
        var propertyNames = [String]()
        let intCount = Int(count)
        for i in 0 ..< intCount {
            let property : objc_property_t = properties[i]
            guard let propertyName = NSString(utf8String: property_getName(property)) as? String else {
                debugPrint("Couldn't unwrap property name for \(property)")
                break
            }
            propertyNames.append(propertyName)
        }
        return propertyNames as NSArray
    }
    
    // MARK: Data Object Methods
    class func getDataObject(_ objectId:AnyObject) -> AnyObject {
        
        let Class: NSObject.Type = self
        let tempObj : NSObject = Class.init()
        let obj:BaseModel = tempObj as! BaseModel
         let baseClass: BaseModel.Type = self
        if(obj.primaryKey != nil && obj.entity != nil) {
//            let result:NSArray = obj.entity.mr_find(byAttribute: obj.primaryKey as! String, withValue:objectId) as NSArray
//            if(result.count > 0) {
//                let dbObj:NSManagedObject = result[0] as! NSManagedObject
//                return baseClass.objectWithDBObject(dbObj);
//            }
        }
        return obj
    }
    class func objectWithID(_ objectId:AnyObject) -> AnyObject? {
        
        let Class: NSObject.Type = self
        let tempObj : NSObject = Class.init()
        let obj:BaseModel = tempObj as! BaseModel
        let baseClass: BaseModel.Type = self
        if(obj.primaryKey != nil && obj.entity != nil) {
//            let result:NSArray = obj.entity.mr_find(byAttribute: obj.primaryKey as! String, withValue:objectId) as NSArray
//            if(result.count > 0) {
//                let dbObj:NSManagedObject = result[0] as! NSManagedObject
//                return baseClass.objectWithDBObject(dbObj);
//            }
        }
        return nil
    }
    class func objectWithDBObject(_ dbObj:NSManagedObject) -> AnyObject{
       
        let Class: NSObject.Type = self
        let tempObj : NSObject = Class.init()
        var keys:NSArray = Array(dbObj.entity.attributesByName.keys) as NSArray
         keys = keys.adding(Array(dbObj.entity.relationshipsByName.keys)) as NSArray
        let baseClass: BaseModel.Type = self
        let propList: NSArray = baseClass.getPropertyNameList()
        for  key in keys {
            if(propList.contains(key)) {
                let value:AnyObject? = dbObj.value(forKey: key as! String) as AnyObject?
                if(value != nil) {
                    
                    if value!.isKind(of: AnyObject.self)  {
                        let propClass:AnyClass =  classOfProperty(key as! String)
                        let baseModelPropClass: BaseModel.Type = propClass as! BaseModel.Type
                        let objVal = baseModelPropClass.objectWithDBObject(value! as! NSManagedObject)
                        tempObj.setValue(objVal, forKey:key as! String)
                    }else if(value!.isKind(of: AnyObject.self)) {
                        continue
                    }else {
                        tempObj.setValue(value, forKey:key as! String)
                    }
                }
            }
        }
        return tempObj
    }
    func save() -> Void {
//        var dbObj = self.dbObject()
//        dbObj = populateDBObject(dbObj)
//        NSManagedObjectContext.mr_contextForCurrentThread().mr_saveToPersistentStore { (isSave, error) in
//
        }
    }
    func dbObject() -> NSManagedObject {
        
        var dbObj:NSManagedObject!
//        let value:AnyObject =  self.value(forKey: self.primaryKey as! String)! as AnyObject
//        let result:NSArray = self.entity.mr_find(byAttribute: self.primaryKey as! String, withValue:value) as NSArray
//        if(result.count > 0) {
//            dbObj = result[0] as? NSManagedObject
//        }else {
//             dbObj = self.entity.mr_create(in: NSManagedObjectContext.mr_contextForCurrentThread()) as? NSManagedObject
//        }
        return dbObj
    }
    func populateDBObject(_ dbObj:NSManagedObject) -> NSManagedObject {
        
        var keys:NSArray = Array(dbObj.entity.attributesByName.keys) as NSArray
        keys = keys.adding(Array(dbObj.entity.relationshipsByName.keys)) as NSArray
//        let baseClass: BaseModel.Type = type(of: self)
//        let propList: NSArray = baseClass.getPropertyNameList()
        for  key in keys {
//            if(propList.contains(key)) {
////                let value:AnyObject? = self.value(forKey: key as! String) as AnyObject?
////                let value = self.value(forKey: key as! String) as AnyObject?
//                if(value != nil) {
//                   if value!.isKind(of: AnyObject.self){
////                    var dbValue =  value?.addObject()
////                    var dbValue =  value!.dbObject()
////                    var dbValue = value!.populateDBObject(dbValue)
////                        dbObj.setValue(dbValue, forKey: key as! String)
//                   }
//                   else if(value!.isKind(of: AnyObject.self)) {
//                     continue
//                   }else {
//                    dbObj.setValue(value, forKey:key as! String)
//                   }
//                }
//            }
        }
        return dbObj
    }
    func delete() -> Void {
//        let obj:NSManagedObject =  self.dbObject()
//        obj.mr_delete(in: NSManagedObjectContext.mr_contextForCurrentThread())
//        NSManagedObjectContext.mr_contextForCurrentThread().mr_saveToPersistentStore { (isSave, error) in
            
        }


