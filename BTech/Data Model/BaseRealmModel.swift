//
//  BaseRealmModel.swift
//  BTech
//
//  Created by Talha Ejaz on 07/09/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import Foundation
//import RealmSwift

class BaseRealmModel: NSObject {
  
    // MARK: Parsing Methods
//    class func autoInitWithDict(_ dictionary:NSDictionary)-> AnyObject {
        
//        let Class: BaseRealmModel.Type = self
//        let obj : BaseRealmModel = Class.init()
//
//        let list:NSArray = obj.getPropertyList() as NSArray
//        for i in 0  ..< list.count  {
//            let dict = list[i] as! NSDictionary
//            let propName = dict["name"] as! String
//            let propType = dict["class"] as! String
//            let dictObj = dictionary[propName]
//            print("nsstreingfromclass(date) changed in swift3")
////            if(propType == NSStringFromClass(Date as! String) {
//            if(propType == NSStringFromClass(NSDate.self)) {
//                var  date:Date? = nil
//                if(dictObj != nil) {
//                    if(propName == "dob") {
//                        date = Date.dobFromServer(dictObj as! String)
//                    }else {
//                        date = Date.dateFromServer(dictObj as! String)
//                    }
//                }
//                if(date != nil) {
//                    obj.setValue(date, forKey: propName)
//                }
//                }
//            if(propType == NSStringFromClass(NSMutableArray.self)) {
//                continue
//            }
//            if(dictObj != nil ) {
//                if (NSClassFromString(propType) != nil && (dictObj! as AnyObject).isKind(of: NSClassFromString(propType as String)!)) {
//                    obj.setValue(dictObj, forKey: propName)
//                }
//                else {
//
//                    // Swift native struct properties
//                    // TODO: Check if properties dictObj type is equal to propType
//                      obj.setValue(dictObj, forKey: propName)
//
//                }
//            }
//
//        }
        
//        return obj
//        return AnyObject.Protocol
//    }
    func getDictFromObject() -> NSMutableDictionary {
        
        let propDict = NSMutableDictionary()
        let list:NSArray = self.getPropertyList() as NSArray
        for i in 0  ..< list.count  {
            let dict = list[i] as! NSDictionary
            let propName = dict["name"] as! String
            let value = "" //self.value(forKey: propName)
            propDict[propName] = value
            
        }
        
        return propDict
        
    }
    func autoEncodeWithCoder(_ coder: NSCoder) {
        let allProp:NSArray =  self.getPropertyList() as NSArray
        for i in 0 ..< allProp.count {
            let dict = allProp[i] as! NSDictionary
            let propName = dict["name"] as! String
//            coder.encode(self .value(forKey: propName), forKey: propName)
        }
    }
    func autoInitWithCoder(_ decoder: NSCoder) {
        
        let allProp:NSArray =  self.getPropertyList() as NSArray
        for i in 0 ..< allProp.count {
            let dict = allProp[i] as! NSDictionary
            let propName = dict["name"] as! String
//            self.setValue(decoder.decodeObject(forKey: propName), forKey: propName)
        }
    }
    class func nullFreeDictForDict(_ dictionary:NSDictionary) ->NSDictionary {
        let nullFreeDict:NSMutableDictionary  = NSMutableDictionary()
        if(dictionary.isKind(of: NSDictionary.self)){
            dictionary.enumerateKeysAndObjects({ (key, obj, stop) in
                if (obj as AnyObject).isKind(of: NSNull.self){
                    nullFreeDict.setObject("", forKey: key as! String as NSCopying)
                }else{
                    nullFreeDict.setObject(obj, forKey: key as! String as NSCopying)
                }
            })
        }
        return nullFreeDict
    }
    class func property_getTypeString(_ property:objc_property_t) -> String {
        let propAttr:UnsafePointer<Int8> =  property_getAttributes(property)!;
        let strProp:String = String(cString: propAttr)
        var componentArr = strProp.components(separatedBy: ",")
        var type:String = componentArr[0]
        type = type.replacingOccurrences(of: "T@", with: "")
        type = type.replacingOccurrences(of: "\\", with: "")
        type = type.replacingOccurrences(of: "\"", with: "")
        return type
    }
    class func classOfProperty(_ name:String) -> AnyClass {
//        let Class: NSObject.Type = self
        let unsafePointerOfN = (name as NSString).utf8String
        let unsafeMutablePointerOfN: UnsafeMutablePointer<Int8> = UnsafeMutablePointer(mutating: unsafePointerOfN!)
//        let property : objc_property_t = class_getProperty(Class,unsafeMutablePointerOfN)!
//        let kind:String = property_getTypeString(property)
        return NSClassFromString("kind")!
    }
    
    func getPropertyNameList() -> [String] {

        return Mirror(reflecting: self).children.flatMap { $0.label }
    }
    
    func getPropertyList()->[NSDictionary]{
        
         var propertyNames = [NSDictionary]()
        for case let (label?, value) in Mirror(reflecting: self).children {
            let type = String(describing: Mirror(reflecting: value).subjectType)
            propertyNames.append(["name":label, "class":type])
            print (label, value)

        }
        
      return propertyNames
    }
   

    // MARK: Data Object Methods
    func save() -> Void {
        // Query and update from any thread
//      dispatch_async(dispatch_queue_create("background", nil)) {
//            let realm = try! Realm()
//            // Persist your data easily
//             let Class: BaseRealmModel.Type = type(of: self)
//            let pKey = Class.primaryKey()!
//            let value = self.value(forKey: pKey)
//            let theObj = realm.objects(Class.self).filter(pKey+" = %@",value!).first
//            if(theObj != nil) {
//
//                try! realm.write {
//                    let allProp:[NSDictionary] =  self.getPropertyList()
//                     print(allProp)
//                    for i in 0 ..< allProp.count {
//                        let dict = allProp[i]
//                        let propName = dict["name"] as! String
//                        let propValue = self.value(forKey: propName)
//                        print(propName)
//                        if(propName != pKey) {
//                            theObj!.setValue(propValue, forKey: propName)
//                        }
//                    }
//                }
//            }else {
//                try! realm.write {
//                    realm.add(self)
//                }
//            }
        }
        
 //   }
    func delete() -> Void {
//        let realm = try! Realm()
//        // Persist your data easily
//        let Class: BaseRealmModel.Type = type(of: self)
//        let pKey = Class.primaryKey()!
//        let value = self.value(forKey: pKey)
//        let theObj = realm.objects(Class.self).filter(pKey+" = %@",value!).first
//        if(theObj != nil) {
//            try! realm.write {
//                print(theObj)
//                realm.delete(theObj!)
//            }
//        }
    }
    
}
