//
//  BlockedMutedAlertsModel.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 16/08/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import Foundation


class BlockedMutedAlertsModel: NSObject, NSCoding {
    
    func encode(with aCoder: NSCoder) {
        //flow
        aCoder.encode(flowType, forKey: "flowType")
        aCoder.encode(flowDetail, forKey: "flowDetail")
        
        aCoder.encode(slotId, forKey: "slotId")
        //sensor
        aCoder.encode(category, forKey: "category")
        aCoder.encode(sensorReading, forKey: "sensorReading")
        
        //connected host
        aCoder.encode(deviceCategory, forKey: "deviceCategory")
        aCoder.encode(hostID, forKey: "hostID")
        aCoder.encode(OS, forKey: "OS")
        aCoder.encode(hostName, forKey: "hostName")
        aCoder.encode(macAddress, forKey: "macAddress")
        aCoder.encode(parameters, forKey: "parameters")
        
        //ids
        aCoder.encode(destHostName, forKey: "destHostName")
        aCoder.encode(srcHostName, forKey: "srcHostName")
        aCoder.encode(dest_port, forKey: "dest_port")
        aCoder.encode(dest_ip, forKey: "dest_ip")
        aCoder.encode(src_ip, forKey: "src_ip")
        aCoder.encode(src_port, forKey: "src_port")
        aCoder.encode(idsBlockType, forKey: "idsBlockType")
        
        //blockIPs
        aCoder.encode(info_type, forKey: "info_type")
        aCoder.encode(timeOn, forKey: "timeOn")
        aCoder.encode(timeOff, forKey: "timeOff")
        
        
        //similar
        aCoder.encode(ip, forKey: "ip")
        aCoder.encode(alertDescription, forKey: "alertDescription")
        aCoder.encode(input_src, forKey: "input_src")
        aCoder.encode(eve_sec, forKey: "eve_sec")
        aCoder.encode(namespace, forKey: "namespace")
        aCoder.encode(app_id, forKey: "app_id")
        aCoder.encode(record_id, forKey: "record_id")
        aCoder.encode(seenStatus, forKey: "seenStatus")
        aCoder.encode(modeFlag, forKey: "modeFlag")
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        flowType = aDecoder.decodeObject(forKey: "flowType") as! String
        flowDetail = aDecoder.decodeObject(forKey: "flowDetail") as! String
        slotId = aDecoder.decodeObject(forKey: "slotId") as! Int
        //sensor
        category = aDecoder.decodeObject(forKey: "category") as! String
        sensorReading = aDecoder.decodeDouble(forKey: "sensorReading") 
        
        //connecred host
        deviceCategory = aDecoder.decodeObject(forKey: "deviceCategory") as! String
        hostID = aDecoder.decodeObject(forKey: "hostID") as! String
        OS = aDecoder.decodeObject(forKey: "OS") as! String
        hostName = aDecoder.decodeObject(forKey: "hostName") as! String
        macAddress = aDecoder.decodeObject(forKey: "macAddress") as! String
        parameters = aDecoder.decodeObject(forKey: "parameters") as! String
        //ids
        destHostName = aDecoder.decodeObject(forKey: "destHostName") as! String
        srcHostName = aDecoder.decodeObject(forKey: "srcHostName") as! String
        dest_port = aDecoder.decodeObject(forKey: "dest_port") as! String
        dest_ip = aDecoder.decodeObject(forKey: "dest_ip") as! String
        src_ip = aDecoder.decodeObject(forKey: "src_ip") as! String
        src_port = aDecoder.decodeObject(forKey: "src_port") as! String
        idsBlockType = aDecoder.decodeObject(forKey: "idsBlockType") as! String
        
        //info
        timeOn = aDecoder.decodeInteger(forKey: "timeOn") as Int
        info_type = aDecoder.decodeObject(forKey: "info_type") as! String
        timeOff = aDecoder.decodeInteger(forKey: "timeOff") as Int
        //similar
        
        ip = aDecoder.decodeObject(forKey: "ip") as! String
        alertDescription = aDecoder.decodeObject(forKey: "alertDescription") as! String
        input_src = aDecoder.decodeObject(forKey: "input_src") as! String
        eve_sec = aDecoder.decodeInteger(forKey: "eve_sec") as Int
        namespace = aDecoder.decodeObject(forKey: "namespace") as! String
        app_id = aDecoder.decodeObject(forKey: "app_id") as! String
        record_id = aDecoder.decodeObject(forKey: "record_id") as! String
        seenStatus = aDecoder.decodeBool(forKey: "seenStatus") as Bool
        alertId = aDecoder.decodeInteger(forKey: "alertId") as Int
        
        
        modeFlag = aDecoder.decodeInteger(forKey: "modeFlag") as Int
        self.version = aDecoder.decodeObject(forKey: "version") as! String
    }
    
    
    
    //flow
    //    private String alertId;
    private var flowType:String
    private var flowDetail:String
    private var slotId : Int
    
    var arrayFlowAlerts = [[String : AnyObject]]()
    var arrayFlowRecordIdsList = [String]()
    
    //sensor
    private var category:String
    private var sensorReading:Double
    
    //connected host
    private var deviceCategory:String
    private var hostID:String
    private var OS:String
    private var hostName:String
    private var macAddress:String
    private var parameters:String
    
    
    //ids
    private var destHostName:String
    private var srcHostName:String
    private var dest_port:String
    private var dest_ip:String
    private var src_ip:String
    private var src_port:String
    private var idsBlockType:String
    
    //blockIps
    
    //info
    private var info_type:String
    private var timeOn:Int
    private var timeOff:Int
    
    //similar
    private var ip:String
    private var alertDescription:String
    private var input_src:String
    private var eve_sec:Int
    private var namespace:String
    private var app_id:String
    private var record_id:String
    private var seenStatus:Bool
    private var alertId:Int
    //other keys
    
    private var modeFlag:Int
    private var version:String
    //29
    
    //    init(withCoder coder : NSCoder){
    //
    //    }
    //    func encodeWithCoder(coder : NSCoder){
    //
    //    }
    
    //    required init(coder aDecoder: NSCoder) {
    //
    //    }
    
    //    func encode(with aCoder : NSCoder) {
    //
    //    }
    
    
    override init() {
        category = ""
        sensorReading = 0.00
        
        //flow
        flowType = ""
        flowDetail = ""
        slotId = -1
        
        //cnctd host
        deviceCategory = ""
        hostID = ""
        OS = ""
        hostName = ""
        macAddress = ""
        parameters = ""
        
        //ids
        destHostName = ""
        srcHostName = ""
        dest_port = ""
        dest_ip = ""
        src_ip = ""
        src_port = ""
        idsBlockType = ""
        
        //info
        info_type = ""
        timeOn = -1
        timeOff = -1
        
        //similar
        ip = ""
        alertDescription = ""
        input_src = ""
        eve_sec = -1
        namespace = ""
        app_id = ""
        record_id = ""
        seenStatus = false
        alertId = -1
        
        //others
        modeFlag = -1
        self.version = ""
        
    }
    func getSeenStatus()->Bool {
        return seenStatus;
    }
    
    func setSeenStatus(_ status : Bool) {
        self.seenStatus = status
    }
    
    func getInfoType()->String {
        return info_type;
    }
    
    func setInfoType(_ type : String) {
        self.info_type = type
    }
    
    func getSlotId()->Int {
        return slotId;
    }
    
    func setSlotId(_ slotId : Int) {
        self.slotId = slotId
    }
    
    func getTimeOn()->Int {
        return timeOn;
    }
    
    func setTimeOn(_ timeOn : Int) {
        self.timeOn = timeOn
    }
    func getTimeOff()->Int {
        return timeOff;
    }
    
    func setTimeOff(_ timeOff : Int) {
        self.timeOff = timeOff
    }
    
    func getIdsBlockType()->String {
        return idsBlockType;
    }
    
    func setIdsBlockType(_ idsBlockType : String) {
        self.idsBlockType = idsBlockType
    }
    
    func getFlowType()->String {
        return flowType;
    }
    
    func setFlowType(_ flowType : String) {
        self.flowType = flowType
    }
    
    func getFlowDetail()->String {
        return flowDetail;
    }
    
    func setFlowDetail(_ flowDetail : String) {
        self.flowDetail = flowDetail
    }
    
    func getCategory()->String {
        return category;
    }
    
    func setCategory(_ category : String) {
        self.category = category
    }
    
    func getSensorReading()->Double {
        return sensorReading;
    }
    
    func setSensorReading(_ sensorReading : Double) {
        self.sensorReading = sensorReading
    }
    
    //old
    
    func getOS()->String {
        return OS;
    }
    func setOS(_ os : String) {
        self.OS = os
    }
    
    func getDeviceCategory()->String {
        return deviceCategory;
    }
    
    func setDeviceCategory(_ deviceCategory : String) {
        self.deviceCategory = deviceCategory
    }
    func getHostName()->String {
        return hostName;
    }
    
    func setHostName(_ hName : String) {
        self.hostName = hName
    }
    
    func gethostID()->String {
        return hostID;
    }
    
    func sethostID(_ hostID : String) {
        self.hostID = hostID
    }
    
    func getMacAddress()->String {
        return macAddress;
    }
    
    func setMacAddress(_ madres : String) {
        self.macAddress = madres
    }
    func getParameters()->String {
        return parameters;
    }
    
    func setParameters(_ paramtrs : String) {
        self.parameters = paramtrs
    }
    
    
    func getSrcPort()->String {
        return src_port;
    }
    
    func setSrcPort(_ src_port : String) {
        self.src_port = src_port
    }
    func getDescription()->String {
        return alertDescription;
    }
    
    func setDescription(_ desc : String) {
        self.alertDescription = desc
    }
    func getInputSrc()->String {
        return input_src;
    }
    
    func setInputSrc(_ input_src : String) {
        self.input_src = input_src
    }
    func getEveSec()->Int {
        return eve_sec;
    }
    
    func setEveSec(_ eveSec : Int) {
        self.eve_sec = eveSec
    }
    func getNameSpace()->String {
        return namespace;
    }
    
    func setNameSpace(_ namespace : String) {
        self.namespace = namespace
    }
    func getDestHostName()->String {
        return destHostName;
    }
    
    func setDestHostName(_ dest_host : String) {
        self.destHostName = dest_host
    }
    
    func getSrcHostName()->String {
        return srcHostName;
    }
    
    func setSrcName(_ src_host : String) {
        self.srcHostName = src_host
    }
    func getAppId()->String {
        return app_id;
    }
    
    func setAppId(_ appId : String) {
        self.app_id = appId
    }
    func getSrcIp()->String {
        return src_ip;
    }
    
    func setSrcIp(_ srcIp : String) {
        self.src_ip = srcIp
    }
    func getRecordId()->String {
        return record_id;
    }
    
    func setRecordId(_ recordID : String) {
        self.record_id = recordID
    }
    func getDestPort()->String {
        return dest_port;
    }
    
    func setDestPort(_ destPOrt : String) {
        self.dest_port = destPOrt
    }
    func getDestIp()->String {
        return dest_ip;
    }
    
    func setDestIp(_ destIp : String) {
        self.dest_ip = destIp
    }
    func getIP()->String {
        return ip;
    }
    
    func setIp(_ ip : String) {
        self.ip = ip
    }
    
    func getalertId()->Int {
        return alertId;
    }
    
    func setAlertId(_ ialertIdp : Int) {
        self.alertId = ialertIdp
    }
    
    func getModeFlag()->Int {
        return modeFlag;
    }
    
    func setModeFlag(flag : Int) {
        self.modeFlag = flag
    }
    func setVersion(_ version: String) {
        self.version = version
    }
    func getVersion()->String {
        return self.version
    }
    
}



//    required init?(coder aDecoder: NSCoder) {
//        description = aDecoder.decodeObject(forKey: "nonce") as! String
//        eve = aDecoder.decodeObject(forKey: "string") as! String
//        isHa = aDecoder.decodeObject(forKey: "bool") as! Bool
//        integer = aDecoder.decodeObject(forKey: "int") as! Int
//        
//        
//        //        aDecoder.dec
//    }
//    
//    
//    func encode(with aCoder: NSCoder) {
//        aCoder.encode(name, forKey: "nonce")
//        aCoder.encode(fName, forKey: "string")
//        aCoder.encode(integer, forKey: "int")
//        aCoder.encode(isHa, forKey: "bool")
//    }

