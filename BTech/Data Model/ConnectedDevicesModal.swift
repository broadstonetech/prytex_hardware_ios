//
//  ConnectedDevicesModal.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 05/07/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class ConnectedDevicesModal{
    
    fileprivate var id_cd: Int;   //_cd denotes connected devices
    fileprivate var highCount_cd:Int;
    fileprivate var mediumCount_cd:Int;
    fileprivate var lowCount_cd:Int;
    fileprivate var eveSec_cd:Int;
    fileprivate var macAdress_cd:String;
    fileprivate var hostName_cd:String;
    fileprivate var hostId_cd:String;
    fileprivate var hostIp_cd:String;
    fileprivate var hostOS_cd:String;
    fileprivate var parameters_cd:String;
    fileprivate var username_cd:String;
    fileprivate var recordId:String
    fileprivate var inputSrc:String
    fileprivate var deviceType:String
    fileprivate var score:Int;
    fileprivate var addToPolicy:Bool;
    var arrayPorts = [[String : String]]()
    var arrayDefaultPasswords = [[String : AnyObject]]()

    
    init() {

        self.id_cd=0
        self.highCount_cd=0
        self.mediumCount_cd=0
        self.lowCount_cd=0
        self.eveSec_cd=0
        self.macAdress_cd=""
        self.hostName_cd=""
        self.hostId_cd=""
        self.hostIp_cd=""
        self.hostOS_cd=""
        self.parameters_cd=""
        self.username_cd=""
        self.recordId=""
        self.inputSrc=""
        self.deviceType = ""
        self.score = 0
        self.addToPolicy = false
    }
    
    func getid_cd()->Int {
        return id_cd;
    }
    
    func setid_cd(_ id_cd : Int) {
        self.id_cd = id_cd;
    }
    
    func getScore()->Int {
        return score;
    }
    
    func setScore(_ score : Int) {
        self.score = score;
    }

    func gethighCount_cd()->Int {
        return highCount_cd;
    }
    func sethighCount_cd(_ highhCount_cd : Int) {
        self.highCount_cd = highhCount_cd;
    }
    
    func getMediumCount_cd()->Int {
        return mediumCount_cd;
    }
    
    func setMediumCount_cd(_ mediumCount_cd : Int) {
        self.mediumCount_cd = mediumCount_cd;
    }
    
    func getLowCount_cd()-> Int {
        return lowCount_cd;
    }
    
    func setLowCount_cd(_ lowCount_cd : Int) {
        self.lowCount_cd = lowCount_cd;
    }
    
    func getEveSec_cd()-> Int {
        return eveSec_cd;
    }
    func setEveSec_cd(_ eveSec_cd : Int) {
        self.eveSec_cd = eveSec_cd;
    }
    
    func getMacAdress_cd()->String {
        return macAdress_cd;
    }
    
    func setMacAdress_cd(_ macAdress_cd : String) {
        self.macAdress_cd = macAdress_cd;
    }
    
    func getHostName_cd()-> String {
        return hostName_cd;
    }
    
    func setHostName_cd(_ hostName_cd : String) {
        self.hostName_cd = hostName_cd;
    }
    
    func getHostId()-> String {
        return hostId_cd;
    }
    
    func setHostId_cd(_ hostID : String) {
        self.hostId_cd = hostID;
    }
    
    func getHostIP_cd()-> String {
        return hostIp_cd;
    }
    
    func setHostIP_cd(_ hostip_cd : String) {
        self.hostIp_cd = hostip_cd;
    }
    
    func getHostOs()-> String {
        return hostOS_cd;
    }
    
    func setHostOs_cd(_ hostOs : String) {
        self.hostOS_cd = hostOs;
    }
    
    func getParameters_cd()-> String {
        return parameters_cd;
    }
    
    func setParameters_cd(_ parameters_cd : String) {
        self.parameters_cd = parameters_cd;
    }
    
    func getUsername_cd()-> String {
        return username_cd;
    }
    
    func setUsername_cd(_ username_cd : String) {
        self.username_cd = username_cd;
    }
    func getInputSrc()->String {
        return inputSrc;
    }
    
    func setInputSrc(_ input_src : String) {
        self.inputSrc = input_src;
    }
    
    func getRecordId()->String {
        return recordId;
    }
    
    func setRecordId(_ recordId : String) {
        self.recordId = recordId;
    }
    
    func getdeviceType()->String {
        return deviceType;
    }
    
    func setdeviceType(_ deviceType : String) {
        self.deviceType = deviceType;
    }
    
    func isInPolicy()->Bool {
        return addToPolicy;
    }
    
    func setToAddInPolicy(_ status : Bool) {
        self.addToPolicy = status
    }

    class func conertToDictionary(selectedHost : [ConnectedDevicesModal]) -> [[String: String]] {
        var list = [[String: String]]()
        
        for e in selectedHost {
            var dic = [String : String]()
            dic["hostname"] = e.getHostName_cd()
            dic["macaddress"] = e.getMacAdress_cd()
            dic["ip"] = e.getHostIP_cd()
            
            list.append(dic)
            
        }
        return list
    }
    
    
    
}
