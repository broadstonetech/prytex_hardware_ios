//
//  LoggedDevicesModel.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 11/09/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class LoggedDevicesModel{

    fileprivate var Ip:String
    fileprivate var location:String
    fileprivate var eveSec:Int
    
    init() {
        Ip = ""
        location = ""
        eveSec = -1
    }
    
    func getIp()->String {
        return Ip;
    }
    
    func setIp(_ ip : String) {
        self.Ip = ip
    }
    
    func getLocation()->String {
        return location;
    }
    
    func setLocation(_ loc : String) {
        self.location = loc
    }
    func getEveSec()->Int {
        return eveSec;
    }
    
    func setEveSec(_ evesec : Int) {
        self.eveSec = evesec
    }
    
}
