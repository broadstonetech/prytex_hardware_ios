//
//  PolicyDurationModel.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 01/03/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit

class PolicyDurationModel {
    
    var hostName:String?
    var totalDuration:String?
    var connectTime:String?
    var disConnectTime:String?
    var eve_sec:Int?
    var slotDuration:String?
    
    var arraySlotsSets = [[String : AnyObject]]()
    
    init() {
        
        self.hostName=""
        self.totalDuration=""
        self.connectTime=""
        self.disConnectTime=""
        self.eve_sec = -1
        self.slotDuration=""
        
}

    func getHostName()->String {
        return hostName!
    }
    
    func setHostName(_ hName : String) {
        self.hostName = hName
    }
    
    
    func getTotalDuration()->String {
        return totalDuration!
    }
    
    func setTotalDuration(_ t_duration : String) {
        self.totalDuration = t_duration
    }
    
    func getConnectTime()->String {
        return connectTime!
    }
    
    func setConnectTime(_ c_time : String) {
        self.connectTime = c_time
    }
    
    func getSlotDuration()->String {
        return slotDuration!
    }
    
    func setSlotDuration(_ slotDuration : String) {
        self.slotDuration = slotDuration
    }
    
}
