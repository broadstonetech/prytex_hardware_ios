//
//  PolicyManagementModel.swift
//  BTech
//
//  Created by Ahmed Akhtar on 02/01/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class PolicyManagementModel: NSObject {

     var title:String?
     var startTime:String?
     var endTime:String?
     var days:String?
     var protocolsArray = [String]()
     var targetURLArray = [String]()
     var hostListArray = [String]()
    
    override init() {
    
    self.title=""
    self.startTime=""
    self.endTime=""
    self.days=""
    self.protocolsArray=[]
    self.targetURLArray=[]
    self.hostListArray=[]
    
    }
    
    func populatePolicyModelWith(_ title : String, startTime : String, endTime : String, days : String, hostList : [String], targetURL : [String], protocols : [String])
    {
        self.title = title
        self.startTime = startTime
        self.endTime = endTime
        self.days = days
        self.hostListArray = hostList
        self.targetURLArray = targetURL
        self.protocolsArray = protocols
        
    }
    
    
}
