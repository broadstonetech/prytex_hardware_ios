//
//  PolicyModel.swift
//  BTech
//
//  Created by Ahmed Akhtar on 01/02/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class PolicyModel: NSObject {

    var ID:Int?
    var title:String?
    var startTime:String?
    var endTime:String?
    var days:String?
    var protocols:String?
    var targetURL:String?
    var hostList:String?

    override init() {
        
        self.ID=0
        self.title=""
        self.startTime=""
        self.endTime=""
        self.days=""
        self.protocols=""
        self.targetURL=""
        self.hostList=""
        
    }

    
}
