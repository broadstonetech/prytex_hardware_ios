//
//  PolicyPlanModel.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 09/05/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit

class PolicyPlanModel {

    var policyTitle : String
    var policyStatus : Int
    var policyDays : Int
    var policyDaysArr = [String]()
//    [[String : AnyObject]]()
//    var arrayFlowRecordIdsList = [String]()
    var policyStartTime : String
    var policyEndTime : String
    var policyStartTimeGMT : String
    var policyEndTimeGMT : String
    var policyId : String
    var poicyDevices = [[String : AnyObject]]()
    //PROTOCOL POLICY
    var procedure : String
    var type : String
    var dataType : String
    var request : String
    var hostMacAddress : String
    var hostIpAdress : String
    var hostName : String
    var hostId : String
    var protocolName : String
    fileprivate var addToPolicy:Bool;
    fileprivate var isHardCoded : Bool
    var descrption: String

//    public JSONArray policyProtocolsLists;
    var policyProtocolsLists = [[String : AnyObject]]()
    var frequentProtocolsList = [String]()

    
    init() {
        policyTitle = ""
        policyStatus = -1
        policyDays = -1
        policyStartTime = ""
        policyEndTime = ""
        policyId = ""
        policyStartTimeGMT = ""
        policyEndTimeGMT = ""
        procedure = ""
        type = ""
        dataType = ""
        request = ""
        hostMacAddress = ""
        hostIpAdress = ""
        hostName = ""
        hostId = ""
        protocolName = ""
        self.addToPolicy = false
        self.isHardCoded = false
        self.descrption = ""
    }
    
    func getProtocolName()->String {
        return protocolName;
    }
    
    func setProtocolName(_ Name : String) {
        self.protocolName = Name
    }
    func getProcedure()->String {
        return procedure;
    }
    
    func setProcedure(_ procedure : String) {
        self.procedure = procedure
    }
    
    func getType()->String {
        return type;
    }
    
    func setType(_ type : String) {
        self.type = type
    }
    func getDataType()->String {
        return dataType;
    }
    
    func setDataType(_ dataType : String) {
        self.dataType = dataType
    }
    func getRequest()->String {
        return request;
    }
    
    func setRequest(_ request : String) {
        self.request = request
    }
    func getMacAdress()->String {
        return hostMacAddress;
    }
    
    func setMacAdress(_ macAdres : String) {
        self.hostMacAddress = macAdres
    }
    func getIpAdress()->String {
        return hostIpAdress;
    }
    
    func setIpAdress(_ ip : String) {
        self.hostIpAdress = ip
    }
    
    func getHostName()->String {
        return hostName;
    }
    
    func setHostName(_ hname : String) {
        self.hostName = hname
    }
    func getHostId()->String {
        return hostId;
    }
    
    func setHostId(_ hId : String) {
        self.hostId = hId
    }
    
    //access control
    func getPolicyTitle()->String {
        return policyTitle;
    }
    
    func setPolicyTitle(_ title : String) {
        self.policyTitle = title
    }
    
    func getpolicyId()->String {
        return policyId;
    }
    
    func setpolicyId(_ id : String) {
        self.policyId = id
    }
    
    func getPolicyStatus()->Int {
        return policyStatus;
    }
    
    func setPolicyStatus(_ status : Int) {
        self.policyStatus = status
    }
    func getPolicyDays()->Int {
        return policyDays;
    }
    
    func setPolicyDays(_ days : Int) {
        self.policyDays = days
    }
    
    func getPolicyStarttime()->String {
        return policyStartTime;
    }
    
    func setPolicyStartTime(_ startTime : String) {
        self.policyStartTime = startTime
    }
    
    func getPolicyEndTime()->String {
        return policyEndTime;
    }
    
    func setPolicyEndTime(_ endTime : String) {
        self.policyEndTime = endTime
    }
    
    func getPolicyStartTimeGMT()->String {
        return policyStartTimeGMT;
    }
    
    func setPolicyStartTimeGMT(_ sTime : String) {
        self.policyStartTimeGMT = sTime
    }
    func getPolicyEndTimeGMT()->String {
        return policyEndTimeGMT;
    }
    
    func setPolicyEndTimeGMT(_ eTime : String) {
        self.policyEndTimeGMT = eTime
    }
    
    func isInPolicy()->Bool {
        return addToPolicy;
    }
    
    func setToAddInPolicy(_ status : Bool) {
        self.addToPolicy = status
    }
    
    class func conertToDictionary(selectedHost : [PolicyPlanModel]) -> [[String: String]] {
        var list = [[String: String]]()
        
        for e in selectedHost {
            var dic = [String : String]()
            dic["protocols"] = e.getProtocolName()
        
            list.append(dic)
            
        }
        return list
    }
    
    func setHardcodeStatus(yes: Bool){
        self.isHardCoded = yes
    }
    func getHardCodeStatus() -> Bool{
        return self.isHardCoded
    }
    
    func setPolicyDescription(_ description : String) {
        self.descrption = description
    }
    
    func getPolicyDescription()->String {
        return self.descrption
    }
    
}
