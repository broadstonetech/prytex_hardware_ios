//
//  PreMutedAlertModel.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 26/12/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class PreMutedAlertModel  {
    private var description:String
    private var eve_sec:Int
    private var src_ip:String
    private var dest_ip:String
    var arraySets = [[String : String]]()
    
    private var flowType:String
    private var flowDetail:String
    private var inputSrc:String
    private var slotId : Int
    
    var arrayFlowAlerts = [[String : AnyObject]]()
    var arrayFlowRecordIdsList = [String]()
    
    init() {
        description = ""
        eve_sec = -1
        src_ip = ""
        dest_ip = ""
        flowType = ""
        flowDetail = ""
        inputSrc = ""
        slotId = -1
        
    }
    
    func getflowType()->String {
        return flowType
    }
    
    func setflowType(_ flowType : String) {
        self.flowType = flowType
    }
    
    func getinputSrc()->String {
        return inputSrc
    }
    
    func setinputSrc(_ inputSrc : String) {
        self.inputSrc = inputSrc
    }
    
    func getDescription()->String {
        return description
    }
    
    func setDescription(_ desc : String) {
        self.description = desc
    }
    
    func getEveSec()->Int {
        return eve_sec;
    }
    
    func setEveSec(_ evesec : Int) {
        self.eve_sec = evesec
    }
    
    func getSrcIP()->String {
        return src_ip
    }
    
    func setSrcIp(_ srcip : String) {
        self.src_ip = srcip
    }
    
    func getDestIP()->String {
        return dest_ip;
    }
    
    func setDestIP(_ destip : String) {
        self.dest_ip = destip
    }
}
