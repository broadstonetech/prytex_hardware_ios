//
//  SensorDataModel.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 23/08/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class SensorDataModel {

    fileprivate var base_res:String
    fileprivate var air_quality:String
    fileprivate var mean_adc:Int
    fileprivate var temperature:Double
    fileprivate var eve_sec:Int
    fileprivate var coppm:Double
    fileprivate var humidity:Double
    fileprivate var validInterval:Int
    fileprivate var mean_res:Int
    fileprivate var mean_corrected_res:Int
    
    
    init() {
        base_res = ""
        air_quality = ""
        mean_adc = 0
        temperature = 0
        eve_sec = 0
        coppm = 0
        humidity = 0
        validInterval = 0
        mean_res = 0
        mean_corrected_res = 0
    }
    
    //get set
    func getBaseRes()->String {
        return base_res;
    }
    
    func setBaseRes(_ bres : String) {
        self.base_res = bres
    }
    func getAirQuality()->String {
        return air_quality;
    }
    
    func setAirQuality(_ air_quality : String) {
        self.air_quality = air_quality
    }
    
    func getMeanAdc()->Int {
        return mean_adc;
    }
    
    func setMeanAdc(_ adc : Int) {
        self.mean_adc = adc;
    }
    func getTemperature()->Double {
        return temperature;
    }
    
    func setTemperature(_ temp : Double) {
        self.temperature = temp;
    }
    func getEveSec()->Int {
        return eve_sec;
    }
    
    func setEveSec(_ eveSec : Int) {
        self.eve_sec = eveSec;
    }
    func getCoppm()->Double {
        return coppm;
    }
    
    func setCoppm(_ coppm : Double) {
        self.coppm = coppm;
    }
    func getHumidity()->Double {
        return humidity;
    }
    
    func setHumidity(_ humidity : Double) {
        self.humidity = humidity;
    }
    func getValidInterval()->Int {
        return validInterval;
    }
    
    func setValidInterval(_ vInterval : Int) {
        self.validInterval = vInterval;
    }
    func getMeanRes()->Int {
        return mean_res;
    }
    
    func setMeanRes(_ meanRes : Int) {
        self.mean_res = meanRes;
    }
    func getMeanCorrectedRes()->Int {
        return mean_corrected_res;
    }
    
    func setMeanCorrectedRes(_ cres : Int) {
        self.mean_corrected_res = cres;
    }
    
}
