//
//  TestDB.swift
//  BTech
//
//  Created by Ahmad Waqas on 26/07/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import ObjectMapper
class TestDB: Mappable {

    var day: String?
    var temperature: Int?
    var conditions: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        day <- map["day"]
        temperature <- map["temperature"]
        conditions <- map["conditions"]
    }
    
    
    //old code 
//    required init?(_ map: Map){
//        
//    }
//    
//    func mapping(_ map: Map) {
//        day <- map["day"]
//        temperature <- map["temperature"]
//        conditions <- map["conditions"]
//    }
    
}
