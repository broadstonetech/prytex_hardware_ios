//
//  AttributeExtension.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 17/07/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import Foundation

extension NSMutableAttributedString {
    func bold(_ text:String) -> NSMutableAttributedString {
        let attrs:[NSAttributedString.Key:Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : UIFont(name: "AvenirNext-Medium", size: 12)!]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }
    
    func normal(_ text:String)->NSMutableAttributedString {
        let normal =  NSAttributedString(string: text)
        self.append(normal)
        return self
    }
}
