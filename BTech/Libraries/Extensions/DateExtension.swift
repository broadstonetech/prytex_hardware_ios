//
//  DateExtension.swift
//  BioPage
//
//  Created by Talha Ejaz on 15/06/2016.
//  Copyright © 2016 Talha Ejaz. All rights reserved.
//

import Foundation

extension Date {
    
    static func dateFromServer(_ fromServerDate: String) -> Date? {
        if(fromServerDate != "") {
            let dateFormatter = DateFormatter()
//            dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")
//                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.dateFormat = "MMM-dd-yyyy h:mm:a"
            dateFormatter.timeZone = TimeZone.current //name: "UTC")
            return dateFormatter.date(from: fromServerDate)!
        }
        return nil
    }
    static func dobFromServer(_ fromServerDate: String) -> Date? {
        if(fromServerDate != "") {
            let dateFormatter = DateFormatter()
//            dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")
//            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.dateFormat = "MMM-dd-yyyy h:mm:a"
            dateFormatter.timeZone = TimeZone.current //name: "UTC")
            return dateFormatter.date(from: fromServerDate)!
        }
        return nil
    }
    static func dayDateMessage(_ fromServerDate: Date) -> String? {
        print("fromServerDate != nil/==nil")
         if(fromServerDate != nil) {
            let dateFormatter = DateFormatter()
            //dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")
            dateFormatter.timeZone = TimeZone.autoupdatingCurrent
//            dateFormatter.dateFormat = "EEEE, MM dd hh:mm a"
            dateFormatter.dateFormat = "MMM-dd-yyyy h:mm:a"
            return dateFormatter.string(from: fromServerDate)
            //return dateFormatter.dateFromString(fromServerDate)!
        }
        return nil
    }
    func timestamp() -> String {
       return NSString(format: "%f",self.timeIntervalSince1970 * 1000) as String
    }
    
    func isGreaterThanDate(_ dateToCompare: Date) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(_ dateToCompare: Date) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(_ dateToCompare: Date) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func daysFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(NSCalendar.Unit.NSDayCalendarUnit, from: date, to: self, options: []).day!
    }
    func hoursFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(NSCalendar.Unit.NSHourCalendarUnit, from: date, to: self, options: []).hour!
    }
    func minutesFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(NSCalendar.Unit.NSMinuteCalendarUnit, from: date, to: self, options: []).minute!
    }
    func secondsFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(NSCalendar.Unit.NSSecondCalendarUnit, from: date, to: self, options: []).second!
    }
    
}
