//
//  OpenUrlsExtension.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 12/06/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import Foundation

extension UIApplication {
    class func tryURL(urls: [String]) {
        let application = UIApplication.shared
        for url in urls {
            if application.canOpenURL(URL(string: url)!) {
                //application.openURL(URL(string: url)!) //depricated in ios 10.*
                application.open(URL(string: url)!, options: [:], completionHandler: nil)
                return
            }
        }
    }
}
