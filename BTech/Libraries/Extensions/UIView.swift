//
//  UIView.swift
//  BTech
//
//  Created by Mufaza Majeed on 5/27/21.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation
extension UIView {
  
  func smoothRoundCorners(to radius: CGFloat) {
    let maskLayer = CAShapeLayer()
    maskLayer.path = UIBezierPath(
      roundedRect: bounds,
      cornerRadius: radius
    ).cgPath
    
    layer.mask = maskLayer
  }
  
}
