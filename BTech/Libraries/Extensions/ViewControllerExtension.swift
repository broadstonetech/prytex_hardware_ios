//
//  ViewControllerExtension.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 24/01/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    func saveViewAlerts(array: Array<Any>, forKey key: String) {
        
        let archiver = NSKeyedArchiver.archivedData(withRootObject: array)
        UserDefaults.standard.set(archiver, forKey: key)
    }
    
    func retrieveViewAlerts(withKey key: String) -> Array<Any>? {
        
        // Check if data exists
        guard let data = UserDefaults.standard.object(forKey: key) else {
            return nil
        }
        
        // Check if retrieved data has correct type
        guard let retrievedData = data as? Data else {
            return nil
        }
        
        // Unarchive data
        let unarchivedObject = NSKeyedUnarchiver.unarchiveObject(with: retrievedData)
        return unarchivedObject as? Array
    }
    
    
    
}
