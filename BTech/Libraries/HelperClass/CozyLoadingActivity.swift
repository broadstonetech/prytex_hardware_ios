//
//  CozyLoadingActivity.swift
//  Cozy
//
//  Created by Goktug Yilmaz on 02/06/15.
//  Copyright (c) 2015 Goktug Yilmaz. All rights reserved.
//

import UIKit

struct CozyLoadingActivity {
    
    //==========================================================================================================
    // Feel free to edit these variables
    //==========================================================================================================
    enum DropDownOtions:String {
        case NewHost = "New Host", NetworkIntrusion = "Network Intrusion", SensorAlerts = "Sensor"
        
        static let viewAlertFilterValues = ["All",NewHost.rawValue,SensorAlerts.rawValue,"Prytex Info"]
        static let blockedFilterValues = ["All",NewHost.rawValue, "Blocked IP Addresses", "Network Alert"]
        static let mutedFilterValues = ["All",NewHost.rawValue, "Blocked IP Addresses", "Network Alert"]
    }
    enum BlockOptions:String {
        case Allow = "Allow", AllowForever = "Allow Forever", TrustOneHour = "Trust For One Hour",TrustDevice = "Trust This Device", BlockForOneMint = "Block for 15 min", BlockForever = "Block Forever", Unmute = "Unmute", Cancel = "Cancel", Mute = "Mute", UnBlock = "Unblock", KeepBlock = "Keepblock", UpdateNow = "Update", UpdateLater = "Later"
    }
    
    struct Settings {
        //        static var CLAppThemeColor = UIColor(red: 5/255, green: 146/255, blue: 196/255, alpha: 1.0)
        
        static var CLAppThemeColor = UIColor(red: 0/255, green: 174/255, blue: 239/255, alpha: 1.0)
        static var CLABackgroundColor = UIColor(red: 227/255, green: 232/255, blue: 235/255, alpha: 1.0)
        static var CLAActivityColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
        static var CLATextColor = UIColor(red: 80/255, green: 80/255, blue: 80/255, alpha: 1.0)
        static var CnctdDevicesCircleColor = UIColor(red: 172/255, green: 172/255, blue: 172/255, alpha: 1.0)
        static var SensorGoodColor = UIColor(red: 61/255, green: 164/255, blue: 55/255, alpha: 1.0)
        static var SensorBadColor = UIColor(red: 255/255, green: 182/255, blue: 11/255, alpha: 1.0)
        static var dmoatOfflineColor = UIColor(red: 149/255, green: 149/255, blue: 149/255, alpha: 1.0)
        static var SensorHazardousColor = UIColor(red: 182/255, green: 3/255, blue: 3/255, alpha: 1.0)
        static var BtnDisabledColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1.0)
        static var CLAFontName = "HelveticaNeue-Bold"
        //muted alert top bar colors
        static var selectedButonColor = UIColor(red: 22/255, green: 190/255, blue: 239/255, alpha: 1.0)
        static var UnselectedButonColor = UIColor(red: 0/255, green: 174/255, blue: 239/255, alpha: 1.0)
        static var UnselectedTabBgColor = UIColor(red: 0/255, green: 162/255, blue: 227/255, alpha: 1.0)
        static var SelectedTabBgColor = UIColor(red: 0/255, green: 174/255, blue: 239/255, alpha: 1.0)
        static var UnselectedButonTextColor = UIColor(red: 220/255, green: 238/255, blue: 255/255, alpha: 1.0)
        static var DashboardBgColor = UIColor(red: 237/255, green: 250/255, blue: 255/255, alpha: 1.0)
        static var WhiteColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        static var TabbarUnselectedBtnColor = UIColor(red: 152/255, green: 217/255, blue: 239/255, alpha: 1.0)
        //alerts pie chart colors
        static var OpenAlertsColor = UIColor(red: 242/255, green: 178/255, blue: 19/255, alpha: 1.0)
        //        static var FlowAlertsColor = UIColor(red: 126/255, green: 167/255, blue: 57/255, alpha: 1.0)
        static var FlowAlertsColor = UIColor(red: 61/255, green: 164/255, blue: 55/255, alpha: 1.0)
        static var BlockedAlertsColor = UIColor(red: 244/255, green: 108/255, blue: 109/255, alpha: 1.0)
        static var ChartAlertsTextColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        
        
        
        //        static var BarAColor = UIColor(red: 8/255, green: 99/255, blue: 167/255, alpha: 1.0)
        //        static var BarBColor = UIColor(red: 34/255, green: 131/255, blue: 186/255, alpha: 1.0)
        //        static var BarCColor = UIColor(red: 66/255, green: 166/255, blue: 204/255, alpha: 1.0)
        //        static var BarDColor = UIColor(red: 103/255, green: 193/255, blue: 203/255, alpha: 1.0)
        //        static var BarEColor = UIColor(red: 143/255, green: 212/255, blue: 189/255, alpha: 1.0)
        
        static var BarAColor = UIColor(red: 8/255, green: 99/255, blue: 167/255, alpha: 1.0)
        static var BarBColor = UIColor(red: 1/255, green: 164/255, blue: 239/255, alpha: 1.0)
        static var BarCColor = UIColor(red: 127/255, green: 186/255, blue: 0/255, alpha: 1.0)
        static var BarDColor = UIColor(red: 192/255, green: 174/255, blue: 29/255, alpha: 1.0)
        static var BarEColor = UIColor(red: 242/255, green: 178/255, blue: 19/255, alpha: 1.0)
        
        // Other possible stuff: ✓ ✓ ✔︎ ✕ ✖︎ ✘
        static var CLASuccessIcon = "✔︎"
        static var CLAFailIcon = "✘"
        static var CLASuccessText = "Success"
        static var CLAFailText = "Request failed. Please try again."
        static var CLASuccessColor = UIColor(red: 68/255, green: 118/255, blue: 4/255, alpha: 1.0)
        static var CLAFailColor = UIColor(red: 255/255, green: 75/255, blue: 56/255, alpha: 1.0)
        static var CLAWidthDivision: CGFloat {
            get {
                if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
                    return  3.5
                } else {
                    return 1.6
                }
            }
        }
    }
    
    fileprivate static var instance: LoadingActivity?
    fileprivate static var hidingInProgress = false
    
    /// Disable UI stops users touch actions until CozyLoadingActivity is hidden. Return success status
    static func show(_ text: String, disableUI: Bool) -> Bool {
        guard instance == nil else {
            print("CozyLoadingActivity: You still have an active activity, please stop that before creating a new one")
            return false
        }
        
        instance = LoadingActivity(text: text, disableUI: disableUI)
        return true
    }
    
    static func showWithDelay(_ text: String, disableUI: Bool, seconds: Double) -> Bool {
        let showValue = show(text, disableUI: disableUI)
        delay(seconds) { () -> () in
            hide(true, animated: false)
        }
        return showValue
    }
    
    /// Returns success status
    static func hide(_ success: Bool? = nil, animated: Bool = false) -> Bool {
        guard instance != nil else {
            print("CozyLoadingActivity: You don't have an activity instance")
            return false
        }
        
        guard hidingInProgress == false else {
            print("CozyLoadingActivity: Hiding already in progress")
            return false
        }
        
        if !Thread.current.isMainThread {
            DispatchQueue.main.async {
                instance?.hideLoadingActivity(success, animated: animated)
            }
        } else {
            instance?.hideLoadingActivity(success, animated: animated)
        }
        
        return true
    }
    
    fileprivate static func delay(_ seconds: Double, after: @escaping ()->()) {
        let queue = DispatchQueue.main
        let time = DispatchTime.now() + Double(Int64(seconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        queue.asyncAfter(deadline: time, execute: after)
    }
    
    fileprivate class LoadingActivity: UIView {
        var textLabel: UILabel!
        var activityView: UIActivityIndicatorView!
        var icon: UILabel!
        var UIDisabled = false
        
        convenience init(text: String, disableUI: Bool) {
            let width = UIScreen.CLAScreenWidth / Settings.CLAWidthDivision
            let height = width / 3
            self.init(frame: CGRect(x: UIScreen.CLAScreenWidth/2 - width/4, y: UIScreen.CLAScreenHeight/2 - height/2, width: width/2, height: height))
            backgroundColor = Settings.CLABackgroundColor
            alpha = 1
            layer.cornerRadius = 8
            createShadow()
            
            let yPosition = frame.height/2 - 20
            
            activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
            activityView.frame = CGRect(x: width/4 - 20, y: yPosition, width: 40, height: 40)
            activityView.color = Settings.CLAActivityColor
            activityView.startAnimating()
            
            textLabel = UILabel(frame: CGRect(x: 0, y: yPosition, width: width/2, height: 40))
            textLabel.textColor = Settings.CLATextColor
            textLabel.font = UIFont(name: Settings.CLAFontName, size: 18)
            textLabel.adjustsFontSizeToFitWidth = true
            textLabel.minimumScaleFactor = 0.25
            //textLabel.backgroundColor=UIColor.orangeColor()
            textLabel.textAlignment = NSTextAlignment.center
            //textLabel.text = text
            
            addSubview(activityView)
            addSubview(textLabel)
            
            UIApplication.CLAtopMostController().view.addSubview(self)
            
            if disableUI {
                UIApplication.shared.beginIgnoringInteractionEvents()
                UIDisabled = true
            }
        }
        
        func createShadow() {
            layer.shadowPath = createShadowPath().cgPath
            layer.masksToBounds = false
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 0)
            layer.shadowRadius = 5
            layer.shadowOpacity = 0.5
        }
        
        func createShadowPath() -> UIBezierPath {
            let myBezier = UIBezierPath()
            myBezier.move(to: CGPoint(x: -3, y: -3))
            myBezier.addLine(to: CGPoint(x: frame.width + 3, y: -3))
            myBezier.addLine(to: CGPoint(x: frame.width + 3, y: frame.height + 3))
            myBezier.addLine(to: CGPoint(x: -3, y: frame.height + 3))
            myBezier.close()
            return myBezier
        }
        
        func hideLoadingActivity(_ success: Bool?, animated: Bool) {
            hidingInProgress = true
            if UIDisabled {
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            
            var animationDuration: Double = 0
            if success != nil {
                if success! {
                    animationDuration = 0.5
                } else {
                    animationDuration = 1
                }
            }
            
            icon = UILabel(frame: CGRect(x: 0, y: frame.height/2 - 20, width: 20, height: 40))
            icon.font = UIFont(name: Settings.CLAFontName, size: 20)
            //icon.backgroundColor=UIColor.redColor()
            icon.textAlignment = NSTextAlignment.center
            icon.isHidden = true
            
            if animated {
                textLabel.fadeTransition(animationDuration)
            }
            
            if success != nil {
                if success! {
                    icon.textColor = Settings.CLASuccessColor
                    icon.text = Settings.CLASuccessIcon
                    textLabel.text = Settings.CLASuccessText
                } else {
                    icon.textColor = Settings.CLAFailColor
                    icon.text = Settings.CLAFailIcon
                    textLabel.text = Settings.CLAFailText
                }
            }
            
            addSubview(icon)
            
            if animated {
                icon.alpha = 0
                activityView.stopAnimating()
                UIView.animate(withDuration: animationDuration, animations: {
                    self.icon.alpha = 1
                }, completion: { (value: Bool) in
                    self.callSelectorAsync(#selector(UIView.removeFromSuperview), delay: animationDuration)
                    instance = nil
                    hidingInProgress = false
                })
            } else {
                activityView.stopAnimating()
                self.callSelectorAsync(#selector(UIView.removeFromSuperview), delay: animationDuration)
                instance = nil
                hidingInProgress = false
            }
        }
    }
}

private extension UIView {
    /// Cozy extension: insert view.fadeTransition right before changing content
    func fadeTransition(_ duration: CFTimeInterval) {
        let animation: CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        self.layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
}

private extension NSObject {
    /// Cozy extension
    func callSelectorAsync(_ selector: Selector, delay: TimeInterval) {
        let timer = Timer.scheduledTimer(timeInterval: delay, target: self, selector: selector, userInfo: nil, repeats: false)
        RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
    }
}

private extension UIScreen {
    class var CLAOrientation: UIInterfaceOrientation {
        get {
            return UIApplication.shared.statusBarOrientation
        }
    }
    class var CLAScreenWidth: CGFloat {
        get {
            if UIDevice.current.orientation.isPortrait {
                return UIScreen.main.bounds.size.width
            } else {
                return UIScreen.main.bounds.size.height
            }
        }
    }
    class var CLAScreenHeight: CGFloat {
        get {
            
            if UIDevice.current.orientation.isPortrait {
                return UIScreen.main.bounds.size.height
            } else {
                return UIScreen.main.bounds.size.height
            }
            
        }
    }
}

extension UIApplication {
    class func CLAtopMostController() -> UIViewController {
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        var topController = appDelegate.window!.rootViewController
        
        while topController?.presentedViewController != nil {
            topController = topController?.presentedViewController
        }
        return topController!
    }
}

