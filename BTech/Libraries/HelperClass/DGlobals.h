
#import <Foundation/Foundation.h>


@interface DGlobals : NSObject


/*
 **************************** Network ****************************
 */
+ (void)setNetwork:(BOOL)val;
+ (BOOL)networkAvailable;

/*
 **************************** Messages ****************************
 */

+ (BOOL) validateEmail: (NSString *) candidate;
+ (BOOL) validJsonResponse: (id) JSON;
+(void)saveUserName: (NSString *)user andPassword: (NSString *)password andCookie: (NSString *)cookie;
+(BOOL)isUserSignedIn;
+(void)logoutUser;
+(NSString *)currentUserID;
+(NSString *)emailAddress;
+(void)saveEmailAddress:(NSString *)email;
+(NSString *)firstName;
+(void)saveFirstName:(NSString *)firstName;
+(NSString *)lastName;
+(void)saveLast:(NSString *)lastName;
+(NSString *)password;
+(void)savePassword:(NSString *)password;

+(void)saveUserName: (NSString *)firstName andLastName : (NSString *)lastName andPassword: (NSString *)password andEmail: (NSString *)email andPhone:(NSString *)phone andUserID:(NSString *)userID;


@end
