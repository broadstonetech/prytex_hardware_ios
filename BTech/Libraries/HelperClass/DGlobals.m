
#import "DGlobals.h"

@implementation DGlobals

static BOOL networkFound = NO;


/*
 **************************** Network ****************************
 */
+(void)setNetwork:(BOOL)val {
    
    networkFound = val;
    //    if(!val){
    //
    //        [AppDelegateInstance checkNetworkConnection];
    //    }
    
}
+ (BOOL) networkAvailable {
    
    
    return networkFound;
    
}


/*
 **************************** Form Validation ****************************
 */

+ (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:candidate];
}

+ (BOOL)validJsonResponse: (id) JSON {
    @try {
        return [[JSON valueForKey:@"status"] isEqualToString:@"200"];
    }
    @catch (NSException *exception) {
        return NO;
        NSLog(@"Exception while check JSON response");
    }
    @finally {
        
    }
    
}



+(BOOL) IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


+(void)saveUserName: (NSString *)user andPassword: (NSString *)password andCookie: (NSString *)cookie{
    
    [[NSUserDefaults standardUserDefaults]setObject:user forKey:@"userName"];
    [[NSUserDefaults standardUserDefaults]setObject:password forKey:@"password"];
    [[NSUserDefaults standardUserDefaults]setObject:cookie forKey:@"cookie"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
}

+(BOOL)isUserSignedIn{
    NSLog(@"user ID : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userID"]);
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"userID"]){
        return YES;
    }else{
        return NO;
    }
}

+(void)logoutUser{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"firstName"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"password"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"userID"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"lastName"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"email"];
     [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"phone"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

+(NSString *)currentUserID{
    
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"userID"];
}


+(NSString *)emailAddress{
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
}
+(void)saveEmailAddress:(NSString *)email{
    [[NSUserDefaults standardUserDefaults]setObject:email forKey:@"email"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
+(NSString *)firstName{
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"firstName"];
}
+(void)saveFirstName:(NSString *)firstName{
    [[NSUserDefaults standardUserDefaults]setObject:firstName forKey:@"firstName"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
+(NSString *)lastName{
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"lastName"];
}
+(void)saveLast:(NSString *)lastName{
    [[NSUserDefaults standardUserDefaults]setObject:lastName forKey:@"lastName"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
+(NSString *)password{
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
}
+(void)savePassword:(NSString *)password{
    [[NSUserDefaults standardUserDefaults]setObject:password forKey:@"password"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

+(void)saveUserName: (NSString *)firstName andLastName : (NSString *)lastName andPassword: (NSString *)password andEmail: (NSString *)email andPhone:(NSString *)phone andUserID:(NSString *)userID{
    [[NSUserDefaults standardUserDefaults]setObject:password forKey:@"password"];
    [[NSUserDefaults standardUserDefaults]setObject:lastName forKey:@"lastName"];
    [[NSUserDefaults standardUserDefaults]setObject:firstName forKey:@"firstName"];
    [[NSUserDefaults standardUserDefaults]setObject:email forKey:@"email"];
    [[NSUserDefaults standardUserDefaults]setObject:userID forKey:@"userID"];
    [[NSUserDefaults standardUserDefaults]setObject:phone forKey:@"phone"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
}

@end

