//
//  MacAddressTextField.swift
//  BTech
//
//  Created by Ahmed Akhtar on 23/01/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class MacAddressTextField: UITextField {

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        //if(self.tag == 100)
        //{
        self.addTarget(self, action: #selector(MacAddressTextField.textFieldResetStyles), for: UIControl.Event.editingDidBegin)
        self.addTarget(self, action: #selector(MacAddressTextField.didEndEditing), for: UIControl.Event.editingDidEnd)
        self.addTarget(self, action: #selector(MacAddressTextField.formatMacAddress), for: UIControl.Event.editingChanged)
        //}
    }
    
    func textFieldIsValid() {
        self.textColor = UIColor.green
    }
    
    func textFieldIsInvalid() {
        self.textColor = UIColor.black
    }
    
    @objc func textFieldResetStyles() {
        self.textColor = UIColor.black
    }
    @objc
    func didEndEditing() {
        
        if(self.tag == 100 || self.tag == 200)
        {
            if isValid() {
                textFieldIsValid()
            } else {
                textFieldIsInvalid()
            }
        }
    }
    
    func isValid() -> Bool {
        
        //
        let macRegex = "([0-9A-Fa-f]{2}[:]){5}([0-9A-Fa-f]{2})"
        if let macTest = NSPredicate(format: "SELF MATCHES %@", macRegex) as NSPredicate? {
            return macTest.evaluate(with: self.text)
        }
        return false
    }
    @objc
    func formatMacAddress() {
        if(self.tag == 100 || self.tag == 200)
        {
            let stringMaxLength = 8
            var string = self.text!.replacingOccurrences(of: "[^0-9A-Za-z@#$&*]", with: "", options: .regularExpression)
            var formattedString = String()
        
            if string.count > stringMaxLength {
                string = string.substring(to: string.index(string.startIndex, offsetBy: stringMaxLength))
            }
        
            for char in string {
                if (formattedString.count + 1) % 5 == 0 {
//                    formattedString.append("-")
                }
                formattedString.append(char)
            }
        
            self.text = formattedString
        }
    }
}
