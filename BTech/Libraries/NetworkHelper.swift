//
//  NetworkHelper.swift
//  BTech
//
//  Created by Ahmad Waqas on 26/07/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import SystemConfiguration
//import Alamofire
//import ObjectMapper
//import AlamofireObjectMapper

//MARK: - Constants & Variables


//typealias successObject   = (Mappable) -> ()
//typealias successObjectsArray   = ([Mappable]) -> ()
typealias errorResponse = (NSError , String) -> ()
//typealias successJSONObject   = (JSON) -> ()

// App modes used for getting base urls
public enum AppMode: Int {
    case stagingMode = 0
    case devMode
    case productionMode
}

// MiddlePath are used for appending middle path string after base urls like base url + middle path
public enum MiddlePath: Int {
    case  mp_Default = 0
    case  mp_User
    case  mp_Listing
    
}

// Service Calls used for appending final string after base urls + middle Path + service call
public enum ServiceCall: Int {
    case  servercal_LOGIN = 0
    case  servercall_REGISTER
    case  servercall_FORGOTPWD
    case  servercall_LOGOUT
    case  servercall_GETDATA
    case  servercall_MODIFYPWD
    case  servercall_COMMAND
    case  servercall_GETALERTS
    case  servercall_REGISTERDEVICE
}

// Service Calls Parameters used for get Data
public enum GETDATA_PARAMETRS: Int {
    case  purpose_GET_HOST_LIST = 0
    case  purpose_GET_READING_SENSOR
    case  purpose_GET_ALERT_SENSOR
    case  purpose_GET_VULN_LIST
    case  purpose_GET_MUTED_ALERT
    case  purpose_GET_BLOCKED_ALERT
}

//Constants used for webservices Urls

struct ApiUrl {
    
    //STORE / WEB LINKS
    static let APPSTORE_DMOAT_URL = "itms://itunes.apple.com/de/app/d-moat/id1260963418?mt=8&uo=4"
    //dashboard screen
//    https://apps.apple.com/app/d-moat/id1260963418
    static let GET_ALL_DASHBOARD_ALERTS_API = "get/alerts/top"
    static let GET_CNCTDHOST_DASHBOARD_API = "get/cnctdhosts/top"
    static let GET_TRAFFIC_STATS_DASHBOARD_API =  "get/trafficstats/top"
    static let GET_SENSOR_DATA_DASHBOARD_API =  "get/sensordata/latest"
    //sensor screen
    static let GET_SENSOR_DATA_API = "get/sensordata"
    static let INIT_SENSOR_API = "initialize/sensor"
    //view all, blocked muted alerts
    static let GET_ALL_VIEW_ALERTS_API = "get/alerts"
    static let GET_BLOCKED_ALERTS_API = "get/blockedalerts"
    static let GET_MUTED_ALERTS_API = "get/mutedalerts"
    static let GET_PRE_MUTED_ALERTS_API = "get/premuted"
    static let RESPONSE_BLOCKED_ALERT_API = "response/blockedalert"
    static let RESPONSE_MUTED_ALERT_API = "response/mutedalert"
    //BANDWIDTH
    static let GET_TRAFFIC_STATS = "get/trafficstats"
    //Connected devices
    static let CHANGE_USERNAME_API = "response/username"
    static let CHANGE_OS_API = "response/os"
    static let GET_CNCTDHOST_API = "get/cnctdhosts"
    static let BLOCK_DEVICE_API = "response/datacnctdhost"
    //Vulnerabilities
    static let GET_VULNERABILITIES = "get/vulnerabilities"
    static let RESPONSE_BLOCKED_ALERTS = "response/blockedalert"
    
    //auto discovery/dmoat discovery
    static let BEACON_API = "http://hoth.dmoat.com:49124/beacon"
    static let ACK_UI_API = "ack/ui"
    static let AUTH_API = "auth"
    //verify code
    static let VERIFY_SETTING_CODE_API = "confirm/settings/code"
    static let VERIFY_PASSWORD_CODE_API = "confirm/password/code"
    //profile
    static let RESET_SETTING_API = "device/reset"//reset/settings"
    static let LED_CONFIG_API = "config/led"
    static let Get_Led_Mode = "demand/led"
    static let CLEAR_BASELINE = "clear/baseline"
    static let REBOOT_DMOAT = "demand/reboot"
    static let GET_NOTIFICATION_SETTING = "get/notification"
    static let SET_NOTIFICATION_SETTING = "set/notification"
    
    //ADVANCE MODE
    static let GET_NET_CONFIG = "get/netconfig"
    static let NET_CONFIG_API = "config/net"
    static let GET_CONFIG_MODE_API = "demand/net"
    //SUBSCRIBER
    static let REMOVE_SUBSCRIBER_API = "remove/subscriber"
    static let ADD_SUBSCRIBER = "add/subscriber"
    //ACCOUNT SETUO/SIGNUP
    static let ACCOUNT_SETUP_API = "accountsetup"
    //LOGIN
    static let USER_LOGIN_API = "userlogin"
    //SIGNOUT
    static let SIGN_OUT_API = "signout"
    //CHANGE PASSWORD
    static let SET_NEW_PASSWORD_API = "set/new/password"
    static let MODIFY_PASSWORD_API = "modify/password"
    //FORGOT USERNAME/PASSWORD
    static let FORGOT_USERNAME_API = "forgot/username"
    static let FORGOT_PASSWORD_API = "forgot/password"
    //VIEW ALERTS
    static let VIEW_ALERTS_RESPONSE_API = "response/"
    static let VIEW_ALERTS_SEEN_API = "alert/status"
    static let UPDATE_APP_ALERTS_ACK_API = "app/ack"
    static let UPDATE_FIRMWARE_ALERTS_ACK_API = "firmware/ack"
    //policy
    static let PAUSE_INTERNET_API = "post/policy"
    static let POLICY_DURATIONS_API = "usage/hours/cnctdhosts"
    static let GET_POLICIES_API = "get/policy"
    static let GET_FREQUENT_PROTOCOLS = "get/protocols"
    static let GET_PROTOCOL_POLICIES_API = "get/blockapp"
    static let CREATE_POLICIES_API = "post/policy"
    static let CREATE_PROTOCOL_POLICIES_API = "post/blockapp"
    static let POST_POLICy = "post/policy"
    static let PARENTAL_CONTROL_SITES = "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/porn/hosts"
    
    //about us
    
    static let OTA_VERSION_API = "test/scripts/version"
    static let SET_CHAT_ID = "set/chatid"
    
    // Social Url's for d.moat
    static let dmoatWebUrl = "https://www.dmoat.com/index.php"
    static let dmoatFacebookUrl = "https://www.facebook.com/Dmoat-Inc-1808973365833603/"
    static let dmoatTwitterUrl = "https://twitter.com/dmoat_inc"
    static let dmoatTInstagramUrl = "https://www.instagram.com/dmoat.inc/"
    
    static let video_account_setup_url = "https://s3.amazonaws.com/dmoat.public.files/Account+setup.mp4"
    static let video_connected_devices_url = "https://s3.amazonaws.com/dmoat.public.files/Connected+Devices.mp4"
     static let video_dashboard_url =  "https://s3.amazonaws.com/dmoat.public.files/Dashboard.mp4"
    static let video_full_url = "https://s3.amazonaws.com/dmoat.public.files/Full+Video+Tutorial.mp4"
    static let video_network_usage_url = "https://s3.amazonaws.com/dmoat.public.files/Network+Usage.mp4"
    static let video_policy_url = "https://s3.amazonaws.com/dmoat.public.files/Policy.mp4"
    static let video_profile_settings_url = "https://s3.amazonaws.com/dmoat.public.files/Profile+and+Settings.mp4"
    static let video_sensors_data_url = "https://s3.amazonaws.com/dmoat.public.files/Sensor+Data.mp4"
    static let video_dmoat_alerts_url = "https://s3.amazonaws.com/dmoat.public.files/d.moat+Alerts.mp4"
    
    static let dmoatQuickStartDocURL = "https://s3.amazonaws.com/dmoat.public.files/Quick+Start.jpg"
    static let dmoatFeatureGuideDocURL = "https://s3.amazonaws.com/dmoat.public.files/dmoat_help_content.pdf"
    static let dmoatPrivacyPolicyDocURL = "https://bstlegal.s3.amazonaws.com/dmoat/pdfs/d.moat_Privacy+Policy.pdf"
    static let dmoatTermsServicesDocURL = "https://bstlegal.s3.amazonaws.com/dmoat/pdfs/d.moat_Terms+of+Service.pdf"
    static let dmoatEULADocURL = "https://bstlegal.s3.amazonaws.com/dmoat/pdfs/dmoat_EULA.pdf"
    
    
    //chat module
    static let SAVE_CHAT = "save/chat"
    static let START_CHAT =  "start/chat"
    static let getUserMsgs = "get/user/msgs"
    static let pushUserMsgs = "push/user/msg"
}
//Constants used for webservice parameters strings

struct ConstantStrings {
    static var aapModeBaseURl = ""
   // static let chatURL = "https://dev.dmoat.com/"
    static let chatURL = "https://api.dmoat.com/"
    static let user_name = "username"
    static let fcm_device_token = "fcm_device_token"
    static let device_uuid = "device_uuid"
    static let email = "email"
    static let password = "password"
    static let uikey = "uikey"
    static let uikeyVAlue = "ui111"
    static let postkey = "postkey"
    static let device_id = "device_id"
    static let verify_code_userenamePwd = "A security code hwas sent to your registered email address.\nPlease check your email and spam folder and enter the code here.\nThis code is only valid for 15 minutes."
    
     static let verify_code_userenamePwd_a = "We have sent a temporary verification code to "
    static let verify_code_userenamePwd_b = ". Enter the code to verify. Code validity 15mins."
    static let verify_code_alreadySent_userenamePwd = "A security code was sent to your registered email address.\nPlease check your email and spam folder and enter the code here.\nThis code is only valid for 15 minutes."
    static let router_no_action = "You cannot block a router."
    static let pausedIntenettext = "Internet traffic was paused for 10 minutes. Internet traffic will resume after the 10 minutes."
    
    static let dmoatOffline = "Prytex is offline. Please restart Prytex and try again."
    static var productIdentifierScanner = "Product Identifier"
    static var noDataLoaded = "Data acquisition is in progress. Please make sure that your Prytex is powered on and connected to the network."
    static var discovery_incomplete_text = "Prytex discovery process was unsuccessful. Please re-run the discovery process again."
    static var blockOwnDevice = "You are about to block your own device.Are you sure?"
    static var blockDevice = "Do you really want to block device?"
    static var noCurrentbandwidthData = "Current bandwidth data was not processed. Please navigate to the 24 hour tab to view historical data."
    static var discovery_screen_text = "\u{2022} Your account was successfully registered.\n\u{2022} Click on 'Discover Prytex' to complete the setup.\n\u{2022} Please contact Prytex support via the Help button if you need any assistance. "
    static var startDiscoveryText = "\u{2022} Please ensure that Prytex is switched on and connected to the Internet.\n\u{2022} Also check that your mobile device and Prytex are connected to the same network/wifi.\n\u{2022} If the discovery process fails, please restart Prytex and try again."
    static var internet_off = "Prytex is unable to reach the Internet. Please check your network connection and try again."
    static var requestTimeOutMsgDiscovery = "Prytex discovery process timed out.\nPlease make sure your mobile device and Prytex are connected to the same network/wifi.\nRestart Prytex to begin the discovery process again."
    static var ThreeRequestDmoatFailedMsgDiscovery = "Prytex discovery process was not successful.\nPlease try again after one (1) hour.\nAlso make sure your mobile device and Prytex are connected to the same network/wifi.\nRestart Prytex to begin the discovery process again."
    static var serverOffTextDiscovery = "Prytex server is not reachable. Please try again later."
    static var succesDiscoveryMsgDiscovery = "Prytex discovery process was successful. Please login using your credentials. If you are unable to login, please repeat the Prytex discovery process by restarting Prytex after (1) hour."
    
    static var initialize_sensor_text = "Initialize the sensors if you observe incorrect sensors values or if you have moved Prytex to a different location.\nDo you want to initialize sensors?"
    
    static var terms_conditions_text = "Please accept terms of service to continue."
    
    //NetConfig guided alerts
    static var already_pnp_text = "You are already in Plug & Play mode."
    static var alert_guide_pnp = "Please make sure your local router's DHCP server is enabled."
    static var alert_guide_advance = "Please make sure your local router's DHCP server is disabled and reboot it."
    static var advance_done_guide = "Prytex is in advanced mode.\nPlease make sure your local router's DHCP server is disabled."
    
    
    //REsponse errors
    static var ErrorText = "Request failed. Please try again."
    
    //form validations
    static var fill_all_fields_text = "Please fill all required fields."
    static var username_empty_text = "Please enter username."
    static var password_empty_text = "Please enter password."
    static var invalid_username_email_text = "Please enter a valid username, email or password."
    static var password_validation_text = "Password should be at least eight (8) characters with a combination of special characters, upper and lower case characters [A-Z,a-z], and digits [0-9]."
    static var password_mismatch_text = "Entries in password and confirm password fields do  not  match. Please try again."
    static var qr_code_mismatch_text = "Product identifier should be 8 characters long. Please try again."
    static var qr_code_scanning_error = "Unable to scan QR code. Please enter Serial Number manually."

    static var no_connected_devices = "No connected devices were discovered. Please refresh your screen to process again."
    
    //screen info messages
    static var view_alerts_screen_txt = "Alerts and notifications generated by Prytex for various events."
    static var sensordata_screen_txt = "Sensor data values and graphs."
    static var blockedAlert_screen_txt = "Malicious threats blocked by Prytex are shown on this screen."
    static var mutedAlert_screen_txt = "Alerts shown here don’t require an immediate response."
    static var connectedDevices_screen_text = "Inventory of network connected devices discovered on the network."
    static var bandwidth_screen_txt = "Network traffic data usage by each connected device."
    static var bandwidthDetail_screen_txt = "Network traffic usage by protocol for each connected device."
    
    static var policy_screen_txt = "Policy and abnormal device behavioral reports. Create a custom access control policy to restrict network usage for any of your devices."
    static var protocol_policy_screen_txt = "Policy manager allows you to create a custom policy to block specific protocols on the entire network or on a specific device. You can specify up to 5 protocols and activate 5 policies simulataneously. The policy will remain active until you inactivate it."
    static var delete_protocol_policy_text = "You are about to remove an active policy. This will remove the restrictions enforced by this policy. Please confirm."
    static var create_protoclo_policy_text = "You are about to activate a new policy. This policy will block the specified protocols. Please confirm."
    static var access_control_text = "Policy and abnormal device behavioral reports."
    static var profile_setting_screen_txt = "User account management and settings."
    static var open_ports_screen_txt = "Open ports are TCP or UDP ports that can accept connections. These ports are used by connected devices and computer for data transfer. The open ports can be exploited by hackers to gain access to your network. You should consult the user͛s manual and close any ports that are not needed."
    static var default_password_screen_txt = "Often for user convenience, many devices and applications are assigned a default password that is easy to remember. It is always recommended to change the default password to something that only you would know. If you continue to use a default password, anyone with this information will be able to access your network, device, or applications. Please check your router and computer passwords and immediately change them if your current password matches the default provided here. You should consult the user manual for your router, device, or computer and follow the steps to change your password."
   static var no_open_ports_discoverd = "No open ports were discovered."
    static var no_default_pwds_discoverd = "No default passwords were discovered on your connected devices."
    
    static var no_sensorData_detected = "Sensor data collection is in progress. Please make sure your Prytex is turned on and connected to the Internet."
    static var ERROR_TEXT_503 = "Error code 503 received from server. Please report this error if you contact support."
    
    //Access Control messages
    static var policy_title_missing = "Policy title was missing."
    static var policy_day_missing = "No day was selected."
    static var policy_time_missing = "No time interval was set."
    static var policy_time_validation = "Policy end time should greater than start time."
    static var policy_hostsList_missing = "No host selected."
    
    static var policy_created_text = "Policy request forwarded to Prytex."

    //app / firmware update
}

class NetworkHelper: NSObject {
    
    fileprivate let kDevbaseURL = "devmode/"
    fileprivate let kProdbaseURL = "prodmode/"
    fileprivate let kStagingURL = "stagingmode/"
    fileprivate let kDomainURL = "http://192.241.190.107:5000/"
    
    var BASE_URL : String = ""
    static let sharedInstance = NetworkHelper()
    
    var isConnectedDevicesSuccess = false
    var isDmoatOnline:Bool = true;
    var isPausedInternet:Bool = true
    var isShowLoggedDeviceDialog = false;
    var isNotificationLocked = false
    var isNetworkAvailable:Bool  = false
    var viewAlertsPushedInNavigationStack = true
    var arrayBlockedAlerts = [AlertModel]()
    var arrayPreMutedAlerts = [PreMutedAlertModel]()
    var arrayLoggedInDevices = [LoggedDevicesModel]()
    var arrayVulnerabilityData = [AlertModel]()
    var arrayConnectedDevices = [AlertModel]()
    var arrayViewAlerts = [AlertModel]()
    var arrayVulProducts = [String]()
    var arrayVulReferences = [String]()
    var arrayVulImpact = [String]()
    
    //tab bar selection bool
    var isHomeTabSelected:Bool = true
    var isPolicyTabSelected:Bool = false
    var isAccountTabSelected:Bool = false
    var isIntercomTabSelected:Bool = false
    
    //pause internet -- mode operations
    var savedPausedDate = Date()
    var currentPausedDateTime = Date()
    var isAdvanceModeActicated:Bool  = false
    //alerts lists new UI
    var viewAlertsList = [BlockedMutedAlertsModel]()
    var blockedAlertsList = [BlockedMutedAlertsModel]()
    var MutedAlertsList = [BlockedMutedAlertsModel]()
    var preMutedAlertsList = [PreMutedAlertModel]()
    var preMutedFlowList = [PreMutedAlertModel]()
    
    var protocolsValuesDictForBlocking = [String: String]()

    
    var app_ID : String? = UIDevice.current.identifierForVendor?.uuidString
    var arraySensorData = [SensorModel]()
    
    var arrayTempReadings = [Double]()
    var arrayHumidityReadings = [Double]()
    var arrayAirPressureReadings = [Double]()
    var arrayTimeReadings = [String]()
    
    var arrayChannelListViewAlert = [String]()
    var arraySubscribers = [String]()
    
    var deviceToken : Data? = nil
    var FCM_DEVICE_TOKEN : String? = ""
    var uuidToken : String? = ""
    
    var tokenSavedForSettingNewPassword : String? = nil
    var usernameSavedForVerifyCode : String? = nil
    var name : String? = nil
    var email : String? = nil
    var username : String? = nil
    var password : String? = nil
    var token : String? = nil
    
    var muteSwitchState : String? = nil
    var promptSwitchState : String? = nil
    var promptUserAlert : String? = nil
    var pushNotification : Bool = false
    var appSettingsTemperatureStatus : String? = nil
    var appSettingsMode : String? = nil
    var appSettingsLEDManagement : String? = nil
    var daysSelected : String? = nil
    
    var advanceModeModel : AdvanceModeModel? = nil
    
    var error401 = false
    var error403 = false
    
    var mondayChecked = false
    var tuesdayChecked = false
    var wednesdayChecked = false
    var thursdayChecked = false
    var fridayChecked = false
    var saturdayChecked = false
    var sundayChecked = false
    
    var connectedHostList = false
    var protocolsMainArray = [ProtocolModel]()
    
    
    
    //MARK: - Configuratrions
    
    
    func currentAppMode() ->  AppMode {
        return .stagingMode
    }
    
    func baseURL() -> String {
        switch self.currentAppMode() {
        case .devMode:
            return  kDomainURL + kDevbaseURL
        case .productionMode:
            return kDomainURL + kProdbaseURL
        case .stagingMode:
            return kDomainURL //+ kProdbaseURL
        }
    }
    
    //MARK: - Webservice contents to be saved + Parameters Scheme +  Project Specific
    
    func deviceId() -> String {
        let uniqueId=UIDevice.current.identifierForVendor!.uuidString as String
        print("Your device identifire =>\(uniqueId)")
        return "00:19:d1:f6:d0:7c"
    }
    
    func getParametersForGetDataAPIWithType(_ purpose:GETDATA_PARAMETRS) -> String {
        
        switch purpose {
        case .purpose_GET_HOST_LIST:
            return ""
        case .purpose_GET_VULN_LIST:
            return ""
        case .purpose_GET_MUTED_ALERT:
            return ""
        case .purpose_GET_ALERT_SENSOR:
            return ""
        case .purpose_GET_BLOCKED_ALERT:
            return ""
        case .purpose_GET_READING_SENSOR:
            return ""
            
        }
        
    }
    func saveFirebaseid(_ firebaseId:String)  {
        
        UserDefaults.standard.set(firebaseId, forKey: "firebaseId")
        UserDefaults.standard.synchronize()
        
    }
    
    //GET INTERNET CONNECTIVITY CHECK
    
  
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    //save current user app settings in nsuserdefault
    
    func saveCurrentUserNameForAppSettings(_ userName:String)  {
        
        UserDefaults.standard.set(userName, forKey: "userNameForProfile")
        UserDefaults.standard.synchronize()
        
    }
    
    func saveCurrentSelectedTheme(_ theme:String)  {
           
           UserDefaults.standard.set(theme, forKey: "themeSelected")
           UserDefaults.standard.synchronize()
           
       }
    func saveCurrentDisplayMode(_ displayMode : Bool) {
        UserDefaults.standard.set(displayMode, forKey: "DisplayMode")
        UserDefaults.standard.synchronize()
    }
    func saveIfParentalControlOn(_ parentalControl: Bool) {
        UserDefaults.standard.set(parentalControl, forKey: "ParentalControl")
        UserDefaults.standard.synchronize()
    }
    func getIfParentalControlOn() -> Bool {
        let control = (UserDefaults.standard.value(forKey: "ParentalControl") as? Bool) ?? false
        print(control)
        return control
    }
    
    func getCurrentDisplayMode() -> Bool {
        var mode = (UserDefaults.standard.value(forKey: "DisplayMode") as? Bool) ?? false
        return mode
    }
    
    func getCurrentSelectedTheme() -> String  {
              if UserDefaults.standard.object(forKey: "themeSelected") != nil {
              var themeSelected = UserDefaults.standard.value(forKey: "themeSelected") as! String?
                    if(themeSelected == nil)
                    {
                        themeSelected = "1"
                    }
                    return themeSelected!
              }
        return "1"
             
         }
    
    func saveCurrentUserPromptSwitchState(_ promptSwitch:String)  {
        
        UserDefaults.standard.set(promptSwitch, forKey: "promptSwitch")
        UserDefaults.standard.synchronize()
        
    }
    
    func saveCurrentUserMuteAllState(_ muteAll:String)  {
        
        UserDefaults.standard.set(muteAll, forKey: "muteAll")
        UserDefaults.standard.synchronize()
        
    }
    
    func saveCurrentUserTemperatureStatus(_ tempStatus:String)  {
        
        UserDefaults.standard.set(tempStatus, forKey: "temperatureStatus")
        UserDefaults.standard.synchronize()
        
    }
    
    func saveCurrentUserMode(_ mode:String)  {
        
        UserDefaults.standard.set(mode, forKey: "mode")
        UserDefaults.standard.synchronize()
        
    }
    
    func saveCurrentUserLEDManagement(_ led:String)  {
        
        UserDefaults.standard.set(led, forKey: "led")
        UserDefaults.standard.synchronize()
        
    }
    
//    func saveCurrentAppID(_ appID:String)  {
//
//        UserDefaults.standard.set(appID, forKey: "appID")
//        UserDefaults.standard.synchronize()
//
//    }
    
    func saveCurrentUserName(_ userName:String)  {
        
        UserDefaults.standard.set(userName, forKey: "userName")
        UserDefaults.standard.synchronize()
        
    }
    func saveCurrentUserPassword(_ password:String)  {
        
        UserDefaults.standard.set(password, forKey: "password")
        UserDefaults.standard.synchronize()
        
    }
    func saveCurrentUserEmail(_ email:String)  {
        
        UserDefaults.standard.set(email, forKey: "email")
        UserDefaults.standard.synchronize()
        
    }
    func saveCurrentUserPostKey(_ postKey:String)  {
        
        UserDefaults.standard.set(postKey, forKey: "postkey")
        UserDefaults.standard.synchronize()
        
    }
    
    func saveCurrentToken(_ token:String)  {
        
        if(!token.isEmpty)
        {
            UserDefaults.standard.set(token, forKey: "token")
            UserDefaults.standard.synchronize()
        }
        
    }
    
    func saveCurrentName(_ name:String)  {
        
        UserDefaults.standard.set(name, forKey: "name")
        UserDefaults.standard.synchronize()
        
    }
    
    func saveSubscribers(_ array:[String])  {
        
        UserDefaults.standard.set(array, forKey: "subscribers")
        UserDefaults.standard.synchronize()
        
    }
    
    
    func saveCurrentUserNamespace(_ namespace:String)  {
        
        UserDefaults.standard.set(namespace, forKey: "namespace")
        UserDefaults.standard.synchronize()
        
    }
    
    func saveAppMode(_ isDev:Bool)  {
        
        UserDefaults.standard.set(isDev, forKey: "appMode")
        UserDefaults.standard.synchronize()
        
    }
    
    
    func getAppMode() -> Bool  {
     
        if UserDefaults.standard.object(forKey: "appMode") != nil {
            let isDev = UserDefaults.standard.value(forKey: "appMode") as! Bool ?? false
            return isDev
        }
        return false
       
    }
    
    //get current user app settings
    func getCurrentFirebaseId() -> String {
        var firebaseId = UserDefaults.standard.value(forKey: "firebaseId") as! String?
        if(firebaseId == nil)
        {
            firebaseId = ""
        }
        return firebaseId!
    }
    func getCurrentUserNameForProfile() -> String {
        var userName = UserDefaults.standard.value(forKey: "userNameForProfile") as! String?
        if(userName == nil)
        {
            userName = ""
        }
        return userName!
    }
    
    func getCurrentUserPromptSwitchState() -> String {
        var promptSwitchState = UserDefaults.standard.value(forKey: "promptSwitch") as! String?
        if(promptSwitchState == nil)
        {
            promptSwitchState = ""
        }
        return promptSwitchState!
    }
    
    func getCurrentUserMuteAllState() -> String {
        var muteAll = UserDefaults.standard.value(forKey: "muteAll") as! String?
        if(muteAll == nil)
        {
            muteAll = ""
        }
        return muteAll!
    }
    
    func getCurrentUserTemperatureStatus() -> String {
        var temperatureStatus = UserDefaults.standard.value(forKey: "temperatureStatus") as! String?
        if(temperatureStatus == nil)
        {
            temperatureStatus = ""
        }
        return temperatureStatus!
    }
    
    func getCurrentUserMode() -> String {
        var mode = UserDefaults.standard.value(forKey: "mode") as! String?
        if(mode == nil)
        {
            mode = ""
        }
        return mode!
    }
    
    func getCurrentUserLEDMangement() -> String {
        var led = UserDefaults.standard.value(forKey: "led") as! String?
        if(led == nil)
        {
            led = ""
        }
        return led!
    }
    
    func getCurrentAppID() -> String {
        var appID = UIDevice.current.identifierForVendor?.uuidString
        if(appID == nil || appID == "")
        {
            appID = ""
        }
        return appID!
    }
    
    func getCurrentUserName() -> String {
        let userName = UserDefaults.standard.value(forKey: "userName") as! String
        return userName
    }
    func getName() -> String {
        let userName = UserDefaults.standard.value(forKey: "name") as! String
        return userName
    }
    func getCurrentToken() -> String {
        var token = UserDefaults.standard.value(forKey: "token") as! String?
        if(token == nil)
        {
            token = ""
        }
        return token!
    }
    func getCurrentUserEmail() -> String {
        let email = UserDefaults.standard.value(forKey: "email") as! String
        return email
    }
    func getCurrentUserPostKey() -> String {
        var postKey = UserDefaults.standard.value(forKey: "postkey") as! String?
        if(postKey == nil)
        {
            postKey = ""
        }
        return postKey!
    }
    func getCurrentUserPassword() -> String {
        let password = UserDefaults.standard.value(forKey: "password") as! String
        return password
    }
    
    func getCurrentUserNamespace() -> String {
        var namespace = UserDefaults.standard.value(forKey: "namespace") as! String?
        if(namespace == nil)
        {
            namespace = ""
        }
        return namespace!
    }
    
    func getCurrentSubscribers() -> [String] {
        let array = UserDefaults.standard.object(forKey: "subscribers") as! [String]
        return array
    }
    
    
    
    
    func clearUserData()  {
        UserDefaults.standard.removeObject(forKey: "userName")
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "postKey")
        UserDefaults.standard.removeObject(forKey: "password")
        UserDefaults.standard.removeObject(forKey: "namespace")
        UserDefaults.standard.removeObject(forKey: "subscribers")
        UserDefaults.standard.removeObject(forKey: "token")
        UserDefaults.standard.synchronize()
    }
    
    
    func clearUserAppSettings()  {
        UserDefaults.standard.removeObject(forKey: "userNameForProfile")
        UserDefaults.standard.removeObject(forKey: "promptSwitch")
        UserDefaults.standard.removeObject(forKey: "muteAll")
        UserDefaults.standard.removeObject(forKey: "temperatureStatus")
        UserDefaults.standard.removeObject(forKey: "mode")
        UserDefaults.standard.removeObject(forKey: "led")
        UserDefaults.standard.synchronize()
    }
    // check app role -- dev / production
    func checkAppRole() {
        if NetworkHelper.sharedInstance.getAppMode() {
            print("dev mode selected")
            ConstantStrings.aapModeBaseURl = "https://dev.dmoat.com/" //dev
        }else{
            print("production mode selected")
            ConstantStrings.aapModeBaseURl = "https://api.dmoat.com/" //prod
        }
    }
    //get 8 chars alphanumeric id for polciy creation
    func getRandomKeyString() -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< 8 {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString //mMhHg9kM
    }
    
    func getRandomUUIDString() -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< 32 {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString //mMhHg9kM
    }
    
    // Return IP address of WiFi interface (en0) as a String, or `nil`
    func getIP()-> String? {
        
//// Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        var address = ""

        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee

            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {

                // Check interface name:
                let name = String(cString: interface.ifa_name)
                if  name == "en0" {

                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)

        return address
    }
    
    //MARK:- URL Schemes
    
    func serviceCallName(_ name:ServiceCall) -> String {
        
        switch name {
        case .servercal_LOGIN:
            return self.appendMiddlePath(.mp_User, afterMiddlePath: "userlogin")
        case .servercall_REGISTER:
            return self.appendMiddlePath(.mp_User, afterMiddlePath: "accountsetup")
        case .servercall_FORGOTPWD:
            return self.appendMiddlePath(.mp_Listing, afterMiddlePath: "forgotpassword")
        case .servercall_LOGOUT:
            return self.appendMiddlePath(.mp_Default, afterMiddlePath: "logout")
        case .servercall_GETDATA:
            return self.appendMiddlePath(.mp_Default, afterMiddlePath: "getdata")
        case .servercall_MODIFYPWD:
            return self.appendMiddlePath(.mp_Default, afterMiddlePath: "modifypassword")
        case .servercall_GETALERTS:
            return self.appendMiddlePath(.mp_Default, afterMiddlePath: "getalerts")
        case .servercall_COMMAND:
            return self.appendMiddlePath(.mp_Default, afterMiddlePath: "commands")
        case .servercall_REGISTERDEVICE:
            return self.appendMiddlePath(.mp_Default, afterMiddlePath: "initialization")
            
            
        }
        
    }
    
    func appendMiddlePath(_ mp:MiddlePath,afterMiddlePath:String) -> String {
        
        return afterMiddlePath
        
        switch mp {
        case .mp_Default:
            return ("default/" + afterMiddlePath)
        case .mp_User:
            return ("user/" + afterMiddlePath)
            
        case .mp_Listing:
            return ("listing/" + afterMiddlePath)
            
        }
        
    }
    
    
    
    
    
    //MARK:- Webservice Calling Functions
    
    func postRequest(serviceName: String,sendData : [String : AnyObject],success:@escaping ( _ data : [String : AnyObject])->Void, failure:@escaping ( _ data:[String : AnyObject])->Void) {
        print("url == \(serviceName)")
        print("params == \(sendData)")
        
        //        if(NetworkHelper.sharedInstance.isDmoatOnline){
        
        CozyLoadingActivity.show("Please Wait..", disableUI: true)
        
        let serviceUrl = ConstantStrings.aapModeBaseURl + serviceName
        print("url == \(serviceUrl)")
        print("url-1 == \(ConstantStrings.aapModeBaseURl)")
        let url = NSURL(string: serviceUrl)
        
        //create the session object
        let session = URLSession.shared
        
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST" //set http method as POST
//        request.timeoutInterval = 20
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: sendData, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            print(" response is \(response)")
            
            //copied
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                UserDefaults.standard.set(httpStatus.statusCode, forKey: "statusCode")
                UserDefaults.standard.synchronize()
                
                if(httpStatus.statusCode == 401)
                {
                    NetworkHelper.sharedInstance.error401 = true
                }
                else if(httpStatus.statusCode == 403)
                {
                    NetworkHelper.sharedInstance.error403 = true
                }
                
            }
            let httpStats = response as? HTTPURLResponse
            UserDefaults.standard.set(httpStats?.statusCode, forKey: "statusCode")
            UserDefaults.standard.synchronize()
            //end copied
            
            guard error == nil else {
                print("this is error")
                DispatchQueue.main.async {
                    CozyLoadingActivity.hide()
                    self.showFailureAlert(ConstantStrings.internet_off)
                }
                return
            }
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                    print("json is \(json)")
                    NetworkHelper.sharedInstance.isConnectedDevicesSuccess = true
                    success(json)
                    //handle json...
                    CozyLoadingActivity.hide()
                }
                
                CozyLoadingActivity.hide(true, animated: false)
            } catch let error {
                CozyLoadingActivity.hide()
                print(error.localizedDescription)
                var errors = [String : AnyObject]()
                errors["Error"] = "Server Error" as AnyObject
                failure(errors)
            }
            
        })
        
        task.resume()
        
    }
    
    //MARK: - PostRequest for chat module
    func postChatRequest(serviceName: String,sendData : [String : AnyObject],success:@escaping ( _ data : [String : AnyObject])->Void, failure:@escaping ( _ data:[String : AnyObject])->Void) {

        
        //        if(NetworkHelper.sharedInstance.isDmoatOnline){
        
//        CozyLoadingActivity.show("Please Wait..", disableUI: true)
        
        let serviceUrl = ConstantStrings.chatURL + serviceName
        print("url == \(serviceUrl)")
        print("url-1 == \(ConstantStrings.aapModeBaseURl)")
        let url = NSURL(string: serviceUrl)
        
        //create the session object
        let session = URLSession.shared
        
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST" //set http method as POST
//        request.timeoutInterval = 20
        
        do {
            
            let data = try JSONSerialization.data(withJSONObject: sendData ,options: .prettyPrinted)
            let dataString = String(data: data, encoding: .utf8)!
            print("data string is",dataString)
            
            request.httpBody = try JSONSerialization.data(withJSONObject: sendData, options: .prettyPrinted)
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            print(" response is \(response)")
            
            //copied
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                UserDefaults.standard.set(httpStatus.statusCode, forKey: "statusCode")
                UserDefaults.standard.synchronize()
                
                if(httpStatus.statusCode == 401)
                {
                    NetworkHelper.sharedInstance.error401 = true
                }
                else if(httpStatus.statusCode == 403)
                {
                    NetworkHelper.sharedInstance.error403 = true
                }
                
            }
            let httpStats = response as? HTTPURLResponse
            UserDefaults.standard.set(httpStats?.statusCode, forKey: "statusCode")
            UserDefaults.standard.synchronize()
            //end copied
            
            guard error == nil else {
                print("this is error")
                DispatchQueue.main.async {
//                    CozyLoadingActivity.hide()
                    self.showFailureAlert(ConstantStrings.internet_off)
                }
                return
            }
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                    print("json is \(json)")
                    NetworkHelper.sharedInstance.isConnectedDevicesSuccess = true
                    success(json)
                    //handle json...
//                    CozyLoadingActivity.hide()
                }
                
//                CozyLoadingActivity.hide(true, animated: false)
            } catch let error {
//                CozyLoadingActivity.hide()
                print(error.localizedDescription)
                var errors = [String : AnyObject]()
                errors["Error"] = "Server Error" as AnyObject
                failure(errors)
            }
            
        })
        
        task.resume()
        
    }
    
    
    //MARK: - PostRequest for dashboard
    //without activity indicator
    func postDashboardRequest(serviceName: String,sendData : [String : AnyObject],success:@escaping ( _ data : [String : AnyObject])->Void, failure:@escaping ( _ data:[String : AnyObject])->Void){
        print("dashboard_url == \(serviceName)")
        print("dashboar_params == \(sendData)")
        
        //        if(NetworkHelper.sharedInstance.isDmoatOnline){
    
        let serviceUrl = ConstantStrings.aapModeBaseURl + serviceName
        let url = NSURL(string: serviceUrl)
        
        //create the session object
        let session = URLSession.shared

        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: sendData, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            print(" response is \(response)")
            
            //copied
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                UserDefaults.standard.set(httpStatus.statusCode, forKey: "statusCode")
                UserDefaults.standard.synchronize()
                
                if(httpStatus.statusCode == 401)
                {
                    NetworkHelper.sharedInstance.error401 = true
                }
                else if(httpStatus.statusCode == 403)
                {
                    NetworkHelper.sharedInstance.error403 = true
                }
                
                
            }
            let httpStats = response as? HTTPURLResponse
            UserDefaults.standard.set(httpStats?.statusCode, forKey: "statusCode")
            UserDefaults.standard.synchronize()
            //end copied
            
            guard error == nil else {
                print("this is error")
                //                CozyLoadingActivity.hide()
                //                self.showFailureAlert(ConstantStrings.ErrorText)
                return
            }
            guard let data = data else {
                return
            }
            
            do {
                
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                    print("json is \(json)")
                    NetworkHelper.sharedInstance.isConnectedDevicesSuccess = true
                    success(json)
                    //handle json...
                    //                    CozyLoadingActivity.hide()
                }
                //                CozyLoadingActivity.hide(true, animated: false)
            } catch let error {
                //                CozyLoadingActivity.hide()
                print(error.localizedDescription)
                var errors = [String : AnyObject]()
                errors["Error"] = "Server Error" as AnyObject
                failure(errors)
            }
            
        })
        
        task.resume()
        
        //        }
        //        else{
        //            let alert = UIAlertController(title: "d.moat", message: ConstantStrings.dmoatOffline, preferredStyle: UIAlertControllerStyle.alert)
        //            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        //            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        //        }
        
    }
    
    //MARK: -Discovery Mode Webservice
    func postDiscoveryRequest(serviceName: String,sendData : [String : AnyObject],success:@escaping ( _ data : [String : AnyObject])->Void, failure:@escaping ( _ data:[String : AnyObject])->Void){
        print("url == \(serviceName)")
        print("params == \(sendData)")
        
        //create the url with NSURL
        let url = NSURL(string: serviceName)
        
        //create the session object
        let session = URLSession.shared
        
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: sendData, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            //            print(" response is \(response)")
            
            //copied
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                UserDefaults.standard.set(httpStatus.statusCode, forKey: "statusCode")
                UserDefaults.standard.synchronize()
                
                if(httpStatus.statusCode == 401)
                {
                    NetworkHelper.sharedInstance.error401 = true
                }
                else if(httpStatus.statusCode == 403)
                {
                    NetworkHelper.sharedInstance.error403 = true
                }
                            }
            let httpStats = response as? HTTPURLResponse
            UserDefaults.standard.set(httpStats?.statusCode, forKey: "statusCode")
            UserDefaults.standard.synchronize()
            //end copied
            
            guard error == nil else {
                print("this is error")
                return
            }
            guard let data = data else {
                return
            }
            
            do {
                
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                    //                    print("json is \(json)")
                    NetworkHelper.sharedInstance.isConnectedDevicesSuccess = true
                    success(json)
                    //handle json...
                }
            } catch let error {
                print(error.localizedDescription)
                var errors = [String : AnyObject]()
                errors["Error"] = "Server Error" as AnyObject
                failure(errors)
            }
            
        })
        
        task.resume()
        
        
    }
    
    func showFailureAlert(_ message:String)
    {
        let alert = UIAlertController(title: "Prytex", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
    
    
    func BlockAppProtocolsKeyValues(){
        protocolsValuesDictForBlocking = [
            "afp": "AFP",
            "aimini": "Aimini",
            "amazon": "Amazon",
            "apple": "Apple",
            "appleicloud": "iCloud",
            "appleitunes": "iTunes",
            "applejuice": "Apple juice",
            "armagetron": "Armagetron",
            "avi": "Avi",
            "ayiya": "AYIYA",
            "bgp": "BGP",
            "bittorrent": "BitTorrent",
            "ciscoskinny": "SCCP",
            "Ciscovpn": "Cisco vpn",
            "citrix": "Citrix",
            "citrix_online": "Citrix Online",
            "cnn": "CNN",
            "coap": "CoAP",
            "collectd": "collectd",
            "corba": "CORBA",
            "crossfire": "CrossFire",
            "dce_rpc": "DCE/RPC",
            "dhcp": "DHCP",
            "dhcpv6": "DHCPv6",
            "direct_download_link": "DDL",
            "directconnect": "DC",
            "dns": "DNS",
            "drda": "DRDA",
            "dropbox": "Dropbox",
            "eaq": "EAQ",
            "edonkey": "eDonkey",
            "egp": "EGP",
            "epp": "EPP",
            "facebook": "Facebook",
            "fasttrack": "",
            "filetopia": "Filetopia",
            "flash": "Flash Video",
            "florensia": "Florensia",
            "ftp_control": "FTP Control ",
            "ftp_data": "FTP Data",
            "git": "Git",
            "gmail": "Gmail",
            "gnutella": "Gnutella",
            "google": "Google",
            "googlehangout": "Google Hangouts",
            "googlemaps": "Google Maps",
            "gre": "GRE",
            "gtp": "GTP",
            "halflife2": "Half-Life 2",
            "hep": "Hep",
            "hotmail": "Hotmail",
            "hotspotshield": "Hotspot Shield",
            "https": "HTTPS",
            "http": "HTTP",
            "http_connect": "HTTP Connect",
            "httpdownload": "HTTP Download",
            "iax": "IAX ",
            "icecast": "Icecast",
            "icmpv6": "ICMPv6",
            "igmp": "IGMP",
            "imap": "IMAP",
            "imaps": "IMAP",
            "imesh": "iMesh",
            "instagram": "Instagram",
            "ip_in_ip": "IP in IP",
            "ipsec": "IPsec",
            "kerberos": "Kerberos",
            "kontiki": "Kontiki ",
            "lastfm": "Lastfm",
            "ldap": "LDAP",
            "llmnr": "LLMNR",
            "lotusnotes": "Lotus Notes ",
            "lync": "Lync",
            "maplestory": "MapleStory",
            "mdns": "mDNS",
            "megaco": "Megaco",
            "mgcp": "MGCP ",
            "microsoft": "Microsoft",
            "mms": "MMS",
            "move": "",
            "mpeg": "MPEG transport stream",
            "mpeg_ts": "MPEG transport stream",
            "mqtt": "MQTT",
            "ms_one_drive": "One drive",
            "msn": "MSN",
            "mssql-tds": "TDS",
            "mysql": "MySQL",
            "netbios": "NBNS",
            "netflix": "Netflix",
            "netflow": "NetFlow",
            "nfs": "NFS",
            "noe": "Noe",
            "ntp": "NTP",
            "ocs": "OCS",
            "office365": "Office 365",
            "oggvorbis": "Ogg Vorbis ",
            "opendns": "OpenDNS",
            "openft": "OpenFT",
            "openvpn": "Openvpn",
            "oracle": "Oracle",
            "oscar": "OSCAR",
            "ospf": "OSPF",
            "pando_media_booster": "PMB",
            "pandora": "Pandora",
            "pcanywhere": "Pc Anywhere",
            "pop3": "POP3",
            "pops": "POP",
            "postgresql": "Postgres",
            "pplive": "PPLIVE",
            "ppstream": "PPStream",
            "pptp": "PPTP",
            "qq": "QQ",
            "qqlive": "QQLive",
            "quic": "QUIC",
            "quicktime": "QuickTime",
            "radius": "RADIUS",
            "rdp": "RDP",
            "realmedia": "RealMedia",
            "redis": "Redis",
            "rsync": "Rsync",
            "rtcp": "RTCP",
            "rtmp": "RTMP",
            "rtp": "RTP",
            "rtsp": "RTSP",
            "rx": "Rx",
            "sap": "SAP",
            "sctp": "SCTP",
            "sflow": "sFlow",
            "shoutcast": "SHOUTcast",
            "sina(weibo)": "Sina Weibo",
            "skyfile_postpaid": "SkyFile Mail",
            "skyfile_prepaid": "SkyFile Mail",
            "skyfile_rudics": "SkyFile Mail",
            "skype": "Skype",
            "slack": "Slack",
            "smb": "SMB",
            "smtp": "SMTP",
            "smtps": "SMTPS",
            "snapchat": "Snapchat",
            "snmp": "SNMP",
            "socks": "Socket Secure",
            "socrates": "SOCRATES",
            "sopcast": "Sopcast",
            "soulseek": "Soulseek",
            "spotify": "Spotify",
            "ssdp": "SSDP",
            "ssh": "SSH",
            "ssl": "SSL",
            "stealthnet": "Stealthnet",
            "steam": "Steam",
            "stun": "STUN",
            "syslog": "Syslog",
            "teamspeak": "TeamSpeak",
            "teamviewer": "TeamViewer",
            "telegram": "Telegram",
            "telnet": "Telnet",
            "teredo": "Teredo",
            "tftp": "TFTP",
            "thunder": "Thunder",
            "tor": "Tor",
            "truphone": "Truphone",
            "tuenti": "Tuenti",
            "tvants": "TVants",
            "tvuplayer": "TVUPlayer",
            "twitch": "Twitch",
            "twitter": "Twitter",
            "ubntac2": "Ubntac2",
            "ubuntuone": "Ubuntuone",
            "unencryped_jabber": "Jabber",
            "unknown": "Unknown",
            "upnp": "UPnP",
            "usenet": "Usenet",
            "vevo": "Vevo",
            "vhua": "Vhua",
            "viber": "Viber",
            "vmware": "VMWare",
            "vnc": "VNC",
            "vrrp": "VRRP",
            "warcraft3": "Warcraft3",
            "waze": "Waze",
            "webex": "Webex",
            "webm": "Webm",
            "whatsapp": "Whatsapp",
            "whatsappvoice": "Whatsapp Voice",
            "whois-das": "Whois",
            "wikipedia": "Wikipedia",
            "windowsmedia": "Windows Media ",
            "windowsupdate": "Windows Update",
            "worldofwarcraft": "World Of War Craft",
            "xbox": "Xbox",
            "xdmcp": "XDMCP",
            "yahoo": "Yahoo",
            "youtube": "Youtube",
            "zattoo": "Zattoo",
            "zeromq": "zeromq",
            
        ]
    }
    
}





