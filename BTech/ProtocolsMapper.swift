//
//  ProtocolsMapper.swift
//  BTech
//
//  Created by Ahmed Akhtar on 30/03/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class ProtocolsMapper: NSObject {



    func mapProtocolsWithArray(_ arrayProtocols : [String]) -> [String]
    {
        var arrayProtocols = arrayProtocols
        for value in arrayProtocols
        {
            
            if(value == "ftp")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "FTP Control"
            }
            if(value == "ipp")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "IPP"
            }
            if(value == "hotmail")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Hotmail"
            }
            if(value == "xbox")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Xbox"
            }
            if(value == "http_download")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "HTTP Download"
            }
            if(value == "ssl_no_cert")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "SSl NO CERT"
            }
            if(value == "irc")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "IRC"
            }
            if(value == "unencryped_jabber")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Jabber"
            }
            if(value == "msn")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "MSN"
            }
            if(value == "yahoo")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Yahoo"
            }
            if(value == "battlefield")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Battlefield"
            }
            if(value == "quake")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Quake"
            }
            if(value == "worldofwarcraft")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "World Of War Craft"
            }
            if(value == "telnet")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Telnet"
            }
            if(value == "icmp")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "ICMP"
            }
            if(value == "rdp")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "RDP"
            }
            if(value == "vnc")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "VNC"
            }
            if(value == "pcanywhere")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Pc Anywhere"
            }
            if(value == "ssl")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "SSL"
            }
            if(value == "ssh")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "SSH"
            }
            if(value == "sip")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "SIP"
            }
            if(value == "dofus")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Dofus"
            }
            if(value == "fiesta")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Fiesta"
            }
            if(value == "guildwars")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Guildwars"
            }
            if(value == "http_application_activesync")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "HTTP Application Active Sync"
            }
            if(value == "warcraft3")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Warcraft3"
            }
            if(value == "world_of_kung_fu")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "World Of Kung Fu"
            }
            if(value == "slack")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Slack"
            }
            if(value == "facebook")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Facebook"
            }
            if(value == "twitter")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Twitter"
            }
            if(value == "dropbox")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Dropbox"
            }
            if(value == "gmail")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Gmail"
            }
            if(value == "google_maps")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Google Maps"
            }
            if(value == "youtube")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Youtube"
            }
            if(value == "skype")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Skype"
            }
            if(value == "google")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Google"
            }
            if(value == "http_connect")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "HTTP Connect"
            }
            if(value == "http_proxy")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "HTTP Proxy"
            }
            if(value == "citrix")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Citrix"
            }
            if(value == "netflix")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Netflix"
            }
            if(value == "lastfm")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Lastfm"
            }
            if(value == "waze")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Waze"
            }
            if(value == "citrix_online")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Citrix Online"
            }
            if(value == "apple")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Apple"
            }
            if(value == "webex")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Webex"
            }
            if(value == "whatsapp")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Whatsapp"
            }
            if(value == "apple_icloud")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "icloud"
            }
            if(value == "viber")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Viber"
            }
            if(value == "apple_itunes")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "itunes"
            }
            if(value == "windows_update")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Windows Update"
            }
            if(value == "remotescan")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Remotescan"
            }
            if(value == "spotify")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Spotify"
            }
            if(value == "h323")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "h323"
            }
            if(value == "openvpn")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Openvpn"
            }
            if(value == "ciscovpn")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Ciscovpn"
            }
            if(value == "tor")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Tor"
            }
            if(value == "ubuntuone")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Ubuntuone"
            }
            if(value == "ftpdata")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Ftpdata"
            }
            if(value == "wikipedia")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Wikipedia"
            }
            if(value == "ebay")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Ebay"
            }
            if(value == "cnn")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "CNN"
            }
            if(value == "telegram")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Telegram"
            }
            if(value == "vevo")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Vevo"
            }
            if(value == "pandora")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Pandora"
            }
            if(value == "whatsapp_voice")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Whatsapp Voice"
            }
            if(value == "kakaotalk")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "kakaotalk"
            }
            if(value == "kakaotalk_voice")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Kakaotalk Voice"
            }
            if(value == "twitch")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Twitch"
            }
            if(value == "quickplay")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Quickplay"
            }
            if(value == "snapchat")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Snapchat"
            }
            if(value == "deezer")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Deezer"
            }
            if(value == "instagram")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Instagram"
            }
            if(value == "microsoft")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Microsoft"
            }
            if(value == "office_365")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Office 365"
            }
            if(value == "cloudflare")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Cloudflare"
            }
            if(value == "ms_one_drive")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "One drive"
            }
            if(value == "mqtt")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "MQTT"
            }
            if(value == "starcraft")
            {
                arrayProtocols[arrayProtocols.firstIndex(of: value)!] = "Starcraft"
            }

            
            
            
        }
        
        return arrayProtocols

    }




}
    
