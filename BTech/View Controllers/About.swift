//
//  About.swift
//  BTech
//
//  Created by Ahmed Akhtar on 15/11/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import FirebaseAnalytics

class About: Parent {
    
    @IBOutlet weak var ota_version_label: UILabel!
    @IBOutlet weak var dmoat_app_version_label: UILabel!
    
    var titleText:String = ""
    
    class func AboutVC() -> About {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "About") as! About
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.titleText = "About"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.navigationItem.title = titleText
        //self.title = titleText
    }
    
    override func viewDidLoad() {
        DispatchQueue.main.async {
            self.callOTAversioApi()
        }
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            dmoat_app_version_label.text = version
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
//        Analytics.setScreenName("About", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "About", AnalyticsParameterScreenClass: screenClass])
    }
    
    
    @IBAction func btn_tos_pressed(_ sender: AnyObject) {
        print("terms of services")
        if let link = URL(string: ApiUrl.dmoatTermsServicesDocURL) {
                           UIApplication.shared.open(link)
                         }
    }
    
    @IBAction func btn_policy_pressed(_ sender: AnyObject) {
        print("privacy policy")
        if let link = URL(string: ApiUrl.dmoatPrivacyPolicyDocURL) {
                           UIApplication.shared.open(link)
                         }
    }
    @IBAction func btn_eula_pressed(_ sender: AnyObject) {
        print("end user licence agreement")
        if let link = URL(string: ApiUrl.dmoatEULADocURL) {
                           UIApplication.shared.open(link)
                         }
    }
    
//    @IBAction func btn_dmoat_url_pressed(_ sender: AnyObject) {
//        print("www.dmoat.com")
//        UIApplication.tryURL(urls: [
//            ApiUrl.dmoatWebUrl, // App
//            ApiUrl.dmoatWebUrl // Website if app fails
//            ])
//    }

    
     //Mark: - openSource Packages
    @IBAction func openSourcePkgs_pressed(_ sender: Any) {
        let openSrcVC = OpenSrcPackagesViewController.OpenSrcPackagesVC()
        self.setNavBar()
        self.navigationController?.pushViewController(openSrcVC, animated: true)
    }
    
    //Mark: - pause internet
    func callOTAversioApi(){
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        let eveSec = NSDate().timeIntervalSince1970
        
        let data : [String : AnyObject] = ["id" : "pause" as AnyObject, "type" : "default" as AnyObject, "eve_sec" : eveSec as AnyObject ]
        
        let params:Dictionary<String,AnyObject> = ["namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "data" : data as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject,"token" : NetworkHelper.sharedInstance.getCurrentToken() as AnyObject,"request_id" : requestID as AnyObject,"postkey" : NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject]
        print(params)
        
        NetworkHelper.sharedInstance.postDashboardRequest(serviceName: ApiUrl.OTA_VERSION_API, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                
                let latestVersion = data["latest"] as AnyObject
                let installedVersion = data["installed"] as AnyObject
                DispatchQueue.main.async {
                self.ota_version_label.text = "Installed: " + (installedVersion as! String) + " / Latest: " + (latestVersion as! String)
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    self.showAlert(message)
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            } else if (data["message"] != nil) {
                let message = data["message"] as! String
                self.showAlert(message)
            } else {
                self.showAlert(ConstantStrings.ErrorText)
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {

        if(self.checkPausedTimeAsAndroid()){
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
       
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
    }

    
  
    
}
