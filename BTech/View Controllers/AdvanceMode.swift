//
//  AdvanceMode.swift
//  BTech
//
//  Created by Ahmed Akhtar on 22/12/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import FirebaseAnalytics
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


class AdvanceMode: Parent {
    //ib outlet
    
    @IBOutlet var ipField: UITextField!
    @IBOutlet weak var ipField_b: UITextField!
    
    @IBOutlet var netMaskField: UITextField!
    
    @IBOutlet var subnetField: UITextField!
    
    @IBOutlet var startIpRangeField: UITextField!
    @IBOutlet weak var startIpRangeField_b: UITextField!
    
    
    @IBOutlet var endIpRangeField: UITextField!
    @IBOutlet weak var endIpRangeField_b: UITextField!
    
    @IBOutlet var routerIpField: UITextField!
    @IBOutlet var defaultLeaseTimeField: UITextField!
    @IBOutlet var maxLeaseTimeField: UITextField!
    @IBOutlet var braodCastField: UITextField!
    @IBOutlet var scrollVIew: UIScrollView!
    
    
    class func AdvanceModeVC() -> AdvanceMode {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "advanceMode") as! AdvanceMode
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        scrollVIew.isDirectionalLockEnabled = true
        self.navigationItem.title = "Advanced Mode"
        
        self.getNetConfig()
        //set field editable = false
        netMaskField.isUserInteractionEnabled = false
        subnetField.isUserInteractionEnabled = false
        routerIpField.isUserInteractionEnabled = false
        braodCastField.isUserInteractionEnabled = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
     //   Analytics.setScreenName("Advanced Mode", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Advanced Mode", AnalyticsParameterScreenClass: screenClass])
    }
    
    // MARK: - Actoins
    
    @IBAction func screen_info_pressed(_ sender: Any) {
    }
    
    @objc func getNetConfigData()
    {
        //        ipField.text = NetworkHelper.sharedInstance.advanceModeModel?.ipString
        //        startIpRangeField.text = NetworkHelper.sharedInstance.advanceModeModel?.startIpRangeString
        //        endIpRangeField.text = NetworkHelper.sharedInstance.advanceModeModel?.endIpRangeString
        
        splitAndSetIP(ip: (NetworkHelper.sharedInstance.advanceModeModel?.ipString!)!, ipLabel_A: ipField, ipLabel_B: ipField_b, isStrtEndRouter: false)
        
        if (NetworkHelper.sharedInstance.advanceModeModel?.startIpRangeString!.count < 1) {
            splitAndSetIP(ip: (NetworkHelper.sharedInstance.advanceModeModel?.routerIpString!)!, ipLabel_A: startIpRangeField, ipLabel_B: startIpRangeField_b, isStrtEndRouter: true)
        }else{
            splitAndSetIP(ip: (NetworkHelper.sharedInstance.advanceModeModel?.startIpRangeString!)!, ipLabel_A: startIpRangeField, ipLabel_B: startIpRangeField_b, isStrtEndRouter: false)
        }
        
        if (NetworkHelper.sharedInstance.advanceModeModel?.startIpRangeString!.count < 1) {
            splitAndSetIP(ip: (NetworkHelper.sharedInstance.advanceModeModel?.routerIpString!)!, ipLabel_A: endIpRangeField, ipLabel_B: endIpRangeField_b, isStrtEndRouter: true)
        }
        else{
            splitAndSetIP(ip: (NetworkHelper.sharedInstance.advanceModeModel?.endIpRangeString!)!, ipLabel_A: endIpRangeField, ipLabel_B: endIpRangeField_b, isStrtEndRouter: false)
        }
        
        netMaskField.text = NetworkHelper.sharedInstance.advanceModeModel?.netmaskString
        subnetField.text = NetworkHelper.sharedInstance.advanceModeModel?.subnetString
        routerIpField.text = NetworkHelper.sharedInstance.advanceModeModel?.routerIpString
        defaultLeaseTimeField.text = NetworkHelper.sharedInstance.advanceModeModel?.defaultLeaseTimeString
        maxLeaseTimeField.text = NetworkHelper.sharedInstance.advanceModeModel?.maxLeaseTimeString
        braodCastField.text = NetworkHelper.sharedInstance.advanceModeModel?.broadcastString
        
        //self.saveAdvanceMode()
        
    }
    
    func splitAndSetIP(ip: String, ipLabel_A : UITextField, ipLabel_B : UITextField, isStrtEndRouter: Bool) {
        
        let index2 = ip.range(of: ".", options: .backwards)?.lowerBound
        
        let substringA = ip.substring(to: index2!)
        var substringB = ip.substring(from: index2!)
        while substringB.hasPrefix(".") {
            substringB.remove(at: substringB.startIndex)
        }
        ipLabel_A.text = substringA + "."
        if (isStrtEndRouter) {
            ipLabel_B.text = ""
        }else{
            ipLabel_B.text = substringB
        }
    }
    
    func getNetConfig()
    {
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let nameSpace = NetworkHelper.sharedInstance.getCurrentUserNamespace()
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": nameSpace as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.GET_NET_CONFIG, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                print("success get/netconfig")
                if data["message"] != nil {
                    self.fetchNetConfig(data: data["message"] as! [String : AnyObject])
                }else{
                    DispatchQueue.main.async {
                         self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "400")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                         self.showAlert(msg)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                         self.showAlert(ConstantStrings.ErrorText)
                    }
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                         self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    func fetchNetConfig(data : [String : AnyObject]) {
        let ipString = data["ip"] as? String
        let startIpRangeString = data["start_ip_range"] as? String
        let routerIpString = data["router_ip"] as? String
        let endIpRangeString = data["end_ip_range"] as? String
        let netmaskString = data["netmask"] as? String
        let subnetString = data["subnet"] as? String
        let defaultLeaseTimeString = data["default_lease_time"] as? String
        let maxLeaseTimeString = data["max_lease_time"] as? String
        let gatewayString = data["gateway"] as? String
        let broadcastString = data["broadcast"] as? String
        
        let advanceMode = AdvanceModeModel()
        advanceMode.broadcastString = broadcastString
        advanceMode.defaultLeaseTimeString = defaultLeaseTimeString
        advanceMode.endIpRangeString = endIpRangeString
        advanceMode.gatewayString = gatewayString
        //advanceMode.ifaceString = ifaceString
        advanceMode.ipString = ipString
        //advanceMode.macAddressString = macAddressString
        advanceMode.maxLeaseTimeString = maxLeaseTimeString
        advanceMode.netmaskString = netmaskString
        advanceMode.routerIpString = routerIpString
        advanceMode.startIpRangeString = startIpRangeString
        advanceMode.subnetString = subnetString
        
        NetworkHelper.sharedInstance.advanceModeModel = advanceMode
        
        DispatchQueue.main.async {
            self.setAndSplitNetConfig()
        }
        
    }
    
    func setAndSplitNetConfig() {
    
    splitAndSetIP(ip: (NetworkHelper.sharedInstance.advanceModeModel?.ipString!)!, ipLabel_A: ipField, ipLabel_B: ipField_b, isStrtEndRouter: false)
    
    if (NetworkHelper.sharedInstance.advanceModeModel?.startIpRangeString!.count < 1) {
    splitAndSetIP(ip: (NetworkHelper.sharedInstance.advanceModeModel?.routerIpString!)!, ipLabel_A: startIpRangeField, ipLabel_B: startIpRangeField_b, isStrtEndRouter: true)
    }else{
    splitAndSetIP(ip: (NetworkHelper.sharedInstance.advanceModeModel?.startIpRangeString!)!, ipLabel_A: startIpRangeField, ipLabel_B: startIpRangeField_b, isStrtEndRouter: false)
    }
    
    if (NetworkHelper.sharedInstance.advanceModeModel?.startIpRangeString!.count < 1) {
    splitAndSetIP(ip: (NetworkHelper.sharedInstance.advanceModeModel?.routerIpString!)!, ipLabel_A: endIpRangeField, ipLabel_B: endIpRangeField_b, isStrtEndRouter: true)
    }
    else{
    splitAndSetIP(ip: (NetworkHelper.sharedInstance.advanceModeModel?.endIpRangeString!)!, ipLabel_A: endIpRangeField, ipLabel_B: endIpRangeField_b, isStrtEndRouter: false)
    }
    
    netMaskField.text = NetworkHelper.sharedInstance.advanceModeModel?.netmaskString
    subnetField.text = NetworkHelper.sharedInstance.advanceModeModel?.subnetString
    routerIpField.text = NetworkHelper.sharedInstance.advanceModeModel?.routerIpString
    defaultLeaseTimeField.text = NetworkHelper.sharedInstance.advanceModeModel?.defaultLeaseTimeString
    maxLeaseTimeField.text = NetworkHelper.sharedInstance.advanceModeModel?.maxLeaseTimeString
    braodCastField.text = NetworkHelper.sharedInstance.advanceModeModel?.broadcastString
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0
        }
    }
    
    
    func saveAdvanceMode()
    {
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        var netConfig = Dictionary<String, String>()
        netConfig["ip"] = ipField.text! + ipField_b.text!
        netConfig["netmask"] = netMaskField.text!
        netConfig["subnet"] = subnetField.text!
        netConfig["start_ip_range"] = startIpRangeField.text! + startIpRangeField_b.text!
        netConfig["end_ip_range"] = endIpRangeField.text! + endIpRangeField_b.text!
        netConfig["router_ip"] = routerIpField.text!
        netConfig["default_lease_time"] = defaultLeaseTimeField.text!
        netConfig["max_lease_time"] = maxLeaseTimeField.text!
        netConfig["broadcast"] = braodCastField.text!
        
        var data = [String : AnyObject]()
        data["request"] = "change_boot_mode" as AnyObject?
        data["mode"] = 1 as AnyObject?
        data["net_config"] = netConfig as AnyObject
        NetworkHelper.sharedInstance.error401 = false
        let params:Dictionary<String,AnyObject> = (["request_id": requestID, "token": NetworkHelper.sharedInstance.getCurrentToken(), "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace(), "data" : data, "app_id" : NetworkHelper.sharedInstance.app_ID!] as NSDictionary) as! Dictionary<String, AnyObject>
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.NET_CONFIG_API, sendData: params, success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async {
                      
                    self.modeChangedAlert(message: ConstantStrings.advance_done_guide)
                }
                //NetworkHelper.sharedInstance.appSettingsMode = "Advanced"
                //NetworkHelper.sharedInstance.saveCurrentUserMode("Advanced")
                DispatchQueue.main.async {
                     self.navigationController?.popViewController(animated: true)
                }
                
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                         self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                         self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                         self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    
    //validate ip addresses
    
    func checkIfIPAddressesLieInPrivateRange() -> Bool
    {
        var validPrivateRange = true
        
        var categoryOne = true
        var categoryTwo = true
        var categoryThree = true
        
        let validIpAddressRegexCategoryOne = "^192\\.168\\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))\\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))"
        let validIpAddressRegexCategoryTwo = "^172\\.(1[6-9]|2[0-9]|3[0-1])\\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))\\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))"
        let validIpAddressRegexCategoryThree = "^10\\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))\\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))\\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))"
        
        
        if(self.ipField.text!.range(of: validIpAddressRegexCategoryOne, options: .regularExpression) == nil || self.netMaskField.text!.range(of: validIpAddressRegexCategoryOne, options: .regularExpression) == nil || self.subnetField.text!.range(of: validIpAddressRegexCategoryOne, options: .regularExpression) == nil || self.startIpRangeField.text!.range(of: validIpAddressRegexCategoryOne, options: .regularExpression) == nil || self.endIpRangeField.text!.range(of: validIpAddressRegexCategoryOne, options: .regularExpression) == nil || self.routerIpField.text!.range(of: validIpAddressRegexCategoryOne, options: .regularExpression) == nil || self.braodCastField.text!.range(of: validIpAddressRegexCategoryOne, options: .regularExpression) == nil)
        {
            categoryOne = false
        }
        else if(self.ipField.text!.range(of: validIpAddressRegexCategoryTwo, options: .regularExpression) == nil || self.netMaskField.text!.range(of: validIpAddressRegexCategoryTwo, options: .regularExpression) == nil || self.subnetField.text!.range(of: validIpAddressRegexCategoryTwo, options: .regularExpression) == nil || self.startIpRangeField.text!.range(of: validIpAddressRegexCategoryTwo, options: .regularExpression) == nil || self.endIpRangeField.text!.range(of: validIpAddressRegexCategoryTwo, options: .regularExpression) == nil || self.routerIpField.text!.range(of: validIpAddressRegexCategoryTwo, options: .regularExpression) == nil || self.braodCastField.text!.range(of: validIpAddressRegexCategoryTwo, options: .regularExpression) == nil)
        {
            categoryTwo = false
        }
        else if(self.ipField.text!.range(of: validIpAddressRegexCategoryThree, options: .regularExpression) == nil || self.netMaskField.text!.range(of: validIpAddressRegexCategoryThree, options: .regularExpression) == nil || self.subnetField.text!.range(of: validIpAddressRegexCategoryThree, options: .regularExpression) == nil || self.startIpRangeField.text!.range(of: validIpAddressRegexCategoryThree, options: .regularExpression) == nil || self.endIpRangeField.text!.range(of: validIpAddressRegexCategoryThree, options: .regularExpression) == nil || self.routerIpField.text!.range(of: validIpAddressRegexCategoryThree, options: .regularExpression) == nil || self.braodCastField.text!.range(of: validIpAddressRegexCategoryThree, options: .regularExpression) == nil)
        {
            categoryThree = false
        }
        
        //check if ip addresses lie in any three ranges
        if(categoryOne == false && categoryTwo == false && categoryThree == false)
        {
            validPrivateRange = false
        }
        
        return validPrivateRange
        
    }
    
    func validateIPAddresses() -> Bool
    {
        let dmoatStaticIp = ipField.text! + ipField_b.text!
        let startIp = startIpRangeField.text! + startIpRangeField_b.text!
        let endIp = endIpRangeField.text! + endIpRangeField_b.text!
        
        let validIpAddressRegex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
        
        if self.ipField.text == "" || self.ipField_b.text == "" || self.netMaskField.text == "" || self.subnetField.text == "" || self.startIpRangeField.text == "" || self.startIpRangeField_b.text == "" || self.endIpRangeField.text == "" || self.endIpRangeField_b.text == "" || self.routerIpField.text == "" || self.braodCastField.text == "" || defaultLeaseTimeField.text == "" || maxLeaseTimeField.text == ""
        {
            DispatchQueue.main.async {
                 self.showAlert(ConstantStrings.fill_all_fields_text)
            }
            return false
        }
        else if(startIp == endIp){
            DispatchQueue.main.async {
                self.showAlert("Start IP and End Ip must be different.")
            }
            return false
        }
            
        else if(Int(defaultLeaseTimeField.text!)! > Int(maxLeaseTimeField.text!))
        {
            DispatchQueue.main.async {
                 self.showAlert("Default lease time cannot be greater than the maximum lease time.")
            }
            return false
        }
        else if(Int(defaultLeaseTimeField.text!)! ==  Int(maxLeaseTimeField.text!))
        {
            DispatchQueue.main.async {
                self.showAlert("Default lease time and maximum lease time should not be same.")
            }
            return false
        }
        else if(Int(defaultLeaseTimeField.text!)! ==  0 || Int(maxLeaseTimeField.text!) == 0)
        {
            DispatchQueue.main.async {
                self.showAlert("Lease time should not be 0.")
            }
            return false
        }
            
        else if(dmoatStaticIp.range(of: validIpAddressRegex, options: .regularExpression) == nil || self.netMaskField.text!.range(of: validIpAddressRegex, options: .regularExpression) == nil || self.subnetField.text!.range(of: validIpAddressRegex, options: .regularExpression) == nil || startIp.range(of: validIpAddressRegex, options: .regularExpression) == nil || endIp.range(of: validIpAddressRegex, options: .regularExpression) == nil || self.routerIpField.text!.range(of: validIpAddressRegex, options: .regularExpression) == nil || self.braodCastField.text!.range(of: validIpAddressRegex, options: .regularExpression) == nil)
        {
            DispatchQueue.main.async {
                 self.showAlert("Please enter a valid IP address.")
            }
            return false
        }
            
        else if(self.checkIfIPAddressesLieInPrivateRange() == false)
        {
            DispatchQueue.main.async {
                 self.showAlert("Please enter IP addresses within the allowed range.")
            }
            return false
        }
            
        else if(self.compareIPS() == false)
        {
            return false
        }
            
        else if(self.sameGourpForAllIps() == false)
        {
            return false
        }
        
        return true
        
    }
    
    
    func getIpAsNumber(_ ip : String) -> Double
    {
        let ipParts : Array = ip.components(separatedBy: ".")
        var result : Double = 0
        var i : Int = 0
        while(i<4)
        {
            result += Double(ipParts[i])! * (pow(256 , (3 - Double(i))))
            i = i + 1
        }
        
        return result
    }
    
    
    func sameGourpForAllIps()->Bool
    {
        let ip : Array = ipField.text!.components(separatedBy: ".")
        let subnet : Array = subnetField.text!.components(separatedBy: ".")
        let startIpRange : Array = startIpRangeField.text!.components(separatedBy: ".")
        let endIpRange : Array = endIpRangeField.text!.components(separatedBy: ".")
        let routerIp : Array = routerIpField.text!.components(separatedBy: ".")
        let braodCast : Array = braodCastField.text!.components(separatedBy: ".")
        
        if(ip[0] == subnet[0] && ip[0] == startIpRange[0] && ip[0] == endIpRange[0] && ip[0] == routerIp[0] && ip[0] == braodCast[0])
        {
            return true
        }
            
        else {
            DispatchQueue.main.async {
                self.showAlert("All Ip should lie in same group")
            }
            return false
        }
        
    }
    
    
    
    func compareIPS()->Bool
    {
        
        var validIP : Bool = true
        let ip : Double = self.getIpAsNumber(self.ipField.text! + self.ipField_b.text!)
        let startIpRange : Double = self.getIpAsNumber(self.startIpRangeField.text! + self.startIpRangeField_b.text!)
        let endIpRange : Double = self.getIpAsNumber(self.endIpRangeField.text!+endIpRangeField_b.text!)
        let routerIp : Double = self.getIpAsNumber(self.routerIpField.text!)
        
        if(ip > startIpRange || ip == routerIp)
        {
            DispatchQueue.main.async {
                self.showAlert("Ip should be less than start ip range and not equal to router ip")
            }
            validIP = false
        }
        if(routerIp >= startIpRange && routerIp <= endIpRange)
        {
            DispatchQueue.main.async {
                self.showAlert("router ip should not lie between start ip and end ip ranges")
            }
            validIP = false
        }
        if(startIpRange > endIpRange)
        {
            DispatchQueue.main.async {
                self.showAlert("start ip range should be less than end ip range")
            }
            validIP = false
        }
        
        return validIP
    }
    
    
    //ibAction
    
    
    @IBAction func SavePressed(_ sender: UIButton) {
        
        if(self.validateIPAddresses() == true)
        {
            self.saveAdvanceMode()
        }
        
    }
    
    func showAdvanceSuccessAlert() {
        
        let pAndPAction = UIAlertAction(title: "Plug & Play", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            // Create the alert controller
            let alertController = UIAlertController(title: "Prytex mode", message: ConstantStrings.advance_done_guide, preferredStyle: .alert)
            
            // Create the actions
            
            let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
        })
    }
    
    func modeChangedAlert(message: String) {
        // Create the alert controller
               let alertController = UIAlertController(title: "Prytex mode", message: message, preferredStyle: .alert)

               // Create the actions
               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                   UIAlertAction in
                self.navigationController?.popViewController(animated: true)
               }
              

               // Add the actions
               alertController.addAction(okAction)
               

               // Present the controller
               self.present(alertController, animated: true, completion: nil)
    }
    
}
