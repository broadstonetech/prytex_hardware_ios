//
//  DiscoveryMode.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 19/05/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation
import SystemConfiguration

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


class DiscoveryMode: Parent, UITextFieldDelegate {
    
    //    var numList = []
    //
    @IBOutlet weak var text_area: UITextView!
    
    
    var key_received = ""
    
    //Mark: - class variables
    var titleText:String = ""
    
    class func dmodeVC() -> DiscoveryMode {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Dmode") as! DiscoveryMode
    }
    
    
    @IBOutlet weak var btn_discover_device: UIButton!
    var beacon_key = "";
    var savedDate = Date();
    var currentDateTime = Date()
    var isDateTrue = false
    var isFirstNum = false;
    var isSecondNum = false
    var isThirdNum = false
    var isRetryAuthUI = false
    var isNumListReceived = false;
    var isNameSpaceReceived = false;
    var isAckUi = true;
    var retryCount = 0;
    var retryCountAck = 0;
    var nameSpace = ""
    var arrayOfInts: [Int] = [2]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        text_area.text = ConstantStrings.discovery_screen_text
        //call intercom
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    
    @IBAction func screen_info_pressed(_ sender: Any) {
    }
    
    @IBAction func discover_device_pressed(_ sender: AnyObject) {
        DispatchQueue.main.async {
            if(self.isInternetAvailable()){
                self.showOkAlert(ConstantStrings.startDiscoveryText, startDiscovery: true, failed: false, errorTimeOut: false, successDiscovery: false)
            }else{
                self.showOkAlert(ConstantStrings.internet_off, startDiscovery: false, failed: true, errorTimeOut: false, successDiscovery: false)
            }
        }
    }
    
    func startDiscovery(){
        DispatchQueue.main.async {
            CozyLoadingActivity.show("Please Wait..", disableUI: true)
        }
        self.getCurrentTime()
        self.checkDateTime();
        if isDateTrue {
            //make a call to beacon api if time is less than 10 min
            print("time in - isDateTrue");
            callBeaconApi();
        }
        else {
            print("time out for 10min");
            DispatchQueue.main.async {
                self.showOkAlert(ConstantStrings.requestTimeOutMsgDiscovery, startDiscovery: false, failed: false, errorTimeOut: true, successDiscovery: false)
                
            }        }
        
    }
    
    //MARK: -Beacon Api Call
    func callBeaconApi(){
        if(isInternetAvailable()){
            beacon_key = "afc2060747e0099d7ee8e2b22f66d0f519d2fe47a31a369fd3e4b4b5eccf1f1f";
            self.view.endEditing(true)
            
            //call sign up service
            NetworkHelper.sharedInstance.error401 = false
            
            let defaults = UserDefaults.standard
            let productId = defaults.string(forKey: "prodId")
            var appMode = "prod"
            if NetworkHelper.sharedInstance.getAppMode() {
                appMode = "dev"
            }else {
                appMode = "prod"
            }
            
            //        let productId = "@36B19"            
            let params:Dictionary<String,AnyObject> = ["beacon_key": beacon_key as AnyObject, "app_mode" : appMode as AnyObject, "prod_id" : productId as AnyObject]
            print(params);
            print("currentTIme== \(currentDateTime)")
            
            NetworkHelper.sharedInstance.postDiscoveryRequest(serviceName: ApiUrl.BEACON_API, sendData: params, success: { (response) in

                var success:Bool = false
                let message:AnyObject
                if(response["success"] != nil) {
                    success = response["success"] as! Bool
                }
                if (response["message"] != nil) {
                    message = (response["message"] as? AnyObject)!
                }
                let defaults = UserDefaults.standard
                let status_code = defaults.string(forKey: "statusCode")
                print("statuscode \(status_code)")
                let statusCode:Int? = Int(status_code!)
                if statusCode == 200 {
                if(response["namespace"] != nil){
                    self.nameSpace = response["namespace"] as! String
                    print("namespace count== \(self.nameSpace.count) \(self.nameSpace)")
                }
                
                if(success){
                    print("success all received")
                    DispatchQueue.main.async {
                        self.showOkAlert(ConstantStrings.succesDiscoveryMsgDiscovery, startDiscovery: false, failed: false, errorTimeOut: false, successDiscovery: true)
                    }
                }
                    
                else if( response["message"] != nil) {
                    let message = response["message"] as? NSDictionary
                    if (message!["num_list"] != nil) {
                        self.isNumListReceived = true
                        print("numlist received")
                        // reset boool to false, may be again numlist received
                        self.isFirstNum = false;
                        self.isSecondNum = false;
                        self.isThirdNum = false;
                        let numList = message!["num_list"] as! [Int]
                        //     var a = response["message"].dictionary!["num_list"]   response["message"].object["key"]
                        let a = message!["key"] as! String
                        self.key_received = a
                        print(numList)
                        self.arrayOfInts = numList
                        print("numlist received")
                        self.callNumListAuth(0)
                    }
                    else{
                        //keep on sendin beacon call if not
                        self.checkDateTime();
                        if self.isDateTrue {
                            print("sleep for 5 sec and again beacon")
                            sleep(5)
                            self.callBeaconApi();
                        }else{
                            DispatchQueue.main.async {
                                self.showOkAlert(ConstantStrings.requestTimeOutMsgDiscovery, startDiscovery: false, failed: false, errorTimeOut: false, successDiscovery: true)
                            }
                        }
                    }
                }
                else if (self.nameSpace.count > 2) {
                    print("namespace received")
                    self.isNameSpaceReceived = true
                    print(self.nameSpace)
                    
                    self.checkDateTime();
                    if self.isDateTrue {
                        if (self.isAckUi){
                            print("call ack ui here")
                            self.callAckUiApi()
                        }else {
                            print("sleep for 5 sec and again beacon")
                            sleep(5)
                            self.callBeaconApi();
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.showOkAlert(ConstantStrings.requestTimeOutMsgDiscovery, startDiscovery: false, failed: false, errorTimeOut: true, successDiscovery: false)
                        }
                    }
                }
                    
                else {
                    //keep on sendin beacon call if not
                    //show message retry after an hour
                    self.checkDateTime();
                    print(self.nameSpace)
                    if self.isDateTrue {
                        print(self.nameSpace)
                        print("sleep for 5 sec and call beacon again")
                        sleep(5)
                        self.callBeaconApi();
                    }else{
                        DispatchQueue.main.async {
                            self.showOkAlert(ConstantStrings.requestTimeOutMsgDiscovery, startDiscovery: false, failed: false, errorTimeOut: true, successDiscovery: false)
                        }
                    }
                    
                }
                } else if  statusCode == 400 {

                    self.checkDateTime()
                    if self.isDateTrue {
                    sleep(5)
                    self.callBeaconApi()
                }else{
                        DispatchQueue.main.async {
                            self.showOkAlert(ConstantStrings.requestTimeOutMsgDiscovery, startDiscovery: false, failed: false, errorTimeOut: true, successDiscovery: false)
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showOkAlert(ConstantStrings.ErrorText, startDiscovery: false, failed: true, errorTimeOut: false, successDiscovery: false)
                    }
                }
            }, failure: { (data) in
                print(data)
                if(NetworkHelper.sharedInstance.error401)
                {
                    NetworkHelper.sharedInstance.isNotificationLocked =  true
                    //mute notifications
                    if(NetworkHelper.sharedInstance.deviceToken != nil)
                    {
                        (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    }
                    //unsubscribe from all
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                    //clear nsuserdefaults
                    NetworkHelper.sharedInstance.clearUserData()
                    //delete all data
                    //AlertModelRealm.deleteAllRecords()
                    PolicyManagementRealm.deleteAllRecords()
                    let loginVC = Login.loginVC()
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(loginVC, animated: true)
                    }
                }
                DispatchQueue.main.async {
                    self.showOkAlert(ConstantStrings.serverOffTextDiscovery, startDiscovery: false, failed: false, errorTimeOut: true, successDiscovery: false)
                }
            })
        }else{
            DispatchQueue.main.async {
                self.showOkAlert(ConstantStrings.internet_off, startDiscovery: false, failed: true, errorTimeOut: false, successDiscovery: false)
            }
        }
        
    }
    
    //MARK: -AckUiApiCall
    func callAckUiApi(){
        if(isInternetAvailable()){
            self.view.endEditing(true)
            print("ACK UI CALLED")
            //call sign up service
            NetworkHelper.sharedInstance.error401 = false
            
            let params:Dictionary<String,AnyObject> = ["uikey": "ui111" as AnyObject, "beacon_key" : beacon_key as AnyObject, "namespace" : nameSpace as AnyObject ]
           let servicename = ConstantStrings.aapModeBaseURl + ApiUrl.ACK_UI_API
            NetworkHelper.sharedInstance.postDiscoveryRequest(serviceName: servicename, sendData: params, success: { (response) in
                print(response)
                
                var success:Bool = false
                var message:AnyObject = "" as AnyObject
                if(response["success"] != nil){
                    success = response["success"] as! Bool
                }
                if (response["message"] != nil) {
                    message = (response["message"] as? AnyObject)!
                }
                
                if(success)
                {
                    print("sleep for 5 sec and again beacon")
                    sleep(5)
                    self.isAckUi = false
                    self.callBeaconApi()
                    
                }
                else{
                    //agr 500 sy 600 ho to 3 try r
                    //nahi to out of discovery
                    
                    let defaults = UserDefaults.standard
                    let status_code = defaults.string(forKey: "statusCode")
                    print(status_code)
                    let b:Int? = Int(status_code!)   // secondText is UITextField
                    
                    if  b == 400 {

                                       self.checkDateTime()
                                       if self.isDateTrue {
                                       sleep(5)
                                       self.callBeaconApi()
                                   }else{
                                           DispatchQueue.main.async {
                                               self.showOkAlert(ConstantStrings.requestTimeOutMsgDiscovery, startDiscovery: false, failed: false, errorTimeOut: true, successDiscovery: false)
                                           }
                                       }
                                   }
                    
                    else if (b > 500){
                        print("status code bw 500-600")
                        
                        self.checkDateTime()
                        if self.isDateTrue {
                            while (self.retryCountAck < 5){
                                print("sleep for 5 sec and again beacon")
                                sleep(5)
                                self.callBeaconApi()
                                self.retryCountAck += 1
                            }
                        }
                        else {
                            
                            DispatchQueue.main.async {
                                self.showOkAlert(ConstantStrings.requestTimeOutMsgDiscovery, startDiscovery: false, failed: false, errorTimeOut: true, successDiscovery: false)
                            }
                        }
                        
                    }
                    else {
                        
                        DispatchQueue.main.async {
                            self.showOkAlert(message as! String, startDiscovery: false, failed: true, errorTimeOut: false, successDiscovery: false)
                        }
                        
                    }
                    
                }
                
            }, failure: { (data) in
                print(data)
                if(NetworkHelper.sharedInstance.error401)
                {
                    NetworkHelper.sharedInstance.isNotificationLocked = true
                    //mute notifications
                    if(NetworkHelper.sharedInstance.deviceToken != nil)
                    {
                        (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    }
                    //unsubscribe from all
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                    //clear nsuserdefaults
                    NetworkHelper.sharedInstance.clearUserData()
                    //delete all data
                    //AlertModelRealm.deleteAllRecords()
                    PolicyManagementRealm.deleteAllRecords()
                    let loginVC = Login.loginVC()
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(loginVC, animated: true)
                    }
                }
                DispatchQueue.main.async {
                    self.showOkAlert(ConstantStrings.serverOffTextDiscovery, startDiscovery: false, failed: false, errorTimeOut: true, successDiscovery: false)
                }
                
            })
        }else{
            DispatchQueue.main.async {
                self.showOkAlert(ConstantStrings.internet_off, startDiscovery: false, failed: true, errorTimeOut: false, successDiscovery: false)
            }
        }
        
    }
    
    //MARK: -NumListAuth
    func callNumListAuth(_ n : Int){
        //get index value fom numList array
        let a = arrayOfInts[n]
        self.checkDateTime()
        if isDateTrue {
            print(a)
            
            if(isInternetAvailable()){
                callNumList(a)
            }
            else{
                DispatchQueue.main.async {
                    self.showOkAlert(ConstantStrings.internet_off, startDiscovery: false, failed: true, errorTimeOut: false, successDiscovery: false)
                }
            }
        }
        else{
            DispatchQueue.main.async {
                self.showOkAlert(ConstantStrings.requestTimeOutMsgDiscovery, startDiscovery: false, failed: false, errorTimeOut: true, successDiscovery: false)
            }
        }
    }
    
    //MARK: -NumListcall
    //sleep app for n sec and then call numListAuth Api
    func callNumList(_ num : Int){
        let n = UInt32(num)
        print("sleeping for \(n) seconds")
        sleep(n)
        self.checkDateTime()
        if isDateTrue {
            callAuthApi(num)
        }else {
            DispatchQueue.main.async {
                self.showOkAlert(ConstantStrings.requestTimeOutMsgDiscovery, startDiscovery: false, failed: false, errorTimeOut: true, successDiscovery: false)
            }
        }
    }
    
    //MARK: -AuthApiCall
    func callAuthApi(_ num : Int){
        if(isInternetAvailable()){
            
            self.view.endEditing(true)
            
            //call sign up service
            NetworkHelper.sharedInstance.error401 = false
            
            
            let data : [String : AnyObject] = ["req_mode": "UI" as AnyObject, "req_time" : num as AnyObject, "key": key_received as AnyObject]
            let params:Dictionary<String,AnyObject> = ["uikey": "ui111" as AnyObject, "data" : data as AnyObject]
            
            
            let servicename = ConstantStrings.aapModeBaseURl + ApiUrl.AUTH_API
            NetworkHelper.sharedInstance.postDiscoveryRequest(serviceName: servicename, sendData: params, success: { (response) in
                print(response)
                
                var success:Bool = false
                var message:AnyObject = "" as AnyObject
                if(response["success"] != nil){
                    success = response["success"] as! Bool
                }
                if (response["message"] != nil) {
                    message = (response["message"] as? AnyObject)!
                }
                
                
                if(success)
                {
                    self.isFirstNum = true;
                    if (!(self.isSecondNum)) {
                        self.isSecondNum = true;
                        self.callNumListAuth(1);
                    } else if (!(self.isThirdNum)) {
                        self.isThirdNum = true;
                        self.callNumListAuth(2);
                        self.isRetryAuthUI  = true
                    } else {
                        //all three completed back to beacon.to get success>true
                        print("sleep for 5 sec and again beacon")
                        sleep(5)
                        self.callBeaconApi();
                    }
                    //                self.showAlert("Success")
                    //    let loginVC = Login.loginVC()
                    //    self.navigationController?.pushViewController(loginVC, animated: true)
                    
                }
                else{
                    //                self.showAlert(message!)
                    //agr 400 error h to time check kro r again beacon
                    //agr 500-600 h to 2 retry r
                    //else out of discovery
                    
                    let defaults = UserDefaults.standard
                    let status_code = defaults.string(forKey: "statusCode")
                    print(status_code)
                    let b:Int? = Int(status_code!)   // secondText is UITextField
                    
                    if b == 400 {
                        self.checkDateTime()
                        if self.isDateTrue {
                        sleep(5)
                        self.callBeaconApi()
                    }else{
                            DispatchQueue.main.async {
                                self.showOkAlert(ConstantStrings.requestTimeOutMsgDiscovery, startDiscovery: false, failed: false, errorTimeOut: true, successDiscovery: false)
                            }
                        }
                }
                    else if (b > 500) {
                        print("status code bw 500-600")
                        if !(self.isRetryAuthUI) {
                            
                            self.checkDateTime()
                            if self.isDateTrue {
                                while (self.retryCount < 2){
                                    print("sleep for 5 sec and again beacon")
                                    sleep(2)
                                    self.callBeaconApi()
                                    self.retryCount += 1
                                }
                            }else {
                                DispatchQueue.main.async {
                                    self.showOkAlert(ConstantStrings.requestTimeOutMsgDiscovery, startDiscovery: false, failed: false, errorTimeOut: true, successDiscovery: false)
                                }
                            }
                        }
                    }
                    else {
                        print("Eror occured, out of discovery")
                        DispatchQueue.main.async {
                            self.showOkAlert(message as! String, startDiscovery: false, failed: true, errorTimeOut: false, successDiscovery: false)
                        }
                    }
                }
                //
                
                
            }, failure: { (data) in
                print(data)
                if(NetworkHelper.sharedInstance.error401)
                {
                    NetworkHelper.sharedInstance.isNotificationLocked = true
                    //mute notifications
                    if(NetworkHelper.sharedInstance.deviceToken != nil)
                    {
                        (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    }
                    //unsubscribe from all
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                    //clear nsuserdefaults
                    NetworkHelper.sharedInstance.clearUserData()
                    //delete all data
                    //AlertModelRealm.deleteAllRecords()
                    PolicyManagementRealm.deleteAllRecords()
                    let loginVC = Login.loginVC()
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(loginVC, animated: true)
                    }
                }
                DispatchQueue.main.async {
                    self.showOkAlert(ConstantStrings.serverOffTextDiscovery, startDiscovery: false, failed: false, errorTimeOut: true, successDiscovery: false)
                }
                
            })
        } else{
            DispatchQueue.main.async {
                self.showOkAlert(ConstantStrings.internet_off, startDiscovery: false, failed: true, errorTimeOut: false, successDiscovery: false)
            }
        }
        
    }
    
    
    //MARK: -GetTime
    func getCurrentTime(){
        
        // initialize the date formatter and set the style
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        formatter.dateStyle = .long
        
        formatter.string(from: currentDateTime)
        print("CURRENT TIME \(currentDateTime)")
        
        savedDate = Date(timeIntervalSinceNow: 600)
        print("SAVED TIME \(savedDate)")
        
    }
    
    func checkDateTime(){
        //        if (NetworkHelper.sharedInstance.con)
        currentDateTime = Date();
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        formatter.dateStyle = .long
        
        formatter.string(from: currentDateTime)
        print("currentdatetime \(currentDateTime)")
        print("savedatetime \(savedDate)")
        
        if savedDate.compare(currentDateTime) == ComparisonResult.orderedDescending
        {
            NSLog("savedDate greater than current");
            isDateTrue = true;
        } else if savedDate.compare(currentDateTime) == ComparisonResult.orderedAscending
        {
            isDateTrue = false
            NSLog("savedDate is less");
            DispatchQueue.main.async {
                CozyLoadingActivity.hide(animated: true)
                self.showOkAlert(ConstantStrings.requestTimeOutMsgDiscovery, startDiscovery: false, failed: false, errorTimeOut: true, successDiscovery: false)
            }
            
        } else
        {
            isDateTrue = false
            NSLog("due in exactly a week (to the second, this will rarely happen in practice)");
        }
    }
    
    //regex pattern
    func matchesForRegexInText(_ regex: String, text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matches(in: text,
                                        options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    //MARK: - //reset booleans
    func resetAllBool() {
        
        isDateTrue = false
        isFirstNum = false;
        isSecondNum = false
        isThirdNum = false
        isRetryAuthUI = false
        isNumListReceived = false;
        isNameSpaceReceived = false;
        isAckUi = true;
    }
    
    //MARK: -Internet connectivity
    
//    override func isInternetAvailable() -> Bool
//    {
//        var zeroAddress = sockaddr_in()
//        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
//        zeroAddress.sin_family = sa_family_t(AF_INET)
//        
//        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
//            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
//                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
//            }
//        }
//        
//        var flags = SCNetworkReachabilityFlags()
//        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
//            return false
//        }
//        let isReachable = flags.contains(.reachable)
//        let needsConnection = flags.contains(.connectionRequired)
//        return (isReachable && !needsConnection)
//    }
    
    //MARK: - AlertDialog
    func showOkAlert(_ message:String, startDiscovery:Bool, failed:Bool, errorTimeOut:Bool, successDiscovery:Bool)
    {
        let systemSoundID: SystemSoundID = 1007
        
        // to play sound
        AudioServicesPlaySystemSound (systemSoundID)
        CozyLoadingActivity.hide(animated: true)
        self.resetAllBool()
        
        let alertController = UIAlertController(title: "Prytex", message: message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            if(startDiscovery){
                self.isAckUi = true
                self.startDiscovery()
            }
            if(errorTimeOut){
                print("timed out")
            }
            if(failed){
                
            }
            if(successDiscovery){
                print("discovery successfull")
                //go back to login page
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        }
        
        // Add the actions
        alertController.addAction(okAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func chat_btn_pressed(_ sender: Any) {
        openConversationWindow()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
}
