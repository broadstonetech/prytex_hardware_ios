//
//  BandwidthChartVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 13/07/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import Charts
import FirebaseAnalytics

//import SwiftCharts
//import Graphs
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}



class BandwidthChartVC: Parent, ChartViewDelegate {
    
    //Mark: - class outlets
    @IBOutlet weak var notes_label: UILabel!
    
    
    @IBOutlet weak var hosts_chart_view: UIView!
    
    @IBOutlet weak var current_data_btn: UIButton!
    @IBOutlet weak var current_data_tab_view: UIView!
    
    @IBOutlet weak var all_data_btn: UIButton!
    @IBOutlet weak var all_data_tab_view: UIView!
    
    @IBOutlet weak var bar_chart_bandwidth: BarChartView!
    
    @IBOutlet weak var showHide_btn: UIButton!
    
    var isChartVisible:Bool = false
    var isTableVisible:Bool = false
    var isCurrentTableVisible:Bool = false
    var isAllTableVisible:Bool = false
    
    
    //MARK: - class variable
    var clrArray = [UIColor]()
    var countClr = 0;
    //    var graphView : ScrollableGraphView? = nil
    var graphDataSet=[Double]()
    let refreshControl = UIRefreshControl()
    let rightButtonBar = UIButton()
    var months: [String]!
    
    
    class func bandwidthVC() -> BandwidthChartVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "bandwidth") as! BandwidthChartVC
    }
    
    //MARK: - class variables
    var titleText:String = ""
    var bandWidthList: [BandwidthModel] = [BandwidthModel]()
    var bandWidthListCurrentData: [BandwidthModel] = [BandwidthModel]()
    var bandWidthListTableViewAll: [BandwidthModel] = [BandwidthModel]()
    var bandWidthListDataTableView: [BandwidthModel] = [BandwidthModel]()
    
    
    //Mark: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        showHide_btn.setTitle("Show Details",for: .normal)
        bar_chart_bandwidth.delegate = self
        
        bar_chart_bandwidth.noDataText = ConstantStrings.noDataLoaded
        self.loadBandwidth()
        isChartVisible = true
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.titleText = "Network Usage"
        self.navigationController!.navigationBar.topItem?.title = ""
       // self.title = titleText
        self.navigationItem.title = titleText
        createRightButtonNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
//        Analytics.setScreenName(titleText, screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "titletxt", AnalyticsParameterScreenClass: screenClass])
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createRightButtonNavigationBar()
    {
        
        rightButtonBar.setImage(UIImage(named: "info"), for: UIControl.State())
        rightButtonBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        rightButtonBar.addTarget(self, action: #selector(topRightButtonBar), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = rightButtonBar
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        //create custom
    }
    @objc func topRightButtonBar()
    {
        self.showAlert(ConstantStrings.bandwidth_screen_txt)
    }
    func refreshBandwidth(refreshControl: UIRefreshControl) {
        print("refreshing!")
        bandWidthList.removeAll()
        self.loadBandwidth()
        // somewhere in your code you might need to call:
        //        refreshControl.endRefreshing()
    }
    
    // MARK: - Actions
    
    @IBAction func screen_info_pressed(_ sender: Any) {
        
    }
    
    @IBAction func load_current_data_pressed(_ sender: Any) {
            let screenClass = classForCoder.description()
//            Analytics.setScreenName("Network Usage chart Current", screenClass: screenClass)
  
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Network Usage chart Current", AnalyticsParameterScreenClass: screenClass])
        
        self.current_data_btn.setTitleColor(CozyLoadingActivity.Settings.WhiteColor, for: .normal)
        //            self.load_all_data_btn.backgroundColor = CozyLoadingActivity.Settings.selectedButonColor
        self.all_data_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
        self.all_data_tab_view.backgroundColor = CozyLoadingActivity.Settings.UnselectedTabBgColor
        self.current_data_tab_view.backgroundColor = CozyLoadingActivity.Settings.SelectedTabBgColor
        
        DispatchQueue.main.async {
            if self.bandWidthListCurrentData.count > 0 {
                self.isAllTableVisible = false
                self.isCurrentTableVisible = true
                var HostNames = [String]()
                var HostUsage = [Double]()
                for i in 0..<self.bandWidthListCurrentData.count {
                    var hName = self.bandWidthListCurrentData[i].getHostName()
                    let hUsage = self.bandWidthListCurrentData[i].getUsage()
                    if (hName.count > 6 ){
                        hName = String(hName.prefix(7))
                    }else{
                        //                    hName = hName
                    }
                    HostNames.append(hName)
//                    if (hUsage == 0 || hUsage < 1){
//                        hUsage = 1
//                    }
                    print("Bandwith hostUsage chart: \(Double(hUsage))")
                    var doubleRoundedusage = Double(round(1000*hUsage)/1000)
                    if doubleRoundedusage < 0.01 {
                        doubleRoundedusage = 0.01
                    }
                                         HostUsage.append(doubleRoundedusage.rounded(digits: 2))
                    
                }
                self.setChart(dataPoints: HostNames, values: HostUsage)
                
            }else{
                self.bar_chart_bandwidth.noDataText = ConstantStrings.noCurrentbandwidthData
                
            }
        }
        
    }
    
    @IBAction func load_all_data_pressed(_ sender: Any) {
            let screenClass = classForCoder.description()
//            Analytics.setScreenName("Network Usage chart 24hour", screenClass: screenClass)
  
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Network Usage chart 24hours", AnalyticsParameterScreenClass: screenClass])
        
        self.all_data_btn.setTitleColor(CozyLoadingActivity.Settings.WhiteColor, for: .normal)
        //            self.load_all_data_btn.backgroundColor = CozyLoadingActivity.Settings.selectedButonColor
        self.current_data_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
        self.all_data_tab_view.backgroundColor = CozyLoadingActivity.Settings.SelectedTabBgColor
        self.current_data_tab_view.backgroundColor = CozyLoadingActivity.Settings.UnselectedTabBgColor
        
        DispatchQueue.main.async {
            if self.bandWidthList.count > 0 {
                self.isAllTableVisible = true
                self.isCurrentTableVisible = false
                var HostNames = [String]()
                var HostUsage = [Double]()
                for i in 0..<self.bandWidthList.count {
                    if self.bandWidthList[i].getIsCurrent() == "no" {
                        var hName = self.bandWidthList[i].getHostName()
                        let hUsage = self.bandWidthList[i].getUsage() // 1048576//1024
                        if (hName.count > 6) {
                            hName = String(hName.prefix(7))
                        }else{
                            //                    hName = hName
                        }
                        HostNames.append(hName)
                        if (hUsage == 0 || hUsage < 1){
                            print("Bandwith hostUsage chart: \(Double(hUsage)) ==<1")
                        }
                        print("Bandwith hostUsage chart: \(Double(hUsage))")
                       var doubleRoundedusage = Double(round(1000*hUsage)/1000)
                        if doubleRoundedusage < 0.01 {
                            doubleRoundedusage = 0.01
                        }
                       HostUsage.append(doubleRoundedusage.rounded(digits: 2))
                    }
                }
                self.setChart(dataPoints: HostNames, values: HostUsage)
            }else{
                self.bar_chart_bandwidth.noDataText = ConstantStrings.noDataLoaded
            }
        }
    }
    
    //Mark: - loadData
    func loadBandwidth() {
        
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.GET_TRAFFIC_STATS, sendData: params, success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            let message = ConstantStrings.ErrorText
            
            if ((statusCode?.range(of: "200")) != nil)
            {
                let jsonArr = data["message_trafficstats_24hour"] as? [[String:AnyObject]]
                let jsonArrCurrent = data["message_trafficstats_1hour"] as? [[String:AnyObject]]
                //                print(jsonArr?.count)
                if(jsonArr?.count > 0){
                    self.bandWidthListCurrentData.removeAll()
                    self.bandWidthList.removeAll()
                    DispatchQueue.main.async {
                        self.hosts_chart_view.isHidden = false
                        //                    self.bar_chart_bandwidth.isHidden = false asdasd
                        self.showHide_btn.isEnabled = true
                    }
                    print("hide table and show chart")
                    DispatchQueue.main.async {
                         self.hosts_chart_view.isHidden = false
                    }
                    self.isChartVisible = true
                    self.isTableVisible = false
                    //                    }
                    
                    self.fetchData(jsonArr!)
                    if(jsonArrCurrent?.count > 0){
                        self.fetchCurrentData(jsonArrCurrent!)
                    }else{
                        self.bar_chart_bandwidth.noDataText = ConstantStrings.noCurrentbandwidthData
                    }
                    DispatchQueue.main.async {
                        var HostNames = [String]()
                        var HostUsage = [Double]()
                        for i in 0..<self.bandWidthListCurrentData.count {
                            var hName = self.self.bandWidthListCurrentData[i].getHostName()
                            let hUsage = self.self.bandWidthListCurrentData[i].getUsage()
                            if (hName.count > 6 ){
                                hName = String(hName.prefix(7))
                            }else{
                            }
                            HostNames.append(hName)
//                            if (hUsage == 0 || hUsage < 1){
//                                hUsage = 1
//                            }
                            print("Bandwith hostUsage chart: \(Double(hUsage))")
                            
                            var roundedValue = Double(round(1000*hUsage)/1000)
                            let floatValue = String(format:"%.2f", roundedValue)
//                            let a = Double(round(1000*hUsage)/1000)
                            if roundedValue < 0.01 {
                                roundedValue = 0.01
                            }
                            print("Bandwith hostUsage chart roundedValue: \(roundedValue)")
//                            let asdf = String(format:"%.2f", roundedValue)
                            HostUsage.append(roundedValue.rounded(digits: 2))
                        }
                        
                        self.setChart(dataPoints: HostNames, values: HostUsage)
                        
                    }
                    
                }else{
                    self.showAlert("Bandwidth data was not processed. Please refresh screen and make sure your Prytex is connected to the Internet.")
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    self.showAlert(message)
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            } else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            }
            else{
                self.showAlert(message)
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        })
        
    }
    
    //MARK: - fetch data
    func fetchData(_ jsonArr : [[String : AnyObject]]) {
        
        for element in jsonArr {
            //            let hID = element["hostid"] as! String
            let hIP = element["hostip"] as! String
            let hName = element["hostname"] as! String
            let protcol = element["protocols"] as! NSDictionary
            let totalUsage = element["totalusage"] as! Int
            let parameters = element["parameters"] as! String
            //add into model class
            let bandwidh = BandwidthModel()
            //            bandwidh.setHostID(hID)
            bandwidh.setHostIP(hIP)
            if ((hName.count < 0) || (hName.range(of: "") != nil) || hName == "" || (hName.range(of: "None") != nil)) {
                bandwidh.setHostName("Unknown")
            }else{
                bandwidh.setHostName(hName)
            }
            bandwidh.setProtocols(protcol)
            var convertedMB = Double(totalUsage) / 1048576// 1024
            if convertedMB < 0.01 {
                                      convertedMB = 0.01
                                  }
            let floatMbs = String(format: "%.2f", ceil(convertedMB*1000)/1000)
            print("Bandwith 24hour \(floatMbs.floatValue)")
            bandwidh.setUsgae(floatMbs.floatValue)
            bandwidh.setParameters(parameters)
            bandwidh.setIsCurrent("no")
            
            bandWidthList.append(bandwidh)
        }
        bandWidthList.sort(by: sorterForFileIDASC)
        //reload table view/decorate chart view
        DispatchQueue.main.async {
             self.refreshControl.endRefreshing()
        }
        
    }
    
    func fetchCurrentData(_ jsonArr : [[String : AnyObject]]){
        
        for element in jsonArr {
            //            let hID = element["hostid"] as! String
            let hIP = element["hostip"] as! String
            let hName = element["hostname"] as! String
            let protcol = element["protocols"] as! NSDictionary
            let totalUsage = element["totalusage"] as! Int
            let parameters = element["parameters"] as! String
            //add into model class
            let bandwidh = BandwidthModel()
            //            bandwidh.setHostID(hID)
            bandwidh.setHostIP(hIP)
            if ((hName.count < 0) || (hName.range(of: "") != nil) || hName == "" || (hName.range(of: "None") != nil)) {
                bandwidh.setHostName("Unknown")
            }else{
                bandwidh.setHostName(hName)
            }
            bandwidh.setProtocols(protcol)
            var convertedMB = Double(totalUsage) / 1048576 //1024
            if convertedMB < 0.01 {
                                      convertedMB = 0.01
                                  }
            let floatMbs = String(format: "%.2f", ceil(convertedMB*1000)/1000)
            print("Bandwith Current \(floatMbs.floatValue)")
            bandwidh.setUsgae(floatMbs.floatValue)
            bandwidh.setParameters(parameters)
            bandwidh.setIsCurrent("yes")
            
            
            bandWidthListCurrentData.append(bandwidh)
        }
        bandWidthListCurrentData.sort(by: sorterForFileIDASC)
        
        //reload table view/decorate chart view
       DispatchQueue.main.async {
                   self.refreshControl.endRefreshing()
              }
    }
    
    
    func sorterForFileIDASC(_ this : BandwidthModel, that : BandwidthModel ) -> Bool {
        return this.getUsage() > that.getUsage()
    }
    //MARK: -switch chart/table view
    
    @IBAction func switchChartTablePressed(_ sender: Any) {
        print("show hide pressed")
        
        let alertVC = BandwidthListVC.bandwidthListVC()
        alertVC.bandWidthList = bandWidthList
        alertVC.bandWidthListCurrentData = bandWidthListCurrentData
        self.navigationController?.pushViewController(alertVC, animated: true)
    }
    
    //MARK: -SetChartData
    func setChart(dataPoints: [String], values: [Double]) {
        bar_chart_bandwidth.noDataText = "Bandwidth data was not processed. Please refresh screen and make sure your Prytex is connected to the Internet."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            var yVal = values[i]
            if yVal == 0 || yVal < 0.01 {
                print("1234lo g == x= \(i) == y== \(yVal)")
                print("oyee hoee \(yVal)")
                yVal = 0.01
            }
//            if yVal > 9 {
//                yVal = 6
//            }
            print("123lo g == x= \(Double(i)) == y== \(yVal)")
            bar_chart_bandwidth.xAxis.valueFormatter = IndexAxisValueFormatter(values: dataPoints)
//            bar_chart_bandwidth.barData.valueform

            let values = BarChartDataEntry(x: Double(i), y: yVal)
            dataEntries.append(values)
        }
        
        let chartDataSet = BarChartDataSet(entries : dataEntries, label: "")
        chartDataSet.drawValuesEnabled = true
//        let formatter = IValueFormatter()
//        formatter.n
//        chartDataSet.valueFormatter = YAxisValueFormatter()
        
           let format = NumberFormatter()
           format.numberStyle = .decimal
           let formatter = DefaultValueFormatter(formatter: format)
           chartDataSet.valueFormatter = formatter
        chartDataSet.colors = [CozyLoadingActivity.Settings.CLAppThemeColor]
        let data = BarChartData()
        data.barWidth = Double(0.50)
   
        
        data.addDataSet(chartDataSet)
        bar_chart_bandwidth.data = data
        //bar_chart_bandwidth.backgroundColor = UIColor.white
        
        bar_chart_bandwidth.gridBackgroundColor = UIColor(named: "View")!
        bar_chart_bandwidth.drawGridBackgroundEnabled = true

        
        bar_chart_bandwidth.xAxis.granularity = 0
        bar_chart_bandwidth.rightAxis.drawLabelsEnabled = false
        bar_chart_bandwidth.xAxis.labelPosition = .bottom
        bar_chart_bandwidth.dragDecelerationEnabled = true
        bar_chart_bandwidth.chartDescription?.text = ""
        bar_chart_bandwidth.drawBarShadowEnabled = false
        bar_chart_bandwidth.xAxis.drawGridLinesEnabled = false
        bar_chart_bandwidth.xAxis.spaceMax = 1
        bar_chart_bandwidth.xAxis.wordWrapEnabled = true
//        bar_chart_bandwidth.barData?.setValueFormatter(format as! IValueFormatter)
        bar_chart_bandwidth.leftAxis.drawGridLinesEnabled = false
        bar_chart_bandwidth.leftAxis.spaceBottom = 0
        bar_chart_bandwidth.rightAxis.spaceBottom = 0
        bar_chart_bandwidth.rightAxis.drawGridLinesEnabled = false
        bar_chart_bandwidth.legend.enabled = false
        bar_chart_bandwidth.leftAxis.axisMinimum = 0
    
        
        bar_chart_bandwidth.rightAxis.enabled = false
        //        bar_chart_bandwidth.legend.enabled = false
        bar_chart_bandwidth.leftAxis.enabled = true
        //        bar_chart_bandwidth.xAxis.drawGridLinesEnabled = false
        bar_chart_bandwidth.xAxis.drawAxisLineEnabled = true
        bar_chart_bandwidth.barData?.barWidth = 0.25
//        bar_chart_bandwidth.xAxis.axisMinimum = 3
        
        //        bar_chart_bandwidth.xAxis.axisMaximum = 10
             
        
    }
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        
        //        account_label_tabbar.textColor = UIColor(red: 0/255, green: 174/255, blue: 239/255, alpha: 1.0)
        //        var image: UIImage = UIImage(named: "tab_home_selected")!
        //        account_img_tabbar.image = image
        
        if(self.checkPausedTimeAsAndroid()){
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
    
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
}
extension Double {
    func rounded(digits: Int) -> Double {
        let multiplier = pow(10.0, Double(digits))
        return (self * multiplier).rounded() / multiplier
    }
}
