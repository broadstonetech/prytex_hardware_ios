//
//  BandwidthListVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 09/03/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class BandwidthListVC: Parent, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var current_data_btn: UIButton!
    @IBOutlet weak var current_data_tab_view: UIView!
    
    @IBOutlet weak var all_data_tab_view: UIView!
    @IBOutlet weak var all_data_btn: UIButton!
    @IBOutlet weak var table_view: UITableView!
    
    
    
    //Mark: - class variables
    var titleText:String = ""
    var bandWidthList : [BandwidthModel] = [BandwidthModel()]
    var bandWidthListCurrentData : [BandwidthModel] = [BandwidthModel()]
    let rightButtonBar = UIButton()
    var isCurrentTableVisible:Bool = false
    var isAllTableVisible:Bool = false
    
    
    class func bandwidthListVC() -> BandwidthListVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "bandwidth_list_vc") as! BandwidthListVC
    }
    //Mark: - class variables override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table_view.delegate = self
        table_view.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
//        Analytics.setScreenName(titleText, screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "titletxt", AnalyticsParameterScreenClass: screenClass])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.titleText = "Network Usage"
        self.navigationController!.navigationBar.topItem?.title = ""
        //self.title = titleText
        self.navigationItem.title = titleText
        createRightButtonNavigationBar()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createRightButtonNavigationBar()
    {
        
        rightButtonBar.setImage(UIImage(named: "info"), for: UIControl.State())
        rightButtonBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        rightButtonBar.addTarget(self, action: #selector(topRightButtonBar), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = rightButtonBar
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        //create custom
    }
    @objc func topRightButtonBar()
    {
        self.showAlert(ConstantStrings.bandwidth_screen_txt)
    }
    
    //MARK: -Top NavBar actions
    
    @IBAction func load_current_data_pressed(_ sender: Any) {
            let screenClass = classForCoder.description()
       //     Analytics.setScreenName("Network Usage current list", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Netowrk Usage Current List", AnalyticsParameterScreenClass: screenClass])
        self.current_data_btn.setTitleColor(CozyLoadingActivity.Settings.WhiteColor, for: .normal)
        //            self.load_all_data_btn.backgroundColor = CozyLoadingActivity.Settings.selectedButonColor
        self.all_data_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
        self.all_data_tab_view.backgroundColor = CozyLoadingActivity.Settings.UnselectedTabBgColor
        self.current_data_tab_view.backgroundColor = CozyLoadingActivity.Settings.SelectedTabBgColor
        
        DispatchQueue.main.async {
            
            self.isAllTableVisible = false
            self.isCurrentTableVisible = true
            
            self.table_view.reloadData()
            
        }
        
        
    }
    
    @IBAction func load_all_data_pressed(_ sender: Any) {
            let screenClass = classForCoder.description()
        //    Analytics.setScreenName("Network Usage 24hour list", screenClass: screenClass)
        
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Network Usage 24 hr list", AnalyticsParameterScreenClass: screenClass])
        
        self.all_data_btn.setTitleColor(CozyLoadingActivity.Settings.WhiteColor, for: .normal)
        //            self.load_all_data_btn.backgroundColor = CozyLoadingActivity.Settings.selectedButonColor
        self.current_data_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
        
        self.current_data_tab_view.backgroundColor = CozyLoadingActivity.Settings.UnselectedTabBgColor
        self.all_data_tab_view.backgroundColor = CozyLoadingActivity.Settings.SelectedTabBgColor
        
        DispatchQueue.main.async {
            
            self.isAllTableVisible = true
            self.isCurrentTableVisible = false
            self.table_view.reloadData()
            
        }
        
    }
    
    //MARK: -Tableview delegates
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("rowSelected\(indexPath.row)")
        
        //        self.isAllTableVisible = false
        //        self.isCurrentTableVisible = true
        if self.isAllTableVisible {
            let hostName = bandWidthList[indexPath.row].getHostName()
            //        let usag = bandWidthList[indexPath.row].getUsage()
            //        print("details: \(hostName) ;; \(usag)")
            let defaults = UserDefaults.standard
            defaults.set(hostName, forKey: "hostName")
            defaults.set(bandWidthList[indexPath.row].getProtocols(), forKey: "protocols")
            defaults.set(bandWidthList[indexPath.row].getHostIP(), forKey: "hostIp")
            
            let alertVC = ProtocolDetailsVC.protocolDetailVC()
            self.navigationController?.pushViewController(alertVC, animated: true)
            
        }else{
            
            let hostName = bandWidthListCurrentData[indexPath.row].getHostName()
            //            let usag = bandWidthListCurrentData[indexPath.row].getUsage()
            //        print("details: \(hostName) ;; \(usag)")
            let defaults = UserDefaults.standard
            defaults.set(hostName, forKey: "hostName")
            defaults.set(bandWidthListCurrentData[indexPath.row].getProtocols(), forKey: "protocols")
            defaults.set(bandWidthListCurrentData[indexPath.row].getHostIP(), forKey: "hostIp")
            
            let alertVC = ProtocolDetailsVC.protocolDetailVC()
            self.navigationController?.pushViewController(alertVC, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isAllTableVisible {
            return bandWidthList.count
        }else{
            return bandWidthListCurrentData.count
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  self.table_view.dequeueReusableCell(withIdentifier: "bandwidth_host_cell") as! BandwidthHostsCell
        
        var item = self.bandWidthList[indexPath.row]
        if self.isAllTableVisible {
            item = self.bandWidthList[indexPath.row]
        }else{
            item = self.bandWidthListCurrentData[indexPath.row]
        }
        cell.host_name.text = item.getHostName()
        let os = item.getParameters()
        if (os.range(of: "Android") != nil || os.range(of: "android") != nil || os.range(of: "Android OS") != nil || os.range(of: "watchOS") != nil) {
            cell.host_os_img.image=UIImage(named: "android_logo")
        }
        else if (os.range(of: "Apple") != nil || os.range(of: "apple") != nil || os.range(of: "iOS") != nil || os.range(of: "mac") != nil || os.range(of: "Mac") != nil || os.range(of: "macOS") != nil || os.range(of: "watch OS") != nil) {
            cell.host_os_img.image=UIImage(named: "ios_logo")
        }else if (os.range(of: "windws") != nil || os.range(of: "Windows") != nil || os.range(of: "Windows OS") != nil) {
            cell.host_os_img.image=UIImage(named: "windows")
        }else if (os.range(of: "Linux") != nil || os.range(of: "inux") != nil || os.range(of: "Linux/Debian Linux/Ubuntu") != nil) {
            cell.host_os_img.image=UIImage(named: "linux")
        }else{
            cell.host_os_img.image=UIImage(named: "laptop_logo")
        }
        return cell
    }
    
    @IBAction func showChart_pressed(_ sender: Any) {
        //        let alertVC = BandwidthChartVC.bandwidthVC()
        //                self.navigationController?.pushViewController(alertVC, animated: true)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        
        //        account_label_tabbar.textColor = UIColor(red: 0/255, green: 174/255, blue: 239/255, alpha: 1.0)
        //        var image: UIImage = UIImage(named: "tab_home_selected")!
        //        account_img_tabbar.image = image
        
        if(self.checkPausedTimeAsAndroid()){
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
        
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
