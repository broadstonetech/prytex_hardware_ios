//
//  BandWidthCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 13/07/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class BandWidthCell: UICollectionViewCell {

    @IBOutlet weak var host_name: UILabel!
    @IBOutlet weak var chart_bar: UILabel!
    @IBOutlet weak var labelHeight: NSLayoutConstraint!
    
}
