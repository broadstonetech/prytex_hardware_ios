//
//  BandwidthHostsCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 18/08/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class BandwidthHostsCell: UITableViewCell {

    @IBOutlet weak var host_name: UILabel!
    @IBOutlet weak var host_os_img: UIImageView!
}
