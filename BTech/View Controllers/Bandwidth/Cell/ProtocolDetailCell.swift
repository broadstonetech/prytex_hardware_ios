//
//  ProtocolDetailCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 14/07/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class ProtocolDetailCell: UITableViewCell {

    @IBOutlet weak var protocol_name: UILabel!
    @IBOutlet weak var protocol_usage: UILabel!
    @IBOutlet weak var protocol_img: UIImageView!
    @IBOutlet weak var protocol_detail: UITextView!
    
}
