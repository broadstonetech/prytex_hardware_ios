//
//  ProtocolDetailsVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 14/07/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class ProtocolDetailsVC: Parent, UITableViewDataSource, UITableViewDelegate {
    
    
    class func protocolDetailVC() -> ProtocolDetailsVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "protocol_detail") as! ProtocolDetailsVC
    }
    
    //Mark: - class outlets
    @IBOutlet weak var host_name: UILabel!
    @IBOutlet weak var host_ip: UILabel!
    @IBOutlet weak var table_view: UITableView!
    
    //Mark: - class variables
    var titleText:String = ""
    var hProtocols : NSDictionary!
    var pArr = [String]()
    var pArrTwo = [String]()
    var cont:Int = 0
    var bandWidthListProtocol: [BandwidthModel] = [BandwidthModel]()
    var imgsProtocolArr  = [String]()
    var pDict = NSDictionary()
    var protocolsValuesDict = [String: String]()
    var protocolsDescDict = [String: String]()
    var clickIndex = -1
    let rightButtonBar = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table_view.delegate = self
        table_view.dataSource = self
        let defaults = UserDefaults.standard
        let hName = defaults.string(forKey: "hostName")
        hProtocols =   defaults.object(forKey: "protocols") as! NSDictionary
        let hIP = defaults.string(forKey: "hostIp")
        
        for p in hProtocols {
            let bp = BandwidthModel()
            bp.setprotocolName(p.key as! String)
            bp.setprotocolUsage(p.value as! Int)
            bandWidthListProtocol.append(bp)
        }
        bandWidthListProtocol.sort(by: sorterForFileIDASC)
        
        addProtocolKeyValues()
        addProtocolKeyDescription()
        
        host_name.text = hName
        host_ip.text = hIP
        self.table_view.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
        
        //Analytics.setScreenName(titleText, screenClass: screenClass)
        
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "title", AnalyticsParameterScreenClass: screenClass])
    }
    
    func sorterForFileIDASC(_ this : BandwidthModel, that : BandwidthModel ) -> Bool {
        return this.getprotocolUsage() > that.getprotocolUsage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.titleText = "Protocol Details"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.navigationItem.title = titleText
        // self.title = titleText
        createRightButtonNavigationBar()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createRightButtonNavigationBar()
    {
        
        rightButtonBar.setImage(UIImage(named: "info"), for: UIControl.State())
        rightButtonBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        rightButtonBar.addTarget(self, action: #selector(topRightButtonBar), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = rightButtonBar
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        //create custom
    }
    @objc func topRightButtonBar()
    {
        self.showAlert(ConstantStrings.bandwidthDetail_screen_txt)
    }
    
    //Mark: -Tableview delegates
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let item = self.bandWidthListProtocol[indexPath.row]
        let protocolName = item.getprotocolName()
        let keyExists = protocolsDescDict[protocolName] != nil
        
        if clickIndex == indexPath.row{
            if !keyExists {  //no desc available
                return 75
            }
            else {  //desc available
                return 120
            }
        }else{
            
            return 40
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("rowSelected\(indexPath.row)")
        if self.clickIndex == indexPath.row{
            
            self.clickIndex = -1
        }else{
            self.clickIndex = indexPath.row
        }
        self.table_view.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hProtocols.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  self.table_view.dequeueReusableCell(withIdentifier: "protocolDetailCell") as! ProtocolDetailCell
        
        let item = self.bandWidthListProtocol[indexPath.row]
        let protocolName = item.getprotocolName()
        
        let keyValueExists = protocolsValuesDict[protocolName] != nil
        let keyDescExists = protocolsDescDict[protocolName] != nil
        
        //image finding
        if (UIImage(named: protocolName) != nil) {
            let image = UIImage(named: protocolName)
            cell.protocol_img.image = image
        }
        else {
            cell.protocol_img.image=UIImage(named: "ip_default")
        }
        //protocol value/onscreenName finding
        if keyValueExists {
            cell.protocol_name.text = protocolsValuesDict[protocolName]!
        }else{
            cell.protocol_name.text = item.getprotocolName()
        }
        if keyDescExists {
            cell.protocol_detail.text = protocolsDescDict[protocolName]!
        }else{
            cell.protocol_detail.text = "No description found."
        }
        var usg = Float(item.getprotocolUsage())
        usg = usg / 1048576 //1024
        if usg < 0.01 {
            usg = 0.01
        }
        let ConvertedUsg =  String(format: "%.2f", usg) // usg / 1024
        
        cell.protocol_usage.text = String("\(ConvertedUsg) MB")
        
        return cell
        
    }
    
    //Mark:- class functions
    
    @IBAction func protocol_manager_pressed(_ sender: Any) {
        
        let protocolManagerVC = ProtocolManagerVC.protocolManagerVC()
        self.navigationController?.pushViewController(protocolManagerVC, animated: true)
    }
    
    
    
    func addProtocolKeyValues(){
        protocolsValuesDict = [
            "afp": "AFP",
            "aimini": "Aimini",
            "amazon": "Amazon",
            "apple": "Apple",
            "appleicloud": "iCloud",
            "appleitunes": "iTunes",
            "applejuice": "Apple juice",
            "armagetron": "Armagetron",
            //            "avi": "Avi",
            "ayiya": "AYIYA",
            "Battlefield": "Battlefield",
            "bgp": "BGP",
            "bittorrent": "BitTorrent",
            "ciscoskinny": "SCCP",
            "Ciscovpn": "Cisco vpn",
            "citrix": "Citrix",
            "citrix_online": "Citrix Online",
            "Cloudflare": "Cloudflare",
            "cnn": "CNN",
            "coap": "CoAP",
            "collectd": "collectd",
            "corba": "CORBA",
            "crossfire": "CrossFire",
            "dce_rpc": "DCE/RPC",
            "deezer": "Deezer",
            "dhcp": "DHCP",
            "dhcpv6": "DHCPv6",
            "direct_download_link": "DDL",
            "directconnect": "DC",
            "dns": "DNS",
            "dofus": "Dofus",
            "drda": "DRDA",
            "dropbox": "Dropbox",
            "eaq": "EAQ",
            "ebay": "Ebay",
            "edonkey": "eDonkey",
            "egp": "EGP",
            //            "epp": "EPP",
            "facebook": "Facebook",
            "fasttrack": "",
            "fiesta": "Fiesta",
            //            "flash": "Flash Video",
            "florensia": "Florensia",
            "ftp_control": "FTP Control ",
            "ftp_data": "FTP Data",
            "git": "Git",
            "gmail": "Gmail",
            "gnutella": "Gnutella",
            "google": "Google",
            "googlehangout": "Google Hangouts",
            "googlemaps": "Google Maps",
            "gre": "GRE",
            "gtp": "GTP",
            "guildwars": "Guild Wars",
            "h323": "h323",
            "halflife2": "Half-Life 2",
            "hep": "Hep",
            "hotmail": "Hotmail",
            "hotspotshield": "Hotspot Shield",
            "http": "HTTP",
            //            "http_application_activesync": "HTTP Application Active Sync",
            "http_connect": "HTTP Connect",
            "http_proxy": "HTTP Proxy",
            "httpdownload": "HTTP Download",
            "iax": "IAX ",
            "icecast": "Icecast",
            "icmp": "ICMP",
            "icmpv6": "ICMPv6",
            "igmp": "IGMP",
            "imap": "IMAP",
            "imaps": "IMAP",
            "instagram": "Instagram",
            "ip_in_ip": "IP in IP",
            "ipp": "IPP",
            "ipsec": "IPsec",
            "irc": "IRC",
            "kakaotalk": "kakaotalk",
            "kakaotalk_voice": "Kakaotalk Voice",
            "kerberos": "Kerberos",
            "kontiki": "Kontiki ",
            "lastfm": "Lastfm",
            "ldap": "LDAP",
            "llmnr": "LLMNR",
            "lotusnotes": "Lotus Notes ",
            "lync": "Lync",
            "maplestory": "MapleStory",
            "mdns": "mDNS",
            "megaco": "Megaco",
            "mgcp": "MGCP ",
            "microsoft": "Microsoft",
            //            "mms": "MMS",
            //            "move": "",
            //            "mpeg_ts": "MPEG transport stream",
            "mqtt": "MQTT",
            "ms_one_drive": "One drive",
            "msn": "MSN",
            "mssql-tds": "TDS",
            "mysql": "MySQL",
            "netbios": "NBNS",
            "netflix": "Netflix",
            "netflow": "NetFlow",
            "nfs": "NFS",
            "noe": "Noe",
            "ntp": "NTP",
            "ocs": "OCS",
            "office365": "Office 365",
            "opendns": "OpenDNS",
            "openft": "OpenFT",
            "openvpn": "Openvpn",
            "oracle": "Oracle",
            "oscar": "OSCAR",
            "ospf": "OSPF",
            "pando_media_booster": "PMB",
            "pandora": "Pandora",
            "pcanywhere": "Pc Anywhere",
            "pop3": "POP3",
            "pops": "POP",
            "postgresql": "Postgres",
            "pplive": "PPLIVE",
            "ppstream": "PPStream",
            "pptp": "PPTP",
            "qq": "QQ",
            "qqlive": "QQLive",
            "quic": "QUIC",
            //            "quickplay": "Quickplay",
            "radius": "RADIUS",
            "rdp": "RDP",
            //            "realmedia": "RealMedia",
            "redis": "Redis",
            "remotescan": "Remotescan",
            "rsync": "Rsync",
            "rtcp": "RTCP",
            "rtmp": "RTMP",
            "rtp": "RTP",
            "rtsp": "RTSP",
            "rx": "Rx",
            "sap": "SAP",
            "sctp": "SCTP",
            "sflow": "sFlow",
            "shoutcast": "SHOUTcast",
            "sina(weibo)": "Sina Weibo",
            "sip": "SIP",
            //            "skyfile_postpaid": "SkyFile Mail",
            //            "skyfile_prepaid": "SkyFile Mail",
            "skype": "Skype",
            "slack": "Slack",
            //            "smb": "SMB",
            "smtp": "SMTP",
            "smtps": "SMTPS",
            "snapchat": "Snapchat",
            "snmp": "SNMP",
            "socks": "Socket Secure",
            //            "socrates": "SOCRATES",
            "sopcast": "Sopcast",
            "soulseek": "Soulseek",
            "spotify": "Spotify",
            "ssdp": "SSDP",
            "ssh": "SSH",
            "ssl": "SSL",
            "ssl_no_cert": "SSL NO CERT",
            "starcraft": "Starcraft",
            "stealthnet": "Stealthnet",
            "steam": "Steam",
            "stun": "STUN",
            "syslog": "Syslog",
            "teamspeak": "TeamSpeak",
            "teamviewer": "TeamViewer",
            "telegram": "Telegram",
            "telnet": "Telnet",
            "teredo": "Teredo",
            "tftp": "TFTP",
            "thunder": "Thunder",
            "tor": "Tor",
            "truphone": "Truphone",
            "tuenti": "Tuenti",
            "tvants": "TVants",
            "tvuplayer": "TVUPlayer",
            "twitch": "Twitch",
            "twitter": "Twitter",
            "ubntac2": "Ubntac2",
            "ubuntuone": "Ubuntuone",
            "unencryped_jabber": "Jabber",
            "upnp": "UPnP",
            "usenet": "Usenet",
            "vevo": "Vevo",
            "vhua": "Vhua",
            "viber": "Viber",
            "vmware": "VMWare",
            "vnc": "VNC",
            "vrrp": "VRRP",
            "warcraft3": "Warcraft3",
            "waze": "Waze",
            "webex": "Webex",
            //            "webm": "Webm",
            "whatsapp": "Whatsapp",
            "whatsappvoice": "Whatsapp Voice",
            "whois-das": "Whois",
            "wikipedia": "Wikipedia",
            //            "windowsmedia": "Windows Media ",
            "windowsupdate": "Windows Update",
            "worldofkungfu": "World Of Kung Fu",
            "worldofwarcraft": "World Of War Craft",
            "xbox": "Xbox",
            "xdmcp": "XDMCP",
            "yahoo": "Yahoo",
            "youtube": "Youtube",
            "zattoo": "Zattoo",
            "zeromq": "zeromq",
            "dnscrypt":"DNScrypt",
            "bjnp":"BJNP",
            "ookla":"Ookla",
            "signal":"Signal",
            "nestlogsink":"NestLogSink",
            "http_activesync":"HTTP_ActiveSync",
            "bloomberg":"Bloomberg",
            "smbv3":"SMBv23",
            "ntop":"ntop",
            "mining":"Mining",
            "nintendo":"Nintendo",
            "amqp":"AMQP",
            "genericprotocol":"GenericProtocol",
            "checkmk":"CHECKMK",
            "lisp":"LISP",
            "playstation":"Playstation",
            "targus_dataspeed":"Targus Dataspeed",
            "memcached":"Memcached",
            "smpp":"SMPP",
            "iflix":"IFLIX",
            "googleservices":"GoogleServices",
            "smbv1":"SMBv1",
            "googledrive":"GoogleDrive",
            "csgo":"CSGO",
            "messenger":"Messenger",
            "skypecall":"SkypeCall",
            "applepush":"ApplePush",
            "linkedin":"LinkedIn",
            "amazonvideo":"AmazonVideo",
            "youtubeupload":"YouTubeUpload",
            "ajp":"AJP",
            "soundcloud":"SoundCloud",
            "googleplus":"GooglePlus",
            "playstore":"PlayStore",
            "pastebin":"Pastebin",
            "modbus":"Modbus",
            "iec60870":"IEC60870",
            "zabbix":"Zabbix",
            "websocket":"WebSocket",
            "s7comm":"s7comm",
            "fix":"FIX",
            "facebookzero":"FacebookZero",
            "tinc":"TINC",
            "diameter":"Diameter",
            "wechat":"WeChat",
            "github":"Github",
            "teams":"Microsoft Teams",
            "whatsappfiles":"WhatsAppFiles",
            "dnp3":"DNP3",
            "applestore":"AppleStore",
            "tiktok":"TikTok",
            "someip":"SOMEIP",
            "capwap":"CAPWAP",
            
            "tls" : "TLS",
            "microsoft365" : "Microsoft 365",
            "googlehangoutduo" : "Google Hangouts"
            //            "https": "HTTPS",
            //"quake": "Quake",
            //"unknown": "Unknown",
            //"mpeg": "MPEG transport stream",
            //"imesh": "iMesh",
            //"skyfile_rudics": "SkyFile Mail",
            //"quicktime": "QuickTime",
            //"oggvorbis": "Ogg Vorbis ",
            //"filetopia": "Filetopia",
        ]
    }
    
    func addProtocolKeyDescription() {
        
        protocolsDescDict = [
            "tls": "Transport Layer Security, and its now-deprecated predecessor, Secure Sockets Layer, are cryptographic protocols designed to provide communications security over a computer network.",
            "microsoft365" : "Office 365 is a line of subscription services offered by Microsoft as part of the Microsoft Office product line.",
            "googlehangoutduo" : "Google Duo is a free, simple video calling app that brings you face-to-face with the people who matter most.",
            "afp": "Apple Filing Protocol is an Apple File Service Network Protocol that offers file services for macOS.",
            "aimini": "Aimini is an online solution to store, send and share files. ",
            "amazon": "This protocol plug-in classifies the generic web traffic related to Amazon services.",
            "apple": "Apple Inc. is a technology provider of consumer electronics, software and services.",
            "appleicloud": "iCloud is a cloud storage and cloud computing service from Apple Inc.",
            "appleitunes": "iTunes is a media player/library, online radio, and mobile device management app by Apple Inc.",
            "applejuice": "AppleJuice is a peer-to-peer file exchange protocol.",
            "armagetron": "Armagetron is a multiplayer snake game in 3D.",
            //            "avi": "Audio Video Interleave (avi) is a multimedia container format as part of Video for Windows software.",
            "ayiya": "Anything In Anything (AYIYA) is a networking protocol for managing IP tunneling protocols.",
            "Battlefield": "Battlefield is a series of first-person shooter video games.",
            "bgp": "BGP; exterior gateway protocol (EGP) used to exchange routing information among routers in different autonomous systems (ASs).",
            "bittorrent": "BitTorrent is a communication protocol for peer-to-peer file sharing over the Internet.",
            "ciscoskinny": "Skinny Client Control Protocol (SCCP) is a Cisco proprietary standard for terminal control for use with VoIP.",
            "Ciscovpn": "Citrix is a provider of servers, application and desktop virtualization, networking, software as a service, and cloud computing technologies.",
            "citrix": "Citrix is a provider of servers, application and desktop virtualization, networking, software as a service, and cloud computing technologies.",
            "citrix_online": "Citrix Online is a web-based remote access, support and collaboration software.",
            "Cloudflare": "Cloudflare is a provider of internet security services, and content delivery network.",
            "cnn": "Cable News Network is a basic cable and satellite television news channel.",
            "coap": "Constrained Application Protocol (CoAP) is a specialized web transfer protocol for use with constrained nodes and constrained (e.g., low-power) networks.",
            "collectd": "collectd is a Unix daemon that collects, transfers and stores performance data of computers and network equipment.",
            "corba": "Common Object Request Broker Architecture (CORBA) is a middleware solution standard to provide interoperability among distributed objects.",
            "crossfire": "CrossFire is an online shooter game for Microsoft Windows.",
            "dce_rpc": "DCE/RPC is a specification for a remote procedure call mechanism that defines both APIs and an over-the-network protocol.",
            "deezer": "Deezer is an internet based music streaming service.",
            "dhcp": "Dynamic Host Configuration Protocol is a standardized network protocol used on Internet Protocol networks.",
            "dhcpv6": "Dynamic Host Configuration Protocol version 6 (DHCPv6) is a network protocol for configuring IPv6 required to operate in an IPv6 network.",
            "direct_download_link": "Direct download link (DDL), is a term used within the Internet-based file sharing community and uses client–server architecture.",
            "directconnect": "Direct Connect (DC) is a peer-to-peer file sharing protocol.",
            "dns": "The DNS protocol is used to translate internet names (www.site.com) into IP addresses and vice versa.",
            "dofus": "Dofus is a multiplayer onilne game.",
            "drda": "DRDA is a universal, distributed data protocol.",
            "dropbox": "Dropbox is a cloud based file hosting service.",
            "eaq": "EAQ measures the quality indicators of telecommunication networks that support the fixed and mobile Broadband internet access.",
            "ebay": "Ebay is an online commerce service.",
            "edonkey": "eDonkey Network is a decentralized, server-based, peer-to-peer file sharing network.",
            "egp": "EGP is used to interconnect autonomous systems.",
            //            "epp": "The Extensible Provisioning Protocol (EPP) is a flexible protocol designed for allocating objects within registries over the Internet.",
            "facebook": "Facebook is an online social and social networking service.",
            "fasttrack": "FastTrack is a peer-to-peer protocol for file sharing.",
            "fiesta": "no description found",
            //            "filetopia": "Filetopia is a free, multi-platform peer-to-peer file sharing client, and networking tool.",
            //            "flash": "Flash Video is a container file format used to deliver digital video content over the internet. OR Adobe Flash Media Playback is a dynamic HTTP streaming protocol used to access video contents from a smart client application.",
            "florensia": "no description found",
            "ftp_control": "FTP Control is an advanced FTP client supporting passive mode (PASV) download between the client/server or host, and cross server transfers.",
            "ftp_data": "FTP data protocol is used for transfer of files on a computer network.",
            "git": "Git is a version control system for tracking changes in computer files and coordinating work on those files among multiple people.",
            "gmail": "Gmail is a free email service.",
            "gnutella": "Gnutella is a peer-to-peer protocol.",
            "google": "Google provides internet related services including search, cloud computing, software and hardware.",
            "googlehangout": "Google Hangouts is a communication platform for instant messaging, video chat, SMS and VOIP features.",
            "Googlemaps": "Google Maps is a web mapping service by Google, offering GPS, satellite imagery and real-time traffic.",
            "gre": "Generic Routing Encapsulation is a tunneling protocol used to encapsulate a variety of network layer protocols inside virtual point-to-point links over an Internet Protocol network.",
            "gtp": "GTP is used to establish a GTP tunnel, for user equipment, between a Serving Gateway (S-GW) and Packet Data Network Gateway (P-GW). OR GTP' (GTP prime) is an IP based protocol used within GSM and UMTS networks",
            "guildwars": "Guild Wars is a multiplayer online game.",
            "h323": "h323 defines the protocols to provide audio visual communication sessions on any packet network.",
            "halflife2": "Half Life 2 is a first person shooter game.",
            "hep": "HEP/EEP Extensible Provisioning Protocol is a flexible protocol designed for allocating objects within registries over the Internet.",
            "hotmail": "Hotmail is a free email service by Microsoft.",
            "hotspotshield": "Hotspot Shield is used for securing Internet connections, often in unsecured networks.",
            "https": "HTTPS (HTTP Secure) is an adaptation of the Hypertext Transfer Protocol (HTTP) for secure communication over a computer network, and is widely used on the Internet.",
            "http": "The Hypertext Transfer Protocol (HTTP) is an application protocol for distributed, collaborative, and hypermedia information systems.",
            //            "http_application_activesync": "HTTP Application Active Sync is a mobile data synchronization app.",
            "http_connect": "HTTP Connect method serves for a handshake between the client and the remote server.",
            "http_proxy": "HTTP Proxy is a content filter that examines web traffic",
            "httpdownload": "HTTP download protocol is used to transfer files from a server to client.",
            "iax": "IAX is a communications protocol used for transporting VoIP telephony sessions between servers and to terminal devices.",
            "icecast": "Icecast is a protocol used to stream audio files over HTTP.",
            "icmp": "ICMP is a supporting protocol in the internet protocol suite.",
            "icmpv6": "ICMPv6 is used by IPv6 nodes to report errors encountered in processing packets, and to perform other internet-layer functions.",
            "igmp": "The Internet Group Management Protocol (IGMP) is used by IP hosts to report their multicast group membership to routers.",
            "imap": "Internet Message Access Protocol (IMAP) is an Internet standard protocol used by e-mail clients to retrieve e-mail messages from a mail server over a TCP/IP connection.",
            "imaps": "Internet Message Access Protocol (IMAP) is an Internet standard protocol used by e-mail clients to retrieve e-mail messages from a mail server over encrypted SSL.",
            //            "imesh": "iMesh is a peer-to-peer protocol.",
            "instagram": "Instagram is a cross platform internet based photo sharing application/service.",
            "ip_in_ip": "IP in IP is an IP tunneling protocol that encapsulates one IP packet in another IP packet.",
            "ipp": "IPP is an internet protocol for communication between computers and printers.",
            "ipsec": "Internet Protocol security (IPsec) uses cryptographic security services to protect communications over Internet Protocol (IP) networks.",
            "irc": "IRC facilitates communication (chat process) in the form of text.",
            "kakaotalk": "kakaotalk is a free mobile instant messaging application.",
            "kakaotalk_voice": "Kakaotalk Voice is a service for telephony using the internet.",
            "kerberos": "Kerberos is a network authentication protocol",
            "kontiki": "Kontiki peer-to-peer protocol drives the Kontiki Delivery Management System - a video and content delivery platform.",
            "lastfm": "Lastfm is a music website.",
            "ldap": "LDAP, Lightweight Directory Access Protocol, is an Internet protocol that email and other programs use to look up information from a server.",
            "llmnr": "Link-Local Multicast Name Resolution (LLMNR) protocol allows IPv4/IPv6 hosts to perform name resolution for hosts on the same local link.",
            "lotusnotes": "Lotus Notes refers to the Notes client used to access mails and notes.",
            "lync": "Lync provides instant messaging, audio, video and instant messaging communications.",
            "maplestory": "MapleStory is a free-to-play multiplayer online role-playing game.",
            "mdns": "Multicast Domain Name System (mDNS) resolves host names to IP addresses within small networks that do not include a local name server.",
            "megaco": "Megaco is a signaling protocol that enables switching of voice, fax and multimedia calls between the PSTN and IP networks.",
            "mgcp": "The Media Gateway Control Protocol is a signaling and call control communications protocol used in voice over IP telecommunication systems.",
            "microsoft": "Microsoft is a technology provider of software and hardware.",
            //            "mms": "Multimedia Messaging Services (MMS) communications allows users to exchange multimedia communications.",
            //            "move": "no description found",
            "mpeg": "MPEG-Transport Stream is a protocol used for MPEG flows transmission.",
            "mpeg_ts": "MPEG-Transport Stream is a protocol used for MPEG flows transmission.",
            "mqtt": "MQTT is an ISO standard publish-subscribe-based messaging protocol.",
            "ms_one_drive": "OneDrive is an online file-hosting service by Microsoft.",
            "msn": "MSN is a web portal and collection of internet services and apps for windows.",
            "mssql-tds": "Tabular Data Stream (TDS) is an application layer protocol, used to transfer data between a database server and a client.",
            "mysql": "MySQL protocol is used between MySQL Clients and a MySQL Server.",
            "netbios": "NetBios Name Service (NBNS) is a protocol which makes it possible to manage the names in a Microsoft NetBios network. ",
            "netflix": "Netflix service provides online streaming media and video-on-demand.",
            "netflow": "NetFlow is a network protocol for the collection and monitoring of network traffic flow data.",
            "nfs": "The NFS protocol is one of several distributed file system standards for network-attached storage ",
            "noe": "no description found",
            "ntp": "Network Time Protocol is used to synchronize system clocks among a set of distributed time servers and clients.",
            "ocs": "Open Collaboration Services (OCS) allows exchange of relevant data and applications use this protocol to access multiple services providing OCS APIs.",
            "office365": "Office 365 is a group of Microsoft services and software’s.",
            //            "oggvorbis": "Ogg Vorbis is a free general-purpose compressed audio format.",
            "opendns": "OpenDNS is a company /service that extends the Domain Name System by adding features such as phishing protection and optional content filtering.",
            "openft": "openFT is Fujitsu's product family of a company-wide file transfer solution  ",
            "openvpn": "Openvpn is an open source application that provides remote access facilities.",
            "oracle": "Oracle develops hardware systems and enterprise software products, particularly its own brand of database management systems.",
            "oscar": "OSCAR (Open System for Communication in Realtime) is used in both ICQ and AIM services.",
            "ospf": "OSPF (Open Short Path First) is a link state routing protocol used within large autonomous system networks.",
            "pando_media_booster": "Pando Media Booster (PMB) is an application used by Pando game and software publishers to ensure safe, complete and speedy downloads of large files.",
            "pandora": "Pandora is a music streaming and automated music service",
            "pcanywhere": "PC Anywhere is a suite of computer programs.",
            "pop3": "POP3 is a client/server standard protocol for receiving e-mails.",
            "pops": "Post Office Protocol is an application-layer Internet standard protocol used by local e-mail clients to retrieve e-mail from a remote server over a TCP/IP connection.",
            "postgresql": "PostgreSQL is an object-relational database management system (ORDBMS) ",
            "pplive": "PPLIVE is an application intended to watch TV in peer-to-peer. ",
            "ppstream": "PPStream protocol provides audio and video streaming. It is based on bittorent (peer-to-peer) technology. ",
            "pptp": "Point-to-Point Tunneling Protocol is a method for implementing virtual private networks, with many known security issues.",
            "qq": "QQ is the most popular free instant messaging computer program in China.",
            "qqlive": "QQLive is an application intended to watch TV in Peer-to-Peer mode.",
            //            "quake": "Quake is a video game.",
            "quic": "QUIC is an experimental protocol, and it supports a set of multiplexed connections over UDP.",
            //            "quickplay": "Quickplay provides services for premium multi-screen video to IP-connected devices.",
            //            "quicktime": "QuickTime is an extensible multimedia framework developed by Apple Inc.",
            "radius": "RADIUS is an AAA protocol that manages network access. ",
            "rdp": "RDP allows a personal computer to be used remotely.",
            //            "realmedia": "RealMedia is a proprietary multimedia container format created by RealNetworks.",
            "redis": "Redis is an open-source in-memory database project implementing a distributed, in-memory key-value store with optional durability.",
            "remotescan": "RemoteScan is the most stable and seamless solution for document scanning and image acquisition within complex network environments.",
            "rsync": "Rsync is a protocol used by various services performing updates.",
            "rtcp": "RTCP provides out-of-band statistics and control information for an RTP session.",
            "rtmp": "Real-Time Messaging Protocol (RTMP) used for streaming audio, video and data over the Internet, between a Flash player and a server.",
            "rtp": "The Real-time Transport Protocol is a network protocol for delivering audio and video over IP networks.",
            "rtsp": "The Real Time Streaming Protocol is a network control protocol designed for use in entertainment and communications systems to control streaming media servers. ",
            "rx": "no description found",
            "sap": "Session Announcement Protocol (SAP) is an experimental protocol for broadcasting multicast session information.",
            "sctp": "SCTP (Stream Control Transmission Protocol) is a protocol designed to transport PSTN signaling messages over IP networks.",
            "sflow": "sFlow is an industry standard technology for monitoring high speed switched networks.",
            "shoutcast": "SHOUTcast DNAS is cross-platform proprietary software for streaming media over the Internet. ",
            "sina(weibo)": "Sina Weibo is a Chinese microblogging website.",
            "sip": "SIP communication protocol is used for signaling and controlling communication sessions in applications of internet telephony for voice/video calls.",
            //            "skyfile_postpaid": "SkyFile Mail is a free messaging and compression tool that allows users to send reliable and cost-effective emails, e-faxes and SMS messages. ",
            //            "skyfile_prepaid": "SkyFile Mail is a free messaging and compression tool that allows users to send reliable and cost-effective emails, e-faxes and SMS messages. ",
            //            "skyfile_rudics": "SkyFile Mail uses Iridium Rudics to boost connection quality, service availability and bandwidth",
            "skype": "Skype is a telecommunications application providing video and voice chat over the internet to different devices and regular telephones.",
            "slack": "Slack is a cloud based set of tools and services.",
            //            "smb": "Stands for \"Server Message Block.\" SMB is a network protocol used by Windows-based computers that allows systems within the same network to share files.",
            "smtp": "Simple Mail Transfer Protocol is an Internet standard for electronic mail transmission. ",
            "smtps": "SMTPS is used to secure SMTP at the transport layer. ",
            "snapchat": "Snapchat is an image messaging and multimedia mobile application",
            "snmp": "Simple Network Management Protocol (SNMP) is the protocol governing network management and the monitoring of network devices and their functions.",
            "socks": "Socket Secure is an Internet protocol that exchanges network packets between a client and server through a proxy server. ",
            //            "socrates": "SOCRATES is a software system for testing correctness of implementations of IP routing protocols such as RIP, OSPF and BGP.",
            "sopcast": "Sopcast is a video streaming service based on a peer-to-peer protocol.",
            "soulseek": "Soulseek is a peer-to-peer file-sharing network and application",
            "spotify": "Spotify is a music, podcase and video streaming service.",
            "ssdp": "Simple Service Discovery Protocol (SSDP) provides a mechanism whereby network clients can discover desired network services",
            "ssh": "SSH is a cryptographic network protocol for operating network services securely.",
            "ssl": "HTTPS is a standard security protocol for establishing encrypted links between server and a browser in an online coommunication.",
            "ssl_no_cert": "no description found",
            "starcraft": "Starcraft is a video game.",
            "stealthnet": "Stealthnet is a file sharing application between two or more hosts.",
            "steam": "Steam offers digital rights management, multiplayer gaming, video streaming and social networking services. ",
            "stun": "Simple Traversal of UDP through NATs (Network Address Translation) is a protocol for assisting devices behind a NAT firewall or router with their packet routing.",
            "syslog": "Syslog is a standard for message logging.",
            "teamspeak": "TeamSpeak is proprietary voice-over-Internet Protocol software for audio communications.",
            "teamviewer": "TeamViewer is a software for remote control, desktop sharing, online meetings, web conferencing and file transfers.",
            "telegram": "Telegram is a cloud based instant messaging service.",
            "telnet": "Telnet is a protocol that provides bidirectional interactive text oriented communication facilities using virtual terminal connection. ",
            "teredo": "Teredo is a transition technology that gives full IPv6 connectivity for IPv6-capable hosts that are on the IPv4 Internet.",
            "tftp": "Trivial File Transfer Protocol (TFTP) is an Internet software utility for transferring files that is simpler to use than the File Transfer Protocol (FTP) but less capable.",
            "thunder": "Thunder is a Chinese multi-protocol download manager.",
            "tor": "Tor is a freeware enabling anonymous communication.",
            "truphone": "Truphone is a global mobile network that operates its service internationally. ",
            "tuenti": "Tuenti protocol plug-in classifies the http traffic to the host tuenti.com.",
            "tvants": "TVants is a peer-to-peer TV software application that provides music, movies, news and more.",
            "tvuplayer": "TVUPlayer is an application intended to watch TV in peer_to-peer.",
            "twitch": "Twitch is a live streaming video platform.",
            "twitter": "Twitter is an online news and social networking service.",
            "ubntac2": "No description found",
            "ubuntuone": "Ubuntuone is an OpenID-based single sign-on service for all services and sites related to Ubuntu.",
            "unencryped_jabber" : "Jabber is the instant messaging service based on extensible messaging and presence protocol.",
            //            "unknown": "No description found",
            "upnp": "Universal Plug and Play (UPnP) is a standard that uses Internet and Web protocol s to enable devices .",
            "usenet": "Usenet is a worldwide distributed discussion system available on computers.",
            "vevo": "Vevo is a video hosting service.",
            "vhua": "NO description found",
            "viber": "Viber is a cross-platform instant messaging and VoIP application.",
            "vmware": "VMWare protocol allows network interfaces and remote access to a virtual machine.",
            "vnc": "VNC is a graphical desktop sharing system.",
            "vrrp": "Virtual Router Redundancy Protocol (VRRP) protocol eliminates the single point of failure inherent in the static default routed environment.",
            "warcraft3": "Warcraft 3 is a video game.",
            "waze": "Waze is a GPS maps & traffic navigation software.",
            "webex": "Webex is a radio broadcasting service.",
            //            "webm": "WebM is a media file format.",
            "whatsapp": "Whatsapp is a freeware cross-platform instant messaging service using the internet.",
            "whatsappvoice": "Whatsapp Voice is a service for telephony using the internet.",
            "whois-das": "The Whois service provides a way for the public to lookup information about registered data. The DAS provides a way for the public to check whether a domain can be registered.",
            "wikipedia": "Wikipedia is a free online encyclopedia.",
            //            "windowsmedia": "This protocol plug-in classifies the http traffic to the host windowsmedia.com.",
            "windowsupdate": "Windows Update is a service by Microsoft for providing updates for windows components.",
            "worldofkungfu": "World Of Kung Fu is a 3D martial arts online game.",
            "worldofwarcraft": "World of War Craft is a multiplayer online role playing game.",
            "xbox": "Xbox is a home video game console by Microsoft.",
            "xdmcp": "XDMCP protocol addresses the issue of discovering and authenticating available X servers on the LAN.",
            "yahoo": "Yahoo is a web services provider.",
            "youtube": "Youtube is a video sharing website.",
            "zattoo": "Zattoo is website and a set of peer-to-peer TV applications.",
            
            "zeromq": "zmq-prebuilt simplifies creating communications for a Node.js application by providing well-tested",
            "dnscrypt" : "Dnscrypt authenticates, encrypts, and optionally anonymizes communication between a DNS client and DNS resolver.",
            "bjnp" : "BJNP is a LAN service discovery protocol used by Canon printers and scanners.",
            "ookla" : "Ookla provides speed test and network performance data.",
            "signal" : "Signal is a non-federated cryptographic protocol used for end-to-end encryption for voice calls and instant messaging.",
            "nestlogsink" : "Proprietary binary protocol used by Google Nest for logging data for Nest devices.",
            "http_activesync" : "Protocol used for synchronization of email, contacts, calendar, tasks, and notes from a messaging server to a smartphone or device.",
            "bloomberg" :    "Protocol used by financial data and news distribution platform.",
            "smbv3" :    "Server Message Block protocol used in windows client server environments.",
            "ntop" :    "Network detection protocol.",
            "mining" :    "Protocol used for pooling resources by cryptocurrency miners.",
            "nintendo" :    "Nintendo gaming console platform.",
            "amqp" :    "Advanced Messaging Queing Protocol used by messaging platforms and services.",
            "genericprotocol" :    "A generic communication protocol.",
            "checkmk" :    "Checkmk is a tool for infrastructure and application monitoring.",
            "lisp" :    "Locator ID Separation Protocol is a network architecture and protocol.",
            "playstation" :    "Sony Playstation gaming protocol.",
            "targus_dataspeed" : "Dataspeed    Targus dataspeed protocol.",
            "memcached" :    "General purpose distributed memory-caching system.",
            "smpp" :    "Short Message Peer to Peer Protocol.",
            "iflix" :    "Video streaming service.",
            "googleservices" :    "Google search services and protocols.",
            "smbv1" :    "Server Message Block protocol used in windows client server environments.",
            "googledrive" :    "Google cloud storage for storing documents.",
            "csgo" :    "Counterstrike Global Offensive protocol.",
            "messenger" :    "Microsoft instant messenger.",
            "skypecall" :    "Skype VOIP call protocol.",
            "applepush" :    "Apple Push Message Services for message notification on iOS devices.",
            "linkedin" :    "LinkedIn social media.",
            "amazonvideo" :    "Amazon streaming video service.",
            "youtubeupload" :    "YouTube streaming video.",
            "ajp" :    "Apache JServ Protocol for proxy.",
            "soundcloud" :    "Streaming music service.",
            "googleplus"    : "GooglePlus social network by Google.",
            "playstore" :    "Android app store for content distribution.",
            "pastebin" :    "Website where you can store text online for a set period of time.",
            "modbus" :     "Communication protocol for use with programmable logic controllers (PLC).",
            "iec60870" :    "Communication protocol for sending data between two systems.",
            "zabbix" :    "Open source monitoring software tool for use with XMPP protocol.",
            "websocket" :    "Websocket enables two-way communication between a client and server.",
            "s7comm" :    "Siemens proprietary protocol for communication between S7 PLCs.",
            "fix" :    "Financial Information Exchange protocol is an open protocol for exchange of financial information.",
            "facebookzero" :    "Protocol that enables users to access Facebook services without incurring any charges.",
            "tinc" :    "Open source self-routing, mesh networking protocol.",
            "diameter" :    "Diameter is used to exchange authentication, authorization, and accounting information in LTE and IMS networks.",
            "wechat" :    "Multi-purpose messaging, social media, and mobile payment app developed by Tencent.",
            "github" :    "Online repository to store data, code, and files.",
            "teams" :    "Communication and collaboration platfrom by Microsoft.",
            "whatsappfiles" :    "WhatsApp communication app.",
            "dnp3" : "Distributed Network Protocol 3 used by water and electric utility companies.",
            "applestore" :    "Apple store app.",
            "tiktok" :    "Video sharing social media network developed by ByteDance.",
            "someip" :    "Automotive and embedded communication protocol.",
            "capwap" :  "Control and Provisioning of Wireless Access Points is used to configure wireless access points."
        ]
        
    }
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        
        //        account_label_tabbar.textColor = UIColor(red: 0/255, green: 174/255, blue: 239/255, alpha: 1.0)
        //        var image: UIImage = UIImage(named: "tab_home_selected")!
        //        account_img_tabbar.image = image
        
        if(self.checkPausedTimeAsAndroid()) {
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
        
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
}
