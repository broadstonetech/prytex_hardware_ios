//
//  PreMutedInnerCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 19/12/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class PreMutedInnerCell: UITableViewCell {

    @IBOutlet weak var src_ip: UILabel!
    @IBOutlet weak var dest_ip: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
