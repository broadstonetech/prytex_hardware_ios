//
//  PreMutedOuterCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 19/12/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

protocol PreMutedHeaderTableViewCellDelegate {
    func didSelectUserHeaderTableViewCell(isShowDetails: Bool, UserHeader: PreMutedOuterCell)
}

class PreMutedOuterCell: UITableViewCell { //}, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var timestamp: UILabel!
    @IBOutlet weak var actions_label: UILabel!
//    @IBOutlet weak var table_view: UITableView!
    
    @IBOutlet weak var headerButtonOutlet: UIButton!
    var delegate : PreMutedHeaderTableViewCellDelegate?

    
    //variables
    var preMutedAlert = PreMutedAlertModel()

    override func draw(_ rect: CGRect) {
        
//        self.table_view.delegate = self
//        self.table_view.dataSource = self
//        var frame = self.table_view.frame
//        frame.size.height = self.table_view.contentSize.height
//        self.table_view.frame = frame
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
    @IBAction func selectedHeader(sender: UIButton) {
        if sender.tag == 0{
            sender.tag = 1
            delegate?.didSelectUserHeaderTableViewCell(isShowDetails: true, UserHeader: self)
        }else{
            sender.tag = 0
            delegate?.didSelectUserHeaderTableViewCell(isShowDetails: false, UserHeader: self)
            
        }
        
    }
}
