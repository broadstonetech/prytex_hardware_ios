//
//  ChangePassword.swift
//  BTech
//
//  Created by Ahmed Akhtar on 15/11/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class ChangePassword: Parent, UITextFieldDelegate {
    
    
    var setNewPassword : Bool?
    //Outlet
    @IBOutlet weak var oldPasswordField: UITextField!
    
    @IBOutlet weak var newPasswordField: UITextField!
    
    @IBOutlet weak var repeatPasswordField: UITextField!
    
    @IBOutlet weak var btnOldPassword: UIButton!
    @IBOutlet weak var btnNewPassword: UIButton!
    @IBOutlet weak var btnRepeatNewPassword: UIButton!
    
    
    class func ChangePasswordVC() -> ChangePassword {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChangePassword") as! ChangePassword
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Change Password"
        
        //if its modify password
        self.oldPasswordField.isSecureTextEntry = true
        self.newPasswordField.isSecureTextEntry = true
        self.repeatPasswordField.isSecureTextEntry = true
        
        //check if its set new password
        if(setNewPassword == true)
        {
            self.oldPasswordField.isSecureTextEntry = false
            self.oldPasswordField.placeholder = "Username"
        }
        
        repeatPasswordField.delegate = self
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField === repeatPasswordField)
        {
            repeatPasswordField.resignFirstResponder()
            repeatPasswordField.endEditing(true)
            return true
        }
        return true
    }
    
    func callServiceForSettingNewPassword()
    {
        NetworkHelper.sharedInstance.error401 = false
        //call service for setting new password
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let plainPass = (newPasswordField.text! as NSString).data(using: String.Encoding.utf8.rawValue)
        
        let base64Pass = plainPass!.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        print("bas364-pass== \(base64Pass)")
        
        let data:Dictionary<String,AnyObject> = ["username": oldPasswordField.text! as AnyObject, "newpassword": base64Pass as AnyObject, "confirmpassword": base64Pass as AnyObject, "accesstoken" : NetworkHelper.sharedInstance.tokenSavedForSettingNewPassword! as AnyObject]
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "data" : data as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.SET_NEW_PASSWORD_API, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                //self.showAlert("Password has been successfully reset. Please login to continue")
                let loginVC = Login.loginVC()
                DispatchQueue.main.async {
                     self.navigationController?.pushViewController(loginVC, animated: true)
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                         self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                         self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async(execute: {
                    self.showAlert(message)
                })
            } else {
                DispatchQueue.main.async(execute: {
                    self.showAlert(ConstantStrings.ErrorText)
                })
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
            
        })
        
    }
    
    
    func callServiceForModifyPassword()
    {
        NetworkHelper.sharedInstance.error401 = false
        //call service for changing password
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        //convert to base64
        let plainPassOld = (oldPasswordField.text! as NSString).data(using: String.Encoding.utf8.rawValue)
        
        let base64PassOld = plainPassOld!.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        print("bas364-Oldpass== \(base64PassOld)")
        
        //convert to base64
        let plainPass = (newPasswordField.text! as NSString).data(using: String.Encoding.utf8.rawValue)
        
        let base64Pass = plainPass!.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        print("bas364-Reppass== \(base64Pass)")
        //convert to base64
        let plainPassCnfrm = (repeatPasswordField.text! as NSString).data(using: String.Encoding.utf8.rawValue)
        
        let base64PassCnfrm = plainPassCnfrm!.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        print("bas364-Confirmpass== \(base64PassCnfrm)")
        
        
        let data = ["old_password": base64PassOld, "new_password": base64Pass , "confirm_password": base64PassCnfrm ]
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "data" : data as AnyObject]
        
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.MODIFY_PASSWORD_API, sendData: params, success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
//                self.showAlert("Password changed successfully.")
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    DispatchQueue.main.async {
                        (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    }
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                DispatchQueue.main.async {
                     let loginVC = Login.loginVC()
                                   self.navigationController?.pushViewController(loginVC, animated: true)
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                //                self.showAlert(message)
                DispatchQueue.main.async(execute: {
                    // work Needs to be done    
                    self.showAlert(message)
                    
                })
            } else {
                DispatchQueue.main.async(execute: {
                    // work Needs to be done
                    self.showAlert(ConstantStrings.ErrorText)
                })
            }
            
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
            
        })
    }
    
    func atleastOneCapitalAlphabetWithString(_ phrase : String)->Bool
    {
        let letters = CharacterSet.uppercaseLetters
        
        let phrase = phrase
        let range = phrase.rangeOfCharacter(from: letters)
        
        // range will be nil if no letters is found
        if range != nil {
            return false
        }
        else {
            return true
        }
    }
    
    func atleastOneSmallAlphabetWithString(_ phrase : String)->Bool
    {
        let letters = CharacterSet.lowercaseLetters
        
        let phrase = phrase
        let range = phrase.rangeOfCharacter(from: letters)
        
        // range will be nil if no letters is found
        if range != nil {
            return false
        }
        else {
            return true
        }
    }
    
    func atleastOneDigitWithString(_ phrase : String)->Bool
    {
        let letters = CharacterSet.decimalDigits
        
        let phrase = phrase
        let range = phrase.rangeOfCharacter(from: letters)
        
        // range will be nil if no letters is found
        if range != nil {
            return false
        }
        else {
            return true
        }
    }
    
    func atleastOneSpecialCharacterWithString(_ phrase : String)->Bool
    {
        
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789")
        if phrase.rangeOfCharacter(from: characterset.inverted) != nil {
            return false
        }
        else{
            return true
        }
        
    }
    
    func showAlertWithTitle(_ title:String,message:String,sender:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        sender.present(alert, animated: true, completion: nil)
        
    }
    
    
    func checkPasswordValidation()->Bool
    {
        if(newPasswordField.text != repeatPasswordField.text)
        {
            self.showAlertWithTitle("Validation Error", message: ConstantStrings.password_mismatch_text, sender: self)
            return false
        }
        if(self.atleastOneCapitalAlphabetWithString((newPasswordField.text)!))
        {
            self.showAlertWithTitle("Validation Error", message: ConstantStrings.password_validation_text, sender: self)
            return false
        }
        if(self.atleastOneSmallAlphabetWithString((newPasswordField.text)!))
        {
            self.showAlertWithTitle("Validation Error", message: ConstantStrings.password_validation_text, sender: self)
            return false
        }
        if(self.atleastOneDigitWithString((newPasswordField.text)!))
        {
            self.showAlertWithTitle("Validation Error", message: ConstantStrings.password_validation_text, sender: self)
            return false
        }
        if(self.atleastOneSpecialCharacterWithString((newPasswordField.text)!))
        {
            self.showAlertWithTitle("Validation Error", message: ConstantStrings.password_validation_text, sender: self)
            return false
        }
        
        
        return true
    }
    
    
    @IBAction func changePassword(_ sender: AnyObject) {
        if (oldPasswordField.text == newPasswordField.text) {
            self.showAlertWithTitle("Validation Error", message: "New Password should not same as old password", sender: self)
        } else {
            if(self.checkPasswordValidation())
            {
                if(setNewPassword == true)
                {
                    self.callServiceForSettingNewPassword()
                }
                else{
                    self.callServiceForModifyPassword()
                }
            }
        }
    }
    
    //hide/show password actions
    
    @IBAction func btnOldPass_pressed(_ sender: Any) {
        if oldPasswordField.isSecureTextEntry {
            oldPasswordField.isSecureTextEntry = false
            //            btn_showHide_pwd.setBackgroundImage(UIImage(named: "hidepass.png"), for: UIControlState.normal)
            btnOldPassword.setBackgroundImage(UIImage(named: "hide_pwd"), for: UIControl.State.normal)
        } else {
            oldPasswordField.isSecureTextEntry = true
            //            btn_showHide_pwd.setBackgroundImage(UIImage(named: "Showpass.png"), for: UIControlState.normal)
            btnOldPassword.setBackgroundImage(UIImage(named: "show_pwd"), for: UIControl.State.normal)
        }
    }
    @IBAction func btnNewPass_pressed(_ sender: Any) {
        if newPasswordField.isSecureTextEntry {
            newPasswordField.isSecureTextEntry = false
            //            btn_showHide_pwd.setBackgroundImage(UIImage(named: "hidepass.png"), for: UIControlState.normal)
            btnNewPassword.setBackgroundImage(UIImage(named: "hide_pwd"), for: UIControl.State.normal)
        } else {
            newPasswordField.isSecureTextEntry = true
            //            btn_showHide_pwd.setBackgroundImage(UIImage(named: "Showpass.png"), for: UIControlState.normal)
            btnNewPassword.setBackgroundImage(UIImage(named: "show_pwd"), for: UIControl.State.normal)
        }
    }
    @IBAction func btnRepeatNewPass_pressed(_ sender: Any) {
        if repeatPasswordField.isSecureTextEntry {
            repeatPasswordField.isSecureTextEntry = false
            //            btn_showHide_pwd.setBackgroundImage(UIImage(named: "hidepass.png"), for: UIControlState.normal)
            btnRepeatNewPassword.setBackgroundImage(UIImage(named: "hide_pwd"), for: UIControl.State.normal)
        } else {
            repeatPasswordField.isSecureTextEntry = true
            //            btn_showHide_pwd.setBackgroundImage(UIImage(named: "Showpass.png"), for: UIControlState.normal)
            btnRepeatNewPassword.setBackgroundImage(UIImage(named: "show_pwd"), for: UIControl.State.normal)
        }
    }
}
