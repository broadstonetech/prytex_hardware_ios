

import UIKit
import Photos
import Firebase
import MessageKit
import FirebaseFirestore
import InputBarAccessoryView

final class ChatViewController: MessagesViewController {
    
  private var isSendingPhoto = false {
    didSet {
      DispatchQueue.main.async {
        self.messageInputBar.leftStackViewItems.forEach { item in
        
//          item.isEnabled = !self.isSendingPhoto
        }
      }
    }
  }
  private let db = Firestore.firestore()
  private var reference: CollectionReference?
  private let storage = Storage.storage().reference()

  private var messages: [chatMessage] = []
  private var messageListener: ListenerRegistration?
  
  private let channel: Channel
    var data  = [String:Any]()
    var chatData  = [String:Any]()
    weak var timer: Timer?
  
  private var channelReference: CollectionReference {
    return db.collection("channels")
  }
  
  deinit {
    timer?.invalidate()
    messageListener?.remove()
    
   // parent.channelReference.remove()
  }

    init(channel: Channel) {
    self.channel = channel
    super.init(nibName: nil, bundle: nil)
    
    title = "Live Support"
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
    
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.isNavigationBarHidden = false
    self.navigationItem.setHidesBackButton(false, animated: true)
    
    let logoutBarButtonItem = UIBarButtonItem(title: "End Chat", style: .done, target: self, action: #selector(endChat))
    self.navigationItem.rightBarButtonItem  = logoutBarButtonItem
    getUserChat()
    timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(getUserChat), userInfo: nil, repeats: true)
    guard let id = channel.id else {
      navigationController?.popViewController(animated: true)

      return
    }

//    reference = db.collection(["channels", id, "thread"].joined(separator: "/"))
    
//    messageListener = reference?.addSnapshotListener { querySnapshot, error in
//      guard let snapshot = querySnapshot else {
//        print("Error listening for channel updates: \(error?.localizedDescription ?? "No error")")
//        return
//      }
//
//      snapshot.documentChanges.forEach { change in
//        self.handleDocumentChange(change)
//      }
//    }
    
    navigationItem.largeTitleDisplayMode = .never
    
    maintainPositionOnKeyboardFrameChanged = true
    messageInputBar.inputTextView.tintColor = .primary
    messageInputBar.sendButton.setTitleColor(.primary, for: .normal)
    messageInputBar.delegate = self
      
      messageInputBar.leftStackView.alignment = .center
      messageInputBar.setLeftStackViewWidthConstant(to: 10, animated: false)
      self.messageInputBar.contentView.cornerRadius = 5
      self.messageInputBar.contentView.borderColor = .gray
      self.messageInputBar.inputTextView.textColor =  UIColor(named: "LabelTxt")
      self.messageInputBar.backgroundView.backgroundColor = UIColor(named: "View")
    
//    InputBarAccessoryViewDelegate.delegate = self
    messagesCollectionView.messagesDataSource = self
    messagesCollectionView.messagesLayoutDelegate = self
    messagesCollectionView.messagesDisplayDelegate = self
    messagesCollectionView.backgroundColor = UIColor(named: "View")
    
//    let cameraItem = InputBarButtonItem(type: .system) // 1
//    cameraItem.tintColor = .primary
//    cameraItem.image = UIImage(named: "portfolio")
//    cameraItem.addTarget(
//      self,
//      action: #selector(cameraButtonPressed), // 2
//      for: .primaryActionTriggered
//    )
//    cameraItem.setSize(CGSize(width: 60, height: 30), animated: false)
    
    
   // self.messageInputBar.contentView.borderWidth = 2
   // messageInputBar.setStackViewItems([cameraItem], forStack: .left, animated: false) // 3
  }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
        timer = nil
    }
    //MARK:- Methods
    
    private func pushUserMsgs(meessage: chatMessage){

//        let ref = self.channelReference.document()
//        let senderid = ref.documentID
        let params: [String: AnyObject] = ["sender_id": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "sender_name": NetworkHelper.sharedInstance.getCurrentUserName() as AnyObject, "firebase_id": channel.id as AnyObject, "sender_msg": meessage.content as AnyObject, "send_by": "client" as AnyObject ]
        let param: [String: AnyObject] = ["data": params as AnyObject]
        print(param)
        NetworkHelper.sharedInstance.postChatRequest(serviceName: ApiUrl.pushUserMsgs, sendData: param as [String : AnyObject], success: { (response) in
            print("response is \(response)")
            DispatchQueue.main.async {
                let jsonData = (response as NSDictionary) as! [String : Any]
                print(jsonData)
//                self.data = response["data"] as! [String : Any]
//                print(self.data)
//                self.parseData(data: self.data)
            }
        }, failure: { (data) in
            print(data)

        })

    }
    
    
    func parseData(data: [String:Any])
    {
        var senderID:String = ""
        var content :String = ""
        var senderName : String = ""
        var created :Date!
        let clientArray = data["client"] as! [[String:Any]]
        let agentArray = data["agent"] as! [[String:Any]]
        
        for item in clientArray{
            senderID = item["sender_id"]! as! String
            content = item["content"]! as! String
            senderName = item["sender_name"]! as! String
            let createdTimeStamp = item["created"] as! TimeInterval
            created = NSDate(timeIntervalSinceReferenceDate: createdTimeStamp) as Date
            let message = chatMessage(content: content,sentDate: created,senderId: senderID,senderName: senderName)
//       //     insertNewMessage(message)
            
            messages.append(message)
        }
    
        for item in agentArray{
            senderID = item["sender_id"]! as! String
            content = item["content"]! as! String
            senderName = item["sender_name"]! as! String
            let createdTimeStamp = item["created"] as! TimeInterval
            created = NSDate(timeIntervalSinceReferenceDate: createdTimeStamp) as Date
            let message = chatMessage(content: content,sentDate: created,senderId: senderID,senderName: senderName)
          // insertNewMessage(message)
            messages.append(message)
        }
        
       
        messages.sort()
        let shouldScrollToBottom = messagesCollectionView.isAtBottom
        
        messagesCollectionView.reloadData()
        
        if shouldScrollToBottom {
          DispatchQueue.main.async {
            self.messagesCollectionView.scrollToBottom(animated: true)
          }
        }
       

    }
  
  // MARK: - Actions
  
    @objc func getUserChat()
    {
        let dict : [String: AnyObject] = ["firebase_id":channel.id as AnyObject]

        let data :Dictionary<String,Any> = ["data": dict as AnyObject]
        print(data)

        NetworkHelper.sharedInstance.postChatRequest(serviceName: ApiUrl.getUserMsgs, sendData: data as [String : AnyObject], success: { (response) in
            DispatchQueue.main.async {
                self.messages.removeAll()
                let status = response["status"] as! Bool
                if status{
                    self.data = response["data"] as! [String : Any]
                    self.chatData = self.data["chat"] as! [String : Any]
                    print(self.chatData)
                    self.parseData(data: self.chatData)
                }
                else{
                    print("something is error")
                }

            }

        }, failure: { (data) in
            print(data)

        })

    }
    
//  @objc private func cameraButtonPressed() {
//    let picker = UIImagePickerController()
////    picker.delegate = self
//
//    if UIImagePickerController.isSourceTypeAvailable(.camera) {
//      picker.sourceType = .camera
//    } else {
//      picker.sourceType = .photoLibrary
//    }
//
//    present(picker, animated: true, completion: nil)
//  }
  
  // MARK: - Helpers
  
    @objc private func endChat() {
      let ac = UIAlertController(title: nil, message: "All your chat will be deleted.Are you sure you want to continue?", preferredStyle: .alert)
      ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        ac.addAction(UIAlertAction(title: "End Chat", style: .destructive, handler: { _ in
             self.deleteChat()
        }))
        present(ac, animated: true, completion: nil)
    }
    private func deleteChat() {
        timer?.invalidate()
//        //first server will save the chat then we will delete from firebase
        let data :Dictionary<String,Any> = ["firebase_id": channel.id! as String]
        print(data)
        NetworkHelper.sharedInstance.postChatRequest(serviceName: ApiUrl.SAVE_CHAT, sendData: data as [String: AnyObject], success: {(response) in
            print(response)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")

            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async {
                    // self.showAlert("Success")
//                    let dictionary = response as [String:Any]
//                    let obj = SaveChatModel(fromDictionary: dictionary)
//                    print(obj.message)
                    NetworkHelper.sharedInstance.saveFirebaseid("")
                    print("chat successfully saved to server from firebase and deleted")
                    self.navigationController?.popViewController(animated: true)
                    
                }

            }

        }, failure: { (data) in
            print(data)

        })
    }
  
//  private func save(_ message: chatMessage) {
//    reference?.addDocument(data: message.representation) { error in
//      if let e = error {
//        print("Error sending message: \(e.localizedDescription)")
//        return
//      }
//    print("message saved successfully")
//      self.messagesCollectionView.scrollToBottom()
//    }
//
//   channelReference.document(channel.id!).updateData(["status":"Unread"])
//
//
//
//  }
//
//  private func insertNewMessage(_ message: chatMessage) {
//    guard !messages.contains(message) else {
//      return
//    }
//
//    messages.append(message)
//    messages.sort()
//
//
//
//
//    let isLatestMessage = messages.firstIndex(of: message) == (messages.count - 1)
//    let shouldScrollToBottom = messagesCollectionView.isAtBottom && isLatestMessage
//
//    messagesCollectionView.reloadData()
//
//    if shouldScrollToBottom {
//      DispatchQueue.main.async {
//        self.messagesCollectionView.scrollToBottom(animated: true)
//      }
//    }
//  }
  
//  private func handleDocumentChange(_ change: DocumentChange) {
//   // print(Message(document:change.document))
//    guard var message = chatMessage(document:change.document) else {
//      return
//    }
//
////    print(change.type)
////    insertNewMessage(message)
////    switch change.type {
////    case .added:
////      if let url = message.downloadURL {
////        downloadImage(at: url) { [weak self] image in
////          guard let `self` = self else {
////            return
////          }
////          guard let image = image else {
////            return
////          }
////
////          message.image = image
////          self.insertNewMessage(message)
////        }
////      } else {
////        insertNewMessage(message)
////      }
////
////    default:
////      break
////    }
//  }
  
//  private func uploadImage(_ image: UIImage, to channel: Channel, completion: @escaping (URL?) -> Void) {
//    guard let channelID = channel.id else {
//      completion(nil)
//      return
//    }
//
//    guard let scaledImage = image.scaledToSafeUploadSize, let data = scaledImage.jpegData(compressionQuality: 0.4) else {
//      completion(nil)
//      return
//    }
//
//    let metadata = StorageMetadata()
//    metadata.contentType = "image/jpeg"
//
//    let imageName = [UUID().uuidString, String(Date().timeIntervalSince1970)].joined()
//    storage.child(channelID).child(imageName).putData(data, metadata: metadata) { meta, error in
//            if (error != nil) {
//
//        } else {
//
//        }
//    }
//  }

//  private func sendPhoto(_ image: UIImage) {
//    isSendingPhoto = true
//
//    uploadImage(image, to: channel) { [weak self] url in
//      guard let `self` = self else {
//        return
//      }
//      self.isSendingPhoto = false
//
//      guard let url = url else {
//        return
//      }
//
//      var message = Message(image: image)
//      message.downloadURL = url
//
//      self.save(message)
//      self.messagesCollectionView.scrollToBottom()
//    }
//  }
  
//  private func downloadImage(at url: URL, completion: @escaping (UIImage?) -> Void) {
//    let ref = Storage.storage().reference(forURL: url.absoluteString)
//    let megaByte = Int64(1 * 1024 * 1024)
//
//    ref.getData(maxSize: megaByte) { data, error in
//      guard let imageData = data else {
//        completion(nil)
//        return
//      }
//
//      completion(UIImage(data: imageData))
//    }
//  }
  
}

// MARK: - MessagesDisplayDelegate

extension ChatViewController: MessagesDisplayDelegate {
  
  func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
    return isFromCurrentSender(message: message) ? .primary : .incomingMessage
  }
 
  
  func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
    let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
    return .bubbleTail(corner, .curved)
  }
  
}

// MARK: - MessagesLayoutDelegate

extension ChatViewController: MessagesLayoutDelegate {
  
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        if isFromCurrentSender(message: message){
            avatarView.backgroundColor = .white
            return avatarView.image = UIImage(named: "userS")
        }
        else{
            avatarView.backgroundColor = .white
            return avatarView.image = UIImage(named: "logo_transparent")
        }
    }
  
  func footerViewSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
    return CGSize(width: 0, height: 8)
  }
  
  func heightForLocation(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
    
    return 0
  }
  
}

// MARK: - MessagesDataSource

extension ChatViewController: MessagesDataSource {
    
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = message.sender.displayName
        if isFromCurrentSender(message: message){
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .right
        return NSAttributedString(string: name, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2)])
        }
        else{
            let supportName = "Support"
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .left
            return NSAttributedString(string: supportName, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption1)])

        }
    }
    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 30
    }
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
    
    func currentSender() -> SenderType {
        let userid = NetworkHelper.sharedInstance.getCurrentUserNamespace()
        let currentSender = senderr(senderId: userid, displayName: NetworkHelper.sharedInstance.getCurrentUserNamespace())
        return currentSender
    }
    
 
  
  func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
    return messages[indexPath.section]
  }
  
    
  
}

// MARK: - MessageInputBarDelegate

extension ChatViewController: InputBarAccessoryViewDelegate {
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        let text = text.trimmingCharacters(in: .newlines)
        print(text)
        let message = chatMessage(content: text)
        pushUserMsgs(meessage: message)
//        insertNewMessage(message)
        inputBar.inputTextView.text = ""
    }
}
//
//    func messageInputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
//    let message = chatMessage(content: text)
//    pushUserMsgs(meessage: message)
//
//
//    inputBar.inputTextView.text = ""
//  }
//
//}

// MARK: - UIImagePickerControllerDelegate

//extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
//
//  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//    picker.dismiss(animated: true, completion: nil)
//
//    if let asset = info[.phAsset] as? PHAsset { // 1
//      let size = CGSize(width: 500, height: 500)
//      PHImageManager.default().requestImage(for: asset, targetSize: size, contentMode: .aspectFit, options: nil) { result, info in
//        guard let image = result else {
//          return
//        }
//
//        self.sendPhoto(image)
//      }
//    } else if let image = info[.originalImage] as? UIImage { // 2
//      sendPhoto(image)
//    }
//  }
//
//  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//    picker.dismiss(animated: true, completion: nil)
//  }
//
//}



// MARK: - UIImagePickerControllerDelegate
//
//extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
//
//  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//    picker.dismiss(animated: true, completion: nil)
//
//    if let asset = info[.phAsset] as? PHAsset { // 1
//      let size = CGSize(width: 500, height: 500)
//      PHImageManager.default().requestImage(for: asset, targetSize: size, contentMode: .aspectFit, options: nil) { result, info in
//        guard let image = result else {
//          return
//        }
//
//        self.sendPhoto(image)
//      }
//    } else if let image = info[.originalImage] as? UIImage { // 2
//      sendPhoto(image)
//    }
//  }
//
//  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//    picker.dismiss(animated: true, completion: nil)
//  }
//
//}
//

