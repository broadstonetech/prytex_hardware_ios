//
//  ChooseDays.swift
//  BTech
//
//  Created by Ahmed Akhtar on 26/12/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class ChooseDays: Parent {

    var days : String = ""
  
    //ibOutlet
    @IBOutlet var mondayCheckBtn: UIButton!
    @IBOutlet var tuesdayCheckBtn: UIButton!
    @IBOutlet var wednesdayCheckBtn: UIButton!
    @IBOutlet var thursdayCheckBtn: UIButton!
    @IBOutlet var fridayCheckBtn: UIButton!
    @IBOutlet var saturdayCheckBtn: UIButton!
    @IBOutlet var sundayCheckBtn: UIButton!
    
    //ibAction
    
    @IBAction func mondayPressed(_ sender: AnyObject) {
        if NetworkHelper.sharedInstance.mondayChecked {
            sender.setImage( UIImage(named:"uncheckbox"), for: UIControl.State())
            NetworkHelper.sharedInstance.mondayChecked = false
        } else {
            sender.setImage(UIImage(named:"checkbox"), for: UIControl.State())
            NetworkHelper.sharedInstance.mondayChecked = true
        }

    }
    @IBAction func tuesdayPressed(_ sender: AnyObject) {
        if NetworkHelper.sharedInstance.tuesdayChecked {
            sender.setImage( UIImage(named:"uncheckbox"), for: UIControl.State())
            NetworkHelper.sharedInstance.tuesdayChecked = false
        } else {
            sender.setImage(UIImage(named:"checkbox"), for: UIControl.State())
            NetworkHelper.sharedInstance.tuesdayChecked = true
        }
    }
    @IBAction func wednesdayPressed(_ sender: AnyObject) {
        if NetworkHelper.sharedInstance.wednesdayChecked {
            sender.setImage( UIImage(named:"uncheckbox"), for: UIControl.State())
            NetworkHelper.sharedInstance.wednesdayChecked = false
        } else {
            sender.setImage(UIImage(named:"checkbox"), for: UIControl.State())
            NetworkHelper.sharedInstance.wednesdayChecked = true
        }
    }
    @IBAction func thursdayPressed(_ sender: AnyObject) {
        if NetworkHelper.sharedInstance.thursdayChecked {
            sender.setImage( UIImage(named:"uncheckbox"), for: UIControl.State())
            NetworkHelper.sharedInstance.thursdayChecked = false
        } else {
            sender.setImage(UIImage(named:"checkbox"), for: UIControl.State())
            NetworkHelper.sharedInstance.thursdayChecked = true
        }
    }
    @IBAction func fridayPressed(_ sender: AnyObject) {
        if NetworkHelper.sharedInstance.fridayChecked {
            sender.setImage( UIImage(named:"uncheckbox"), for: UIControl.State())
            NetworkHelper.sharedInstance.fridayChecked = false
        } else {
            sender.setImage(UIImage(named:"checkbox"), for: UIControl.State())
            NetworkHelper.sharedInstance.fridayChecked = true
        }
    }
    @IBAction func saturdayPressed(_ sender: AnyObject) {
        if NetworkHelper.sharedInstance.saturdayChecked {
            sender.setImage( UIImage(named:"uncheckbox"), for: UIControl.State())
            NetworkHelper.sharedInstance.saturdayChecked = false
        } else {
            sender.setImage(UIImage(named:"checkbox"), for: UIControl.State())
            NetworkHelper.sharedInstance.saturdayChecked = true
        }
    }
    @IBAction func sundayPressed(_ sender: AnyObject) {
        if NetworkHelper.sharedInstance.sundayChecked {
            sender.setImage( UIImage(named:"uncheckbox"), for: UIControl.State())
            NetworkHelper.sharedInstance.sundayChecked = false
        } else {
            sender.setImage(UIImage(named:"checkbox"), for: UIControl.State())
            NetworkHelper.sharedInstance.sundayChecked = true
        }
    }
    
    
    @IBAction func cancelPressed(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func okPressed(_ sender: AnyObject) {
        self.checkDaysSelected()
        print(days)
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ShowDaysNotification"), object: nil);
        })
    }
    
    class func chooseDaysVC() -> ChooseDays {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chooseDays") as! ChooseDays
    }
    
    
    func checkDaysSelected()
    {
        if(NetworkHelper.sharedInstance.mondayChecked)
        {
            days = "Mon,"
        }
        if(NetworkHelper.sharedInstance.tuesdayChecked)
        {
            days = days + "Tue,"
        }
        if(NetworkHelper.sharedInstance.wednesdayChecked)
        {
            days = days + "Wed,"
        }
        if(NetworkHelper.sharedInstance.thursdayChecked)
        {
            days = days + "Thu,"
        }
        if(NetworkHelper.sharedInstance.fridayChecked)
        {
            days = days + "Fri,"
        }
        if(NetworkHelper.sharedInstance.saturdayChecked)
        {
            days = days + "Sat,"
        }
        if(NetworkHelper.sharedInstance.sundayChecked)
        {
            days = days + "Sun,"
        }
        
        let daysString: String = days
        let daysStringFinal = String(daysString.dropLast())
        days = daysStringFinal
        NetworkHelper.sharedInstance.daysSelected = days
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(NetworkHelper.sharedInstance.mondayChecked)
        {
            mondayCheckBtn.setImage( UIImage(named:"checkbox"), for: UIControl.State())
        }
        if(NetworkHelper.sharedInstance.tuesdayChecked)
        {
            tuesdayCheckBtn.setImage( UIImage(named:"checkbox"), for: UIControl.State())
        }
        if(NetworkHelper.sharedInstance.wednesdayChecked)
        {
            wednesdayCheckBtn.setImage( UIImage(named:"checkbox"), for: UIControl.State())
        }
        if(NetworkHelper.sharedInstance.thursdayChecked)
        {
            thursdayCheckBtn.setImage( UIImage(named:"checkbox"), for: UIControl.State())
        }
        if(NetworkHelper.sharedInstance.fridayChecked)
        {
            fridayCheckBtn.setImage( UIImage(named:"checkbox"), for: UIControl.State())
        }
        if(NetworkHelper.sharedInstance.saturdayChecked)
        {
            saturdayCheckBtn.setImage( UIImage(named:"checkbox"), for: UIControl.State())
        }
        if(NetworkHelper.sharedInstance.sundayChecked)
        {
            sundayCheckBtn.setImage( UIImage(named:"checkbox"), for: UIControl.State())
        }

        
    }

}
