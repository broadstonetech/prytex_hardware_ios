//
//  ChooseProtocols.swift
//  BTech
//
//  Created by Ahmed Akhtar on 06/01/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class ChooseProtocols: Parent {

    //iboutlet
    @IBOutlet var chooseProtocolTableView: UITableView!
    //ibaction
    @IBAction func okPressed(_ sender: AnyObject) {
    
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ShowProtocolsNotification"), object: nil);
        })
        
        
    
    }
    
    class func chooseProtocolsVC() -> ChooseProtocols {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chooseProtocols") as! ChooseProtocols
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //table view delegates
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NetworkHelper.sharedInstance.protocolsMainArray.count
    }
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let chooseProtocolCell =  self.chooseProtocolTableView.dequeueReusableCell(withIdentifier: "protocols") as! ProtocolCell
        let model : ProtocolModel = NetworkHelper.sharedInstance.protocolsMainArray[indexPath.row]
        chooseProtocolCell.protocolName.text = model.title
        chooseProtocolCell.checkedBtn.tag = indexPath.row
        if(model.checked == true)
        {
            chooseProtocolCell.checkedBtn.setImage( UIImage(named:"checkbox"), for: UIControl.State())
        }
        else{
            chooseProtocolCell.checkedBtn.setImage( UIImage(named:"uncheckbox"), for: UIControl.State())
        }

        return chooseProtocolCell
        
        
    }

    
    
}
