//
//  ActivePortsVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 27/12/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class ActivePortsVC: Parent, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var hostname_label: UILabel!
    @IBOutlet weak var host_ip_label: UILabel!
    @IBOutlet weak var table_view: UITableView!
    
    @IBOutlet weak var hostname_defaultPwds: UILabel!
    @IBOutlet weak var hostip_deaultPwds: UILabel!
    @IBOutlet weak var table_view_default_pwds: UITableView!
    @IBOutlet weak var open_ports_nav_btn: UIButton!
    @IBOutlet weak var default_pwds_nav_btn: UIButton!
    @IBOutlet weak var open_ports_view: UIView!
    @IBOutlet weak var default_pwds_view: UIView!
    
    @IBOutlet weak var no_data_found_label: UILabel!
    
    //MARK: -Class Variables
    var titleText:String = ""
    var hostname:String = ""
    var hostIp:String = ""
    var activePort = ConnectedDevicesModal()
    let rightButtonBar = UIButton()
    
    
    class func activePortsVC() -> ActivePortsVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "active_ports_vc") as! ActivePortsVC
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleText = "Risk Profile"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.title = titleText
        
        self.open_ports_nav_btn.backgroundColor = UIColor(named: "UpperTabSelected")
        self.default_pwds_nav_btn.backgroundColor =  UIColor(named: "ViewPatchB")
        
        hostname_label.text = hostname
        host_ip_label.text = hostIp
        
        hostname_defaultPwds.text = hostname
        hostip_deaultPwds.text = hostIp
        // Do any additional setup after loading the view.
        
        createRightButtonNavigationBar()
        table_view.delegate = self
        table_view.dataSource = self
        table_view_default_pwds.delegate = self
        table_view_default_pwds.dataSource = self
        
        if activePort.arrayPorts.count == 0 {
            open_ports_view.isHidden = true
            default_pwds_view.isHidden = false
            updateTopBarColors(isOpenPorts: false)
            //change topbar title colors selection
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
    //    Analytics.setScreenName(titleText, screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Titletxt", AnalyticsParameterScreenClass: screenClass])
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createRightButtonNavigationBar()
    {
        
        rightButtonBar.setImage(UIImage(named: "info"), for: UIControl.State())
        rightButtonBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        rightButtonBar.addTarget(self, action: #selector(topRightButtonBar), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = rightButtonBar
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        //create custom
    }
    @objc func topRightButtonBar()
    {
        self.showAlert(ConstantStrings.open_ports_screen_txt)
    }
    
    //Mark: - NavBar Actions
    @IBAction func open_ports_pressed(_ sender: Any) {
        
        no_data_found_label.isHidden = true
        no_data_found_label.text = ConstantStrings.no_open_ports_discoverd
        if activePort.arrayPorts.count > 0 {
            open_ports_view.isHidden = false
        }else{
            open_ports_view.isHidden = true
            no_data_found_label.isHidden = false
        }
        default_pwds_view.isHidden = true
        updateTopBarColors(isOpenPorts: true)
    }
    
    @IBAction func default_pwds_pressed(_ sender: Any) {
        no_data_found_label.isHidden = true
        no_data_found_label.text = ConstantStrings.no_default_pwds_discoverd
        
        if activePort.arrayDefaultPasswords.count > 0 {
            default_pwds_view.isHidden = false
        }else{
            no_data_found_label.isHidden = false
            default_pwds_view.isHidden = true
        }
        open_ports_view.isHidden = true
        updateTopBarColors(isOpenPorts: false)
    }
    
    func updateTopBarColors(isOpenPorts : Bool) {
        if isOpenPorts {
            self.open_ports_nav_btn.setTitleColor(CozyLoadingActivity.Settings.WhiteColor, for: .normal)
            self.default_pwds_nav_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
            
            self.open_ports_nav_btn.backgroundColor = UIColor(named: "UpperTabSelected")
            self.default_pwds_nav_btn.backgroundColor =  UIColor(named: "ViewPatchB")
        }
        else{
            self.default_pwds_nav_btn.setTitleColor(CozyLoadingActivity.Settings.WhiteColor, for: .normal)
            self.open_ports_nav_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
            
            self.default_pwds_nav_btn.backgroundColor = UIColor(named: "UpperTabSelected")
            self.open_ports_nav_btn.backgroundColor =  UIColor(named: "ViewPatchB")
            
        }
    }
    
    //Mark: - Tableview delegates
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 40
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("rowSelected\(indexPath.row)")
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == table_view {
            return activePort.arrayPorts.count
        }else{
            return activePort.arrayDefaultPasswords.count
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  self.table_view.dequeueReusableCell(withIdentifier: "active_port_cell") as! ActivePortCell
      
        let cellDefaultPwds =  self.table_view_default_pwds.dequeueReusableCell(withIdentifier: "default_device_cell") as! DefaltDevicesCell
        
        if  tableView == table_view {
            
            let item = activePort.arrayPorts[indexPath.row]
            cell.port_number_label.text = item["port"]
            cell.port_name_label.text = item["service_name"]
            
            return cell
        }else{
            
            let item = activePort.arrayDefaultPasswords[indexPath.row]
            let productName = item["productname"] as? String
            if productName?.range(of: "<null>") != nil || productName?.range(of: "nil") != nil {
                cellDefaultPwds.device_model.text = "!=nill"
            }else{
                cellDefaultPwds.device_model.text = productName//"==nil" //title as! String
            }
            
            let userName = item["username"] as? String
            if userName?.range(of: "<null>") != nil || userName?.range(of: "nil") != nil {
                cellDefaultPwds.device_username.text = "!=nill"
            }else{
                cellDefaultPwds.device_username.text = userName//"==nil" //title as! String
            }
            
            let title = item["password"] as? String
            if title?.range(of: "<null>") != nil || title?.range(of: "nil") != nil {
                cellDefaultPwds.device_password.text = "!= nill"
            }else{
                cellDefaultPwds.device_password.text = title//"==nil" //title as! String
            }
            
            return cellDefaultPwds
            
        }
    }
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        
        //        account_label_tabbar.textColor = UIColor(red: 0/255, green: 174/255, blue: 239/255, alpha: 1.0)
        //        var image: UIImage = UIImage(named: "tab_home_selected")!
        //        account_img_tabbar.image = image
        
        if(self.checkPausedTimeAsAndroid()){
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
      
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
