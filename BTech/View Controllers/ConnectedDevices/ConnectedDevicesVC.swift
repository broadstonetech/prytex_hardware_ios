//
//  ConnectedDevicesVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 05/07/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import DropDown
import FirebaseAnalytics

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    
    switch (lhs, rhs) {
        
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    
    switch (lhs, rhs) {
        
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}

class ConnectedDevicesVC: Parent ,UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate{
    
    // MARK: - Class Outlets
    
    @IBOutlet weak var table_view: UITableView!
    
    var textField : UITextField!
    var currentIndex = -1
    // MARK: - Class Variables
    var titleText:String = ""
    let dropDown = DropDown()
    var isChangeUserName:Bool = false
    var isChangeOS:Bool = false
    var devicesList: [ConnectedDevicesModal] = [ConnectedDevicesModal]()
    let refreshControl = UIRefreshControl()
    var isConnectedDevicesVisible : Bool = false
    var isActiveDevicesVisible : Bool = false
    var isUserNameEmpty : Bool = false
    var isHostNameEmpty : Bool = false
    var isHostIPEmpty : Bool = false
    var isHostOSEmpty : Bool = false
    var isHostCategoryEmpty : Bool = false
    let rightButtonBar = UIButton()
    var DEVICE_IP : String = ""
    
    //    @IBOutlet weak var tabs_view: UIView!
    @IBOutlet weak var btn_cnctd_devices_tab: UIButton!
    @IBOutlet weak var btn_active_devices_tab: UIButton!
    
    
    class func connectedDeviceVc() -> ConnectedDevicesVC{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "connected_devices") as! ConnectedDevicesVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table_view.delegate = self
        table_view.dataSource = self
        isConnectedDevicesVisible = true
        //        if #available(iOS 11.0, *) {
        //                table_view.insetsContentViewsToSafeArea = true;
        //        }
        
        refreshControl.addTarget(self, action: #selector(refreshCnctdHosts), for: .valueChanged)
        table_view.refreshControl = refreshControl
        DispatchQueue.main.async(execute: {
            self.loadData()
        })
        self.titleText = "Connected Devices"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.title = titleText
        self.table_view.rowHeight = UITableView.automaticDimension
        table_view.estimatedRowHeight = 300
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
       // Analytics.setScreenName(titleText, screenClass: screenClass)
        
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "titletxt", AnalyticsParameterScreenClass: screenClass])
    }
    
    @objc func refreshCnctdHosts(refreshControl: UIRefreshControl) {
        self.loadData()
        // somewhere in your code you might need to call:
        UIRefreshControl.animate(withDuration: 1, animations: {
            refreshControl.endRefreshing()
            let point = CGPoint(x: 0,y :0) // CGFloat, Double, Int
            
            self.table_view.contentOffset = point;
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.titleText = "Connected Devices"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.navigationItem.title = titleText
        createRightButtonNavigationBar()
       
    
        if NetworkHelper.sharedInstance.isInternetAvailable() {
            DEVICE_IP = ""
            guard let IP : String = NetworkHelper.sharedInstance.getIP() else {
                    print("cellular data")
                return
            }

            print("reachable via internet \(IP)")
            DEVICE_IP = IP
        }else{
            DEVICE_IP = ""
            print("no internet found")
        }
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createRightButtonNavigationBar()
    {
        
        rightButtonBar.setImage(UIImage(named: "info"), for: UIControl.State())
        rightButtonBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        rightButtonBar.addTarget(self, action: #selector(topRightButtonBar), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = rightButtonBar
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        //create custom
    }
    @objc func topRightButtonBar()
    {
        self.showAlert(ConstantStrings.connectedDevices_screen_text)
    }
    
    //Mark: - Tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return devicesList.count
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1//
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        table_view.isUserInteractionEnabled = true
        if NetworkHelper.sharedInstance.isDmoatOnline {
            let position = indexPath.row
            currentIndex = position
            DispatchQueue.main.async {
                self.showActionSheet(position : position)
            }
        }else{
            showAlert(ConstantStrings.dmoatOffline)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  self.table_view.dequeueReusableCell(withIdentifier: "Connected_devices_cell_new") as! Connected_devices_cell
        
        let item = self.devicesList[indexPath.row]
        currentIndex = indexPath.row
        cell.btn_activeports.tag = currentIndex
        cell.blockDeviceTest.tag = currentIndex
        cell.rename_device.tag = currentIndex
        
        let userName = item.getUsername_cd()
        let hostName = item.getHostName_cd()
        let hostIp = item.getHostIP_cd()
        let hostOS = item.getHostOs()
        let category = item.getdeviceType()
        //        let score = item.getScore()
        
        if (category == "Router"){
            cell.block_btn_width.constant = 0
            cell.block_btn_view.isHidden = true
        }else{
            cell.block_btn_width.constant = 80
            cell.block_btn_view.isHidden = false
        }
        if userName == "Unknown" {
            cell.username_view.isHidden = true
        }else{
            cell.username_view.isHidden = false
            cell.user_name.text = userName
        }
        
        //        if hostName == "Unknown" {
        //            cell.hostname_view.isHidden = true
        //        }else{
        //            cell.hostname_view.isHidden = false
        //            cell.host_name.text = hostName
        //        }
        cell.host_name.text = hostName
        
        cell.host_ip.text = item.getHostIP_cd()
        
        if hostOS == "Unknown" {
            cell.os_view.isHidden = true
        }else {
            cell.os_view.isHidden = false
            cell.host_os.text = item.getHostOs()
        }
        
        if category == "Unknown"{
            cell.category_view.isHidden = true
        }else{
            cell.category_view.isHidden = false
            cell.device_category.text = item.getdeviceType()
        }
        
        if  item.arrayPorts.count > 0 || item.arrayDefaultPasswords.count > 0{
            cell.score_view.isHidden = false
        }else{
            cell.score_view.isHidden = true
        }
            cell.default_devices_view.isHidden = true
        let time = item.getEveSec_cd()
        let timeInterval  = TimeInterval(time) // as NSTimeInterval
        //        //Convert to Date
        let date = Date(timeIntervalSince1970: timeInterval)
        //Date formatting
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM-dd-yyyy h:mm:a"
        dateFormatter.timeZone = TimeZone.current //name: "UTC")
        let dateString = dateFormatter.string(from: date)
        
        cell.eve_sec.text = dateString
        
        return cell
        
    }
    
    // MARK: - cell Button's Action
    
    @IBAction func rename_device_pressed(_ sender: AnyObject) {
        //show popup with textview
        let  renameBtn = sender as! UIButton
        currentIndex = renameBtn.tag
        print("selected index== \(currentIndex)")
        showEditAlert()
    }
    
    @IBAction func block_presed(_ sender: AnyObject) {
        let  renameBtn = sender as! UIButton
        currentIndex = renameBtn.tag
        print("selected index== \(currentIndex)")
        let device_categry = self.devicesList[currentIndex].getdeviceType()
        if (device_categry == "Router"){
            showAlert(ConstantStrings.router_no_action)
        }else{
            showBlockAlert(position: 0) //0 is dummy id, we have to pass item position
        }
    }
    
    @IBAction func active_ports_pressed(_ sender: Any) {
        let activePortBtn = sender as! UIButton
        currentIndex = activePortBtn.tag
        print("ActiveportIndex== \(currentIndex)")
        let alertVC = ActivePortsVC.activePortsVC()
        alertVC.activePort = self.devicesList[currentIndex]
        alertVC.hostname = devicesList[currentIndex].getHostName_cd()
        alertVC.hostIp = devicesList[currentIndex].getHostIP_cd()
        print("ActiveportIndex== \(devicesList[currentIndex].getHostIP_cd())")
        self.navigationController?.pushViewController(alertVC, animated: true)
    }
    //
    
    // MARK: - Class Function
    
    func showBlockAlert(position : Int) {
        let alertController = UIAlertController(title: "Prytex", message: ConstantStrings.blockDevice, preferredStyle: .alert)
        
        // Create the actions
        let osAction = UIAlertAction(title: "Block", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("block pressed")
            self.blockDevice(position)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("cancel Pressed")
        }
        // Add the actions
        alertController.addAction(osAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showBlockAlertOwnDevice(position : Int) {
        let alertController = UIAlertController(title: "Prytex", message: ConstantStrings.blockOwnDevice, preferredStyle: .alert)
        
        // Create the actions
        let osAction = UIAlertAction(title: "Block", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("block pressed")
            self.blockDevice(position)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("cancel Pressed")
        }
        // Add the actions
        alertController.addAction(osAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func showEditAlert() {
        let alertController = UIAlertController(title: "Prytex", message: "Rename the device name or operating system label.", preferredStyle: .alert)
        
        // Create the actions
        let osAction = UIAlertAction(title: "Operating System", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.isChangeOS = true
            self.isChangeUserName = false
            self.getDeviceName("Change operating system label.", ph: "Enter operating system")
            NSLog("os pressed")
        }
        let userNameAction = UIAlertAction(title: "Device Name", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.isChangeUserName = true
            self.isChangeOS = false
            self.getDeviceName("Change device name label.", ph: "Enter device name")
            NSLog("username Pressed")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            self.isChangeOS = false
            self.isChangeUserName = false
            NSLog("username Pressed")
        }
        
        // Add the actions
        alertController.addAction(osAction)
        alertController.addAction(userNameAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func getDeviceName(_ title:String, ph:String) {
        let alert = UIAlertView()
        alert.delegate = self
        alert.title = title
        alert.addButton(withTitle: "Done")
        alert.alertViewStyle = UIAlertViewStyle.plainTextInput
        alert.addButton(withTitle: "Cancel")
        alert.show()
        
        self.textField = alert.textField(at: 0)
        self.textField!.placeholder = ph
    }
    
    //MARK: - UIviewALert delegate
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 0{
            if !(self.textField.text == nil || self.textField.text == "" || self.textField.text == " " || self.textField.text?.range(of: "") != nil) {
            if isChangeUserName {
                devicesList[currentIndex].setUsername_cd(self.textField.text!)
                
                renameDevice(currentIndex, userName: self.textField.text!, os:devicesList[currentIndex].getHostOs(), webUrl: ApiUrl.CHANGE_USERNAME_API)
            }
            else if isChangeOS {
                devicesList[currentIndex].setHostOs_cd(self.textField.text!)
                
                renameDevice(currentIndex, userName: devicesList[currentIndex].getUsername_cd(), os: self.textField.text!, webUrl: ApiUrl.CHANGE_OS_API)
                
            }
            
            //            self.table_view.reloadData()
            
        }else {
            //empty fiels
                showAlert("Please enter a valid name.")
            }
        }else{
        }
    }
    
    //MARK: web service calls
    func loadData(){
        
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.GET_CNCTDHOST_API, sendData: params, success: { (data) in
            print(data)
            let message = "Request failed.Please try again"
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if((statusCode?.range(of: "200")) != nil)
            {
                if let jsonArr = data["message"] as? [[String:AnyObject]] {
                
                print("Connection Data::\n \(jsonArr)\n:::::::::::::")
                    if(jsonArr.count > 0){
                    self.devicesList.removeAll()
                        DispatchQueue.main.async {
                            self.refreshControl.endRefreshing()
                        }
                        self.fetchData(jsonArr)
                    
                }else{
                    self.showAlert(ConstantStrings.no_connected_devices)
                }
            }
            }
                
            else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                    //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                    //clear nsuserdefaults
                    NetworkHelper.sharedInstance.clearUserData()
                    //delete all data
                    AlertModelRealm.deleteAllRecords()
                    PolicyManagementRealm.deleteAllRecords()
                    let loginVC = Login.loginVC()
                    self.navigationController?.pushViewController(loginVC, animated: true)
                
            }
            else{
                self.showAlert(message)
            }
            //
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
    }
    
    //MARK: - block device call
    func blockDevice(_ position : Int) {
        
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let hostId = devicesList[position].getHostId() as AnyObject
        let macAdres = devicesList[position].getMacAdress_cd() as AnyObject
        let ParamList = devicesList[position].getParameters_cd() as AnyObject
        let eveSec = devicesList[position].getEveSec_cd() as AnyObject
        let hostIP = devicesList[position].getHostIP_cd() as AnyObject
        let hostName = devicesList[position].getHostName_cd() as AnyObject
        let hostID = devicesList[position].getHostId() as AnyObject
        let hostOs = devicesList[position].getHostOs() as AnyObject
        let inoutSrc = devicesList[position].getInputSrc() as AnyObject
        let recorDID = devicesList[position].getRecordId() as AnyObject
        
        let response : [String : AnyObject] = ["interval": "" as AnyObject, "type" : "Block" as AnyObject, "mode" : "Forever" as AnyObject]
        
        let data : [String : AnyObject] = ["HOSTID": hostId,"macaddress" : macAdres,"parameters" : ParamList, "eve_sec" : eveSec,"ip" : hostIP,"hostname" : hostName,"record_id" : recorDID,"host_id" : hostID,"OS" : hostOs, "response" : response as AnyObject, "device_category" : inoutSrc ]
        
        let params:Dictionary<String,AnyObject> = ["namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "data" : data as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject,"token" : NetworkHelper.sharedInstance.getCurrentToken() as AnyObject,"request_id" : requestID as AnyObject,"postkey" : NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject]
        
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.BLOCK_DEVICE_API, sendData: params, success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async {
                    self.devicesList.remove(at: position)
                    self.table_view.reloadData()
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                         self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            } else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    //MARK: - rename device call
    func renameDevice(_ position : Int, userName : String, os:String, webUrl : String){
        
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let recorDID = devicesList[position].getRecordId() as AnyObject
        
        let data : [String : AnyObject] = ["record_id" : recorDID, "devicename" : os as AnyObject, "usersname" : userName as AnyObject]
        
        let params:Dictionary<String,AnyObject> = ["namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "data" : data as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject,"token" : NetworkHelper.sharedInstance.getCurrentToken() as AnyObject,"request_id" : requestID as AnyObject,"postkey" : NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: webUrl, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                //                self.table_view.reloadData()
                self.loadData()
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            } else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            } else{
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
            
        })
        
    }
    
    //MARK: - fetch data
    func fetchData(_ jsonArr : [[String : AnyObject]]){
        
        for element in jsonArr {
            let hostId = element["HOSTID"] as! String
            let evesec = element["eve_sec"] as! Int
            let macAdres = element["macaddress"] as! String
            let recordId = element["record_id"] as! String
            let ip = element["ip"] as! String
            
            let os = element["OS"] as! String
            let hostName = element["hostname"] as! String
            let device_category = element["device_category"] as! String
            let parameters = element["parameters"] as! String
            let userName = element["usersname"] as! String
            let score = element["score"] as! Int
            let jsonArrPosrts = element["activeports"] as? [[String:AnyObject]]
            let jsonArrDefaultPwds = element["default_password"] as? [[String:AnyObject]]
            //add into model class
            let device = ConnectedDevicesModal()
            device.setEveSec_cd(evesec)
            device.setHostId_cd(hostId)
            device.setMacAdress_cd(macAdres)
            device.setRecordId(recordId)
            device.setHostIP_cd(ip)
            device.setScore(score)
            device.arrayPorts = jsonArrPosrts! as! [[String : String]]
            if jsonArrDefaultPwds != nil {
                device.arrayDefaultPasswords = jsonArrDefaultPwds! //as! [[String : String]]
            }
            
            if (os.range(of: "") != nil) || os == "unknown" || (os.range(of: "<null>") != nil || os.count < 1) {
                device.setHostOs_cd("Unknown")
            }else{
                device.setHostOs_cd(os)
            }
            if (device_category.range(of: "") != nil) || device_category == "unknown" || (device_category.range(of: "<null>") != nil || device_category.count < 1) {
                device.setdeviceType("Unknown")
            }else{
                device.setdeviceType(device_category)
            }
            
            if (hostName == nil || hostName == "unknown" || (hostName.range(of: "") != nil) || (hostName.range(of: "nul") != nil) || hostName.count < 1) {
                device.setHostName_cd("Unknown")
            }else{
                device.setHostName_cd(hostName)
            }
            
            if (parameters == nil || (parameters.range(of: "") != nil) || (parameters.range(of: "nul") != nil) || parameters.count < 1) {
                device.setParameters_cd("Unknown")
            }else{
                device.setParameters_cd(parameters)
            }
            if (userName == nil || (userName.range(of: "") != nil) || userName == "unknown" || (userName.range(of: "nul") != nil) || userName.count < 1) {
                device.setUsername_cd("Unknown")
            }else{
                device.setUsername_cd(userName)
            }
            
            devicesList.append(device)
            
        }
        
        DispatchQueue.main.async {
             self.refreshControl.endRefreshing()
        }
        //            self.table_view.refreshControl?.endRefreshing()
        self.devicesList.sort(by: self.sorterForFileIDASC)
        DispatchQueue.main.async {
            self.table_view.reloadData()
        }
        
        DispatchQueue.main.async {
              self.refreshControl.endRefreshing()
              self.table_view.refreshControl?.endRefreshing()
        }
      
    }
    
    func sorterForFileIDASC(_ this:ConnectedDevicesModal, that:ConnectedDevicesModal) -> Bool {
        
        let f = this.getHostIP_cd().components(separatedBy: ".")
        let s = that.getHostIP_cd().components(separatedBy: ".")
        print("fLast \(f.last) sLast \(s.last)")
        if f.last?.count > 0 && s.last?.count > 0 {
        return Int(f.last!)! < Int(s.last!)!
        } else {
            return false
        }
        
    }
    
    
    
    ////advance mode cp end
    
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        
        if(self.checkPausedTimeAsAndroid()){
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
        
       
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    //MARK: -Action Sheet
    func showActionSheet(position : Int) {
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        
        let changeOS = UIAlertAction(title: "Rename Operating System", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.isChangeUserName = false
            self.isChangeOS = true
            self.getDeviceName("Rename operating system label.", ph: "Enter operating system")
            
        })
        let changeNeviceName = UIAlertAction(title: "Change Device Name", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.isChangeUserName = true
            self.isChangeOS = false
            self.getDeviceName("Change device name label.", ph: "Enter device name")
            
        })
        let blockDevice = UIAlertAction(title: "Block Device", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            if self.devicesList[position].getHostIP_cd() == self.DEVICE_IP{
                self.showBlockAlertOwnDevice(position: position)
            }else{
            self.showBlockAlert(position: position)
            }
            
        })
        
        let activePorts = UIAlertAction(title: "View Risk Profile", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let alertVC = ActivePortsVC.activePortsVC()
            alertVC.activePort = self.devicesList[position]
            alertVC.hostname = self.devicesList[position].getHostName_cd()
            alertVC.hostIp = self.devicesList[position].getHostIP_cd()
            print("OpenPortIndex== \(self.devicesList[position].getHostIP_cd())")
            self.navigationController?.pushViewController(alertVC, animated: true)
        })
        
        
        let cancelAction = UIAlertAction(title:CozyLoadingActivity.BlockOptions.Cancel.rawValue, style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        //check if input source is connected host
        let device_categry = devicesList[position].getdeviceType()
        if (devicesList[position].arrayPorts.count > 0) || devicesList[position].arrayDefaultPasswords.count > 0{
            
            optionMenu.addAction(changeOS)
            optionMenu.addAction(changeNeviceName)
            if (device_categry == "Router"){
            }else{
                optionMenu.addAction(blockDevice)
            }
            
//            if devicesList[position].arrayDefaultPasswords.count > 0 {
//                optionMenu.addAction(defaultDevicesPwdsAction)
//            }
            optionMenu.addAction(activePorts)
            optionMenu.addAction(cancelAction)
        }else {
            
            optionMenu.addAction(changeOS)
            optionMenu.addAction(changeNeviceName)
            if (device_categry == "Router"){
            }else{
                optionMenu.addAction(blockDevice)
            }
            
        if (devicesList[position].arrayPorts.count > 0) || devicesList[position].arrayDefaultPasswords.count > 0{
                optionMenu.addAction(activePorts)
            }
            optionMenu.addAction(cancelAction)
        }
        
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
}

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */

