//
//  DefaltDeviceCredentialsVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 06/04/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class DefaltDeviceCredentialsVC: Parent {

    
    //outlets
    @IBOutlet weak var hostname_label: UILabel!
    @IBOutlet weak var host_ip_label: UILabel!
    @IBOutlet weak var table_view: UITableView!
    
    //MARK: -Class Variables
    var titleText:String = ""
    var hostname:String = ""
    var hostIp:String = ""
    var defaultCredDevice = ConnectedDevicesModal()
    let rightButtonBar = UIButton()
    
    class func defaltDeviceCredentialsVC() -> DefaltDeviceCredentialsVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "default_credential_vc") as! DefaltDeviceCredentialsVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleText = "Default Passwords"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.navigationItem.title = titleText
        
        hostname_label.text = hostname
        host_ip_label.text = hostIp
        // Do any additional setup after loading the view.
        createRightButtonNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
//        Analytics.setScreenName(titleText, screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "titletxt", AnalyticsParameterScreenClass: screenClass])
    }
       
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createRightButtonNavigationBar()
    {
        
        rightButtonBar.setImage(UIImage(named: "info"), for: UIControl.State())
        rightButtonBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        rightButtonBar.addTarget(self, action: #selector(topRightButtonBar), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = rightButtonBar
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        //create custom
    }
    @objc func topRightButtonBar()
    {
        self.showAlert(ConstantStrings.default_password_screen_txt)
    }
    
    //Mark: - Tableview delegates
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat
    {
        return 33
    }
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("rowSelected\(indexPath.row)")
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return defaultCredDevice.arrayDefaultPasswords.count //.arrayPorts.count
        
    }
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  self.table_view.dequeueReusableCell(withIdentifier: "default_device_cell") as! DefaltDevicesCell
        
        let item = defaultCredDevice.arrayDefaultPasswords[indexPath.row]
//        cell.device_model.text = item["productname"] as! String
//        cell.device_username.text = "username"// item["username"] as! String
//        if let item["password"] as! String {
//            cell.device_password.text = ""
//        }else{
//        cell.device_password.text = item["password"] as! String
//        }
        let productName = item["productname"] as? String
        if productName?.range(of: "<null>") != nil || productName?.range(of: "nil") != nil {
            cell.device_model.text = "!=nill"
        }else{
            cell.device_model.text = productName//"==nil" //title as! String
        }
        
        let userName = item["username"] as? String
        if userName?.range(of: "<null>") != nil || userName?.range(of: "nil") != nil {
            cell.device_username.text = "!=nill"
        }else{
            cell.device_username.text = userName//"==nil" //title as! String
        }
        
        let title = item["password"] as? String
            if title?.range(of: "<null>") != nil || title?.range(of: "nil") != nil {
                cell.device_password.text = "!= nill"
            }else{
                cell.device_password.text = title//"==nil" //title as! String
            }
        
        return cell
        
    }
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        
        //        account_label_tabbar.textColor = UIColor(red: 0/255, green: 174/255, blue: 239/255, alpha: 1.0)
        //        var image: UIImage = UIImage(named: "tab_home_selected")!
        //        account_img_tabbar.image = image
        
        if(self.checkPausedTimeAsAndroid()){
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
        
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
