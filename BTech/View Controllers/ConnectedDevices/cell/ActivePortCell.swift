//
//  ActivePortCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 27/12/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class ActivePortCell: UITableViewCell {
    
    //MARK: -Outlets
    @IBOutlet weak var port_number_label: UILabel!
    @IBOutlet weak var port_name_label: UILabel!
    
    //MARK: -Class Variables
    var hostPorts = ConnectedDevicesModal()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
