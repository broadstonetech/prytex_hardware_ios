//
//  Connected_devices_cell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 05/07/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class Connected_devices_cell: UITableViewCell {

    // MARK: - cell Outlets
////    @IBOutlet weak var host_id: UILabel!
//    @IBOutlet weak var host_ip: UILabel!
//    @IBOutlet weak var host_os: UILabel!
//    @IBOutlet weak var device_category: UILabel!
//    @IBOutlet weak var high_vuln: UILabel!
//    @IBOutlet weak var medium_vuln: UILabel!
//    @IBOutlet weak var low_vuln: UILabel!
//    @IBOutlet weak var rename_device: UIButton!
//    @IBOutlet weak var blockDeviceTest: UIButton!
//    @IBOutlet weak var btn_next: UIButton!
//    @IBOutlet weak var eve_sec: UILabel!
//
//    
//    @IBOutlet weak var convert_view_cell: UIView!
//
//
//    @IBOutlet weak var btn_activeports: UIButton!
//    
//    
//    @IBOutlet weak var vuln_heading_label: UILabel!
//    @IBOutlet weak var no_vuln_found_label: UILabel!
    
//    @IBOutlet weak var vuln_view: UIView!

    @IBOutlet weak var username_view: UIView!
    @IBOutlet weak var hostname_view: UIView!
    @IBOutlet weak var ip_view: UIView!
    @IBOutlet weak var os_view: UIView!
    @IBOutlet weak var category_view: UIView!
    @IBOutlet weak var score_view: UIView!
    @IBOutlet weak var default_devices_view: UIView!
    //
    @IBOutlet weak var block_btn_width: NSLayoutConstraint!

    
    
    //labels
      @IBOutlet weak var username_heading_label: UILabel!
    @IBOutlet weak var user_name: UILabel!
    @IBOutlet weak var hostname_heading_label: UILabel!
    @IBOutlet weak var host_name: UILabel!
    
        @IBOutlet weak var ip_heading_label: UILabel!
    @IBOutlet weak var host_ip: UILabel!
    
    
        @IBOutlet weak var os_heading_label: UILabel!
    @IBOutlet weak var host_os: UILabel!
    
        @IBOutlet weak var category_heading_label: UILabel!
    @IBOutlet weak var device_category: UILabel!
 
    @IBOutlet weak var score_heading_label: UILabel!
    @IBOutlet weak var score_label: UILabel!
//    @IBOutlet weak var high_vuln: UILabel!
//    @IBOutlet weak var medium_vuln: UILabel!
//    @IBOutlet weak var low_vuln: UILabel!
    @IBOutlet weak var defaultDevice_heading_label: UILabel!
    @IBOutlet weak var defaultDevice_label: UILabel!
    
    
    @IBOutlet weak var eve_sec: UILabel!

    
    //action button views
    @IBOutlet weak var customize_btn_view: UIView!
    @IBOutlet weak var block_btn_view: UIView!
    @IBOutlet weak var activePorts_btn_view: UIView!
    
        @IBOutlet weak var btn_activeports: UIButton!
    
        @IBOutlet weak var rename_device: UIButton!
        @IBOutlet weak var blockDeviceTest: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  

}
