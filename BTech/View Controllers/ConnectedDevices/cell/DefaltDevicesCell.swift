//
//  DefaltDevicesCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 06/04/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class DefaltDevicesCell: UITableViewCell {

    @IBOutlet weak var device_model: UILabel!
    @IBOutlet weak var device_username: UILabel!
    @IBOutlet weak var device_password: UILabel!
    
    //MARK: -Class Variables
//    var hostPorts = ConnectedDevicesModal()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

