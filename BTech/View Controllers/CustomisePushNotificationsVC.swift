//
//  CustomisePushNotificationsVC.swift
//  BTech
//
//  Created by MacBook-Mubashar on 06/08/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class CustomisePushNotificationsVC: Parent {

    //MARK: -IB outlets
    @IBOutlet weak var allTypes_switch: UISwitch!
    @IBOutlet weak var connectedDevice_switch: UISwitch!
    @IBOutlet weak var blockedAlerts_switch: UISwitch!
    @IBOutlet weak var infoAlerts_switch: UISwitch!
    @IBOutlet weak var flowAlerts_switch: UISwitch!
    @IBOutlet weak var sensorAlerts_switch: UISwitch!
    @IBOutlet weak var applyNotificationsbtn: UIButton!
    
    //MARK: -class variables
    var titleText:String = ""
    
    
    class func customPushNotificationsVC() -> CustomisePushNotificationsVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomisePushNotificationsVC") as! CustomisePushNotificationsVC
    }
    //MARK: -Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        getNotificationSettings()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.titleText = "Push Notifications"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.title = titleText
        self.navigationItem.title = self.title
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
//        Analytics.setScreenName("Customise Push Notifications", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Customise Push Notofications", AnalyticsParameterScreenClass: screenClass])
    
    }

    // MARK: - SWITCH Actions
    @IBAction func allTypesSwitch_pressed(_ sender: Any) {
        if allTypes_switch.isOn {
            connectedDevice_switch.setOn(true, animated: true)
            blockedAlerts_switch.setOn(true, animated: true)
            infoAlerts_switch.setOn(true, animated: true)
            flowAlerts_switch.setOn(true, animated: true)
            sensorAlerts_switch.setOn(true, animated: true)
            
        } else {
            connectedDevice_switch.setOn(false, animated: true)
            blockedAlerts_switch.setOn(false, animated: true)
            infoAlerts_switch.setOn(false, animated: true)
            flowAlerts_switch.setOn(false, animated: true)
            sensorAlerts_switch.setOn(false, animated: true)
        }
    }
    @IBAction func connectedDeviceSwitch_pressed(_ sender: Any) {
        if connectedDevice_switch.isOn {
            allTypes_switch.setOn(true, animated: true)
        }
    }
    @IBAction func blockedAlertSwitch_pressed(_ sender: Any) {
        if blockedAlerts_switch.isOn {
             allTypes_switch.setOn(true, animated: true)
        }
    }
    @IBAction func infoAlertSwitch_pressed(_ sender: Any) {
        if infoAlerts_switch.isOn {
             allTypes_switch.setOn(true, animated: true)
        }
    }
    @IBAction func flowAlertSwitch_pressed(_ sender: Any) {
        if flowAlerts_switch.isOn {
             allTypes_switch.setOn(true, animated: true)
        }
    }
    @IBAction func sensorAlertSwitch_pressed(_ sender: Any) {
        if sensorAlerts_switch.isOn {
             allTypes_switch.setOn(true, animated: true)
        }
    }
    
    
    // MARK: - IB Actions
    
    @IBAction func applyNotifications_pressed(_ sender: Any) {
        print("apply notification settings")
        setNotificationSettings()
    }
    
    //MARK: - API calls
    func getNotificationSettings() {
        
            NetworkHelper.sharedInstance.error401 = false
            let randomNumber = Int(arc4random_uniform(10000) + 1)
            let requestID : String = String(randomNumber)
            let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject]
            
            NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.GET_NOTIFICATION_SETTING, sendData: params, success: { (data) in
                print(data)
                
                let defaults = UserDefaults.standard
                let statusCode = defaults.string(forKey: "statusCode")
                
                if ((statusCode?.range(of: "200")) != nil){
                    print("success \(data)")
                    DispatchQueue.main.async {
                        let dataReceived = data["data"] as? [String : AnyObject]
                        if dataReceived!["blocked"] as! Int == 0 {
                            self.blockedAlerts_switch.setOn(false, animated: true)
                        }
                        if dataReceived!["cnctd_host"] as! Int == 0 {
                            self.connectedDevice_switch.setOn(false, animated: true)
                        }
                        if dataReceived!["devices"] as! Int == 0 {
                            self.flowAlerts_switch.setOn(false, animated: true)
                        }
                        if dataReceived!["info"] as! Int == 0 {
                            self.infoAlerts_switch.setOn(false, animated: true)
                        }
                        if dataReceived!["sensors"] as! Int == 0 {
                            self.sensorAlerts_switch.setOn(false, animated: true)
                        }
                        
                        if dataReceived!["blocked"] as! Int == 0 && dataReceived!["cnctd_host"] as! Int == 0 && dataReceived!["devices"] as! Int == 0 && dataReceived!["info"] as! Int == 0 && dataReceived!["sensors"] as! Int == 0 {
                            self.allTypes_switch.setOn(false, animated: true)
                        }
                    }
                }
                else if ((statusCode?.range(of: "503")) != nil) {
                    if (data["message"] != nil) {
                        let msg = data["message"] as! String
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }else{
                        self.showAlert(ConstantStrings.ErrorText)
                        
                    }
                }
                else if ((statusCode?.range(of: "409")) != nil){
                    NetworkHelper.sharedInstance.isDmoatOnline = false
                    if (data["message"] != nil) {
                        let message = data["message"] as! String
                        self.showAlert(message)
                    }else{
                        self.showAlert(ConstantStrings.ErrorText)
                        
                    }
                } else if ((statusCode?.range(of: "401")) != nil) {
                    if(NetworkHelper.sharedInstance.deviceToken != nil)
                    {
                        (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                        (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                        (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                    }
                    //unsubscribe from all
                    DispatchQueue.main.async {
                        (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                    }
                    //clear nsuserdefaults
                    NetworkHelper.sharedInstance.clearUserData()
                    //delete all data
                    AlertModelRealm.deleteAllRecords()
                    PolicyManagementRealm.deleteAllRecords()
                    let loginVC = Login.loginVC()
                    self.navigationController?.pushViewController(loginVC, animated: true)
                    
                }  else if (data["message"] != nil) {
                    let message = data["message"] as! String
                    self.showAlert(message)
                } else {
                    self.showAlert(ConstantStrings.ErrorText)
                }
                
            }, failure: { (data) in
                print(data)
                if(NetworkHelper.sharedInstance.error401)
                {
                    NetworkHelper.sharedInstance.isNotificationLocked = true
                    //mute notifications
                    if(NetworkHelper.sharedInstance.deviceToken != nil)
                    {
                        (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    }
                    //unsubscribe from all
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                    //clear nsuserdefaults
                    NetworkHelper.sharedInstance.clearUserData()
                    //delete all data
                    //AlertModelRealm.deleteAllRecords()
                    PolicyManagementRealm.deleteAllRecords()
                    let loginVC = Login.loginVC()
                    self.navigationController?.pushViewController(loginVC, animated: true)
                }
                
            })
    }

    //set notifications settings
    func setNotificationSettings() {
        
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        var infoAlert : Int = 0
        var cnctdHostAlert : Int = 0
        var blockedAlert : Int = 0
        var sensorsAlert : Int = 0
        var devicesAlert : Int = 0
        
        if infoAlerts_switch.isOn {
            infoAlert = 1
        }
        if connectedDevice_switch.isOn {
            cnctdHostAlert = 1
        }
        if blockedAlerts_switch.isOn {
            blockedAlert = 1
        }
        if sensorAlerts_switch.isOn {
            sensorsAlert = 1
        }
        if flowAlerts_switch.isOn {
            devicesAlert = 1
        }
        let data : [String : AnyObject] = ["info" : infoAlert as AnyObject, "cnctd_host" : cnctdHostAlert as AnyObject, "blocked" : blockedAlert as AnyObject, "devices" : devicesAlert as AnyObject, "sensors" : sensorsAlert as AnyObject ]
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "data" : data as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.SET_NOTIFICATION_SETTING, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil) {
                print("success \(data)")
              self.showAlert("Settings applied successfully.")
            }
            else if ((statusCode?.range(of: "503")) != nil) {
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    self.showAlert(message)
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            } else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                self.showAlert(message)
            } else {
                self.showAlert(ConstantStrings.ErrorText)
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
