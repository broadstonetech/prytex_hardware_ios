//
//  FaqsVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 12/01/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class FaqsVC: Parent, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var table_view: UITableView!
    
    //MARK: -class variables
    var titleText:String = ""
    var questionsArr  = [String]()
    var ansArr  = [String]()
    
    class func faqVC() -> FaqsVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "faqs") as! FaqsVC
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table_view.delegate = self
        self.table_view.dataSource = self
        self.table_view.rowHeight = UITableView.automaticDimension
        table_view.estimatedRowHeight = 200
        prepareData()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
//        Analytics.setScreenName(titleText, screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "titletxt", AnalyticsParameterScreenClass: screenClass])
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.titleText = "FAQs"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.navigationItem.title = titleText
        
    }

    //Mark: -Tableview delegates
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("rowSelected\(indexPath.row)")
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionsArr.count
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  self.table_view.dequeueReusableCell(withIdentifier: "faqs_cell") as! FaqsTableViewCell
        
            cell.questionLabel.text = questionsArr[indexPath.row]
        cell.ans_label.text = ansArr[indexPath.row]
        return cell
        
    }

    //MARK: -Add faqsData
    func prepareData() {
        questionsArr.append(" What is Prytex? ")
        ansArr.append(" Prytex is a unique product that allows you to discover network connected devices, detect cybersecurity threats, intrusions, known malware, anomalous device behavior, communication failure, and perform vulnerability assessment on discovered devices. ")
        
        questionsArr.append(" How does Prytex protect my network?")
        ansArr.append(" Prytex uses enterprise grade cybersecurity software to discover, detect, and defend against malicious attacks. During the discover phase, Prytex identifies the connected devices and learns their trusted behavior and then automatically detects any abnormal behavior to alert the user. Prytex also blocks any malicious activity to proactively shut down both insider and outside attacks.")
        
        questionsArr.append(" How does Prytex keep up with the changing threat landscape?")
        ansArr.append(" The Prytex team is always on the lookout to research and identify any known threats that are identified in the cyber realm. As soon as a remedial action is developed, it is automatically pushed to each Prytex using our proprietary software update mechanism. No user intervention is required to update the software, so you can be assured that your Prytex is always running the latest software as long as it is connected to the Internet.")
        
        questionsArr.append(" What type of information does Prytex collect about my network and usage patterns?")
        ansArr.append(" The majority of the processing is performed on the Prytex inside your network and very little information is transmitted. The majority of today’s network traffic is encrypted so the actual traffic payload is hidden from Prytex. Prytex only observes the network traffic metadata to perform its behavioral analysis. ")
        
        questionsArr.append(" Does Prytex sell any information collected from my network? ")
        ansArr.append(" We take your privacy very seriously. We do not sell any information collected from our user’s network.")
        
        questionsArr.append(" If I have a problem or need to report an issue how can I do that?")
        ansArr.append(" You can use the chat communication within the mobile app to report any issues or get help on any topic.")
        
        questionsArr.append(" Is there a help manual?")
        ansArr.append(" A feature guide is included in the mobile app. You can access it from the Help & Tutorials section.")
        
        questionsArr.append(" Do the mobile apps cost money?")
        ansArr.append(" The Prytex mobile apps are free. ")
        
    }
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {
       
        self.navigationController?.popToRootViewController(animated: true)

    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        
        //        account_label_tabbar.textColor = UIColor(red: 0/255, green: 174/255, blue: 239/255, alpha: 1.0)
        //        var image: UIImage = UIImage(named: "tab_home_selected")!
        //        account_img_tabbar.image = image
        
        if(self.checkPausedTimeAsAndroid()){
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
      
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
