//
//  ForgotPassword.swift
//  BTech
//
//  Created by Ahmad Waqas on 22/07/2016..
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}


class ForgotPassword: Parent, UITextFieldDelegate {
    
    var isForgotUsername : Bool?
    
    @IBOutlet var heading: UILabel!
    
    @IBOutlet var fieldTitle: UILabel!
    
    @IBOutlet weak var product_identifier_txt: UILabel!
    @IBOutlet weak var tfEmail: UITextField!
    
    @IBOutlet weak var forgot_title: UILabel!
    @IBOutlet weak var scanning_btn: UIButton!
    
    class func forgotPasswordVC() -> ForgotPassword {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "forgotPassword") as! ForgotPassword
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("product identifier== \(ConstantStrings.productIdentifierScanner)")
        if(isForgotUsername == true)
        {
            scanning_btn.isHidden = false
            self.forgot_title.text = "Forgot Username"
            self.heading.text = "Prytex will send you an email, follow the instructions."
            self.fieldTitle.text = "Product Identifier"
            self.tfEmail.placeholder = ConstantStrings.productIdentifierScanner
            product_identifier_txt.isHidden = false
            self.tfEmail.tag = 100
        }
        else {
            product_identifier_txt.isHidden = true
            scanning_btn.isHidden = true
            self.forgot_title.text = "Get a Verification Code"
            self.heading.text = "To get a verification code, first enter your registered username."
            self.tfEmail.placeholder = "Enter Username"
            self.tfEmail.tag = 0
        }
        tfEmail.delegate = self
        
        //self.setAppearance()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(isForgotUsername == true){
            if ConstantStrings.productIdentifierScanner == "Product Identifier"{
                self.tfEmail.placeholder = ConstantStrings.productIdentifierScanner
            }else{
                  self.tfEmail.text = ConstantStrings.productIdentifierScanner
            }
                    }
        else{
            self.tfEmail.placeholder = "Enter username"
        }
    }
    //MARK:- TextField Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField === tfEmail)
        {
            tfEmail.resignFirstResponder()
            tfEmail.endEditing(true)
            return true
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfEmail{
            if isForgotUsername == true {
                return string.rangeOfCharacter(from: CharacterSet(charactersIn: "±!~|`^+()-= []{};:/?.>,<")) == nil
            }else{
            return string.rangeOfCharacter(from: CharacterSet(charactersIn: "§±!~|`@#$%^&*+()-= []{};:/?.>,<")) == nil
        }
        }
        return string.rangeOfCharacter(from: CharacterSet(charactersIn: "")) == nil
    }
    
    //MARK:- Class functions
    func setAppearance(){
        
        self.navigationController!.navigationBar.barTintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Book", size: 20)!]
        self.title = "Sign In"
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnCloseTapped(_ sender: AnyObject) {
        ConstantStrings.productIdentifierScanner = "Product Identifier"
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnScanQrCode_pressed(_ sender: Any) {
        self.showAlert(ConstantStrings.qr_code_scanning_error)
    }
    func restoreUsernameWithCredentials(){
        self.view.endEditing(true)
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let data = ["prod_id": tfEmail.text!]
        print("reqID= \(requestID)")
        print("appID= \(NetworkHelper.sharedInstance.app_ID!)")
        print("data= \(data)")
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "data": data as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.FORGOT_USERNAME_API, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async {
                     self.showAlert("Please check your email that was used to register your account and follow the instructions.")
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                         self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                         self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                         self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async(execute: {
                    self.showAlert(message)
                })
            } else {
                DispatchQueue.main.async(execute: {
                    self.showAlert(ConstantStrings.ErrorText)
                })
            }
            
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
            
        })
        
    }
    
    func restorePassWithCredentials(){
        self.view.endEditing(true)
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        
        
        
        let data = ["username": tfEmail.text!]
        print("reqID= \(requestID)")
        print("appID= \(NetworkHelper.sharedInstance.app_ID!)")
        print("data= \(data)")
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "data": data as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.FORGOT_PASSWORD_API, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                if (data["message"] != nil || data["email"] != nil) {
                    
                    
                    let message = data["message"] as! String
                    if((message.range(of: "Request is already in process")) != nil){
                        let reg_email = data["email"] as! String
                        DispatchQueue.main.async {
                              NetworkHelper.sharedInstance.usernameSavedForVerifyCode = self.tfEmail.text!
                        }
                        let verifyCodeVC = VerifyCode.verifyCodeVC()
                        verifyCodeVC.codeDescriptionText = ConstantStrings.verify_code_userenamePwd_a + reg_email + ConstantStrings.verify_code_userenamePwd_b
                        DispatchQueue.main.async {
                            self.navigationController?.pushViewController(verifyCodeVC, animated: true)
                        }
                    } else if data["email"] != nil {
                    let reg_email = data["email"] as! String
                        DispatchQueue.main.async {
                            NetworkHelper.sharedInstance.usernameSavedForVerifyCode = self.tfEmail.text!
                        }
                    let verifyCodeVC = VerifyCode.verifyCodeVC()
                    verifyCodeVC.codeDescriptionText = ConstantStrings.verify_code_userenamePwd_a + reg_email + ConstantStrings.verify_code_userenamePwd_b
                        DispatchQueue.main.async {
                            self.navigationController?.pushViewController(verifyCodeVC, animated: true)
                        }
                    }
                    else if data["message"] != nil {
                        DispatchQueue.main.async {
                            self.showAlert(data["message"] as! String)
                        }
                    }
                 }
                else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                         self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }             else if (data["message"] != nil) {
                let message = data["message"] as! String
                if ((message.range(of: "Please request again after")) != nil){
                    //                    NetworkHelper.sharedInstance.usernameSavedForVerifyCode = self.tfEmail.text!
                    //                    let verifyCodeVC = VerifyCode.verifyCodeVC()
                    //                    self.navigationController?.pushViewController(verifyCodeVC, animated: true)
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                } else{
                    DispatchQueue.main.async(execute: {
                        self.showAlert(message)
                    })
                }
            }
            else {
                DispatchQueue.main.async(execute: {
                    self.showAlert(ConstantStrings.ErrorText)
                })
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
            
        })
        
    }
    
    func showAlertWithTitle(_ title:String,message:String,sender:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        sender.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func backBtnPressed(_ sender: AnyObject) {
        ConstantStrings.productIdentifierScanner = "Product Identifier"
        self.navigationController?.popViewController(animated: true)
        
        
    }
    
    @IBAction func btnSubmit_tapped(_ sender: AnyObject) {
        /*
         if(!DGlobals.validateEmail(self.tfEmail.text)){
         self.showAlertWithTitle("Validation Error", message: "Please enter correct email address", sender: self)
         return
         }
         */
        
        if(isForgotUsername == true)
        {
            if(tfEmail.text == "")
            {
                self.showAlert("Product identifier was empty")
            }
            else{
                let prod_count = tfEmail.text?.count
                if (prod_count < 8) {
                    self.showAlertWithTitle("Prytex", message: "Product identifier should be 8 characters long. Please try again.", sender: self)
                } else {
                    self.restoreUsernameWithCredentials()
                }
            }
        }
        else{
            
            if(tfEmail.text == "")
            {
                self.showAlert("Username was empty")
            }
            else{
                self.restorePassWithCredentials()
            }
        }
        ConstantStrings.productIdentifierScanner = "Product Identifier"
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
