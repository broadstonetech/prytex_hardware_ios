//
//  HelpTutorialsVC.swift
//  BTech
//
//  Created by MacBook-Mubashar on 18/10/2018.
//  Copyright © 2018 Broadstone Technologies. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import FirebaseAnalytics

class HelpTutorialsVC: Parent {

    
    //MARK: -class variables
    var titleText:String = ""
    
    class func helpTutorialsVC() -> HelpTutorialsVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "help_tutorial_vc") as! HelpTutorialsVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
      //  Analytics.setScreenName(titleText, screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "titletxt", AnalyticsParameterScreenClass: screenClass])
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.titleText = "Support"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.title = titleText
        self.navigationItem.title = self.title
    }
    
    // MARK: - Social/video click listener
    
    @IBAction func watchCompletetutorial(_ sender: Any) {
        let videoURL = URL(string: ApiUrl.video_full_url)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    @IBAction func watchDashboardVideo_pressed(_ sender: Any) {
        let videoURL = URL(string: ApiUrl.video_dashboard_url)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    @IBAction func watchAlertsVideo_pressed(_ sender: Any) {
        let videoURL = URL(string: ApiUrl.video_dmoat_alerts_url)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    @IBAction func watchConnectedDevicesVideo_pressed(_ sender: Any) {
        let videoURL = URL(string: ApiUrl.video_connected_devices_url)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    @IBAction func watchNetworkUsageVideo_pressed(_ sender: Any) {
        let videoURL = URL(string: ApiUrl.video_network_usage_url)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    @IBAction func watchSensorsDataVideo_pressed(_ sender: Any) {
        let videoURL = URL(string: ApiUrl.video_sensors_data_url)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    @IBAction func watchPolicyVideo_pressed(_ sender: Any) {
        let videoURL = URL(string: ApiUrl.video_policy_url)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    @IBAction func watchProfileVideo_pressed(_ sender: Any) {
        let videoURL = URL(string: ApiUrl.video_profile_settings_url)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    @IBAction func watchAccountSetupVideo_pressed(_ sender: Any) {
        let videoURL = URL(string: ApiUrl.video_account_setup_url)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    
    @IBAction func btn_readQuickStartGuide_pressed(_ sender: Any) {
        if let url = URL(string: ApiUrl.dmoatQuickStartDocURL) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @IBAction func btn_read_document_pressed(_ sender: Any) {
        if let url = URL(string: ApiUrl.dmoatFeatureGuideDocURL) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @IBAction func btn_dmoat_fb_pressed(_ sender: AnyObject) {
        print("dmoat.com/fb")
        UIApplication.tryURL(urls: [
            ApiUrl.dmoatFacebookUrl, // App
            ApiUrl.dmoatWebUrl // Website if app fails
            ])
    }
    @IBAction func btn_dmoat_twitter_pressed(_ sender: AnyObject) {
        print("dmoat.com/twitter")
        UIApplication.tryURL(urls: [
            ApiUrl.dmoatTwitterUrl, // App
            ApiUrl.dmoatWebUrl // Website if app fails
            ])
    }
    
    @IBAction func btn_dmoat_instagram_pressed(_ sender: Any) {
        print("dmoat.com/instagram")
        UIApplication.tryURL(urls: [
            ApiUrl.dmoatTInstagramUrl, // App
            ApiUrl.dmoatWebUrl // Website if app fails
            ])
    }
    
        @IBAction func faqs_pressed(_ sender: Any) {
            //faq page
            let faqVC = FaqsVC.faqVC()
            self.setNavBar()
            self.navigationController?.pushViewController(faqVC, animated: true)
    }
        @IBAction func liveSupport_pressed(_ sender: Any) {
//
//            print("chat started.....................")
//            if NetworkHelper.sharedInstance.getCurrentFirebaseId() == ""{
//                startChat()
//            }
//            else{
//                let firebaseId = NetworkHelper.sharedInstance.getCurrentFirebaseId()
//                print("firebase id is ......\(firebaseId)")
//                let channel = Channel(id: firebaseId)
//                let vc = ChatViewController(channel: channel)
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
            
            startChat()
            
        }
    
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        
        if(self.checkPausedTimeAsAndroid()){
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
        print("already on this page.")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- Methods
    
      func startChat(){
            let sentDate = Date()
            var userName:String?
            var email:String?
            var phone:String?
            var namespace:String?
            if NetworkHelper.sharedInstance.name != nil {
                phone = "123456879"
                userName = NetworkHelper.sharedInstance.name
                email = NetworkHelper.sharedInstance.email
                namespace = NetworkHelper.sharedInstance.getCurrentUserNamespace()

            }else{
                phone = ""
                userName = ""
                email = ""
            }
            var device_info : Dictionary <String, String> = [:]
    //        var receiver_info : Dictionary <String, String> = [:]

             device_info = [
                "device_manufacturar": "Apple" as String,
              "device_name":UIDevice.current.name as String,
              "device_os":UIDevice.current.systemName as String,
                "device_type": "iPhone" as String,
              "device_version":UIDevice.current.systemVersion as String
            ]
        
        let data :Dictionary<String,Any> = ["creator" :NetworkHelper.sharedInstance.getCurrentUserName() as String,
                                            "creator_name": NetworkHelper.sharedInstance.getCurrentUserName() as String,
                                            "phone":phone as Any,
                                            "creator_email":NetworkHelper.sharedInstance.getCurrentUserEmail() as String,
                                            "creator_namespace":NetworkHelper.sharedInstance.getCurrentUserNamespace() as String,
                                            "creater_device_info":device_info as Any,
                                            "app_id":"dmoat","creator_device_token":NetworkHelper.sharedInstance.getCurrentToken() as Any, "msg_timestamp": Int64((sentDate.timeIntervalSince1970).rounded())]
        
        print(data)
        NetworkHelper.sharedInstance.postChatRequest(serviceName: ApiUrl.START_CHAT, sendData: data as [String : AnyObject], success: { (response) in
                print(response)
                let defaults = UserDefaults.standard
                let statusCode = defaults.string(forKey: "statusCode")

                if ((statusCode?.range(of: "200")) != nil){
                    DispatchQueue.main.async {
                       // self.showAlert("Success")
                        print("successly saved to server")
                        let status = response["status"] as! Bool
                        if status{
                            let data = response["data"] as! NSDictionary
                            let firebaseId = data["firebase_id"] as! String
                            print("firebase id is ......\(firebaseId)")
                            NetworkHelper.sharedInstance.saveFirebaseid(firebaseId)
                            let channel = Channel(id: firebaseId)
                            let vc = ChatViewController(channel: channel)
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        else{
                            self.showAlert(response["message"] as! String)
                        }
                        
                    }

                }

            }, failure: { (data) in
                print(data)

            })
        }
    }

