//
//  OtherHomeCell.swift
//  BTech
//
//  Created by Talha Ejaz on 05/09/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class OtherHomeCell: UITableViewCell {

    @IBOutlet weak var lblCellText: UILabel!
    @IBOutlet weak var imgOtherCell: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
