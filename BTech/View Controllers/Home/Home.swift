//
//  Home.swift
//  BTech
//
//  Created by Talha Ejaz on 22/08/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
//import PageMenu
class Home: Parent {

    @IBOutlet weak var tableView: UITableView!
//     var pageMenu : CAPSPageMenu?
    class func homeVC() -> Home {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Home") as! Home
    }
    let data:[String] = ["View Alerts","Sensor readings","Blocked/Muted Alerts","Connected Devices","Threat & Vulnerabilities","Profile"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Prytex"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0) {
           let alertCell = self.tableView.dequeueReusableCell(withIdentifier: "AlertCell")! as! AlertHomeCell
            return alertCell
        }
        let otherCell = self.tableView.dequeueReusableCell(withIdentifier: "OtherCell")! as! OtherHomeCell
        otherCell.lblCellText.text = data[indexPath.row]
        return otherCell
    }

    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(indexPath.row == 0) {
            let alertVC = ViewAlerts.viewAlerts()
            self.navigationController?.pushViewController(alertVC, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

}
