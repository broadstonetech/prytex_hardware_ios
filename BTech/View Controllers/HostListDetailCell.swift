//
//  HostListDetailCell.swift
//  BTech
//
//  Created by Ahmed Akhtar on 27/12/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class HostListDetailCell: UITableViewCell {

    var checked = false
    
    //iboutlet
    @IBOutlet var osName: UILabel!
    @IBOutlet var ipAddress: UILabel!
    @IBOutlet var hostName: UILabel!
    @IBOutlet var macAddress: UILabel!
    @IBOutlet var checkBox: UIButton!
    
    //ibAction
    
    @IBAction func checkBoxPressed(_ sender: AnyObject) {
        if checked {
            var model = AlertModel()
            model = NetworkHelper.sharedInstance.arrayConnectedDevices[sender.tag]
            model.checked = false
            NetworkHelper.sharedInstance.arrayConnectedDevices[sender.tag] = model
            checkBox.setImage( UIImage(named:"uncheckbox"), for: UIControl.State())
            checked = false
        } else {
            var model = AlertModel()
            model = NetworkHelper.sharedInstance.arrayConnectedDevices[sender.tag]
            model.checked = true
            NetworkHelper.sharedInstance.arrayConnectedDevices[sender.tag] = model
            checkBox.setImage(UIImage(named:"checkbox"), for: UIControl.State())
            checked = true
        }

        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
