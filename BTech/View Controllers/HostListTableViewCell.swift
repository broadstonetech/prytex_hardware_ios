//
//  HostListTableViewCell.swift
//  BTech
//
//  Created by Ahmed Akhtar on 23/12/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class HostListTableViewCell: UITableViewCell {

    //IBOutlet
    @IBOutlet var hostListField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
