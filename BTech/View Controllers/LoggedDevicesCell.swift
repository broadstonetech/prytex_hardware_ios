//
//  LoggedDevicesCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 11/09/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class LoggedDevicesCell: UITableViewCell {

    @IBOutlet weak var ip_label: UILabel!
    
    @IBOutlet weak var location_label: UILabel!
    
    @IBOutlet weak var timestamp_label: UILabel!
}
