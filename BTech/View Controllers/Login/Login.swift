//
//  Login.swift
//  BTech
//
//  Created by Ahmad Waqas on 22/07/2016..
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
//import RealmSwift
import Foundation


class Login: Parent,UITextFieldDelegate {
    class func loginVC() -> Login {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Login") as! Login
    }
    
    open var loggedDevicesList: [LoggedDevicesModel] = [LoggedDevicesModel]()
    
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var btn_showHide_pwd: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        tfPassword.delegate = self
        tfEmail.delegate = self
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(pushNotificationReceived),
            name: NSNotification.Name(rawValue: "pushNotification"),
            object: nil)
        
        self.navigationController?.isNavigationBarHidden = true
        //check if login credentials exist
        if(UserDefaults.standard.object(forKey: "postkey") != nil && UserDefaults.standard.object(forKey: "namespace") != nil)
        {
            
            self.getCurrentUserAppSettings()
            NetworkHelper.sharedInstance.appSettingsMode = NetworkHelper.sharedInstance.getCurrentUserMode()
            NetworkHelper.sharedInstance.appSettingsLEDManagement = NetworkHelper.sharedInstance.getCurrentUserLEDMangement()
            
            NetworkHelper.sharedInstance.app_ID = NetworkHelper.sharedInstance.getCurrentAppID()
            NetworkHelper.sharedInstance.username = NetworkHelper.sharedInstance.getCurrentUserName()
            NetworkHelper.sharedInstance.email = NetworkHelper.sharedInstance.getCurrentUserEmail()
            NetworkHelper.sharedInstance.password = NetworkHelper.sharedInstance.getCurrentUserPassword()
            NetworkHelper.sharedInstance.arraySubscribers = NetworkHelper.sharedInstance.getCurrentSubscribers()
            NetworkHelper.sharedInstance.name = NetworkHelper.sharedInstance.getName()
            NetworkHelper.sharedInstance.token = NetworkHelper.sharedInstance.getCurrentToken()
            
            //create channels list after login
            var channels = [String]()
            channels.append("info_f" + NetworkHelper.sharedInstance.getCurrentUserNamespace())
            channels.append("netconfig_f" + NetworkHelper.sharedInstance.getCurrentUserNamespace())
            
            (UIApplication.shared.delegate as! AppDelegate).setRootViewController()
            
            let delayTime = DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
//            DispatchQueue.main.asyncAfter(deadline: delayTime) {
//                (UIApplication.shared.delegate as! AppDelegate).registerForPushNotifications(UIApplication.shared)
//                
//            }
//            (UIApplication.shared.delegate as! AppDelegate).subscribeToChannelsList(channels)
            
//            if(NetworkHelper.sharedInstance.deviceToken != nil)
//            {
//                (UIApplication.shared.delegate as! AppDelegate).UnmuteNotifications()
//            }
            
        }  else {
            let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.roleSelection()
            }
        }
        
    }
    //MARK: -Role Selection dev-prod
    //comment this function for production builds
    func roleSelection() {
//        change here and change in appdelegate file for mode selection
//        let alertController = UIAlertController(title: "d.moat", message: "select app mode:", preferredStyle: .alert)

//         Create the actions
//        let okAction = UIAlertAction(title: "Production", style: UIAlertAction.Style.default) {
//            UIAlertAction in
            ConstantStrings.aapModeBaseURl = "https://api.dmoat.com/" //prod
        
            NetworkHelper.sharedInstance.BASE_URL = ConstantStrings.aapModeBaseURl
            NetworkHelper.sharedInstance.saveAppMode(false)
//        }
//        let cancelAction = UIAlertAction(title: "Development", style: UIAlertAction.Style.cancel) {
//            UIAlertAction in
//            ConstantStrings.aapModeBaseURl = "https://dev.dmoat.com/" //dev
//            NetworkHelper.sharedInstance.BASE_URL = ConstantStrings.aapModeBaseURl
//            NetworkHelper.sharedInstance.saveAppMode(true)
//        }
//         Add the actions
//        alertController.addAction(okAction)
//        alertController.addAction(cancelAction)
//         Present the controller
//        self.present(alertController, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField === tfPassword)
        {
            tfPassword.resignFirstResponder()
            tfPassword.endEditing(true)
            return true
        }else if (textField == tfEmail){
            //            tfEmail.resignFirstResponder()
            tfPassword.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
            textField.endEditing(true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfEmail{
        return string.rangeOfCharacter(from: CharacterSet(charactersIn: "§±!@#$%^&*+()-= []{};:/?.>,<")) == nil
        }
        return string.rangeOfCharacter(from: CharacterSet(charactersIn: "")) == nil
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button ACtions
    
    
    @IBAction func forgotUsernamePressed(_ sender: AnyObject) {
        
        let forgotPassword = ForgotPassword.forgotPasswordVC()
        forgotPassword.isForgotUsername = true
        self.navigationController?.pushViewController(forgotPassword, animated: true)
        
    }
    
    
    @IBAction func btnCloseTapped(_ sender: AnyObject) {
        self.dismiss(animated: true) { () -> Void in
            
            print("login dismissed")
            
        }
        
    }
    @IBAction func btnSignIn_tapped(_ sender: AnyObject) {
        
        self.loginUserWithCredentials()
        
    }
    
    
    
    
    // MARK :- API CALL
    func loginUserWithCredentials(){
        
        //(UIApplication.sharedApplication().delegate as! AppDelegate).setRootViewController()
        //return
        
        
        if(self.tfEmail.text=="" && self.tfPassword.text=="")
        {
            self.showAlert(ConstantStrings.fill_all_fields_text)
        }
            
        else if(self.tfEmail.text=="")
        {
            self.showAlert(ConstantStrings.username_empty_text)
        }
            
        else if(self.tfPassword.text=="")
        {
            self.showAlert(ConstantStrings.password_empty_text)
        }
            
        else
        {
            DispatchQueue.main.async(execute: {
                self.callLoginService()
            })
        }
    }
    
    @IBAction func btnSignup_tapped(_ sender: AnyObject) {
        
    }
    
    @IBAction func btnShowHide_pwd_tapped(_ sender: Any) {
        if tfPassword.isSecureTextEntry {
            tfPassword.isSecureTextEntry = false
            //            btn_showHide_pwd.setBackgroundImage(UIImage(named: "hidepass.png"), for: UIControlState.normal)
            btn_showHide_pwd.setBackgroundImage(UIImage(named: "hide_pwd"), for: UIControl.State.normal)
        } else {
            tfPassword.isSecureTextEntry = true
            //            btn_showHide_pwd.setBackgroundImage(UIImage(named: "Showpass.png"), for: UIControlState.normal)
            btn_showHide_pwd.setBackgroundImage(UIImage(named: "show_pwd"), for: UIControl.State.normal)
        }
    }
    
    //MARK: -Login service call
    func callLoginService(){
        
        let appID = NetworkHelper.sharedInstance.getCurrentAppID()
        NetworkHelper.sharedInstance.app_ID = appID
        if(appID == "")
        {
            let randomAppID = Int(arc4random_uniform(10000) + 1)
            NetworkHelper.sharedInstance.app_ID = UIDevice.current.identifierForVendor?.uuidString
            //            NetworkHelper.sharedInstance.saveCurrentAppID(NetworkHelper.sharedInstance.app_ID!)
        }
        self.view.endEditing(true)
        //            var pwdLength = tfPassword.text?.characters.count
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        
        //convert to base64
        let plainEmail = (self.tfEmail.text! as NSString).data(using: String.Encoding.utf8.rawValue)
        let plainPass = (self.tfPassword.text! as NSString).data(using: String.Encoding.utf8.rawValue)
        let base64Email = plainEmail!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        let base64Pass = plainPass!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        let plainReqId = (requestID as NSString).data(using: String.Encoding.utf8.rawValue)
        let base64ReqId = plainReqId!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        //        print("token: \(NetworkHelper.sharedInstance.deviceToken)")
        print("token== \(String(describing: NetworkHelper.sharedInstance.deviceToken))")
        
        //send uname and reqid+pwd
        
        let userName = self.tfEmail.text?.lowercased()
        
        let formattedUserName = userName!.replacingOccurrences(of: " ", with: "")
        
        ///wewewe
        let url = ApiUrl.USER_LOGIN_API
        
        let data:Dictionary<String,AnyObject> = [ConstantStrings.user_name: userName as AnyObject , ConstantStrings.password:base64Pass as AnyObject, ConstantStrings.fcm_device_token : NetworkHelper.sharedInstance.FCM_DEVICE_TOKEN as AnyObject, ConstantStrings.device_uuid : UIDevice.current.identifierForVendor?.uuidString as AnyObject]
        let params:Dictionary<String,AnyObject> = ["app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject, "request_id" : requestID as AnyObject, "data" : data as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (response) in
            print(response)
            
            DispatchQueue.main.async(execute: {
                self.fetchLogInData(response: response )
            })
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                

                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        })
        
    }
    
    //MARK: -Fetch login data
    func fetchLogInData(response : [String:AnyObject] ) {
        
        let defaults = UserDefaults.standard
        let statusCode = defaults.string(forKey: "statusCode")
        
        //get current version and store in ud
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            UserDefaults.standard.set(version, forKey: "app_version")
            UserDefaults.standard.synchronize()
        }
        
        
        let namespace = response["namespace"]
        if(namespace != nil){
            
            NetworkHelper.sharedInstance.isNotificationLocked = false
            let postKeyString = response["postkey"]
            let namespaceString = response["namespace"]
            let name = response["name"]
            let email = response["email"]
            let username = response["username"]
            let token = response["token"]
            let dmoatStatus = response["dmoat_status"] as! String
            
            NetworkHelper.sharedInstance.name = name as! String?
            NetworkHelper.sharedInstance.email = email as! String?
            NetworkHelper.sharedInstance.username = username as! String?
            NetworkHelper.sharedInstance.password = self.tfPassword.text
            NetworkHelper.sharedInstance.token = token as! String?
            if (dmoatStatus == "online") {
                NetworkHelper.sharedInstance.isDmoatOnline = true
            }else{
                NetworkHelper.sharedInstance.isDmoatOnline = false
            }
            
            //parse subscribers
            NetworkHelper.sharedInstance.arraySubscribers.removeAll()
            if(response["subscriber"] != nil)
            {
                let subscribers = response["subscriber"] as! [String]
                for user in subscribers{
                    NetworkHelper.sharedInstance.arraySubscribers.append(user)
                }
            }
            
            //parse led
            let ledObject = response["led"]
            let mode = ledObject?["mode"] as! String
            let frequency = "5"//ledObject?["frequency"] as? AnyObject
            let ledModel = LED()
            ledModel.mode = mode
            ledModel.frequency = frequency as? String
            if(ledModel.frequency == "5")
            {
                ledModel.frequency = "5 seconds"
            }
            if(ledModel.frequency == "10")
            {
                ledModel.frequency = "10 seconds"
            }
            //save led
            if(ledModel.mode != nil)
            {
                NetworkHelper.sharedInstance.saveCurrentUserLEDManagement(ledModel.mode!)
            }
            if(ledModel.mode != "OFF")
            {
                if(ledModel.frequency != nil)
                {
                    NetworkHelper.sharedInstance.saveCurrentUserLEDManagement(ledModel.frequency!)
                }
            }
            
            //current loggedin devices
            NetworkHelper.sharedInstance.isShowLoggedDeviceDialog = true
            
            // save mode
            let net = response["net"]
            var type = net?["type"] as! String
            if(type == "advance")
            {
                type = "Advanced"
            }
            if(type == "P&P")
            {
                type = "Plug & Play"
            }
            
            NetworkHelper.sharedInstance.saveCurrentUserMode(type)
            NetworkHelper.sharedInstance.appSettingsMode = NetworkHelper.sharedInstance.getCurrentUserMode()
            NetworkHelper.sharedInstance.appSettingsLEDManagement = NetworkHelper.sharedInstance.getCurrentUserLEDMangement()
            
            
            NetworkHelper.sharedInstance.saveCurrentUserPostKey(postKeyString! as! String)
            NetworkHelper.sharedInstance.saveCurrentUserNamespace(namespaceString! as! String)
            NetworkHelper.sharedInstance.saveCurrentName(name! as! String)
            NetworkHelper.sharedInstance.saveCurrentUserEmail(email! as! String)
            NetworkHelper.sharedInstance.saveCurrentToken(token! as! String)
            NetworkHelper.sharedInstance.saveCurrentUserName(username! as! String)
            NetworkHelper.sharedInstance.saveCurrentUserPassword(self.tfPassword.text!)
            NetworkHelper.sharedInstance.saveSubscribers(NetworkHelper.sharedInstance.arraySubscribers)
            
            
            var usernameForProfile: String? = nil
            usernameForProfile = NetworkHelper.sharedInstance.getCurrentUserNameForProfile()
            if(usernameForProfile?.isEmpty == true)
            {
                NetworkHelper.sharedInstance.saveCurrentUserNameForAppSettings(username! as! String)
            }
            
            //check if another user has logged in ! if yes then clear previous app settings
            if(NetworkHelper.sharedInstance.getCurrentUserName() != NetworkHelper.sharedInstance.getCurrentUserNameForProfile())
            {
                AlertModelRealm.deleteAllRecords()
                NetworkHelper.sharedInstance.clearUserAppSettings()
                NetworkHelper.sharedInstance.saveCurrentUserNameForAppSettings(NetworkHelper.sharedInstance.getCurrentUserName())
            }
                //Same user has again logged in so transfer the same app settings to user profile
            else{
                self.getCurrentUserAppSettings()
            }
            
            //create channels list after login
            var channels = [String]()
            channels.append("info_f" + (namespaceString! as! String))
            channels.append("netconfig_f" + (namespaceString! as! String))
            channels.append("result_f" + (namespaceString! as! String))
            
            (UIApplication.shared.delegate as! AppDelegate).setRootViewController()
//            (UIApplication.shared.delegate as! AppDelegate).registerForPushNotifications(UIApplication.shared)
            
//            let delayTime = DispatchTime.now() + Double(Int64(3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
//                                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
//                                        (UIApplication.shared.delegate as! AppDelegate).subscribeToChannelsList(channels)
//
//                        }
            


//            if(NetworkHelper.sharedInstance.deviceToken != nil)
//            {
//                (UIApplication.shared.delegate as! AppDelegate).UnmuteNotifications()
//            }
            
        }
        else {
            let msg = response["message"] as! String
            if ((msg.range(of: "Device not discovered yet")) != nil){
                //                    if ((msg.range(of:"Swift")) != nil){
                print("auto discovery not complete, goining to discover")
                let email = response["email"]
                
                NetworkHelper.sharedInstance.email = email as! String?
                
                let alertController = UIAlertController(title: "Prytex", message: ConstantStrings.discovery_incomplete_text, preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    let prod_id = response["prod_id"] as! String
                    let restoreID = response["chatRestoreId"] as? String
                    UserDefaults.standard.set(restoreID, forKey: "chatRestoreID")
                    UserDefaults.standard.set(prod_id, forKey: "prodId")
                    UserDefaults.standard.synchronize()
                    
                    let DiscoveryMod = DiscoveryMode.dmodeVC()
                    self.navigationController?.pushViewController(DiscoveryMod, animated: true)
                    //go back to login page
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                    UIAlertAction in
                }
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
                
                //device not registered, go to discovery
            }else{
                if ((statusCode?.range(of: "503")) != nil){
                    if (response["message"] != nil) {
                        let msg = response["message"] as! String
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }else{
                        self.showAlert(ConstantStrings.ErrorText)
                        
                    }
                }
                else if ((statusCode?.range(of: "409")) != nil){
                    NetworkHelper.sharedInstance.isDmoatOnline = false
                    if (response["message"] != nil) {
                        let message = response["message"] as! String
                        self.showAlert(message)
                    }else{
                        self.showAlert(ConstantStrings.ErrorText)
                        
                    }
                }  else if (response["message"] != nil) {
                    let message = response["message"] as! String
                    self.showAlert(message)
                } else {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
        }
        
        
    }
    
    //MARK: - Class functins/uer defaults
    func getCurrentUserAppSettings()
    {
        
        NetworkHelper.sharedInstance.promptSwitchState = NetworkHelper.sharedInstance.getCurrentUserPromptSwitchState()
        NetworkHelper.sharedInstance.muteSwitchState = NetworkHelper.sharedInstance.getCurrentUserMuteAllState()
        NetworkHelper.sharedInstance.appSettingsTemperatureStatus = NetworkHelper.sharedInstance.getCurrentUserTemperatureStatus()
        NetworkHelper.sharedInstance.appSettingsMode = NetworkHelper.sharedInstance.getCurrentUserMode()
        NetworkHelper.sharedInstance.appSettingsLEDManagement = NetworkHelper.sharedInstance.getCurrentUserLEDMangement()
        
    }
    
    func showAlertWithTitle(_ title:String,message:String,sender:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        sender.present(alert, animated: true, completion: nil)
        
    }
    
   
}
