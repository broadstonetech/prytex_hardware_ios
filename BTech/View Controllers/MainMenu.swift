
//
//  MainMenu.swift
//  BTech
//
//  Created by Ahmed on 29/09/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import BTNavigationDropdownMenu
import Charts
import SystemConfiguration
import WatchConnectivity
import FirebaseAnalytics
//import Crashlytics

class MainMenu: Parent,UICollectionViewDelegate,UICollectionViewDataSource,UIPopoverPresentationControllerDelegate, ChartViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource {
    var modes = ["Plug & Play","Advanced"]
    var selectedRow: Int = 0
    var show : Bool = true
    let rightButtonBar = UIButton()
    var menuView = BTNavigationDropdownMenu(title: "Prytex", items: [])
    let uName = NetworkHelper.sharedInstance.name!
    let screenSize:CGRect = UIScreen.main.bounds
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    var currentParentalCOntrolStatus : String = ""
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var pickerVew: UIPickerView!
    
    @IBOutlet weak var online_offline_view: UIView!
    
    @IBOutlet weak var parental_control_btn: UIButton!
    @IBOutlet weak var parental_control_status_lbl: UILabel!
    @IBOutlet weak var online_offline_status_lbl: UILabel!
    
    @IBOutlet weak var dmoat_status_view: UIView!
    @IBOutlet weak var dmoat_user_name: UILabel!
    @IBOutlet weak var dmoat_status: UILabel!
    
    //new ui outlets
    
    @IBOutlet weak var pnp_btn: UIButton!
    
    @IBOutlet weak var active_mode_lbl: UILabel!
    @IBOutlet weak var main_view_dashboard: UIView!
    @IBOutlet weak var alerts_view_dashboard: UIView!
    @IBOutlet weak var connectedDevices_view_dashboard: UIView!
    @IBOutlet weak var sensor_view_dashboard: UIView!
    @IBOutlet weak var bandwidth_view_dashboard: UIView!
    
    @IBOutlet weak var alerts_view_height_dashboard: NSLayoutConstraint!
    @IBOutlet weak var connectedDevices_view_height_dashboard: NSLayoutConstraint!
    @IBOutlet weak var bandwidth_view_height_dashboard: NSLayoutConstraint!
    @IBOutlet weak var sensors_view_height_dashboard: NSLayoutConstraint!
    
    //alerts view outlets
    @IBOutlet weak var pieChartAlerts: PieChartView!
    @IBOutlet weak var bandwidthChartDashboard: BarChartView!
    @IBOutlet weak var mb_label_bandwidth: UILabel!
    
    @IBOutlet weak var pieChartTemp: PieChartView!
    @IBOutlet weak var pieChartHumidity: PieChartView!
    @IBOutlet weak var pieChartAirQuality: PieChartView!
    @IBOutlet weak var android_pie_chart: PieChartView!
    
    @IBOutlet weak var ios_pie_chart: PieChartView!
    @IBOutlet weak var windows_pie_chart: PieChartView!
    @IBOutlet weak var others_pie_chart: PieChartView!
    
    
    @IBOutlet weak var total_connected_devices_count_label: UILabel!
    @IBOutlet weak var android_devices_count_label: UILabel!
    @IBOutlet weak var mac_devices_count_label: UILabel!
    @IBOutlet weak var windows_devices_count_label: UILabel!
    @IBOutlet weak var others_devices_count_label: UILabel!
    @IBOutlet weak var dmoat_status_label: UILabel!
    
    @IBOutlet weak var connectedDevicesStackView: UIStackView!
    
    @IBOutlet weak var table_view_alerts: UITableView!
    @IBOutlet weak var table_view_bandwidth: UITableView!
    //tab bar outlets
    
    @IBOutlet weak var home_btn_dashboard: UIButton!
    @IBOutlet weak var home_label_dashboard: UILabel!
    @IBOutlet weak var home_img_dashboard: UIImageView!
    
    @IBOutlet weak var no_alerts_data_label: UILabel!
    
    @IBOutlet weak var policy_btn_dashboard: UIButton!
    @IBOutlet weak var policy_label_dashboard: UILabel!
    @IBOutlet weak var policy_img_dashboard: UIImageView!
    @IBOutlet weak var account_label_tabbar: UILabel!
    @IBOutlet weak var account_img_tabbar: UIImageView!
    
    //Connected devices stack icons
    @IBOutlet weak var android_logo: UIImageView!
    @IBOutlet weak var ios_logo: UIImageView!
    @IBOutlet weak var windows_logo: UIImageView!
    @IBOutlet weak var otherDevices_logo: UIImageView!
    @IBOutlet weak var noConnectedDevicesAlert_lbl: UILabel!
    
    //d.moat status outlets
    
    @IBOutlet weak var simplified_view: UIView!
    @IBOutlet weak var simplifiedOnline_image: UIImageView!
    @IBOutlet weak var navigationTitle_lbl: UILabel!
    @IBOutlet weak var dmoatStatus_img: UIImageView!
    @IBOutlet weak var simplifiedEvironmentBG_img: UIImageView!
    @IBOutlet weak var dmoatStatusSimplified_lbl: UILabel!
    @IBOutlet weak var devicesConnectedCountSimplified_lbl: UILabel!
    @IBOutlet weak var parentalControl_switchBtn: UISwitch!
    @IBOutlet weak var temperatureValue_lbl: UILabel!
    @IBOutlet weak var humidityValue_lbl: UILabel!
    @IBOutlet weak var airQualityValue_lbl: UILabel!
    @IBOutlet weak var simplified_tabBar_view: UIView!
    @IBOutlet weak var detailed_tabBar_view: UIView!
    
    
    @IBOutlet weak var account_btn_dashboard: UIButton!
    @IBOutlet weak var help_btn_dashboard: UIButton!
    @IBOutlet weak var help_label_tabbar: UILabel!
    @IBOutlet weak var help_img_tabbar: UIImageView!
    @IBOutlet weak var simplifiedTheme_switch: UISwitch!
    
    let wcSession = WCSession.default
    
    //views action buttons
    @IBOutlet weak var alertsSection_btn: UIButton!
    
    //MARK: - class variables
    var viewAlertsForDashboard: [BlockedMutedAlertsModel] = [BlockedMutedAlertsModel]()
    var devicesListForDashboard: [ConnectedDevicesModal] = [ConnectedDevicesModal]()
    var bandWidthListForDashboard: [BandwidthModel] = [BandwidthModel]()
    var sensorDataListForDashboard: [SensorDataModel] = [SensorDataModel]()
    let defaults = UserDefaults.standard
    var appStoreVersion : String = ""
    
    
    //get curentlogin values from user defaults in var's
    var dataIP:[String] = []
    var dataLoc:[String] = []
    var dataTime:[Int] = []
    //    let surveyData = ["open": 20, "blocked": 30, "muted": 5]
    //    let sensorData = ["": 20]
    
    var SensorsAlertsCount = 0
    var BlockedAlertsCount = 0
    var InfoAlertsCount = 0
    var FlowAlertsCount = 0
    var TotalConnectedDevices = 0
    var AndroidConnectedDevices = 0
    var MacConnectedDevices = 0
    var WindowsConnectedDevices = 0
    var OtherConnectedDevices = 0
    
    var newAlertsReceived : [[String:AnyObject]] = []
    var bandwidthHostsReceived : [[String:AnyObject]] = []
    
    var TempValues = 0
    var HumidityValues = 0
    var AirQualityValue = 0
    
    var isParentalControlModified = false
    var DMOAT_ONLINE_IMAGE = "dmoatOnlineIcon"
    var DMOAT_OFFLINE_IMAGE = "dmoatOfflineIcon"
    var DMOAT_ONLINE_BG_IMAGE = "onlineBgDashboard"
    var DMOAT_OFFLINE_BG_IMAGE = "offlineBgDashboard"
    var DMOAT_ENVIRONMENT_ONLINE_BG_IMAGE = "simplifiedOnline_environment_bg"
    var DMOAT_ENVIRONMENT_OFFLINE_BG_IMAGE = "simplifiedOffline_environment_bg"
    var DMOAT_OFFLINE_SIMPLIFIED_TEXT = "Unusual network activity detected. Please check."
    var DMOAT_ONLINE_SIMPLIFIED_TEXT = "You are safe."
    
    
    @IBAction func connectedDevicesPressed(_ sender: AnyObject) {
        let alertVC = ViewAlerts.viewAlerts()
        alertVC.titleText="Connected Devices"
        alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(alertVC, animated: true)
        
    }
    
    class func MainMenuVC() -> MainMenu {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainMenu") as! MainMenu
    }
    
    @objc func appDidEnterBackground()
    {
        //        self.navigationItem.titleView = menuView
        if menuView.isShown == false {
            //            menuView.show()
        }
        else{
            menuView.hide()
        }
        
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(messageReceivedFromWatch), name: NSNotification.Name(rawValue: "receivedwatchMessage"), object: nil)
        
        //        self.title = "Mathew" + "'s d.moat"
        self.title = NetworkHelper.sharedInstance.name! + "'s Prytex"
        navigationTitle_lbl.text = NetworkHelper.sharedInstance.name! + "'s Prytex"
        table_view_alerts.delegate = self
        table_view_alerts.dataSource = self
        table_view_bandwidth.delegate = self
        table_view_bandwidth.dataSource = self
        pickerVew.dataSource = self
        pickerVew.delegate = self
        pieChartAlerts.delegate = self
        bandwidthChartDashboard.delegate = self
        android_pie_chart.delegate = self
        ios_pie_chart.delegate = self
        windows_pie_chart.delegate = self
        others_pie_chart.delegate = self
        
        pieChartTemp.delegate = self
        pieChartHumidity.delegate = self
        pieChartAirQuality.delegate = self
        
        //                checkAppVersion()  //uncomment while uploading for production
        
        pieChartTemp.noDataText = "No sensor data detected."
        pieChartHumidity.noDataText = ""
        pieChartAirQuality.noDataText = ""
        bandwidthChartDashboard.noDataText = "Currently no network stats processed."
        pieChartAlerts.noDataText = "No data detected."
        
        //        table_view_bandwidth.backgroundColor = CozyLoadingActivity.Settings.DashboardBgColor
        //        table_view_alerts.backgroundColor = CozyLoadingActivity.Settings.DashboardBgColor
        
        
        NetworkHelper.sharedInstance.isDmoatOnline = false
        self.online_offline_status_lbl.text = "Offline"
        self.dmoatStatus_img.image = UIImage(named: self.DMOAT_OFFLINE_IMAGE)
        self.simplifiedOnline_image.image = UIImage(named: self.DMOAT_OFFLINE_BG_IMAGE)
        self.simplifiedEvironmentBG_img.image = UIImage(named: self.DMOAT_ENVIRONMENT_OFFLINE_BG_IMAGE)
        self.dmoatStatusSimplified_lbl.text = self.DMOAT_OFFLINE_SIMPLIFIED_TEXT
        self.online_offline_status_lbl.textColor = CozyLoadingActivity.Settings.dmoatOfflineColor
        self.online_offline_view.backgroundColor = CozyLoadingActivity.Settings.dmoatOfflineColor
        self.connectedDevicesStackView.isHidden = false
        self.connectedDevicesStackView.removeArrangedSubview(self.android_pie_chart)
        self.connectedDevicesStackView.removeArrangedSubview(self.ios_pie_chart)
        self.connectedDevicesStackView.removeArrangedSubview(self.windows_pie_chart)
        self.connectedDevicesStackView.removeArrangedSubview(self.others_pie_chart)
        
        self.fillDmoatOfflineChart()
        
        
        self.fillAlertsChart(isDmoatOffline: true)
        self.fillSensorChart(isDmoatOffline: true)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(appDidEnterBackground),
            name: NSNotification.Name(rawValue: "enterBackgroundNotification"),
            object: nil)
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(pushNotificationReceived),
            name: NSNotification.Name(rawValue: "pushNotification"),
            object: nil)
        
        self.table_view_alerts.rowHeight = UITableView.automaticDimension
        table_view_alerts.estimatedRowHeight = 200
        
        
        self.table_view_bandwidth.rowHeight = UITableView.automaticDimension
        table_view_bandwidth.estimatedRowHeight = 200
        
        if NetworkHelper.sharedInstance.getCurrentDisplayMode() == false{
            if #available(iOS 13.0, *) {
            self.navigationController?.overrideUserInterfaceStyle = .light
            }
        }
        else { 
            if #available(iOS 13.0, *) {
            self.navigationController?.overrideUserInterfaceStyle = .dark
            }
        }
        
        //NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: #selector(sendNotification), userInfo: nil, repeats: true)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        DispatchQueue.main.async {
        //        self.navigationController?.navigationBar.isHidden = true
        //        }
//        let height: CGFloat = 150 //whatever height you want to add to the existing height
//        let bounds = self.navigationController!.navigationBar.bounds
//        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height + height)

        self.pickerVew.isHidden = true
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        //        self.title = "Mathew" + "'s d.moat"
        self.navigationItem.title = NetworkHelper.sharedInstance.name! + "'s Prytex"
        navigationTitle_lbl.text = NetworkHelper.sharedInstance.name! + "'s Prytex"
        if NetworkHelper.sharedInstance.getIfParentalControlOn() == false {
            parentalControl_switchBtn.setOn(false, animated: true)
        } else {
            parentalControl_switchBtn.setOn(true, animated: true)
        }
        //create right bar button
        self.createRightButtonNavigationBar()
        setTabBarSelection(true, false, false, false)
        updateUI()
        
        loadAlertsData()
        loadConnectedDevicesData()
        loadBandwidthData()
        loadSensorsData()
        
        if isParentalControlModified {
            loadAlertsData()
            isParentalControlModified = false
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
      //  Analytics.setScreenName("Dashboard", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Dashboard", AnalyticsParameterScreenClass: screenClass])
    }
    
    @objc func messageReceivedFromWatch(info : NSNotification) {
        let msg = info.userInfo
        print("\(msg!["msg"] as? String)!)")
        self.loadAlertsData()
        self.loadConnectedDevicesData()
        self.loadBandwidthData()
        self.loadSensorsData()
        sleep(1)
        populateDataInWatch()
    }
    
    func populateDataInWatch() {
        if self.wcSession.isPaired == true && self.wcSession.isWatchAppInstalled {
            
            let currentTime = getCurrentTime()
            self.wcSession.sendMessage(["info" : self.InfoAlertsCount, "devices" : self.FlowAlertsCount, "blocked" : self.BlockedAlertsCount, "connected_devices" : self.TotalConnectedDevices, "temperature" : self.TempValues, "humidity" : self.HumidityValues, "air_quality" : self.AirQualityValue, "new_alerts" : newAlertsReceived, "bandwidth" : bandwidthHostsReceived, "current_time" : currentTime], replyHandler: nil, errorHandler: nil)
            //            }
            
        }else{
            print("watch not paired properly")
        }
    }
    
    
    
    func getCurrentTime() -> String{
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd h:mm a"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        //        let interval = date.timeIntervalSince1970
        
        return dateString
    }
    
    func sendNotification()
    {
        print("notification Received")
        NetworkHelper.sharedInstance.viewAlertsPushedInNavigationStack = false
        NetworkHelper.sharedInstance.pushNotification = true
        NotificationCenter.default.post(name: Notification.Name(rawValue: "pushNotification"), object: nil);
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    func createRightButtonNavigationBar()
    {
        //right navigation bar buttom removed, items moved to help& support section
        //create custom
        self.title = NetworkHelper.sharedInstance.name! + "'s Prytex"
        navigationTitle_lbl.text = NetworkHelper.sharedInstance.name! + "'s Prytex"
    }
    
    @objc func topRightButtonBar()
    {
    }
    
    private func setupBarButtonItem() {
        let offLabel = UILabel()
        offLabel.font = UIFont.boldSystemFont(ofSize: UIFont.smallSystemFontSize)
        offLabel.text = "Switch theme"
        offLabel.textColor = .white
        
        let onLabel = UILabel()
        onLabel.font = UIFont.boldSystemFont(ofSize: UIFont.smallSystemFontSize)
        onLabel.text = ""
        
        let toggle = UISwitch()
        toggle.addTarget(self, action: #selector(toggleValueChanged(_:)), for: .valueChanged)
        
        let stackView = UIStackView(arrangedSubviews: [offLabel, toggle, onLabel])
        stackView.spacing = 0
        stackView.alignment = .trailing
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: stackView)
    }
    
    @objc func toggleValueChanged(_ toggle: UISwitch) {
        if toggle.isOn {
            NetworkHelper.sharedInstance.saveCurrentSelectedTheme("1")
            self.navigationController?.navigationBar.isHidden = true
            showSimplifiedView()
        } else {
            loadAlertsData()
            loadConnectedDevicesData()
            loadBandwidthData()
            loadSensorsData()
            NetworkHelper.sharedInstance.saveCurrentSelectedTheme("0")
            showDetailedView()
            //            setupBarButtonItem()
            
            
        }
    }
    
    @IBAction func switchtheme(_ sender: Any) {
        
        if simplifiedTheme_switch.isOn {
            NetworkHelper.sharedInstance.saveCurrentSelectedTheme("1")
            DispatchQueue.main.async {
//                self.navigationController?.setNavigationBarHidden(true, animated: true)
                self.showSimplifiedView()
            }
        } else {
            if #available(iOS 13.0, *) {
//                self.navigationController?.overrideUserInterfaceStyle = .dark
//                NetworkHelper.sharedInstance.saveCurrentDisplayMode(true)
            } else {
                // Fallback on earlier versions
            }
            
            loadAlertsData()
            loadConnectedDevicesData()
            loadBandwidthData()
            loadSensorsData()
            NetworkHelper.sharedInstance.saveCurrentSelectedTheme("0")
            DispatchQueue.main.async {
                self.showDetailedView()
            }
        }
    }
    
    func updateUI() {
        //if simplifiedUi
        let themeSelected = NetworkHelper.sharedInstance.getCurrentSelectedTheme()
        if themeSelected == "0" {
            print("detailed viw")
            DispatchQueue.main.async {
                self.showDetailedView()
            }
        } else {
            print("simple viw")
            DispatchQueue.main.async {
                self.showSimplifiedView()
            }
        }
    }
    
    func showDetailedView() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        setupBarButtonItem()
        UIView.animate(withDuration: 0.5, delay: 1.0, options: [], animations: {
             //Animations
//                 self.main_view_dashboard.alpha = 1
//                 self.simplified_view.alpha = 0
                 self.simplifiedOnline_image.isHidden = true
                 self.simplified_view.isHidden = true
                 self.main_view_dashboard.isHidden = false
                 self.detailed_tabBar_view.isHidden = false
                 self.simplified_tabBar_view.isHidden = true
             }) { (finished) in
        }
        NetworkHelper.sharedInstance.saveCurrentSelectedTheme("0") //1for simplified, 0 for detailed
        
    }
    
    func showSimplifiedView() {
//        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        UIView.animate(withDuration: 0.5, delay: 1.0, options: [], animations: {
        //Animations
//            self.main_view_dashboard.alpha = 0
//            self.simplified_view.alpha = 1
            self.simplifiedTheme_switch.isOn = true
            self.detailed_tabBar_view.isHidden = true
            self.main_view_dashboard.isHidden = true
                self.simplified_tabBar_view.isHidden = false
                self.simplifiedOnline_image.isHidden = false
                self.simplified_view.isHidden = false
        }) { (finished) in
        //Perform segue
        }
        
        
        NetworkHelper.sharedInstance.saveCurrentSelectedTheme("1") //1for simplified, 0 for detailed
    }
    
    //Mark: -logout service
    func callLogOutService()
    {
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        //        call service for log out
        //        success
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.SIGN_OUT_API, sendData: params as [String : AnyObject], success: { (data) in
            print(data)
            
            let statusCode = self.defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            }else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    self.showAlert(message)
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            }
            else{
                self.showAlert("Failed to Sign out!")
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
        
    }
    
    
    //MARK: - details btn actions
    
    @IBAction func alertsDetails_pressed(_ sender: Any) {
        alertsSectionTapped()
    }
    
    func alertsSectionTapped() {
        self.navigationController?.navigationBar.isHidden = false
        let alertVC = ViewAlerts.viewAlerts()
        alertVC.titleText="Prytex Alerts"
        alertVC.alertType="view_alerts"
        self.navigationController?.pushViewController(alertVC, animated: true)
        
    }
    
    @IBAction func cnctdDeviceDetails_pressed(_ sender: Any) {
        
        self.navigationController?.navigationBar.isHidden = false
        let vcc = ConnectedDevicesVC.connectedDeviceVc()
        vcc.titleText="Connected Devices"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
        
    }
    
    @IBAction func bandwidthDetails_pressed(_ sender: Any) {
        bandwidthSectionTapped()
    }
    
    func bandwidthSectionTapped() {
        self.navigationController?.navigationBar.isHidden = false
        let vcc = BandwidthChartVC.bandwidthVC()
        vcc.titleText="Bandwidth"
        self.navigationController?.pushViewController(vcc, animated: true)
        
    }
    
    @IBAction func parentalControl_pressed(_ sender: Any) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.isParentalControlModified = true
        
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func modeOperations_pressed(_ sender: Any) {
        let profileVC = Profile.ProfileVC()
        self.setNavBar()
        profileVC.isModeShown = true
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @IBAction func sensorsDetails_pressed(_ sender: Any) {
//        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        let sensorVC = SensorDataVC.sensorDataVC()
        sensorVC.titleText="Sensor Data"
        sensorVC.currentTemperatureValue = String(describing: TempValues)
        sensorVC.currentHumidityValue = String(describing: HumidityValues)
        sensorVC.currentAirQualityValue = String(describing: AirQualityValue)
        self.navigationController?.pushViewController(sensorVC, animated: true)
        
    }
    
    //MARK: - collection view details
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for:indexPath) as! MainMenuCell
        
        if(indexPath.item==0)
        {
            cell.imageCell.image = UIImage(named: "ViewAlertsBig")
            cell.titleCell.text="Prytex Alerts"
        }
        else if(indexPath.item==1)
        {
            //            if(self.checkPausedTimeAsAndroid()){
            cell.imageCell.image = UIImage(named: "sensorData")
            cell.titleCell.text="Sensors Data"
            //            }
            //            else{
            //                self.showAlert(ConstantStrings.pausedIntenettext)
            //            }
        }
            
        else if(indexPath.item==2)
        {
            cell.imageCell.image = UIImage(named: "connectedDevices")
            cell.titleCell.text="Connected Devices"
        }
            
        else if(indexPath.item==3)
        {
            cell.imageCell.image = UIImage(named: "mode")
            cell.titleCell.text="Mode"
        }
            
        else if(indexPath.item==4)
        {
            cell.imageCell.image = UIImage(named: "bandwidth")
            cell.titleCell.text="Bandwidth"
        }
        else if(indexPath.item==5)
        {
            cell.imageCell.image = UIImage(named: "pause_internet")
            cell.titleCell.text="Pause Internet"
        }
        
        
        
        return cell
        
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
    }
    
    
    //MARK: -Mode Pickerview delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 0//modes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return "mode"//modes[row]
    }
    
    //MARK: -class functions
    
    
    
    //MARK: - PieChartAlerts
    func fillAlertsChart(isDmoatOffline : Bool) {
        var dataEntries = [PieChartDataEntry]()
        
        let months = ["Prytex info", "Flow", "Blocked"]
        var unitsSold = [InfoAlertsCount, FlowAlertsCount, BlockedAlertsCount]
        if  isDmoatOffline {
            unitsSold = [33, 33, 33]
        }
        
        for i in 0..<months.count {
            let dataEntry = PieChartDataEntry(value: Double(unitsSold[i]), label: "")
            //            PieChartDataEntry(value: unitsSold[i], xIndex: i)
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = PieChartDataSet(entries: dataEntries, label: "")
        let chartData = PieChartData(dataSet: chartDataSet)
        
        
//        chartDataSet.colors = [CozyLoadingActivity.Settings.OpenAlertsColor,  CozyLoadingActivity.Settings.FlowAlertsColor, CozyLoadingActivity.Settings.BlockedAlertsColor]
//
        
        chartDataSet.colors = [UIColor(named: "PAyellow")!,UIColor(named: "PAgreen")!,UIColor(named: "PAred")!]
        
        chartDataSet.sliceSpace = 0
        chartDataSet.selectionShift = 0
        chartData.setValueTextColor(CozyLoadingActivity.Settings.ChartAlertsTextColor)
        chartDataSet.drawIconsEnabled = false
        let format = NumberFormatter()
        format.numberStyle = .none
        let formatter = DefaultValueFormatter(formatter: format)
        chartDataSet.valueFormatter = formatter
        
        if isDmoatOffline {
            chartData.setDrawValues(false)
            pieChartAlerts.drawEntryLabelsEnabled = false
        }
        
        pieChartAlerts.data = chartData
        
        pieChartAlerts.rotationAngle = -90.0
        pieChartAlerts.dragDecelerationEnabled = true
        pieChartAlerts.chartDescription?.text = ""
        pieChartAlerts.legend.enabled = false
        pieChartAlerts.holeColor = nil
        if isDmoatOffline {
            //            pieChartAlerts.anima
        }else{
            pieChartAlerts.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        }
        //        pieChartTemp.holeRadiusPercent = 70
    }
    
    
    func fillConnectedDevicesChart() {
        var dataEntriesTotal = [PieChartDataEntry]()
        var dataEntriesAndroid = [PieChartDataEntry]()
        var dataEntriesMac = [PieChartDataEntry]()
        var dataEntriesWindows = [PieChartDataEntry]()
        var dataEntriesDmoatStatus = [PieChartDataEntry]()
        
        let dataEntryTotal = PieChartDataEntry(value: Double(TotalConnectedDevices), label: "")
        dataEntriesTotal.append(dataEntryTotal)
        let dataEntryAndroid = PieChartDataEntry(value: Double(AndroidConnectedDevices), label: "")
        dataEntriesAndroid.append(dataEntryAndroid)
        let dataEntryMac = PieChartDataEntry(value: Double(MacConnectedDevices), label: "")
        dataEntriesMac.append(dataEntryMac)
        let dataEntryWindows = PieChartDataEntry(value: Double(WindowsConnectedDevices), label: "")
        dataEntriesWindows.append(dataEntryWindows)
        let dataEntryDmoat = PieChartDataEntry(value: Double(1), label: "")
        dataEntriesDmoatStatus.append(dataEntryDmoat)
        
        let chartDataSetTotal = PieChartDataSet(entries: dataEntriesTotal, label: "")
        chartDataSetTotal.colors = [CozyLoadingActivity.Settings.CnctdDevicesCircleColor]
        chartDataSetTotal.sliceSpace = 0
        chartDataSetTotal.selectionShift = 0
        chartDataSetTotal.drawIconsEnabled = false
        chartDataSetTotal.drawValuesEnabled = false
        let chartDataTotal = PieChartData(dataSet: chartDataSetTotal)
        
        let chartDataSetAndroid = PieChartDataSet(entries: dataEntriesAndroid, label: "")
        chartDataSetAndroid.colors = [CozyLoadingActivity.Settings.CnctdDevicesCircleColor]
        chartDataSetAndroid.sliceSpace = 0
        chartDataSetAndroid.selectionShift = 0
        chartDataSetAndroid.drawIconsEnabled = false
        let chartDataAndroid = PieChartData(dataSet: chartDataSetAndroid)
        
        let chartDataSetMac = PieChartDataSet(entries: dataEntriesTotal, label: "")
        chartDataSetMac.colors = [CozyLoadingActivity.Settings.CnctdDevicesCircleColor]
        chartDataSetMac.sliceSpace = 0
        chartDataSetMac.selectionShift = 0
        chartDataSetMac.drawIconsEnabled = false
        let chartDataMac = PieChartData(dataSet: chartDataSetMac)
        
        let chartDataSetWindows = PieChartDataSet(entries: dataEntriesTotal, label: "")
        chartDataSetWindows.colors = [CozyLoadingActivity.Settings.CnctdDevicesCircleColor]
        chartDataSetWindows.sliceSpace = 0
        chartDataSetWindows.selectionShift = 0
        chartDataSetWindows.drawIconsEnabled = false
        let chartDataWindows = PieChartData(dataSet: chartDataSetWindows)
        
        let chartDataSetDmoatStatus = PieChartDataSet(entries: dataEntriesDmoatStatus, label: "")
        if NetworkHelper.sharedInstance.isDmoatOnline {
            chartDataSetDmoatStatus.colors = [CozyLoadingActivity.Settings.SensorGoodColor]
        }else{
            chartDataSetDmoatStatus.colors = [CozyLoadingActivity.Settings.dmoatOfflineColor]
        }
        chartDataSetDmoatStatus.sliceSpace = 0
        chartDataSetDmoatStatus.selectionShift = 0
        chartDataSetDmoatStatus.drawIconsEnabled = false
        chartDataSetDmoatStatus.drawValuesEnabled = false
        let chartDatadmoatStatus = PieChartData(dataSet: chartDataSetDmoatStatus)
        
        others_pie_chart.data = chartDataTotal
        android_pie_chart.data = chartDataAndroid
        ios_pie_chart.data = chartDataMac
        windows_pie_chart.data = chartDataWindows
        
        android_pie_chart.chartDescription?.text = ""
        android_pie_chart.legend.enabled = false
        ios_pie_chart.chartDescription?.text = ""
        ios_pie_chart.legend.enabled = false
        windows_pie_chart.chartDescription?.text = ""
        windows_pie_chart.legend.enabled = false
        others_pie_chart.chartDescription?.text = ""
        others_pie_chart.legend.enabled = false
        
        
        
        others_pie_chart.dragDecelerationEnabled = true
        others_pie_chart.chartDescription?.text = ""
        others_pie_chart.legend.enabled = false
        others_pie_chart.drawCenterTextEnabled = false
        others_pie_chart.drawEntryLabelsEnabled = false
        others_pie_chart.holeColor = nil
        others_pie_chart.rotationEnabled = false
        others_pie_chart.drawCenterTextEnabled = false
        others_pie_chart.transparentCircleColor = UIColor.clear
        others_pie_chart.holeRadiusPercent = 0.95
        
        android_pie_chart.drawCenterTextEnabled = false
        android_pie_chart.drawEntryLabelsEnabled = false
        android_pie_chart.holeColor = nil
        android_pie_chart.rotationEnabled = false
        android_pie_chart.drawCenterTextEnabled = false
        android_pie_chart.holeRadiusPercent = 0.95
        
        ios_pie_chart.drawCenterTextEnabled = false
        ios_pie_chart.drawEntryLabelsEnabled = false
        ios_pie_chart.holeColor = nil
        ios_pie_chart.rotationEnabled = false
        ios_pie_chart.drawCenterTextEnabled = false
        ios_pie_chart.holeRadiusPercent = 0.95
        
        windows_pie_chart.drawCenterTextEnabled = false
        windows_pie_chart.drawEntryLabelsEnabled = false
        windows_pie_chart.holeColor = nil
        windows_pie_chart.rotationEnabled = false
        windows_pie_chart.drawCenterTextEnabled = false
        windows_pie_chart.holeRadiusPercent = 0.95
        
        
        
        //        android_pie_chart.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        //        ios_pie_chart.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        //        windows_pie_chart.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        //        others_pie_chart.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        //        dmoat_status_pie_chart.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
    }
    
    func setChart(dataPoints: [String], values: [Double]) {
        //        bandwidthChartDashboard.noDataText = "Bandwidth data was not processed. Please refresh screen and make sure your d.moat is connected to the Internet."
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let yVal = values[i]
            bandwidthChartDashboard.xAxis.valueFormatter = IndexAxisValueFormatter(values: dataPoints)
            let values = BarChartDataEntry(x: Double(i), y: yVal)
            dataEntries.append(values)
        }
        let chartDataSet = BarChartDataSet(entries : dataEntries, label: "")
        
        chartDataSet.drawValuesEnabled = false
        
        chartDataSet.colors = [CozyLoadingActivity.Settings.BarAColor, CozyLoadingActivity.Settings.BarBColor, CozyLoadingActivity.Settings.BarCColor, CozyLoadingActivity.Settings.BarDColor, CozyLoadingActivity.Settings.BarEColor]
        
        
        
        let data = BarChartData()
        data.barWidth = 0.2
        data.addDataSet(chartDataSet)
        bandwidthChartDashboard.data = data
        bandwidthChartDashboard.setScaleEnabled(false)
        

        //        bandwidthChartDashboard.xAxis.granularity = 0
        bandwidthChartDashboard.rightAxis.drawLabelsEnabled = false
        //        bandwidthChartDashboard.xAxis.labelPosition = .bottom
        bandwidthChartDashboard.dragDecelerationEnabled = true
        bandwidthChartDashboard.chartDescription?.text = ""
        bandwidthChartDashboard.drawBarShadowEnabled = false
        bandwidthChartDashboard.xAxis.drawGridLinesEnabled = false
        //                bandwidthChartDashboard.xAxis.spaceMax = 1
        bandwidthChartDashboard.xAxis.drawLabelsEnabled = false
        bandwidthChartDashboard.leftAxis.drawGridLinesEnabled = true
        bandwidthChartDashboard.leftAxis.spaceBottom = 0
        bandwidthChartDashboard.rightAxis.spaceBottom = 0
        bandwidthChartDashboard.rightAxis.drawGridLinesEnabled = false
        bandwidthChartDashboard.legend.enabled = false
        bandwidthChartDashboard.leftAxis.axisMinimum = 0
        //        bandwidthChartDashboard.leftAxis.granularityEnabled = true
        bandwidthChartDashboard.leftAxis.granularity = 1.0
        bandwidthChartDashboard.drawValueAboveBarEnabled = true
        //        bar_chart_bandwidth.xAxis.axisMaximum = 10
        //        bar_chart_bandwidth.barData?.barWidth = 0.5
        bandwidthChartDashboard.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        
    }
    
    func fillSensorChart(isDmoatOffline : Bool) {
        var dataEntriesTotal = [PieChartDataEntry]()
        var dataEntriesAndroid = [PieChartDataEntry]()
        var dataEntriesMac = [PieChartDataEntry]()
        
        
        if isDmoatOffline {
            let dataEntryTotal = PieChartDataEntry(value: Double(100), label: "")
            dataEntriesTotal.append(dataEntryTotal)
            let dataEntryAndroid = PieChartDataEntry(value: Double(100), label: "")
            dataEntriesAndroid.append(dataEntryAndroid)
            let dataEntryMac = PieChartDataEntry(value: Double(100), label: "")
            dataEntriesMac.append(dataEntryMac)
        }else{
            let dataEntryTotal = PieChartDataEntry(value: Double(TempValues), label: "")
            dataEntriesTotal.append(dataEntryTotal)
            let dataEntryAndroid = PieChartDataEntry(value: Double(HumidityValues), label: "")
            dataEntriesAndroid.append(dataEntryAndroid)
            let dataEntryMac = PieChartDataEntry(value: Double(AirQualityValue), label: "")
            dataEntriesMac.append(dataEntryMac)
        }
        let chartDataSetTotal = PieChartDataSet(entries: dataEntriesTotal, label: "")
        
        //        >= 32 hazardous
        //        >= 6 and <= 31 green
        //        <6 bad
        
        //        celcius ===   falhrenheit
        //        31             87.8
        //        5-32             41- 89.6
        if isDmoatOffline {
            chartDataSetTotal.colors = [CozyLoadingActivity.Settings.dmoatOfflineColor]
            pieChartTemp.drawCenterTextEnabled = true
            pieChartTemp.drawEntryLabelsEnabled = false
            chartDataSetTotal.drawValuesEnabled = false
            pieChartTemp.centerText = "0"
            
        }else{
            if (TempValues >= 87) {
                chartDataSetTotal.colors = [UIColor.red]
            }
            else if(TempValues >= 40 ){ //}&& TempValues < 90) {
                chartDataSetTotal.colors = [CozyLoadingActivity.Settings.SensorGoodColor]
            }
            else {
                chartDataSetTotal.colors = [CozyLoadingActivity.Settings.SensorBadColor]
            }
            pieChartTemp.drawCenterTextEnabled = true
            chartDataSetTotal.drawValuesEnabled = false
            
        }
        chartDataSetTotal.sliceSpace = 0
        chartDataSetTotal.selectionShift = 0
        //        chartDataSetTotal.drawIconsEnabled = false
        let chartDataTotal = PieChartData(dataSet: chartDataSetTotal)
        
        let chartDataSetAndroid = PieChartDataSet(entries: dataEntriesAndroid, label: "")
        chartDataSetAndroid.colors = [CozyLoadingActivity.Settings.CLASuccessColor]
        
        if isDmoatOffline {
            pieChartHumidity.drawCenterTextEnabled = true
            pieChartHumidity.drawEntryLabelsEnabled = false
            chartDataSetAndroid.drawValuesEnabled = false
            chartDataSetAndroid.colors = [CozyLoadingActivity.Settings.dmoatOfflineColor]
            pieChartHumidity.centerText = "0"
        }else{
            
            if HumidityValues >= 61 {
                chartDataSetAndroid.colors = [UIColor.red]
            }else if HumidityValues >= 30 { //} && HumidityValues < 60{
                chartDataSetAndroid.colors = [CozyLoadingActivity.Settings.SensorGoodColor]
            }else {
                chartDataSetAndroid.colors = [CozyLoadingActivity.Settings.SensorBadColor]
            }
            pieChartHumidity.drawCenterTextEnabled = true
            chartDataSetAndroid.drawValuesEnabled = false
        }
        chartDataSetAndroid.sliceSpace = 0
        chartDataSetAndroid.drawValuesEnabled = false
        pieChartHumidity.drawEntryLabelsEnabled = false
        chartDataSetAndroid.selectionShift = 0
        let chartDataAndroid = PieChartData(dataSet: chartDataSetAndroid)
        
        let chartDataSetMac = PieChartDataSet(entries: dataEntriesTotal, label: "")
        
        if isDmoatOffline {
            chartDataSetMac.colors = [CozyLoadingActivity.Settings.dmoatOfflineColor]
            pieChartAirQuality.drawCenterTextEnabled = true
            pieChartAirQuality.drawEntryLabelsEnabled = false
            chartDataSetMac.drawValuesEnabled = false
            pieChartAirQuality.centerText = "0"
        }else{
            
            if AirQualityValue >= 1000 {
                chartDataSetMac.colors = [UIColor.red]
            }
            else if AirQualityValue >= 600 { //} && AirQualityValue < 1000 {
                chartDataSetMac.colors = [CozyLoadingActivity.Settings.SensorBadColor]
            }
            else if AirQualityValue >= 450 { //} && AirQualityValue < 1000 {
                chartDataSetMac.colors = [CozyLoadingActivity.Settings.SensorBadColor]
            }else{
                chartDataSetMac.colors = [CozyLoadingActivity.Settings.SensorGoodColor]
            }
            pieChartAirQuality.drawCenterTextEnabled = true
            chartDataSetMac.drawValuesEnabled = false
        }
        chartDataSetMac.sliceSpace = 0
        chartDataSetMac.selectionShift = 0
        let chartDataMac = PieChartData(dataSet: chartDataSetMac)
        
        
        pieChartTemp.data = chartDataTotal
        pieChartHumidity.data = chartDataAndroid
        pieChartAirQuality.data = chartDataMac
        
        pieChartTemp.chartDescription?.text = ""
        pieChartTemp.legend.enabled = false
        pieChartHumidity.chartDescription?.text = ""
        pieChartHumidity.legend.enabled = false
        pieChartAirQuality.chartDescription?.text = ""
        pieChartAirQuality.legend.enabled = false
        
        
        pieChartTemp.dragDecelerationEnabled = true
        pieChartTemp.chartDescription?.text = ""
        pieChartTemp.holeColor = nil
        pieChartTemp.rotationEnabled = false
        //        pieChartTemp.drawCenterTextEnabled = true
        //        pieChartTemp.transparentCircleColor = UIColor.clear
        pieChartTemp.holeRadiusPercent = 0.95
        
        pieChartHumidity.holeColor = nil
        pieChartHumidity.rotationEnabled = false
        pieChartHumidity.holeRadiusPercent = 0.95
        
        pieChartAirQuality.holeColor = nil
        pieChartAirQuality.rotationEnabled = false
        pieChartAirQuality.holeRadiusPercent = 0.95
        
        pieChartTemp.isUserInteractionEnabled = true
        pieChartTemp.centerText = String(TempValues)
        pieChartHumidity.centerText = String(HumidityValues)
        pieChartAirQuality.centerText = String(AirQualityValue)
        
        //        pieChartTemp.transform.rotated(by: 180)
        if isDmoatOffline {
        }else{
            pieChartTemp.rotationAngle = 45.0
            pieChartHumidity.rotationAngle = 45.0
            pieChartAirQuality.rotationAngle = 45.0
            pieChartTemp.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
            pieChartHumidity.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
            pieChartAirQuality.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        }
    }
    
    
    func fillDmoatOfflineChart() {
        var dataEntriesDmoatStatus = [PieChartDataEntry]()
        
        let dataEntryDmoat = PieChartDataEntry(value: Double(100), label: "")
        dataEntriesDmoatStatus.append(dataEntryDmoat)
        
        
        let chartDataSetDmoatStatus = PieChartDataSet(entries: dataEntriesDmoatStatus, label: "")
        
        chartDataSetDmoatStatus.colors = [CozyLoadingActivity.Settings.dmoatOfflineColor]
        
        chartDataSetDmoatStatus.sliceSpace = 0
        chartDataSetDmoatStatus.selectionShift = 0
        chartDataSetDmoatStatus.drawIconsEnabled = false
        chartDataSetDmoatStatus.drawValuesEnabled = false
        let chartDatadmoatStatus = PieChartData(dataSet: chartDataSetDmoatStatus)
        
        
    }
    
    
    func fillStatusesChart(isActive : Bool, chart: PieChartView) {
        
        var dataEntriesTotal = [PieChartDataEntry]()
        
        let dataEntryTotal = PieChartDataEntry(value: Double(100), label: "")
        dataEntriesTotal.append(dataEntryTotal)
        
        
        let chartDataSetTotal = PieChartDataSet(entries: dataEntriesTotal, label: "")
        chartDataSetTotal.colors = [CozyLoadingActivity.Settings.CnctdDevicesCircleColor]
        chartDataSetTotal.sliceSpace = 0
        chartDataSetTotal.selectionShift = 0
        chartDataSetTotal.drawIconsEnabled = false
        chartDataSetTotal.drawValuesEnabled = false
        let chartDataTotal = PieChartData(dataSet: chartDataSetTotal)
        
        chart.data = chartDataTotal
        chart.chartDescription?.text = ""
        chart.legend.enabled = false
        chart.dragDecelerationEnabled = true
        chart.drawCenterTextEnabled = false
        chart.drawEntryLabelsEnabled = false
        chart.holeColor = nil
        chart.holeRadiusPercent = 0.95
        
    }
    
    //MARK: -Load data for dashboard
    func loadAlertsData() {
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let uuid_string = NetworkHelper.sharedInstance.uuidToken!
        let device_name = UIDevice.current.name
        let data = ["device_uuid": uuid_string, "device_name" : device_name ]
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "data" : data as AnyObject]
        
        NetworkHelper.sharedInstance.postDashboardRequest(serviceName: ApiUrl.GET_ALL_DASHBOARD_ALERTS_API, sendData: params, success: { (data) in
            print(data)
            let message = "Request failed.Please try again"
            
            let statusCode = self.defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil) {
                //                print(jsonArr?.count)
                let dataB = data["message"] as! NSDictionary
                self.BlockedAlertsCount = dataB["blocked"] as! Int
                self.InfoAlertsCount = dataB["info"] as! Int
                self.FlowAlertsCount = dataB["flow"] as! Int
                self.newAlertsReceived = dataB["alerts"] as! [[String : AnyObject]]
                print("info \(self.InfoAlertsCount) sensors \(self.SensorsAlertsCount) flow \(self.FlowAlertsCount) blocked \(self.BlockedAlertsCount)")
                
                //               self.populateDataInWatch()
                
                DispatchQueue.main.async(execute: {
                    if self.InfoAlertsCount > 0 || self.BlockedAlertsCount > 0 || self.SensorsAlertsCount > 0 || self.FlowAlertsCount > 0 {
                        self.fillAlertsChart(isDmoatOffline: false)
                    }else{
                        self.no_alerts_data_label.isHidden = false
                        //                        self.fillAlertsChart(isDmoatOffline: true)
                    }
                })
                DispatchQueue.main.async(execute: {
                    self.fetchAlertsData(self.newAlertsReceived)
                })
                
            }else{
                //                self.fillAlertsChart(isDmoatOffline: true)
                if ((statusCode?.range(of: "409")) != nil){
                    NetworkHelper.sharedInstance.isDmoatOnline = false
                }
                else{
                    //                self.showAlert(message)
                }
                //
            }
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    func loadConnectedDevicesData() {
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        NetworkHelper.sharedInstance.postDashboardRequest(serviceName: ApiUrl.GET_CNCTDHOST_DASHBOARD_API, sendData: params, success: { (data) in
            print(data)
            let message = "Request failed.Please try again"
            
            let statusCode = self.defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                let element = data["message"] as! NSDictionary
                self.TotalConnectedDevices = element["total"] as! Int
                self.AndroidConnectedDevices = element["android"] as! Int
                self.MacConnectedDevices = element["apple"] as! Int
                self.WindowsConnectedDevices = element["windows"] as! Int
                self.OtherConnectedDevices = element["others"] as! Int
                let dmoatStatus = element["dmoat_status"] as! String
                
                DispatchQueue.main.async(execute: {
                    if dmoatStatus == "online" {
                        self.dmoatStatus_img.image = UIImage(named: self.DMOAT_ONLINE_IMAGE)
                        self.dmoatStatusSimplified_lbl.text = self.DMOAT_ONLINE_SIMPLIFIED_TEXT
                        self.simplifiedOnline_image.image = UIImage(named: self.DMOAT_ONLINE_BG_IMAGE)
                        self.simplifiedEvironmentBG_img.image = UIImage(named: self.DMOAT_ENVIRONMENT_ONLINE_BG_IMAGE)
                        NetworkHelper.sharedInstance.isDmoatOnline = true
                        self.online_offline_status_lbl.text = "Online"
                        self.online_offline_status_lbl.textColor = CozyLoadingActivity.Settings.SensorGoodColor
                        self.online_offline_view.backgroundColor = CozyLoadingActivity.Settings.SensorGoodColor
                    }else{
                        self.dmoatStatus_img.image = UIImage(named: self.DMOAT_OFFLINE_IMAGE)
                        self.simplifiedOnline_image.image = UIImage(named: self.DMOAT_OFFLINE_BG_IMAGE)
                        self.simplifiedEvironmentBG_img.image = UIImage(named: self.DMOAT_ENVIRONMENT_OFFLINE_BG_IMAGE)
                        self.dmoatStatusSimplified_lbl.text = self.DMOAT_OFFLINE_SIMPLIFIED_TEXT
                        NetworkHelper.sharedInstance.isDmoatOnline = false
                        self.online_offline_status_lbl.text = "Offline"
                        self.online_offline_view.backgroundColor = CozyLoadingActivity.Settings.dmoatOfflineColor
                        
                        self.online_offline_status_lbl.textColor = CozyLoadingActivity.Settings.dmoatOfflineColor
                    }
                    
                    self.total_connected_devices_count_label.text = "(\(String(self.TotalConnectedDevices)))"
                    self.devicesConnectedCountSimplified_lbl.text = String(self.TotalConnectedDevices)
                    self.android_devices_count_label.text = "\(String(self.AndroidConnectedDevices))"
                    self.mac_devices_count_label.text = "\(String(self.MacConnectedDevices))"
                    self.windows_devices_count_label.text = "\(String(self.WindowsConnectedDevices))"
                    self.others_devices_count_label.text = "\(String(self.OtherConnectedDevices))"
                    
                })
                DispatchQueue.main.async(execute: {
                    if self.AndroidConnectedDevices == 0 && self.MacConnectedDevices == 0 && self.WindowsConnectedDevices == 0 && self.OtherConnectedDevices == 0 {
                        self.noConnectedDevicesAlert_lbl.isHidden = false
                    } else {
                        self.noConnectedDevicesAlert_lbl.isHidden = true
                    }
                    self.connectedDevicesStackView.isHidden = false
                    var count = 1
                    if self.AndroidConnectedDevices > 0 {
                        count += 1
                        self.android_logo.isHidden = false
                        self.android_pie_chart.isHidden = false
                        self.connectedDevicesStackView.addArrangedSubview(self.android_pie_chart)
                    }else{
                        self.android_logo.isHidden = true
                        self.android_pie_chart.isHidden = true
                        self.connectedDevicesStackView.removeArrangedSubview(self.android_pie_chart)
                    }
                    if self.MacConnectedDevices > 0 {
                        count += 1
                        self.ios_logo.isHidden = false
                        self.ios_pie_chart.isHidden = false
                        self.connectedDevicesStackView.addArrangedSubview(self.ios_pie_chart)
                    }else{
                        self.ios_logo.isHidden = true
                        self.ios_pie_chart.isHidden = true
                        self.connectedDevicesStackView.removeArrangedSubview(self.ios_pie_chart)
                    }
                    if self.WindowsConnectedDevices > 0 {
                        count += 1
                        self.windows_logo.isHidden = false
                        self.windows_pie_chart.isHidden = false
                        self.connectedDevicesStackView.addArrangedSubview(self.windows_pie_chart)
                    }else{
                        self.windows_logo.isHidden = true
                        self.windows_pie_chart.isHidden = true
                        self.connectedDevicesStackView.removeArrangedSubview(self.windows_pie_chart)
                    }
                    if self.OtherConnectedDevices > 0 {
                        count += 1
                        self.otherDevices_logo.isHidden = false
                        self.others_pie_chart.isHidden = false
                        self.connectedDevicesStackView.addArrangedSubview(self.others_pie_chart)
                    }else{
                        self.otherDevices_logo.isHidden = true
                        self.others_pie_chart.isHidden = true
                        self.connectedDevicesStackView.removeArrangedSubview(self.others_pie_chart)
                    }
                    
                    //                    self.connectedDevicesStackView.
                    self.fillConnectedDevicesChart()
                })
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                
            }
            else{
                //                self.showAlert(message)
            }
            //
        }, failure: { (data) in
            print(data)
            self.noConnectedDevicesAlert_lbl.isHidden = false
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    func loadBandwidthData() {
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        NetworkHelper.sharedInstance.postDashboardRequest(serviceName: ApiUrl.GET_TRAFFIC_STATS_DASHBOARD_API, sendData: params, success: { (data) in
            print(data)
            //            let message = ConstantStrings.ErrorText
            
            let statusCode = self.defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                self.bandwidthHostsReceived = (data["message"] as? [[String:AnyObject]])!
                //                print(jsonArr?.count)
                if((self.bandwidthHostsReceived.count) > 0){
                    self.fetchBandwidthData(self.bandwidthHostsReceived)
                    
                }else{
                    //                    self.showAlert("Bandwidth data was not processed. Please refresh screen and make sure your d.moat is connected to the Internet.")
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
            }
            
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        })
        
        
    }
    
    func loadSensorsData() {
        //clear previous sensor data
        self.TempValues = 0
        self.HumidityValues = 0
        self.AirQualityValue = 0
        //call service for getting sensor data
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let params:Dictionary<String,String> = ["request_id": requestID, "token": NetworkHelper.sharedInstance.getCurrentToken(), "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace(), "app_id": NetworkHelper.sharedInstance.app_ID!]
        
        NetworkHelper.sharedInstance.postDashboardRequest(serviceName: ApiUrl.GET_SENSOR_DATA_DASHBOARD_API, sendData: params as [String : AnyObject], success: { (data) in
            print(data)
            let message = ConstantStrings.ErrorText
            
            let statusCode = self.defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                let element = data["message"] as? [String:AnyObject]
                print("response== \(data["message"])")
                //                let dataa :AnyObject = element?["coppm"] as AnyObject //as AnyObject
                if element?["coppm"] != nil{
                    let score = element?["coppm"]
                    self.AirQualityValue = Int((score! ) as! Double)
                }
                
                if element?["humidity"] != nil {
                    let humidtyValue = element?["humidity"]
                    self.HumidityValues = Int((humidtyValue! ) as! NSNumber)
                }
                if element?["temperature"] != nil {
                    let temperatureValue = element?["temperature"]
                    var tempVal = Int((temperatureValue! ) as! NSNumber)
                    self.TempValues = ((tempVal * 9) / 5 + 32);
                }
                
                print("temp \(self.TempValues) humidity \(self.HumidityValues) air quality \(self.AirQualityValue)")
                DispatchQueue.main.async(execute: {
                    self.populateDataInWatch()
                    self.populateSensorValuesOnSimplified()
                    if self.TempValues > 0 {
                        self.fillSensorChart(isDmoatOffline: false)
                    }else{
                        //                        self.fillSensorChart(isDmoatOffline: true)
                    }
                })
            }else{
                if ((statusCode?.range(of: "409")) != nil){
                    NetworkHelper.sharedInstance.isDmoatOnline = false
                }
                
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    func populateSensorValuesOnSimplified() {
        self.airQualityValue_lbl.text = String(describing: self.AirQualityValue) + "PPM"
        self.humidityValue_lbl.text = String(describing: self.HumidityValues) + "%"
        self.temperatureValue_lbl.text = String(describing: self.TempValues) + "°F"
    }
    
    //MARK: - fetch data for dashboard
    
    func fetchAlertsData(_ jsonArr : [[String : AnyObject]]){
        viewAlertsForDashboard.removeAll()
        for element in jsonArr {
            let blockedAlrt = BlockedMutedAlertsModel()
            let inputSrc = element["input_src"] as! String
            
            if (inputSrc == "ids") {
                let description = element["description"] as! String
                let eveSec = element["eve_sec"] as! Int
                let blockType = element["blocktype"] as! String
                
                blockedAlrt.setDescription(description)
                blockedAlrt.setInputSrc(inputSrc)
                blockedAlrt.setIdsBlockType(blockType)
                
            }
                
            else if (inputSrc == "blockips") {
                let description = element["description"] as! String
                let eveSec = element["eve_sec"] as! Int
                let recordId = element["record_id"] as! String
                blockedAlrt.setInputSrc(inputSrc)
                blockedAlrt.setDescription(description)
                
            }
            else if(inputSrc == "cnctdhost") {
                let description = element["description"] as! String
                
                blockedAlrt.setDescription(description)
                blockedAlrt.setInputSrc(inputSrc)
                viewAlertsForDashboard.append(blockedAlrt)
            }
            else if inputSrc == "mode" {
                let modeFlag = element["mode_flag"] as! Int
                
                if modeFlag == 0 {
                    self.active_mode_lbl.text = "Plug & Play Mode Activated"
                    self.pnp_btn.setImage(UIImage(named: "plug_and_play"), for: .normal)
                    NetworkHelper.sharedInstance.isAdvanceModeActicated = false
                } else {
                    NetworkHelper.sharedInstance.isAdvanceModeActicated = true
                    self.active_mode_lbl.text = "Advanced Mode Activated"
                    self.pnp_btn.setImage(UIImage(named: "active_mode"), for: .normal)
                }
                
            } else if inputSrc == "parental_control" {
                let modeFlag = element["mode_flag"] as! Int
                
                if modeFlag == 1 {
                    
                    self.parental_control_status_lbl.text = "Parental Controls Activated"
                    self.parental_control_btn.setImage(UIImage(named: "parental_controal_active"), for: .normal)
                    // self.parental_control_status_lbl.textColor = CozyLoadingActivity.Settings.SensorGoodColor
                    
                } else {
                    
                    self.parental_control_status_lbl.text = "Activate Parental control"
                    self.parental_control_btn.setImage(UIImage(named: "parental_controal_disactive"), for: .normal)
                    //  self.parental_control_status_lbl.textColor = CozyLoadingActivity.Settings.dmoatOfflineColor
                    
                    
                }
                
            }
            else{
                print("some other alert")
            }
        }
        
        //reload table view/decorate chart view
        if viewAlertsForDashboard.count > 0 {
            self.no_alerts_data_label.isHidden = true
        }else{
            self.no_alerts_data_label.isHidden = false
        }
        self.table_view_alerts.reloadData()
    }
    
    
    func fetchConnectedDevicesData(_ jsonArr : [[String : AnyObject]]){
        
        for element in jsonArr {
            let totalConnectedDevices = element["total"] as! String
            let android_devices = element["android"] as! Int
            let mac_devices = element["apple"] as! String
            let windows_devices = element["windows"] as! String
            let other_devices = element["others"] as! String
            
            
        }
        //set count for connected devices
    }
    
    
    
    
    func fetchBandwidthData(_ jsonArr : [[String : AnyObject]]){
        bandWidthListForDashboard.removeAll()
        for element in jsonArr {
            //            let hID = element["hostid"] as! String
            let hIP = element["hostip"] as! String
            let hName = element["hostname"] as! String
            let protcol = element["protocols"] as! NSDictionary
            let totalUsage = element["totalusage"] as! Int
            let parameters = element["parameters"] as! String
            //add into model class
            let bandwidh = BandwidthModel()
            //            bandwidh.setHostID(hID)
            bandwidh.setHostIP(hIP)
            if ((hName.count < 0) || (hName.range(of: "") != nil) || (hName.range(of: "None") != nil)) {
                bandwidh.setHostName("Unknown")
            }else{
                bandwidh.setHostName(hName)
            }
            bandwidh.setProtocols(protcol)
            var convertedMB = Double(totalUsage) / 1048576 //1024
            if convertedMB < 0.01 {
                           convertedMB = 0.01
                       }
            var flotMBs = String(format: "%.1f", ceil(convertedMB*1000)/1000)
           
            bandwidh.setUsgae(flotMBs.floatValue)
            bandwidh.setParameters(parameters)
            
            bandWidthListForDashboard.append(bandwidh)
        }
        //reload table view/decorate chart view
        DispatchQueue.main.async {
            self.table_view_bandwidth.reloadData()
        }
        DispatchQueue.main.async {
            self.mb_label_bandwidth.isHidden = false
            
            var HostNames = [String]()
            var HostUsage = [Double]()
            for i in 0..<self.bandWidthListForDashboard.count {
                var hName = self.bandWidthListForDashboard[i].getHostName()
                var hUsage = self.bandWidthListForDashboard[i].getUsage() // 131072//1024
                if (hName.count > 6 ){
                    hName = String(hName.prefix(7))
                }else{
                    //                    hName = hName
                }
                HostNames.append(hName)
                HostUsage.append(Double(hUsage))
            }
            self.setChart(dataPoints: HostNames, values: HostUsage)
        }
    }
    
    //MARK: -Alerts Tableview delegates
    //Mark: - Tableview delegates
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        tableView.deselectRow(at: indexPath, animated: true)
        print("rowSelected\(indexPath.row)")
        
        if tableView == self.table_view_alerts {
            alertsSectionTapped()
        } else if tableView == self.table_view_bandwidth{
            bandwidthSectionTapped()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.table_view_alerts {
            return viewAlertsForDashboard.count
        }else if tableView == self.table_view_bandwidth{
            return bandWidthListForDashboard.count
        }
        return 0
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  self.table_view_alerts.dequeueReusableCell(withIdentifier: "alerts_dashboard") as! AlertsDashboardCell
        if tableView == self.table_view_alerts {
            
            let infoCell =  self.table_view_alerts.dequeueReusableCell(withIdentifier: "alerts_dashboard") as! AlertsDashboardCell
            
            //            let infoCell =  self.table_view_alerts.dequeueReusableCell(withIdentifier: "InfoAlertsDashboardCell") as! InfoAlertsDashboardCell // for info text -- mode, parental control mode, internet pause status
            
            let item = self.viewAlertsForDashboard[indexPath.row]
            if item.getInputSrc() == "cnctdhost" {
                infoCell.alert_description.text = item.getDescription()
                return infoCell
            }
        }
        else if tableView == self.table_view_bandwidth{
            let cell =  self.table_view_bandwidth.dequeueReusableCell(withIdentifier: "bandwidth_dashboard") as! BandwidthDashboardCell
            let item = self.bandWidthListForDashboard[indexPath.row]
            cell.hostname_label.text = item.getHostName()
            if indexPath.row == 0 {
                cell.legend_clr_label.backgroundColor = CozyLoadingActivity.Settings.BarAColor
            } else if indexPath.row == 1 {
                cell.legend_clr_label.backgroundColor = CozyLoadingActivity.Settings.BarBColor
            }else  if indexPath.row == 2 {
                cell.legend_clr_label.backgroundColor = CozyLoadingActivity.Settings.BarCColor
            }else  if indexPath.row == 3 {
                cell.legend_clr_label.backgroundColor = CozyLoadingActivity.Settings.BarDColor
            }else  if indexPath.row == 04 {
                cell.legend_clr_label.backgroundColor = CozyLoadingActivity.Settings.BarEColor
            }
            return cell
        }
        
        return cell
    }
    //
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {
        
        home_label_dashboard.textColor = UIColor(red: 19/255, green: 101/255, blue: 165/255, alpha: 1.0)
        let image: UIImage = UIImage(named: "tab_home_selected")!
        home_img_dashboard.image = image
        
        loadAlertsData()
        loadConnectedDevicesData()
        loadBandwidthData()
        loadSensorsData()
    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        //        account_label_tabbar.textColor = UIColor(red: 0/255, green: 174/255, blue: 239/255, alpha: 1.0)
        //        var image: UIImage = UIImage(named: "tab_home_selected")!
        //        account_img_tabbar.image = image
        
        if(self.checkPausedTimeAsAndroid()){
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
        
    }
    
    
    func appStoreAppVersion() -> (String,String,Bool)?
    {
        guard let bundleInfo = Bundle.main.infoDictionary else {
            print("Counldn't fetch bundleInfo.")
            return nil
        }
        let bundleId = bundleInfo[kCFBundleIdentifierKey as String] as! String
        // dbug__print("bundleId = \(bundleId)")
        
        let currentVersion = bundleInfo["CFBundleShortVersionString"] as! String
        
        print(currentVersion)
        
        let address = "http://itunes.apple.com/lookup?bundleId=\(bundleId)"
        // dbug__print("address = \(address)")
        
        guard let url = URLComponents.init(string: address)?.url else {
            print("Malformed internet address: \(address)")
            return nil
        }
        guard let data = try? Data.init(contentsOf: url) else {
            //if Util.isInternetAvailable() {
            print("Web server request failed. Yet internet is reachable. Url was: \(address)")
            //}// else: internet is unreachable. All ok. It is of course impossible to fetch the appStoreAppVersion like this.
            return nil
        }
        // dbug__print("data.length = \(data.length)")
        
        if data.count < 100 { //: We got 42 for a wrong address. And aproximately 4684 for a good response
            print("Web server message is unexpectedly short: \(data.count) bytes")
        }
        
        guard let response = try? JSONSerialization.jsonObject(with: data, options: []) else {
            print("Failed to parse server response.")
            return nil
        }
        guard let responseDic = response as? [String: AnyObject] else {
            print("Not a dictionary keyed with strings. Response with unexpected format.")
            return nil
        }
        guard let resultCount = responseDic["resultCount"] else {
            print("No resultCount found.")
            return nil
        }
        guard let count = resultCount as? Int else { //: Swift will handle NSNumber.integerValue
            print("Server response resultCount is not an NSNumber.integer.")
            return nil
        }
        //:~ Determine how many results we got. There should be exactly one, but will be zero if the URL was wrong
        guard count == 1 else {
            print("Server response resultCount=\(count), but was expected to be 1. URL (\(address)) must be wrong or something.")
            return nil
        }
        guard let rawResults = responseDic["results"] else {
            print("Response does not contain a field called results. Results with unexpected format.")
            return nil
        }
        guard let resultsArray = rawResults as? [AnyObject] else {
            print("Not an array of results. Results with unexpected format.")
            return nil
        }
        guard let resultsDic = resultsArray[0] as? [String: AnyObject] else {
            print("Not a dictionary keyed with strings. Results with unexpected format.")
            return nil
        }
        guard let rawVersion = resultsDic["version"]  else {
            print("The key version is not part of the results")
            return nil
        }
        
        guard let trackUrl = resultsDic["trackViewUrl"] else {
            print("The key version is not part of the results")
            return nil
        }
        guard let urlStr = trackUrl as? String else {
            print("Version is not a String")
            return nil
        }
        let appStoreUrl = urlStr.replacingOccurrences(of: "&uo=4", with: "")
        print(appStoreUrl)
        guard let versionStr = rawVersion as? String else {
            print("Version is not a String")
            return nil
        }
        
        appStoreVersion = versionStr.e_trimmed()
        
        if versionStr.e_trimmed().compare(currentVersion, options: NSString.CompareOptions.numeric) == ComparisonResult.orderedDescending {
            return (versionStr.e_trimmed(),appStoreUrl,true)
        }
        
        
        return nil
    }
    
    func openAppStoreUrl(_ version: String, url: String) {
        //    var urls = url
        //    urls = "https://itunes.apple.com/es/app/d-moat/id1260963418?mt=8"
        let alert = UIAlertController(title: "Update App!", message: "New version \(version) is available on AppStore.Update app and re-login.", preferredStyle: .alert)
        let update = UIAlertAction(title: "Update", style: .default) { (action) in
            if let checkURL = URL(string: url) {
                
                if UIApplication.shared.canOpenURL(checkURL) {
                    print("url successfully opened")
                    
                }
                
            } else {
                print("invalid url")
            }
        }
        alert.addAction(update)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - parental Control Actions
    @IBAction func ParentalControlValueChanged(_ sender: Any) {
        if parentalControl_switchBtn.isOn {
            print("parental control is on")
            
            currentParentalCOntrolStatus = "Enable"
            showParentalControlDialog(switchButton: parentalControl_switchBtn)
            
        } else {
            
            currentParentalCOntrolStatus = "Disable"
            showParentalControlDialog(switchButton: parentalControl_switchBtn)
            
            print("parental control is off")
        }
    }
    
    func showParentalControlDialog(switchButton : UISwitch) {
        var msg : String = "Are you sure you want to \(currentParentalCOntrolStatus) the parental control?"
        if NetworkHelper.sharedInstance.isAdvanceModeActicated {
            if parentalControl_switchBtn.isOn {
                msg = "Please make sure you have added 165.227.193.150 as your DNS in your router settings."
            } else {
                msg = "Please make sure you have removed 165.227.193.150 as your DNS from your router settings."
            }
        } else {
            msg = "Are you sure you want to \(currentParentalCOntrolStatus) the parental control?"
        }
        let alertController = UIAlertController(title: "Parental Control", message: msg, preferredStyle: .alert)
        
        // Create the actions
        let pauseAction = UIAlertAction(title: currentParentalCOntrolStatus, style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("\(self.currentParentalCOntrolStatus) pressed")
            self.callPauseInternet(timeInterval: "00", isParentalControl : true)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("cancel Pressed")
            
            if switchButton.isOn {
                switchButton.setOn(false, animated: true)
            } else {
                switchButton.setOn(true, animated: true)
            }
            
        }
        // Add the actions
        alertController.addAction(pauseAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func callPauseInternet(timeInterval : String, isParentalControl : Bool){
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        let eveSec = NSDate().timeIntervalSince1970
        var parentalControltype : String = "active"
        
        
        var data : [String : AnyObject] = ["id" : "pause" as AnyObject, "type" : timeInterval as AnyObject, "eve_sec" : eveSec as AnyObject ]
        if isParentalControl {
            if currentParentalCOntrolStatus == "Enable" {
                parentalControltype = "active"
            } else {
                parentalControltype = "inactive"
            }
            data = ["id" : "parental_control" as AnyObject, "type" : parentalControltype as AnyObject]
        } else {
            data = ["id" : "pause" as AnyObject, "type" : timeInterval as AnyObject, "eve_sec" : eveSec as AnyObject ]
        }
        
        let params:Dictionary<String,AnyObject> = ["namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "data" : data as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject,"token" : NetworkHelper.sharedInstance.getCurrentToken() as AnyObject,"request_id" : requestID as AnyObject,"postkey" : NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject]
        print(params)
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.PAUSE_INTERNET_API, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                if isParentalControl {
                    if parentalControltype == "active" {
                        NetworkHelper.sharedInstance.saveIfParentalControlOn(true)
                        DispatchQueue.main.async {
                            self.showAlert("Parental control enabled.")
                        }
                        DispatchQueue.main.async {
                            self.parentalControl_switchBtn.setOn(true, animated: true)
                        }
                        //                        self.parentalControlStatus_lbl.text = "Disable"
                        //                        self.parentalControlStatus_lbl.textColor = UIColor.gray
                    } else {
                        NetworkHelper.sharedInstance.saveIfParentalControlOn(false)
                        DispatchQueue.main.async {
                            self.showAlert("Parental control disabled")
                            self.parentalControl_switchBtn.setOn(false, animated: true)
                            
                        }
                        
                        //                        self.parentalControlStatus_lbl.text = "Enable"
                        //                        self.parentalControlStatus_lbl.textColor = UIColor.white
                    }
                    //change button text and color
                } else {
                    DispatchQueue.main.async {
                        self.showAlert("Internet traffic paused. It will resume after the specified interval.")
                    }
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }  else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            }else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    //MARK: -APPstore version check
    func checkAppVersion() {
        if let version = appStoreAppVersion() {
            print("version is \(version)")
            self.openAppStoreUrl(version.0, url: version.1)
        }else {
            //if ok then work
            print("App version is updated.")
            let defaults = UserDefaults.standard
            //
            if defaults.integer(forKey: "app_version") != nil {
                let appPreviousVersionSaved = defaults.string(forKey: "app_version")
                //
                if appPreviousVersionSaved != nil{
                    print("currrent and appstore versions are \(appPreviousVersionSaved) -- \(appStoreVersion)")
                    
                    if appPreviousVersionSaved != appStoreVersion {
                        showReLogInAlert() //uncomment while uploading app for production
                        print("you have updated version, go to login and run app")
                    }else{
                        print("version check is OK")
                    }
                    
                }else{
                    print("no appstore version saved, please relogin")
                    showReLogInAlert()
                }
            }
            else{
                //                showReLogInAlert()
                print("appversion key not found, go to login")
            }
        }
        
    }
    func showReLogInAlert(){
        let alertController = UIAlertController(title: "Prytex", message: "Prytex has been updated. Kindly re-login.", preferredStyle: .alert)
        
        // Create the actions
        let osAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.dismiss(animated: true, completion: {});
            self.goToLogIn()
            //            self.navigationController?.popViewController(animated: true);
        }
        
        // Add the actions
        alertController.addAction(osAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func goToLogIn() {
        UserDefaults.standard.set(appStoreVersion, forKey: "app_version")
        UserDefaults.standard.synchronize()
        NetworkHelper.sharedInstance.isNotificationLocked = true
        //unregister for apns
        UIApplication.shared.unregisterForRemoteNotifications()
        //mute notifications
        if(NetworkHelper.sharedInstance.deviceToken != nil)
        {
            (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
            (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
        }
        if(self.isInternetAvailable()){
            //unsubscribe from all
            (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
            //clear nsuserdefaults
            NetworkHelper.sharedInstance.clearUserData()
            //delete all data
            AlertModelRealm.deleteAllRecords()
            PolicyManagementRealm.deleteAllRecords()
            let loginVC = Login.loginVC()
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
    }
    
}
extension String {
    func e_trimmed() -> String
    {
        return trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    
}



