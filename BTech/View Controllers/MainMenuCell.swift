//
//  MainMenuCell.swift
//  BTech
//
//  Created by Ahmed on 30/09/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class MainMenuCell: UICollectionViewCell {
    
    @IBOutlet var imageCell: UIImageView!
    
    @IBOutlet var labelCell: UILabel!
    
    @IBOutlet var titleCell: UILabel!
  
}
