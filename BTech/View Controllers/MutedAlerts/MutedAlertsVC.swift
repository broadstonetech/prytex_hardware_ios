//
//  MutedAlertsVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 22/12/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import FirebaseAnalytics
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


class MutedAlertsVC: Parent {
    
    
    //Mark: - class outlets
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var load_muted_btn: UIButton!
    @IBOutlet weak var load_pre_muted_btn: UIButton!
    @IBOutlet weak var muted_view: UIView!
    @IBOutlet weak var pre_muted_view: UIView!
    
    @IBOutlet weak var error_label: UILabel!
    //Mark: - class variables
    var clickIndex = -1
    var currentIndex = -1
    var alertTypeBlockedMuted:String = ""
    var titleText:String = ""
    let rightButtonBar = UIButton()
    var isMutedShowing : Bool = true
    var isPreMutedShowing : Bool = false
    var MutedAlertsList: [BlockedMutedAlertsModel] = [BlockedMutedAlertsModel]()
    var preMutedAlertsList: [PreMutedAlertModel] = [PreMutedAlertModel]()
    
    class func mutedAlertsVC() -> MutedAlertsVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "muted_vc") as! MutedAlertsVC
    }
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //call muted api
        self.loadMutedAlerts(ApiUrl.GET_MUTED_ALERTS_API)
        
        refreshControl.addTarget(self, action: #selector(refreshBlockedMutedAlerts), for: .valueChanged)
        if #available(iOS 10.0, *) {
            table_view.refreshControl = refreshControl
        } else {
            // Fallback on earlier versions
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
//        Analytics.setScreenName(titleText, screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "TitleTxt", AnalyticsParameterScreenClass: screenClass])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.titleText = "Muted Alerts"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.navigationItem.title = titleText
        //self.title = titleText
        createRightButtonNavigationBar()
        
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createRightButtonNavigationBar()
    {
        
        rightButtonBar.setImage(UIImage(named: "info"), for: UIControl.State())
        rightButtonBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        rightButtonBar.addTarget(self, action: #selector(topRightButtonBar), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = rightButtonBar
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        //create custom
    }
    
    @objc func topRightButtonBar()
    {
        self.showAlert(ConstantStrings.mutedAlert_screen_txt)
    }
    
    @objc func refreshBlockedMutedAlerts(refreshControl: UIRefreshControl) {
        MutedAlertsList.removeAll()
        //call muted api
        if (isPreMutedShowing) {
            print("refreshing pre muted!")
            self.loadMutedAlerts(ApiUrl.GET_PRE_MUTED_ALERTS_API)
        }else{
            print("refreshing muted!")
            self.loadMutedAlerts(ApiUrl.GET_MUTED_ALERTS_API)
        }
        // somewhere in your code you might need to call:
        refreshControl.endRefreshing()
    }
    
    //MARK: -TopBar actions
    
    @IBAction func load_muted_pressed(_ sender: Any) {
        self.error_label.isHidden = true
        //        load_muted_btn.backgroundColor = CozyLoadingActivity.Settings.selectedButonColor
        load_muted_btn.setTitleColor(CozyLoadingActivity.Settings.WhiteColor, for: .normal)
        //        load_pre_muted_btn.backgroundColor = CozyLoadingActivity.Settings.UnselectedButonColor
        load_pre_muted_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
        isMutedShowing = true
        isPreMutedShowing = false
        if MutedAlertsList.count == 0  {
            error_label.isHidden = false
        }
        DispatchQueue.main.async {
            self.table_view.reloadData()
        }
    }
    @IBAction func load_pre_muted_pressed(_ sender: Any) {
        self.error_label.isHidden = true
        //        load_muted_btn.backgroundColor = CozyLoadingActivity.Settings.UnselectedButonColor
        load_muted_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
        //        load_pre_muted_btn.backgroundColor = CozyLoadingActivity.Settings.selectedButonColor
        load_pre_muted_btn.setTitleColor(CozyLoadingActivity.Settings.WhiteColor, for: .normal)
        isPreMutedShowing = true
        isMutedShowing = false
        if preMutedAlertsList.count == 0  {
            error_label.isHidden = false
        }
        self.loadMutedAlerts(ApiUrl.GET_PRE_MUTED_ALERTS_API)
        DispatchQueue.main.async {
            self.table_view.reloadData()
        }
    }
    
    //Mark: -Tableview delegates
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if isMutedShowing {
            print("rowSelected\(indexPath.row)")
            table_view.isUserInteractionEnabled = true
        }else {
            
            if self.clickIndex == indexPath.row{
                
                self.clickIndex = -1
            }else{
                self.clickIndex = indexPath.row
            }
            self.table_view.reloadData()
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat
    {
        if isMutedShowing {
            let item = self.MutedAlertsList[indexPath.row]
            let inputSrc = item.getInputSrc()
            if (inputSrc == "ids") {
                return 140
            } else if (inputSrc == "cnctdhost"){
                return 150
            } else{
                return 85
            }
        }else{
            if clickIndex == indexPath.row{
                return CGFloat(77 + (44 * preMutedAlertsList[indexPath.row].arraySets.count))
            }else{
                return 77  //for pre muted... i have to set it according to no of values in each section later
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isMutedShowing {
            return MutedAlertsList.count
        }else{
            return preMutedAlertsList.count
        }
    }
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let flow_cell =  self.table_view.dequeueReusableCell(withIdentifier: "flow_blockedmuted_cell") as! FlowCellTableViewCell
        let cnctd_cell =  self.table_view.dequeueReusableCell(withIdentifier: "cnctd_blockedmuted_cell") as! ConnectedDevicesViewCell
        let idsHost_cell =  self.table_view.dequeueReusableCell(withIdentifier: "ids_blockedmuted_cell") as! AlertViewCell
        let preMuted_cell = self.table_view.dequeueReusableCell(withIdentifier: "pre_muted_outer_cell") as! PreMutedOuterCell
        
        if isMutedShowing {
            
            if MutedAlertsList.count > 0 {
                
                let item = self.MutedAlertsList[indexPath.row]
                //convert evesec to time,date
                let time = item.getEveSec()
                let timeInterval  = TimeInterval(time) // as NSTimeInterval
                //Convert to Date
                let date = Date(timeIntervalSince1970: timeInterval)
                //Date formatting
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM-dd-yyyy h:mm:a"
                dateFormatter.timeZone = TimeZone.current //name: "UTC")
                let dateString = dateFormatter.string(from: date)
                
                //
                let inputSrc = item.getInputSrc()
                let alertType = item.getFlowType()
                if (inputSrc == "flow") {
                    
                    
                    if(alertType == "normal" || alertType == "limit_exceed" || alertType == "metadata"){
                        let day = getDayHourTimeStamp(time, hour: false, day: true, format: false)
                        let hour = getDayHourTimeStamp(time, hour: true, day: false, format: false)
                        var format = getDayHourTimeStamp(time, hour: false, day: false, format: true)
                        let nextHour = Int(hour)! + 1
                        if format.range(of: "am") != nil {
                            format = "am"
                        }else{
                            format = "pm"
                        }
                        
                        flow_cell.descLabel.text = item.getDescription() + " on \(day) between \(hour)\(format)-\(nextHour)\(format)."// +  + hour
                        flow_cell.ipLabel_heading.isHidden = true
                        flow_cell.ipLabel.isHidden = true
                    }else{
                        flow_cell.ipLabel.isHidden = false
                        flow_cell.ipLabel_heading.isHidden = false
                        flow_cell.ipLabel_heading.text = "Client Info:"
                        flow_cell.ipLabel.text = item.getFlowDetail()
                        flow_cell.descLabel.text = item.getDescription()
                    }
                    
                    flow_cell.blockButton.setImage( UIImage(named:"unmute_blue"), for: UIControl.State())
                    flow_cell.timestampLabel.text = String(dateString)
                    flow_cell.blockButton.tag = indexPath.row
                    flow_cell.setNeedsDisplay()
                    return flow_cell
                } else if (inputSrc == "cnctdhost") {
                    cnctd_cell.blockButton.setImage( UIImage(named:"unmute_blue"), for: UIControl.State())
                    
                    cnctd_cell.blockButton.tag = indexPath.row
                    cnctd_cell.descriptionLabel.text = item.getDescription()
                    cnctd_cell.timeStampLabel.text = String(dateString)
                    cnctd_cell.OSNameLabel.text = item.getOS()
                    cnctd_cell.ipAddressLabel.text = item.getIP()
                    cnctd_cell.hostnameLabel.text = item.getHostName()
                    cnctd_cell.macAddressLabel.text = item.getMacAddress()
                    cnctd_cell.setNeedsDisplay()
                    return cnctd_cell
                }else if (inputSrc == "blockips") {
                    
                    
                    if (alertTypeBlockedMuted == "blocked") {
                        flow_cell.blockButton.setImage( UIImage(named:"unmute_blue"), for: UIControl.State())
                        
                        
                        flow_cell.descLabel.text = item.getDescription()
                        flow_cell.ipLabel.isHidden = false
                        flow_cell.ipLabel.text = item.getIP()
                        flow_cell.timestampLabel.text = String(dateString)
                        flow_cell.blockButton.tag = indexPath.row
                        flow_cell.setNeedsDisplay()
                        return flow_cell
                    }
                    else{
                        idsHost_cell.blockButton.setImage( UIImage(named:"unmute_blue"), for: UIControl.State())
                        
                        idsHost_cell.blockButton.tag = indexPath.row
                        idsHost_cell.nameLabel.text = item.getDescription()
                        idsHost_cell.categoryOneIP.text = item.getSrcIp()
                        idsHost_cell.protocolSSH.text = item.getSrcPort()
                        idsHost_cell.categoryTwoIP.text = item.getDestIp()
                        idsHost_cell.protocolSSHTwo.text = item.getDestPort()
                        idsHost_cell.eve_sec.text = String(dateString)
                        idsHost_cell.setNeedsDisplay()
                        return idsHost_cell
                    }
                }
                    
                else{
                    return flow_cell
                }
            }
        }else if isPreMutedShowing {
            if preMutedAlertsList.count > 0 {
                //                preMuted_cell.preMutedAlertsListIPs = preMutedAlertsList
                let items = self.preMutedAlertsList[indexPath.row]
                //convert evesec to time,date
                let time = items.getEveSec()
                
                let timeInterval  = TimeInterval(time) // as NSTimeInterval
                
                //Convert to Date
                let date = Date(timeIntervalSince1970: timeInterval)
                
                //Date formatting
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM-dd-yyyy h:mm:a"
                dateFormatter.timeZone = TimeZone.current //name: "UTC")
                let dateString = dateFormatter.string(from: date)
                
                preMuted_cell.descLabel.text = items.getDescription()
                preMuted_cell.timestamp.text = dateString
                preMuted_cell.preMutedAlert = items
                return preMuted_cell
            }
        }
        return flow_cell
    }
    
    //MARK: -Unmute alert actions
    
    @IBAction func unmute_flowAlert_pressed(_ sender: Any) {
        let  flowBtn = sender as! UIButton
        currentIndex = flowBtn.tag
        self.performUnMuteAction(user_action: "unmute_action")
        
    }
    @IBAction func unmute_cnctdHostAlert_pressed(_ sender: Any) {
        let  cnctdHstBtn = sender as! UIButton
        print(cnctdHstBtn.tag)
        currentIndex = cnctdHstBtn.tag
        self.performUnMuteAction(user_action: "unmute_action")
        
    }
    @IBAction func unmute_idsAlert_pressed(_ sender: Any) {
        let  idsBtn = sender as! UIButton
        print(idsBtn.tag)
        currentIndex = idsBtn.tag
        self.performUnMuteAction(user_action: "unmute_action")
        
    }
    
    //MARK: - web service call
    func loadMutedAlerts(_ url : String) {
        NetworkHelper.sharedInstance.arrayBlockedAlerts.removeAll()
        //call service for getting blocked alerts data
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        NetworkHelper.sharedInstance.error401 = false
        let params:Dictionary<String,String> = ["request_id": requestID, "token": NetworkHelper.sharedInstance.getCurrentToken(), "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace(), "app_id": NetworkHelper.sharedInstance.app_ID!]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: url, sendData: params as [String : AnyObject], success: { (data) in
            print(data)
            let message = ConstantStrings.ErrorText
            
            if(NetworkHelper.sharedInstance.isConnectedDevicesSuccess)
            {
                let jsonArr = data["message"] as? [[String:AnyObject]]
                
                //                print(jsonArr?.count)
                if(jsonArr?.count > 0){
                    print("json array received for muted alerts")
//                    self.error_label.isHidden = true
                    if self.isPreMutedShowing{
                        //                        self.MutedAlertsList.removeAll()
                        self.fetchPreMutedAlertsData(jsonArr!)
                    }else{
                        self.preMutedAlertsList.removeAll()
                        self.fetchMutedAlertsData(jsonArr!)
                    }
                }else{
//                    self.showAlert("No alerts were detected.")
                    
//                    DispatchQueue.main.async {
//                    self.error_label.text = "No alerts were detected."
//                    }
                    
                }
            }
            else{
                self.showAlert(message)
//                self.error_label.isHidden = false
//                self.error_label.text = message

            }
            
        }, failure: { (data) in
            print("failure error is \(data)")
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
            
        })
        
    }
    
    //MARK: - fetch data
    func fetchMutedAlertsData(_ jsonArr : [[String : AnyObject]]){
        //        MutedAlertsList.removeAll()
        for element in jsonArr {
            let blockedAlrt = BlockedMutedAlertsModel()
            let inputSrc = element["input_src"] as! String
            if (inputSrc == "ids") {
                let description = element["description"] as! String
                let eveSec = element["eve_sec"] as! Int
                //                let destHostname = element["dest_hostname"] as? String
                //                let srcHostName = element["src_hostname"] as? String
                let srcIp = element["src_ip"] as! String
                let recordId = element["record_id"] as! String
                let destPort = element["dest_port"] as! String
                let srcPort = element["src_port"] as! String
                let destIp = element["dest_ip"] as! String
                
                blockedAlrt.setDescription(description)
                blockedAlrt.setEveSec(eveSec)
                //                blockedAlrt.setDestHostName(destHostname!)
                //                blockedAlrt.setSrcName(srcHostName!)
                blockedAlrt.setSrcIp(srcIp)
                blockedAlrt.setRecordId(recordId)
                blockedAlrt.setDestIp(destIp)
                blockedAlrt.setInputSrc(inputSrc)
                
                if(srcPort.range(of: "") != nil || srcPort.count < 1) {
                    blockedAlrt.setSrcPort("Unknown")
                }else{
                    blockedAlrt.setSrcPort(srcPort)
                }
                if (destPort.range(of: "") != nil || destPort.count < 1) {
                    blockedAlrt.setDestPort("Unknown")
                }else{
                    blockedAlrt.setDestPort(destPort)
                }
            }
            else if (inputSrc == "flow") {
                let description = element["description"] as! String
                let eveSec = element["eve_sec"] as! Int
                let recordId = element["record_id"] as! String
                if(element["type"] != nil){
                    let type = element["type"] as! String
                    if(type.range(of: "") != nil || type.count < 1){
                        blockedAlrt.setFlowType("Unknown")
                    }else{
                        blockedAlrt.setFlowType(type)
                    }
                }
                
                if(element["detail"] != nil){
                    let detail = element["detail"] as! String
                    blockedAlrt.setFlowDetail(detail)
                }else{
                    blockedAlrt.setFlowDetail("[Unknown]")
                }
                blockedAlrt.setInputSrc(inputSrc)
                blockedAlrt.setDescription(description)
                blockedAlrt.setEveSec(eveSec)
                blockedAlrt.setRecordId(recordId)
                
            }
            else if (inputSrc == "blockips") {
                let description = element["description"] as! String
                let eveSec = element["eve_sec"] as! Int
                let recordId = element["record_id"] as! String
                let ip = element["ip"] as! String
                blockedAlrt.setInputSrc(inputSrc)
                blockedAlrt.setDescription(description)
                blockedAlrt.setEveSec(eveSec)
                blockedAlrt.setRecordId(recordId)
                blockedAlrt.setIp(ip)
                
            }
            else if(inputSrc == "cnctdhost") {
                let description = element["description"] as! String
                let eveSec = element["eve_sec"] as! Int
                let recordId = element["record_id"] as! String
                let parameter = element["parameters"] as! String
                let macAdres = element["macaddress"] as! String
                let hostName = element["hostname"] as! String
                let hostID = element["HOSTID"] as! String
                let ip = element["ip"] as! String
                let os = element["OS"] as! String
                
                blockedAlrt.setDescription(description)
                blockedAlrt.setEveSec(eveSec)
                blockedAlrt.setInputSrc(inputSrc)
                blockedAlrt.setRecordId(recordId)
                blockedAlrt.setParameters(parameter)
                blockedAlrt.setMacAddress(macAdres)
                blockedAlrt.setHostName(hostName)
                blockedAlrt.sethostID(hostID)
                blockedAlrt.setIp(ip)
                blockedAlrt.setOS(os)
            }
            else{
                print("some other alert")
            }
            
            MutedAlertsList.append(blockedAlrt)
        }
        //reload table view/decorate chart view
        
        MutedAlertsList.sort(by: sorterForFileIDASC)
        self.refreshControl.endRefreshing()
        DispatchQueue.main.async {
            self.error_label.isHidden = true
            self.table_view.reloadData()
        }
    }
    //MARK: -Fetch pre-muted alerets
    func fetchPreMutedAlertsData(_ jsonArr : [[String : AnyObject]]){
        preMutedAlertsList.removeAll()
        for element in jsonArr {
            let preMuteAlrt = PreMutedAlertModel()
            let description = element["description"] as! String
            let eveSec = element["eve_sec"] as! Int
            
            preMuteAlrt.setDescription(description)
            preMuteAlrt.setEveSec(eveSec)
            
            let jsonArrIPs = element["sets"] as? [[String:AnyObject]]
            preMuteAlrt.arraySets = jsonArrIPs! as! [[String : String]]
            preMutedAlertsList.append(preMuteAlrt)
            
        }
        //reload table view/decorate chart view
        
        preMutedAlertsList.sort(by: sorterForPreMuted)
        self.refreshControl.endRefreshing()
        DispatchQueue.main.async {
            self.table_view.reloadData()
            
        }
    }
    //MARK: -Sort alerts
    func sorterForFileIDASC(_ this:BlockedMutedAlertsModel, that:BlockedMutedAlertsModel) -> Bool {
        return this.getEveSec() > that.getEveSec()
    }
    func sorterForPreMuted(_ this:PreMutedAlertModel, that:PreMutedAlertModel) -> Bool {
        return this.getEveSec() > that.getEveSec()
    }
    
    //MARK: - Unblock or mute alerts
    
    func performUnMuteAction(user_action : String) {
        
        let inputSrc = MutedAlertsList[self.currentIndex].getInputSrc()
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        
        let allowAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.Allow.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.Allow.rawValue)
            self.sendBlockSheetResponseMutedBlocked("", type: "Allow", interval: "")
            
        })
        let trustOneHourAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.TrustOneHour.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.Allow.rawValue)
            self.sendBlockSheetResponseMutedBlocked("", type: "Allow", interval: "")
            
        })
        let trustDeviceAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.TrustDevice.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.Allow.rawValue)
            self.sendBlockSheetResponseMutedBlocked("", type: "Allow", interval: "")
            
        })
        
        let unmuteAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.Unmute.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.Unmute.rawValue)
            self.sendBlockSheetResponseMutedBlocked("", type: "Unmute", interval: "")
        })
        let blockForeverAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.BlockForever.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.BlockForever.rawValue)
            self.sendBlockSheetResponseMutedBlocked("Forever", type: "Block", interval: "")
        })
        
        
        let blockForOneMinAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.BlockForOneMint.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.BlockForOneMint.rawValue)
            self.sendBlockSheetResponseMutedBlocked("Interval", type: "Block", interval: "900")
        })
        
        
        let muteAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.Mute.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.Mute.rawValue)
            self.sendBlockSheetResponseMutedBlocked("", type: "Mute", interval: "")
        })
        
        let unblockAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.UnBlock.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.UnBlock.rawValue)
            self.sendBlockSheetResponseMutedBlocked("", type: "Unblock", interval: "")
        })
        
        let keepBlockAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.KeepBlock.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.KeepBlock.rawValue)
            self.sendBlockSheetResponseMutedBlocked("", type: "Keepblock", interval: "")
        })
        
        let cancelAction = UIAlertAction(title:CozyLoadingActivity.BlockOptions.Cancel.rawValue, style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        //check if input source is connected host
        if (user_action == "unblock_action") {
            
            optionMenu.addAction(allowAction)
            optionMenu.addAction(unblockAction)
            optionMenu.addAction(cancelAction)
        }else {
            if(inputSrc=="cnctdhost")
            {
                optionMenu.addAction(allowAction)
                optionMenu.addAction(blockForeverAction)
                optionMenu.addAction(unmuteAction)
                optionMenu.addAction(cancelAction)
            }
                
            else if(inputSrc=="ids")
            {
                
                if(inputSrc=="post")
                {
                    optionMenu.addAction(allowAction)
                    optionMenu.addAction(muteAction)
                    optionMenu.addAction(blockForeverAction)
                    optionMenu.addAction(blockForOneMinAction)
                    optionMenu.addAction(cancelAction)
                }
                    
                else if(inputSrc=="pre")
                {
                    optionMenu.addAction(unblockAction)
                    optionMenu.addAction(muteAction)
                    optionMenu.addAction(keepBlockAction)
                    optionMenu.addAction(blockForOneMinAction)
                    optionMenu.addAction(cancelAction)
                }else{
                    
                    optionMenu.addAction(allowAction)
                    optionMenu.addAction(blockForeverAction)
                    optionMenu.addAction(unmuteAction)
                    optionMenu.addAction(cancelAction)
                }
            }
                
            else if(inputSrc=="flow")
            {
                let flowType = MutedAlertsList[self.currentIndex].getFlowType()
                //                print(model.blocktype)
                //no type post/pre
                if ((flowType.range(of: "metadata") != nil) || (flowType.range(of: "new_host") != nil)) {
                    optionMenu.addAction(trustOneHourAction)
                    optionMenu.addAction(blockForeverAction)
                    optionMenu.addAction(unmuteAction)
                    optionMenu.addAction(trustDeviceAction)
                    optionMenu.addAction(cancelAction)
                }else{
                    optionMenu.addAction(allowAction)
                    //            optionMenu.addAction(blockForeverAction)
                    optionMenu.addAction(unmuteAction)
                    optionMenu.addAction(cancelAction)
                }
                
            }
            else if(inputSrc=="blockips")
            {
                optionMenu.addAction(allowAction)
                optionMenu.addAction(unblockAction)
                optionMenu.addAction(cancelAction)
            }
                
            else {
                return
            }
        }
        
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    func sendBlockSheetResponseMutedBlocked(_ mode : String, type : String, interval : String)
    {
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let inputSrc = MutedAlertsList[currentIndex].getInputSrc()
        let recordId = MutedAlertsList[currentIndex].getRecordId()
        let eveSec = MutedAlertsList[currentIndex].getEveSec()
        let description = MutedAlertsList[currentIndex].getDescription()
        //        let alertId =
        var url:String = ""
        url = ApiUrl.RESPONSE_MUTED_ALERT_API
        
        //call service for changing password
        let response : Dictionary <String, AnyObject> = ["interval": interval as AnyObject, "mode": mode as AnyObject, "type": type as AnyObject]
        var data : Dictionary <String, AnyObject> = [:]
        //create dictionary for IDS
        if(inputSrc=="ids")
        {
            let destIP = MutedAlertsList[currentIndex].getDestIp();
            let destPort = MutedAlertsList[currentIndex].getDestPort();
            let srcPort = MutedAlertsList[currentIndex].getSrcPort();
            let srcIP = MutedAlertsList[currentIndex].getSrcIp();
            data = ["description": description as AnyObject, "dest_ip": destIP as AnyObject, "dest_port": destPort as AnyObject, "eve_sec": eveSec as AnyObject, "input_src": inputSrc as AnyObject, "record_id": recordId as AnyObject, "src_ip": srcIP as AnyObject, "src_port": srcPort as AnyObject, "response": response as AnyObject]
        }
            //create dictionary for flow
        else if(inputSrc=="flow")
        {
            data = ["record_id" : recordId as AnyObject, "input_src" : inputSrc as AnyObject,  "description": description as AnyObject, "eve_sec": eveSec as AnyObject, "response": response as AnyObject]
            
        }
            //create dict for blockips
        else if(inputSrc=="blockips")
        {
            let type = MutedAlertsList[currentIndex].getIP();
            data = ["record_id" : recordId as AnyObject, "input_src" : inputSrc as AnyObject,  "description": description as AnyObject, "eve_sec": eveSec as AnyObject, "response": response as AnyObject]
            
        }
            
            //create dictionary for connected host
        else if(inputSrc=="cnctdhost")
        {
            
            let Ip = MutedAlertsList[currentIndex].getIP();
            let hostNAme = MutedAlertsList[currentIndex].getHostName();
            let macAddres = MutedAlertsList[currentIndex].getMacAddress()
            let Os = MutedAlertsList[currentIndex].getOS()
            let hostId = MutedAlertsList[currentIndex].gethostID()
            
            data = ["description" : description as AnyObject, "input_src" :  inputSrc as AnyObject, "device_category" :  inputSrc as AnyObject,"ip" : Ip as AnyObject, "hostname" : hostNAme as AnyObject,"HOSTID" : hostId as AnyObject, "mac_address": macAddres as AnyObject
                , "record_id": recordId as AnyObject, "OS": Os as AnyObject, "response": response as AnyObject]
            
        }
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "data" : data as AnyObject]
        
        
        NetworkHelper.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                self.MutedAlertsList.remove(at: self.currentIndex)
                self.table_view.reloadData()
                
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                let message = data["message"] as! String
                self.showAlert(message)
            }else{
                self.showAlert(ConstantStrings.ErrorText)
            }
            
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
            
        })
        
    }
    //MARK: -GetHourDayFormat
    func getDayHourTimeStamp(_ oldDate: Int, hour: Bool, day: Bool, format: Bool) -> String {
        //        let time = Int(oldDate)
        let timeInterval  = TimeInterval(oldDate) // as NSTimeInterval
        //Convert to Date
        let date = Date(timeIntervalSince1970: timeInterval)
        //Date formatting
        let dateFormatter = DateFormatter()
        
        if hour{
            dateFormatter.dateFormat = "h"
            
        }else if day{
            dateFormatter.dateFormat = "EEEE"
            
        }else{
            dateFormatter.dateFormat = "h:mm:ss"
            
        }
        
        dateFormatter.timeZone = TimeZone.current //name: "UTC")
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    
}
