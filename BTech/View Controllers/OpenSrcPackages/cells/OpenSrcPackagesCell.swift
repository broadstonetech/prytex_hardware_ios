//
//  OpenSrcPackagesCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 02/08/2018.
//  Copyright © 2018 Broadstone Technologies. All rights reserved.
//

import UIKit

class OpenSrcPackagesCell: UITableViewCell {

    @IBOutlet weak var package_name_label: UILabel!
    @IBOutlet weak var package_version_label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
