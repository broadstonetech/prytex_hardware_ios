//
//  Parent.swift
//  BTech
//
//  Created by Talha Ejaz on 22/08/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
//import QRCodeReader
//import ZDCChat
//import ChatProvidersSDK
//import ChatSDK
//import SDKConfigurations
//import MessagingSDK
import SystemConfiguration
import PDFKit
import FirebaseAuth
import FirebaseFirestore


class Parent: UIViewController {
    
    var channelListener: ListenerRegistration?
    private let db = Firestore.firestore()
    
    var documentQueryObject: QueryDocumentSnapshot?
     private var messageReference: CollectionReference?
    
    private var channelReference: CollectionReference {
        return db.collection("channels")
    }
    var isFirstTime :Bool = false
    var documentID : String?
    var window:UIWindow?
    
    deinit {
        channelListener?.remove()
    }
    //var myController : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ViewAlerts") as! ViewAlerts
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setNavBar()
        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc
    func pushNotificationReceived() {
         print("Notification Received!")
         if(NetworkHelper.sharedInstance.pushNotification)
         {
            NetworkHelper.sharedInstance.pushNotification = false
            //let alertVC = ViewAlerts.viewAlerts()
            //alertVC.titleText="View Alerts"
            //alertVC.alertType="view_alerts"

            if(NetworkHelper.sharedInstance.viewAlertsPushedInNavigationStack == false)
            {
                if((UIApplication.shared.delegate as! AppDelegate).checkTopControllerViewAlerts() == false)
                {
                    self.navigationController?.pushViewController(ViewAlerts.viewAlerts(), animated: true)
                }
                NetworkHelper.sharedInstance.viewAlertsPushedInNavigationStack = true
            }
            
        }
    }
    
    func sendWelcomeMessage(_ channel: Channel){
        guard let id = channel.id else {
          navigationController?.popViewController(animated: true)
          return
        }

        var userName:String?
        var email:String?
        var phone:String?
        var namespace:String?
        if NetworkHelper.sharedInstance.name != nil {
            phone = ""
            userName = NetworkHelper.sharedInstance.name
            email = NetworkHelper.sharedInstance.email
            namespace = NetworkHelper.sharedInstance.getCurrentUserNamespace()
            
        }else{
            phone = ""
            userName = ""
            email = ""
        }
        let rep: [String : Any] = [
          "userNAME": userName,
          "email": email,
          "phone": phone,
          "namespace":namespace,
          "deviceName":UIDevice.current.name,
          "deviceOS":UIDevice.current.systemName,
          "deviceVersion":UIDevice.current.systemVersion
        ]
        let messageString = "Username: " + userName! + "\n email : " + email! + "\n phone : " + phone! + "\n namespace : " + namespace! + "\n deviceName : " + UIDevice.current.name + "\n deviceOS : " + UIDevice.current.systemName + "\n deviceVersion : " + UIDevice.current.systemVersion
        let message = chatMessage(content: messageString)
        messageReference = db.collection(["channels", id, "thread"].joined(separator: "/"))
    
        messageReference?.addDocument(data: rep) { error in
            if let e = error {
              print("Error sending message: \(e.localizedDescription)")
              return
            }
            print("Welcome message added in following channel",channel.id!)
          }
        
    }
    
    
    func setNavBar()
    {
      //  self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.white
      //  self.navigationController?.navigationBar.barTintColor = UIColor(named: "navBar")
        
        if #available(iOS 15.0, *) {
                    let appearance = UINavigationBarAppearance()
                    appearance.configureWithOpaqueBackground()
                    appearance.backgroundColor = UIColor(named: "navBar")!
                    appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
                    UINavigationBar.appearance().standardAppearance = appearance
                    
                    UINavigationBar.appearance().scrollEdgeAppearance = appearance
                
                } else {
                    self.navigationController?.navigationBar.barTintColor = UIColor(named: "navBar")!
                    self.navigationController?.navigationBar.topItem?.title = ""
                    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
                }

        
       
        
//        let button1 = UIBarButtonItem(image: UIImage(named: "alert"), style: .plain, target: self, action: Selector("action")) // action:#selector(Class.MethodName) for swift 3
//        self.navigationItem.rightBarButtonItem  = button1
//        self.navigationController?.navigationItem.rightBarButtonItem = button1
    }
    
    func showAlert(_ message:String)
    {
//        let alert = UIAlertController(title: "d.moat", message: message, preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//
//        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        let alertController = UIAlertController(title: "Prytex", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(OKAction)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        
        let messageText = NSMutableAttributedString(
            string: message,
            attributes: [
                NSAttributedString.Key.paragraphStyle: paragraphStyle,
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0)
            ]
        )
        
        alertController.setValue(messageText, forKey: "attributedMessage")
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    func openConversationWindow() {
        Auth.auth().signInAnonymously(completion: nil)

        print("chat button pressed")
        let namespace = NetworkHelper.sharedInstance.getCurrentUserNamespace()
        let filterString =  namespace
        channelListener = channelReference.whereField("name", isEqualTo: filterString).addSnapshotListener { [self] querySnapshot, error in
            
            if let error = error{
                print("error is",error.localizedDescription)
            }else{
                if let snapshot = querySnapshot {
                    
                    for document in snapshot.documents{
                        self.documentQueryObject = document
                    }
                    
                    if snapshot.documents.count != 0 {
                        self.documentQueryObject = snapshot.documents[0]
                        let channel = Channel(document: self.documentQueryObject!)
                        self.channelListener?.remove()
                        self.documentID = channel?.id
                        UserDefaults.standard.set((channel?.id!)! as String, forKey: "channelFirebaseID")
                        let vc = ChatViewController(channel: channel!)
                        self.navigationController?.pushViewController(vc, animated: true)
                       
                    }else{
                        // no thread ,create new one
                       
                        let channel = Channel(name: namespace , creator:NetworkHelper.sharedInstance.name!)
                        
                        let ref = self.channelReference.document()
                        // ref is a DocumentReference
                        self.documentID = ref.documentID
                        // id contains the random ID
                        ref.setData(channel.representation){error in
                            if let e = error {
                                print("Error saving channel: \(e.localizedDescription)")
                            }else{
                                print("document created: \(self.documentID)")
                                self.saveToServer()
                                self.isFirstTime  = true
                            }
                        }

                    }
                }
            }
        }
        //Create a configuration object
        
//
////        ZDCChat.instance().chatViewController.title = "Support"
//        Chat.init().backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
//
//
//        ZDCChat.updateVisitor { user in
//            if NetworkHelper.sharedInstance.name != nil {
//                user?.phone = ""
//                user?.name = NetworkHelper.sharedInstance.name
//                user?.email = NetworkHelper.sharedInstance.email
//            }else{
//            user?.phone = ""
//            user?.name = ""
//            user?.email = ""
//        }
//        }
//        ZDCChat.start { config in
//            config?.department = "Billing"
//            config?.tags = ["subscription", "mobile_app"]
//            config?.preChatDataRequirements.name = .required
//            config?.preChatDataRequirements.email = .required
//            config?.preChatDataRequirements.phone = .optionalEditable
//            config?.preChatDataRequirements.message = .required
//            config?.emailTranscriptAction = .prompt
//        }
//
//        let chatAPIConfiguration = ChatAPIConfiguration()
//        chatAPIConfiguration.department = "Billing"
//        chatAPIConfiguration.tags = ["subscription", "mobile_app"]
//        Chat.instance?.configuration = chatAPIConfiguration
//
//        //Chat.instance.ba
//        let sdkConfigs = SDKConfigurations.Configurations.init()
//
//        do {
//          let chatEngine = try ChatEngine.engine()
//            chatEngine.configuration.preChatFormConfiguration.name = .required
//            chatEngine.configuration.preChatFormConfiguration.email = .required
//            chatEngine.configuration.preChatFormConfiguration.phoneNumber = .optional
//          let viewController = try Messaging.instance.buildUI(engines: [chatEngine], configs: [])
//
//            navigationController?.pushViewController(viewController, animated: true)
//            navigationController?.navigationBar.tintColor = CozyLoadingActivity.Settings.CLAppThemeColor
//            navigationController?.navigationBar.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
//            navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
//
//        } catch {
//          // handle error
//        }
        
        
//        ZDCChat.instance().chatViewController.navigationController?.navigationBar.tintColor = CozyLoadingActivity.Settings.CLAppThemeColor
//
//        ZDCChat.instance().chatViewController.navigationController?.navigationBar.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
    
//        ZDCChat.instance().chatViewController.navigationController?.navigationBar.topItem?.titleView?.placeholderText = "Support"
//        ZDCChat.instance().chatViewController.title = "Support"
        
//        ZDCChat.instance().chatViewController.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
//
//                ZDCChat.start(nil)

//                ZDCChat.start(in: self.navigationController, withConfig: nil)

       // Chat.instance.api.trackEvent("Event description")
//        ZDCChat.instance().overlay.setEnabled(false)

    }
    
    func saveToServer(){
        
        var device_info : Dictionary <String, String> = [:]
      
         device_info = [
          "device_name":UIDevice.current.name as String,
          "device_os":UIDevice.current.systemName as String,
            "device_version":UIDevice.current.systemVersion as String,
            "device_type":UIDevice.current.model,
            "device_manufacturar":"Apple"
        ]
        let namespace = NetworkHelper.sharedInstance.getCurrentUserNamespace()
       // let deviceToken = UserDefaults.standard.value(forKey: "deviceToken") as! String

        let data :Dictionary<String,Any> = ["creator" :NetworkHelper.sharedInstance.name! as String,"creator_name":NetworkHelper.sharedInstance.name! as String, "phone":"","creator_email":NetworkHelper.sharedInstance.email! as String,"creator_namespace":namespace,"creater_device_info":device_info as Any,"firebase_id":self.documentID! as String,"app_id":"dmoat","creator_device_token":NetworkHelper.sharedInstance.uuidToken! as String]
        
        
        print(data)
 
        
        NetworkHelper.sharedInstance.postChatRequest(serviceName:ApiUrl.START_CHAT, sendData: data as [String : AnyObject], success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            let status = data["status"] as! Bool
            
            if ((statusCode?.range(of: "200")) != nil){
                if status{
                    DispatchQueue.main.async {
                       // self.showAlert("Success")
                        print("successly saved to server")
                    }
                }else{
                    self.showAlert("All agents are busy at the moment.Try again later")
                }

      
            }
  
        }, failure: { (data) in
            print(data)
            
        })
        
  
    }
    
    func isValidEmail(_ testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    
    func setTabBarSelection(_ isHome:Bool, _ isPolicy:Bool, _ isAccount:Bool, _ isIntercom:Bool) {
        
        NetworkHelper.sharedInstance.isHomeTabSelected = false
        NetworkHelper.sharedInstance.isPolicyTabSelected = false
        NetworkHelper.sharedInstance.isAccountTabSelected = false
        NetworkHelper.sharedInstance.isIntercomTabSelected = false
        
        if isHome {
            NetworkHelper.sharedInstance.isHomeTabSelected = true
        } else if isPolicy {
            NetworkHelper.sharedInstance.isPolicyTabSelected = true
        } else if isAccount {
            NetworkHelper.sharedInstance.isAccountTabSelected = true
        } else if isIntercom {
            NetworkHelper.sharedInstance.isIntercomTabSelected = true
        }
    }
     // MARK: - check pause internet time expiry
    func checkPausedTimeAsAndroid() -> Bool{
        
        NetworkHelper.sharedInstance.currentPausedDateTime = Date();
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        formatter.dateStyle = .long
        
        formatter.string(from: NetworkHelper.sharedInstance.currentPausedDateTime)
        if (NetworkHelper.sharedInstance.savedPausedDate.compare(NetworkHelper.sharedInstance.currentPausedDateTime) == ComparisonResult.orderedDescending){
            ///saved date is greater
            NetworkHelper.sharedInstance.isPausedInternet = true
            print("saved date greater")
            return false
        }else{
            print("saved less than current")
            NetworkHelper.sharedInstance.isPausedInternet = false
            return true
        }
        
    }


    //MARK: -Internet connectivity
    
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    func openPdfFileFromURL(fileurl : String) {
        if #available(iOS 11.0, *) {
                      let pdfView = PDFView(frame: self.view.bounds)
                  
                  self.view.addSubview(pdfView)

                  // Fit content in PDFView.
                  pdfView.autoScales = true

                  // Load .pdf file from URL.
                      pdfView.document = PDFDocument(url: URL.init(string: fileurl)!)
             } else {
                 // Fallback on earlier versions
             }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
