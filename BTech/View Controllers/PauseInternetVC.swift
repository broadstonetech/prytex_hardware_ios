//
//  PauseInternetVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 13/02/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import FirebaseAnalytics
// hard coded policy
   var hardCodePolicy : PolicyPlanModel = PolicyPlanModel()
   var hardCodePolicy2 : PolicyPlanModel = PolicyPlanModel()
   

class PauseInternetVC: Parent, UITableViewDataSource, UITableViewDelegate{
    
    //MARK: - Class Variables
    var titleText:String = ""
    let rightButtonBar = UIButton()
    var clickIndex = -1
    var currentIndex = -1
    var policyDurationList: [PolicyDurationModel] = [PolicyDurationModel]()
    var policyControlsList: [PolicyPlanModel] = [PolicyPlanModel]()
    var protocolControlsList: [PolicyPlanModel] = [PolicyPlanModel]()
    var selectedIndex = -1
    var policySession:String = ""
    var isPolicyControlPressed : Bool = false
    var isProtocolManagerPressed : Bool = false
    var currentParentalCOntrolStatus : String = ""
    var isSessionDataHiden = true
    
    //MARK: - Class outlets
    
    @IBOutlet weak var sesssion_heading_lbl: UILabel!
    @IBOutlet weak var pause_internet_text_label: UILabel!
    
    @IBOutlet weak var parentalControlText_lbl: UILabel!
    
    @IBOutlet weak var parentalControl_switchBtn: UISwitch!
    
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var table_view_policy_plans: UITableView!
    
    @IBOutlet weak var policyDataSessionLabel: UILabel!
    @IBOutlet weak var policy_usage_view: UIView!
    @IBOutlet weak var policy_plans_view: UIView!
    @IBOutlet weak var policy_usage_tab_btn: UIButton!
    @IBOutlet weak var parental_plan_tab_btn: UIButton!
    @IBOutlet weak var noPolicyTextLabel: UILabel!
    
    @IBOutlet weak var protocol_manager_btn: UIButton!
    
    
    //MARK: - Class functions
    class func PauseInternetVC() -> PauseInternetVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "pause_internet") as! PauseInternetVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table_view.delegate = self
        self.table_view.dataSource = self
        self.table_view_policy_plans.delegate = self
        self.table_view_policy_plans.dataSource = self
        self.titleText = "Policy"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.title = titleText
        
        self.table_view_policy_plans.sectionHeaderHeight = UITableView.automaticDimension
        self.table_view_policy_plans.rowHeight = UITableView.automaticDimension
        table_view_policy_plans.estimatedSectionHeaderHeight = 300
        table_view_policy_plans.estimatedRowHeight = 200
        
        self.table_view.sectionHeaderHeight = UITableView.automaticDimension
        self.table_view.rowHeight = UITableView.automaticDimension
        table_view.estimatedSectionHeaderHeight = 300
        table_view.estimatedRowHeight = 200
        
        self.loadPolicyDurationsData()
        
        // Do any additional setup after loading the view.
        self.policy_usage_tab_btn.backgroundColor = UIColor(named: "UpperTabSelected")
        self.parental_plan_tab_btn.backgroundColor = UIColor(named: "ViewPatchB")
        self.protocol_manager_btn.backgroundColor = UIColor(named: "ViewPatchB")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
     //   Analytics.setScreenName("Policy Usage", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Policy Usage", AnalyticsParameterScreenClass: screenClass])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.titleText = "Policy"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.navigationItem.title = titleText
        createRightButtonNavigationBar()
        self.populateHardCodedData()
        
//        if NetworkHelper.sharedInstance.isAdvanceModeActicated {
//
//        } else {
//            self.protocol_manager_btn.setTitleColor(.gray, for: .normal)
//
//        } //uncomment as its now available for both p&p and advanced modes. (kashif)
        if isPolicyControlPressed {
            loadAllPolicies()
        }else if isProtocolManagerPressed {
            loadProtocolPolicies()
        }
        self.showHideSessionData()
    }
    
    func createRightButtonNavigationBar()
    {
        
        rightButtonBar.setImage(UIImage(named: "info"), for: UIControl.State())
        rightButtonBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        rightButtonBar.addTarget(self, action: #selector(topRightButtonBar), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = rightButtonBar
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        //create custom
    }
    
    @objc func topRightButtonBar()
    {
        self.showAlert(ConstantStrings.policy_screen_txt)
    }
    
    //MARK: -Top Nav BarActions
    
    @IBAction func policy_usage_btn_pressed(_ sender: Any) {
        isPolicyControlPressed = false
        isProtocolManagerPressed = false;
        policy_usage_view.isHidden = false
        policy_plans_view.isHidden = true
        updateTopBarColors(isUsage: true, isAccessControl: false)
        
    }
    
    @IBAction func parental_control_btn_pressed(_ sender: Any) {
        isPolicyControlPressed = true
        isProtocolManagerPressed = false
        policy_plans_view.isHidden = false
        policy_usage_view.isHidden = true
        updateTopBarColors(isUsage: false, isAccessControl: true)
        loadAllPolicies()
      
        
    }
    
    @IBAction func protocol_manager_btn_pressed(_ sender: Any) {
//        if NetworkHelper.sharedInstance.isAdvanceModeActicated {
        print("protocol manger pressed")
        NetworkHelper.sharedInstance.BlockAppProtocolsKeyValues() //prepare protocol key values
        loadProtocolPolicies()
            
//        } else {
//            self.protocol_manager_btn.setTitleColor(.gray, for: .normal)
//                  self.showAlert("d.moat is in plug & play mode. Protocols can be blocked in advanced mode. Switch d.moat to advanced mode.")
//              } // commented as its available for both P&P and Advanced modes (kashif)
    }
    
    
    func updateTopBarColors(isUsage : Bool, isAccessControl : Bool) {
        if isUsage {
            self.policy_usage_tab_btn.setTitleColor(CozyLoadingActivity.Settings.WhiteColor, for: .normal)
            self.parental_plan_tab_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
            self.protocol_manager_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
            
            self.policy_usage_tab_btn.backgroundColor = UIColor(named: "UpperTabSelected")
            self.parental_plan_tab_btn.backgroundColor = UIColor(named: "ViewPatchB")
            self.protocol_manager_btn.backgroundColor = UIColor(named: "ViewPatchB")
        }
        else if isAccessControl {
            self.parental_plan_tab_btn.setTitleColor(CozyLoadingActivity.Settings.WhiteColor, for: .normal)
            self.policy_usage_tab_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
            self.protocol_manager_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
            
            self.parental_plan_tab_btn.backgroundColor = UIColor(named: "UpperTabSelected")
            self.policy_usage_tab_btn.backgroundColor = UIColor(named: "ViewPatchB")
            self.protocol_manager_btn.backgroundColor = UIColor(named: "ViewPatchB")
            
        }
        else{
            self.protocol_manager_btn.setTitleColor(CozyLoadingActivity.Settings.WhiteColor, for: .normal)
            self.parental_plan_tab_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
            self.policy_usage_tab_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
            
            self.protocol_manager_btn.backgroundColor = UIColor(named: "UpperTabSelected")
            self.parental_plan_tab_btn.backgroundColor = UIColor(named: "ViewPatchB")
            self.policy_usage_tab_btn.backgroundColor = UIColor(named: "ViewPatchB")
        }
        
    }
    
    //MARK: -PauseButton Action
    @IBAction func pause_internet_pressed(_ sender: Any) {
        if(NetworkHelper.sharedInstance.isDmoatOnline){
            if(self.checkPausedTimeAsAndroid()){
                self.showPauseIntervalDialog()  //hidden in phase1
            }
            else{
                self.showAlert(ConstantStrings.pausedIntenettext)
            }
        }else{
            self.showAlert(ConstantStrings.dmoatOffline)
        }
    }
    // MARK: -Device usage report btn action
    @IBAction func device_usage_report_pressed(_ sender: Any) {
        
        self.showHideSessionData()
    }
    func showPauseIntervalDialog() {
        let optionMenu = UIAlertController(title: nil, message: "Pause Internet Interval:", preferredStyle: .actionSheet)
        let off = UIAlertAction(title: "10 minutes", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.showPauseInternetDialog(timeInterval: "10")
            
        })
        let fiveSeconds = UIAlertAction(title: "20 minutes", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.showPauseInternetDialog(timeInterval: "20")
            
        })
        
        let tenSeconds = UIAlertAction(title: "30 minutes", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.showPauseInternetDialog(timeInterval: "30")
            
        })
        
        let cancelAction = UIAlertAction(title:"Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        optionMenu.addAction(off)
        optionMenu.addAction(fiveSeconds)
        optionMenu.addAction(tenSeconds)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    func showParentalControlDialog(switchButton : UISwitch) {
        var msg : String = "Are you sure you want to \(currentParentalCOntrolStatus) the parental control?"
        if NetworkHelper.sharedInstance.isAdvanceModeActicated {
             if parentalControl_switchBtn.isOn {
                msg = "Please make sure you have added 165.227.193.150 as your DNS in your router settings."
             } else {
                msg = "Please make sure you have removed 165.227.193.150 as your DNS from your router settings."
            }
        } else {
            msg = "Are you sure you want to \(currentParentalCOntrolStatus) the parental control?"
        }
        let alertController = UIAlertController(title: "Parental Control", message: msg, preferredStyle: .alert)
        
        // Create the actions
        let pauseAction = UIAlertAction(title: currentParentalCOntrolStatus, style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("\(self.currentParentalCOntrolStatus) pressed")
            self.callPauseInternet(timeInterval: "00", isParentalControl : true)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("cancel Pressed")
            
            if switchButton.isOn {
                switchButton.setOn(false, animated: true)
            } else {
                switchButton.setOn(true, animated: true)
            }
            
        }
        // Add the actions
        alertController.addAction(pauseAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func showPauseInternetDialog(timeInterval : String) {
        let alertController = UIAlertController(title: "Pause Internet", message: "Are you sure you want to pause internet? You will not be able to use internet for next \(timeInterval) minutes.", preferredStyle: .alert)
        
        // Create the actions
        let pauseAction = UIAlertAction(title: "Pause", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("pause internet pressed")
            self.callPauseInternet(timeInterval: timeInterval, isParentalControl: false)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("cancel Pressed")
        }
        // Add the actions
        alertController.addAction(pauseAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func callPauseInternet(timeInterval : String, isParentalControl : Bool){
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        let eveSec = NSDate().timeIntervalSince1970
        var parentalControltype : String = "active"
        
        
        var data : [String : AnyObject] = ["id" : "pause" as AnyObject, "type" : timeInterval as AnyObject, "eve_sec" : eveSec as AnyObject ]
        if isParentalControl {
            if currentParentalCOntrolStatus == "Enable" {
                parentalControltype = "active"
            } else {
                parentalControltype = "inactive"
            }
            data = ["id" : "parental_control" as AnyObject, "type" : parentalControltype as AnyObject]
        } else {
            data = ["id" : "pause" as AnyObject, "type" : timeInterval as AnyObject, "eve_sec" : eveSec as AnyObject ]
        }
        
        let params:Dictionary<String,AnyObject> = ["namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "data" : data as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject,"token" : NetworkHelper.sharedInstance.getCurrentToken() as AnyObject,"request_id" : requestID as AnyObject,"postkey" : NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject]
        print(params)
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.PAUSE_INTERNET_API, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                if isParentalControl {
                    if parentalControltype == "active" {
                        DispatchQueue.main.async {
                             self.showAlert("Parental control enabled.")
                        }
                        DispatchQueue.main.async {
                            self.parentalControl_switchBtn.setOn(true, animated: true)
                        }
                        //                        self.parentalControlStatus_lbl.text = "Disable"
                        //                        self.parentalControlStatus_lbl.textColor = UIColor.gray
                    } else {
                        DispatchQueue.main.async {
                            self.showAlert("Parental control disabled")
                             self.parentalControl_switchBtn.setOn(false, animated: true)
                        }
                       
                        //                        self.parentalControlStatus_lbl.text = "Enable"
                        //                        self.parentalControlStatus_lbl.textColor = UIColor.white
                    }
                    //change button text and color
                } else {
                    DispatchQueue.main.async {
                         self.showAlert("Internet traffic paused. It will resume after the specified interval.")
                    }
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                         self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                         self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }  else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            }else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                     self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    //MARK: -Tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == table_view {
            
            if self.selectedIndex >= 0 && section == self.selectedIndex {
                return self.policyDurationList[section].arraySlotsSets.count
            }
            return 0
            
        }
        else if tableView == table_view_policy_plans{
            print("policy plan pressed")
            return 0
        }else{
            return 0
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == table_view {
            return policyDurationList.count
        }
        else if tableView == table_view_policy_plans {
            if isPolicyControlPressed {
               
                return policyControlsList.count
            }else {
                return protocolControlsList.count
            }
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == table_view {
            return 55
        } else {
            return UITableView.automaticDimension
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == table_view {
            return 30
        }else{
            return UITableView.automaticDimension
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let policyDurationOuterCell = self.table_view.dequeueReusableCell(withIdentifier: "policy_duration_outer_cell") as! PolicyDurationOuterCell
        
        let policyPlanCell = self.table_view_policy_plans.dequeueReusableCell(withIdentifier: "policy_plans_cell") as! PolicyPlansCell
        
        if tableView == table_view {
            
            let items = self.policyDurationList[section]
            
            policyDurationOuterCell.hostName.text = items.getHostName()
            policyDurationOuterCell.totalDuration.text = items.getTotalDuration()
            policyDurationOuterCell.tag = section
            policyDurationOuterCell.headerButtonOutlet.tag = 0
            if section == self.selectedIndex{
                policyDurationOuterCell.headerButtonOutlet.tag = 1
            }
            policyDurationOuterCell.backgroundColor = UIColor.white
            policyDurationOuterCell.delegate = self
            //        policyDurationOuterCell.slotDurations = items
            
            return policyDurationOuterCell
            
        } else if tableView == table_view_policy_plans {
            if isPolicyControlPressed {
                table_view_policy_plans.isUserInteractionEnabled = true
                
                let item = self.policyControlsList[section]
                
                
                policyPlanCell.title_label.text = item.getPolicyTitle()
                policyPlanCell.duration_label.text = String(item.getPolicyStarttime()) + " - " + String(item.getPolicyEndTime())
                policyPlanCell.days_label.text = "Sat,Sun,Thu"
                if item.getHardCodeStatus() {
                    policyPlanCell.description_lbl_width.constant = 250
                    policyPlanCell.devices_heading_label.text = item.getPolicyDescription()
                    DispatchQueue.main.async {
                        policyPlanCell.devices_label.isHidden = true
                    }
                } else {
                    DispatchQueue.main.async {
                        policyPlanCell.devices_label.isHidden = false
                    }
                     policyPlanCell.description_lbl_width.constant = 67
                     policyPlanCell.devices_label.text = String(item.poicyDevices.count)
                }
               
                policyPlanCell.timestamp_label.text = "2018-May-06 1:02 pm"
                policyPlanCell.timestamp_label.isHidden = true
                policyPlanCell.policy_switch_btn.isHidden = false
                policyPlanCell.policy_delete_btn.isHidden = true
                if item.getPolicyStatus() == 0 {
                    policyPlanCell.policy_switch_btn.isOn = false
                }else{
                    policyPlanCell.policy_switch_btn.isOn = true
                }
                policyPlanCell.selectPolicyPlanBtn.tag = section
                policyPlanCell.policy_switch_btn.tag = section
                if policyPlanCell.devices_label.text == "0" {
                    policyPlanCell.devices_label.text = "No devices resticted"
                }
                return policyPlanCell
            }else{
                table_view_policy_plans.isUserInteractionEnabled = true
                let item = self.protocolControlsList[section]
                policyPlanCell.title_label.text = item.getPolicyTitle()
                policyPlanCell.duration_label.text = String(item.getHostName())
                policyPlanCell.days_label.text = "Sat,Sun,Thu"
                policyPlanCell.duration_heading_label.text = "Applied on:"
                policyPlanCell.devices_heading_label.text = "Protocols:"
                policyPlanCell.policy_type_icon.isHidden = false
                policyPlanCell.policy_type_icon_width.constant = 20
                policyPlanCell.actions_label.isHidden = true
                if item.getType() == "device"{
                    policyPlanCell.policy_type_icon.image = UIImage(named: "devices_icon")
                }else{
                    policyPlanCell.policy_type_icon.image = UIImage(named: "network_icon")
                }
                var protocolsString : String = ""
                for element in item.policyProtocolsLists{
                    let protocolName = element["protocols"] as! String
                    
                    let keyValueExists = NetworkHelper.sharedInstance.protocolsValuesDictForBlocking[protocolName] != nil
                    if keyValueExists {
                        protocolsString = protocolsString + NetworkHelper.sharedInstance.protocolsValuesDictForBlocking[protocolName]! + ", "
                    }else{
                        protocolsString = protocolsString + protocolName + ", "
                    }
                    protocolsString.remove(at: protocolsString.index(before: protocolsString.endIndex))
                }
                
                policyPlanCell.devices_label.text = protocolsString
                
                policyPlanCell.timestamp_label.text = "YYYY-MM-DD hh:mm"
                policyPlanCell.timestamp_label.isHidden = true
                
                policyPlanCell.policy_switch_btn.isHidden = true
                policyPlanCell.policy_switch_btn_width.constant = 0
                policyPlanCell.policy_delete_btn.isHidden = false
                
                policyPlanCell.selectPolicyPlanBtn.tag = section
                policyPlanCell.policy_switch_btn.tag = section
                policyPlanCell.policy_delete_btn.tag = section
                return policyPlanCell
            }
            
        }
        return policyDurationOuterCell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let policyDuration_inner_cell =  self.table_view.dequeueReusableCell(withIdentifier: "policy_durartion_inner_cell") as! PolicyDurationInnerCell
        
        let item = self.policyDurationList[indexPath.section].arraySlotsSets[indexPath.row]
        let timeInterval = item["interval"] as! String
        if ((timeInterval.range(of: "date")) != nil){
            //            if (timeInterval as! String == "date"){
            
            policyDuration_inner_cell.slotConnectionTime.isHidden = false
            policyDuration_inner_cell.slotDuration.isHidden = true
            policyDuration_inner_cell.slotConnectionTime.text = item["time"] as! String
            policyDuration_inner_cell.slotConnectionTime.textColor = CozyLoadingActivity.Settings.CLAppThemeColor
            
        }else{
            policyDuration_inner_cell.slotConnectionTime.isHidden = false
            policyDuration_inner_cell.slotDuration.isHidden = false
            policyDuration_inner_cell.slotConnectionTime.text = item["time"] as! String
            policyDuration_inner_cell.slotDuration.text = String(describing: timeInterval) + "min"
            policyDuration_inner_cell.slotConnectionTime.textColor = UIColor.black
        }
        
        return policyDuration_inner_cell
        
    }
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        //        let vcc = PauseInternetVC.PauseInternetVC()
        //        vcc.titleText="Policy"
        //        //            alertVC.alertType="connected_devices"
        //        self.navigationController?.pushViewController(vcc, animated: true)
        loadPolicyDurationsData()
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        
        if(self.checkPausedTimeAsAndroid()){
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
       
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
    // MARK: - Load PolicyDurationsData
    func loadPolicyDurationsData() {
        let screenClass = classForCoder.description()
      //  Analytics.setScreenName("Policy Usage", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Policy Usage", AnalyticsParameterScreenClass: screenClass])
        
        let timeZone = TimeZone.current.localizedName(for: .shortStandard, locale: .current) ?? ""  // "GMT-3
        
        //  Converted to Swift 4 by Swiftify v4.1.6654 - https://objectivec2swift.com/
        var localTimeZoneFormatter = DateFormatter()
        localTimeZoneFormatter.timeZone = NSTimeZone.local
        localTimeZoneFormatter.dateFormat = "Z"
        var localTimeZoneOffset = localTimeZoneFormatter.string(from: Date())
        
        localTimeZoneOffset.insert(":", at: localTimeZoneOffset.index(localTimeZoneOffset.startIndex, offsetBy: 3)) // prints hel!lo
        
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        NetworkHelper.sharedInstance.error401 = false
        
        var data : Dictionary <String, AnyObject> = [:]
        data = ["time_zone": localTimeZoneOffset as AnyObject]
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject, "data" : data as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.POLICY_DURATIONS_API, sendData: params as [String : AnyObject], success: { (data) in
            print(data)
            let message = ConstantStrings.ErrorText
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil)
            {
                
                if let jsonObj = data["message"] as? [String : AnyObject]{
                    self.policySession = jsonObj["duration"] as! String
                    let parentalControlStatus = jsonObj["parental_control"] as! String
                    print("parenatal control status is  \(parentalControlStatus)")
                    DispatchQueue.main.async {
                        
                        if parentalControlStatus == "active" {
                            self.parentalControl_switchBtn.setOn(true, animated: true)
                            NetworkHelper.sharedInstance.saveIfParentalControlOn(true)
                            //                            self.currentParentalCOntrolStatus = "Disable"
                            //                            self.parentalControlStatus_lbl.text = "Disable"
                            //                            self.parentalControlStatus_lbl.textColor = UIColor.gray
                        } else {
                            self.parentalControl_switchBtn.setOn(false, animated: true)
                            NetworkHelper.sharedInstance.saveIfParentalControlOn(false)
                            //                            self.parentalControlStatus_lbl.text = "Enable"
                            //                            self.currentParentalCOntrolStatus = "Enable"
                            //                            self.parentalControlStatus_lbl.textColor = UIColor.white
                        }
                        
                    }
                    
                    if let jsonArr = jsonObj["devices"] as? [[String:AnyObject]] {
                        
                        if((jsonArr.count) > 0) {
                            print("json array received for policy durations")
                            self.fetchPolicyDurations(jsonArr)
                        }
                            
                        else{
                            DispatchQueue.main.async {
                                self.showAlert("No data detected.")
                            }
                            
                        }
                    }
                }
            }
            else if ((statusCode?.range(of: "503")) != nil) {
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                         self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else{
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            }
            
        }, failure: { (data) in
            print("failure error is \(data)")
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
    }
    
    
    func fetchPolicyDurations(_ jsonArr : [[String : AnyObject]]){
        policyDurationList.removeAll()
        for element in jsonArr {
            let policyDurationItem = PolicyDurationModel()
            let hostName = element["hostname"] as! String
            let totalDuration = element["usage"] as! String
            
            policyDurationItem.setHostName(hostName)
            policyDurationItem.setTotalDuration(totalDuration)
            
            let jsonArrIPs = element["details"] as? [[String:AnyObject]]
            policyDurationItem.arraySlotsSets = jsonArrIPs! 
            policyDurationList.append(policyDurationItem)
            
        }
        //reload table view/decorate chart view
        
        //        self.refreshControl.endRefreshing()
        DispatchQueue.main.sync {
            self.policyDataSessionLabel.text = self.policySession
         //   self.policyDataSessionLabel.isHidden = false
            self.table_view.reloadData()
            self.table_view_policy_plans.reloadData()
        }
    }
    
    // MARK: - LoadAllPolicies
    func loadAllPolicies() {
        let screenClass = classForCoder.description()
   //     Analytics.setScreenName("Access Control", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Access Control", AnalyticsParameterScreenClass: screenClass])
        
        policyControlsList.removeAll()
        
        NetworkHelper.sharedInstance.arrayBlockedAlerts.removeAll()

        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        NetworkHelper.sharedInstance.error401 = false
        
        let timeZone = TimeZone.current.localizedName(for: .shortStandard, locale: .current) ?? ""  // "GMT-3
        
        //  Converted to Swift 4 by Swiftify v4.1.6654 - https://objectivec2swift.com/
        var localTimeZoneFormatter = DateFormatter()
        localTimeZoneFormatter.timeZone = NSTimeZone.local
        localTimeZoneFormatter.dateFormat = "Z"
        var localTimeZoneOffset = localTimeZoneFormatter.string(from: Date())
        
        localTimeZoneOffset.insert(":", at: localTimeZoneOffset.index(localTimeZoneOffset.startIndex, offsetBy: 3)) // prints hel!lo
        
        let data : Dictionary<String,AnyObject> = ["time_zone": localTimeZoneOffset as AnyObject]
        
        let params:Dictionary<String,AnyObject> = ["data": data as AnyObject, "request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.GET_POLICIES_API, sendData: params as [String : AnyObject], success: { (data) in
            let message = ConstantStrings.ErrorText
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil)
            {
                
                
             
                if let jsonArr = data["policies"] as? [[String : AnyObject]] {
                    
                    if((jsonArr.count) > 0 || self.policyControlsList.isEmpty){
                        
                        DispatchQueue.main.sync {
                            self.noPolicyTextLabel.isHidden = true
                            self.table_view_policy_plans.isHidden = false
                            self.fetchAllPolicies(jsonArr)
                        }
                      
                        print("json array received for policy durations")
                    }
                        
                    else{
                        
                        DispatchQueue.main.sync {
                            self.noPolicyTextLabel.isHidden = false
                            self.table_view_policy_plans.isHidden = true
                        }
                        //hide tableview and show a message with create new policy
                        
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "503")) != nil) {
                DispatchQueue.main.sync {
                    self.noPolicyTextLabel.isHidden = false
                    self.table_view_policy_plans.isHidden = true
                }
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                    self.noPolicyTextLabel.text = msg
                }else{
                    DispatchQueue.main.async {
                         self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                DispatchQueue.main.sync {
                    self.noPolicyTextLabel.isHidden = false
                    self.table_view_policy_plans.isHidden = true
                }
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                         self.showAlert(message)
                    }
                    self.noPolicyTextLabel.text = message
                    
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else{
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            }
            
        }, failure: { (data) in
            print("failure error is \(data)")
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    func fetchAllPolicies(_ jsonArr : [[String : AnyObject]]) {
       // policyControlsList.removeAll()
        for element in jsonArr {
            let policyItem = PolicyPlanModel()
            
            
            let title = element["title"] as! String
            let id = element["id"] as! String
            let status = element["status"] as! Int
            let days = element["days"] as! Int
            let sTime = element["s_time"] as! String
            let eTime = element["e_time"] as! String
            if element["s_time_gmt"] != nil {
                let sTimeGMT = element["s_time_gmt"] as! String
                policyItem.setPolicyStartTime(sTimeGMT)
            }else{
                policyItem.setPolicyStartTime(sTime)
                
            }
            if element["e_time_gmt"] != nil {
                let eTimeGMT = element["e_time_gmt"] as! String
                policyItem.setPolicyEndTimeGMT(eTimeGMT)
            }else{
                policyItem.setPolicyEndTimeGMT(eTime)
            }
            if element["hosts_list"] != nil{
                let hostsList = element["hosts_list"] as! [[String : AnyObject]]
                policyItem.poicyDevices = (hostsList as? [[String:AnyObject]])!
            }else{
                let hostsList = [[String : AnyObject]]()
                policyItem.poicyDevices = (hostsList as? [[String:AnyObject]])!
            }
            
            
            policyItem.setPolicyTitle(title)
            policyItem.setpolicyId(id)
            policyItem.setPolicyDays(days)
            policyItem.setPolicyStatus(status)
            policyItem.setPolicyStartTime(sTime)
            policyItem.setPolicyEndTime(eTime)
            
            
            
           
            policyControlsList.append(policyItem)
            
            
        }
        
        var sample1  = false
        var sample2 = false
        for policy in self.policyControlsList {
            if policy.getPolicyTitle() == hardCodePolicy.getPolicyTitle() {
                sample1 = true
            }
            if policy.getPolicyTitle() == hardCodePolicy2.getPolicyTitle() {
                sample2 = true
            }
        }
        if !sample1 {
            self.policyControlsList.append(hardCodePolicy)
        }
        if !sample2 {
            self.policyControlsList.append(hardCodePolicy2)
        }
        
        DispatchQueue.main.async {
            self.table_view_policy_plans.reloadData()
        }
    }
    
    //MARK: - Load Protocol Policies
    func loadProtocolPolicies() {
        let screenClass = classForCoder.description()
       // Analytics.setScreenName("Protocol Control", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Protocol Control", AnalyticsParameterScreenClass: screenClass])
        
        protocolControlsList.removeAll()
       
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        NetworkHelper.sharedInstance.error401 = false
        
        let timeZone = TimeZone.current.localizedName(for: .shortStandard, locale: .current) ?? ""  // "GMT-3
        
        var localTimeZoneFormatter = DateFormatter()
        localTimeZoneFormatter.timeZone = NSTimeZone.local
        localTimeZoneFormatter.dateFormat = "Z"
        var localTimeZoneOffset = localTimeZoneFormatter.string(from: Date())
        
        localTimeZoneOffset.insert(":", at: localTimeZoneOffset.index(localTimeZoneOffset.startIndex, offsetBy: 3)) // prints hel!lo
        
        let data : Dictionary<String,AnyObject> = ["time_zone": localTimeZoneOffset as AnyObject]
        
        let params:Dictionary<String,AnyObject> = ["data": data as AnyObject, "request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.GET_PROTOCOL_POLICIES_API, sendData: params as [String : AnyObject], success: { (data) in
            let message = ConstantStrings.ErrorText
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil)
            {
                
                if let jsonArr = data["blockapps"] as? [[String : AnyObject]] {
                    
                    if((jsonArr.count) > 0){
                        DispatchQueue.main.sync {

                            self.policy_plans_view.isHidden = false
                            self.policy_usage_view.isHidden = true
                            self.isPolicyControlPressed = false
                            self.isProtocolManagerPressed = true
                            self.updateTopBarColors(isUsage: false, isAccessControl: false)
                            self.noPolicyTextLabel.isHidden = true
                            self.table_view_policy_plans.isHidden = false
                            self.fetchAllProtocolPolicies(jsonArr)
                        }
                        print("json array received for policy durations")
                    }
                        
                    else{
                        
                        DispatchQueue.main.sync {
                            self.noPolicyTextLabel.isHidden = false
                            self.table_view_policy_plans.isHidden = true
                            let VC = HostSelectionVC.hostSelectionVC()
                            self.navigationController?.pushViewController(VC, animated: true)
                        }
                        //hide tableview and show a message with create new policy
                        
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "503")) != nil) {
                DispatchQueue.main.sync {
                    self.noPolicyTextLabel.isHidden = false
                    self.table_view_policy_plans.isHidden = true
                }
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                         self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                    self.noPolicyTextLabel.text = msg
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                DispatchQueue.main.sync {
                    self.noPolicyTextLabel.isHidden = false
                    self.table_view_policy_plans.isHidden = true
                }
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                         self.showAlert(message)
                    }
                    self.noPolicyTextLabel.text = message
                    
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else{
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            }
            
        }, failure: { (data) in
            print("failure error is \(data)")
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    func fetchAllProtocolPolicies(_ jsonArr : [[String : AnyObject]]) {
        protocolControlsList.removeAll()
        for element in jsonArr {
            let policyItem = PolicyPlanModel()
            
            
            let dataType = element["data_type"] as! String
            let data = element["data"] as! [String : AnyObject]
            print(data)
            let policyId = data["id"] as! String
            let policyTitle = data["title"] as! String
            let policyProcedure = data["procedure"] as! String
            let policyType = data["type"] as! String
            
            
            if (policyType.range(of: "network") != nil) {
                policyItem.setHostName("Network")
            }else{
                let macAdress = data["macaddress"] as! String
                let hostId = data["hostid"] as! String
                let hostIp = data["ip"] as! String
                let hostName = data["hostname"] as! String
                
                policyItem.setMacAdress(macAdress);
                policyItem.setIpAdress(hostIp);
                policyItem.setHostName(hostName);
                policyItem.setHostId(hostId);
            }
            let policyRequest = data["request"] as! String
            let protocolsList = data["protocols"] as! [[String : AnyObject]]
            policyItem.policyProtocolsLists = (protocolsList as? [[String:AnyObject]])!
            
            print("id and title== \(policyId) == \(policyTitle)")
            policyItem.setPolicyTitle(policyTitle)
            policyItem.setpolicyId(policyId)
            policyItem.setRequest(policyRequest)
            policyItem.setType(policyType)
            
            protocolControlsList.append(policyItem)
            
        }
        
        self.table_view_policy_plans.reloadData()
    }
    
    
    //MARK: -AccessControl Actions
    
    @IBAction func ParentalControlValueChanged(_ sender: Any) {
        if parentalControl_switchBtn.isOn {
            print("parental control is on")
            
            currentParentalCOntrolStatus = "Enable"
            showParentalControlDialog(switchButton: parentalControl_switchBtn)
            
        } else {
            
            currentParentalCOntrolStatus = "Disable"
            showParentalControlDialog(switchButton: parentalControl_switchBtn)
            
            print("parental control is off")
        }
    }
    
    
    @IBAction func selectPolicyPlanPressed(_ sender: Any) {
        let  policySelected = sender as! UIButton
        let index = policySelected.tag
        print("policy selected at \(index)")
        
        if isPolicyControlPressed {
            let item = self.policyControlsList[index]
            
            let policyPlanVc = PolicyPlanVC.policyPlanVc()
            policyPlanVc.policyTitle = item.getPolicyTitle()
            policyPlanVc.policyStartTime = item.policyStartTime
            policyPlanVc.policyStartTimeGMT = item.policyStartTimeGMT
            policyPlanVc.policyEndTime = item.policyEndTime
            policyPlanVc.policyEndTimeGMT = item.policyEndTimeGMT
            policyPlanVc.policyDaysWeight = item.getPolicyDays()
            policyPlanVc.isCreateNewPolicy = false
            
            policyPlanVc.policyStatus = item.getPolicyStatus()
            policyPlanVc.policyKey = item.getpolicyId()
            if policyPlanVc.policyStatus == 1 || (item.poicyDevices != nil && item.poicyDevices.count > 0 ) {
                policyPlanVc.selectedHostsToUpdate = item.poicyDevices
            } else {
                policyPlanVc.isPredefinedPolicy = true
            }
            
            policyPlanVc.isHardCoded = item.getHardCodeStatus()
            self.navigationController?.pushViewController(policyPlanVc, animated: true)
        }else{
            //go to protocol policy update page
            //            let protocolPlanVc = ProtocolManagerVC.protocolManagerVC()
            //            self.navigationController?.pushViewController(protocolPlanVc, animated: true)
        }
    }
    
    @IBAction func policy_delete_btn_pressed(_ sender: Any) {
        let polcySelected = sender as! UIButton
        let index = polcySelected.tag
        deleteProtocolPolicy(position: index)
        print("protocol policy selected at \(index)")
    }
    
    func deleteProtocolPolicy(position : Int) {
        
        deletePolicyPrompt(position : position)
    }
    
    func deletePolicyPrompt(position : Int) {
        let alertController = UIAlertController(title: "Prytex", message: ConstantStrings.delete_protocol_policy_text, preferredStyle: .alert)
        
        // Create the actions
        let osAction = UIAlertAction(title: "Delete", style: UIAlertAction.Style.default) {
            UIAlertAction in
            if NetworkHelper.sharedInstance.isDmoatOnline {
                self.callDeletePolicyApi(position: position)
            }
            else{
                self.showAlert(ConstantStrings.dmoatOffline)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("cancel Pressed")
        }
        // Add the actions
        alertController.addAction(osAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func callDeletePolicyApi(position : Int) {
        //aaaa
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        var data : Dictionary<String,AnyObject>
        
        if protocolControlsList[position].getType() == "device"{
            
            data = ["type": protocolControlsList[position].getType() as AnyObject, "id" : protocolControlsList[position].getpolicyId() as AnyObject, "procedure" : "delete" as AnyObject, "title": protocolControlsList[position].getPolicyTitle() as AnyObject, "hostname" : protocolControlsList[position].getHostName() as AnyObject, "hostid" : protocolControlsList[position].getHostId() as AnyObject, "ip" : protocolControlsList[position].getIpAdress() as AnyObject, "protocols" : protocolControlsList[position].policyProtocolsLists as AnyObject]
        }else{
            
            data = ["type": protocolControlsList[position].getType() as AnyObject, "id" : protocolControlsList[position].getpolicyId() as AnyObject, "procedure" : "delete" as AnyObject, "title": protocolControlsList[position].getPolicyTitle() as AnyObject, "protocols" : protocolControlsList[position].policyProtocolsLists as AnyObject]
        }
        let params : Dictionary<String,AnyObject> = ["data": data as AnyObject, "request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.CREATE_PROTOCOL_POLICIES_API, sendData: params, success: { (data) in
            print(params)
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                print("policy deleted successfuly, remove from list and reload table")
                DispatchQueue.main.async {
                    self.protocolControlsList.remove(at: position)
                    self.table_view_policy_plans.reloadData()
                }
                //policy updated created
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                let msg = data["message"] as! String
                DispatchQueue.main.async {
                     self.showAlert(msg)
                }
            } else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                     self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
    }
    
    @IBAction func PolicyStatusSwitch_pressed(_ sender: UISwitch) {
        let polcySelected = sender 
        let index = polcySelected.tag
        let item = self.policyControlsList[index]
        if policyControlsList[index].poicyDevices.isEmpty {
            self.showAlert("No hosts selected. Please update your policy to acitvate.")
            sender.setOn(false, animated: true)
            return
        }
        
       
        if item.getPolicyStatus() == 1 {
            print("Policy is Active at \(index)")
            PolicyStatusChangePromptDialog(status: 0, index: index)
        }else{
            print("Policy is inactive at \(index)")
            PolicyStatusChangePromptDialog(status: 1, index: index)
        }
    }
    
    func PolicyStatusChangePromptDialog(status: Int, index : Int) {
        let alertController = UIAlertController(title: "Prytex", message: "Do you really want to update policy status?", preferredStyle: .alert)
        
        // Create the actions
        let osAction = UIAlertAction(title: "Update", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.updatePolicy(policyStatus: status, index: index)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("cancel Pressed")
            self.changePolicySwitchState(status : status, index : index)
        }
        // Add the actions
        alertController.addAction(osAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func changePolicySwitchState(status : Int, index : Int) {
        DispatchQueue.main.async {
            let policyPlanCell = self.table_view_policy_plans.dequeueReusableCell(withIdentifier: "policy_plans_cell") as! PolicyPlansCell
            self.policyControlsList[index].setPolicyStatus(status)
                   if status == 0 {
                       policyPlanCell.policy_switch_btn.isOn = false
                   }else{
                       policyPlanCell.policy_switch_btn.isOn = true
                   }
        }
       
        DispatchQueue.main.async {
            self.table_view_policy_plans.reloadData()
        }
    }
    
    
    //MARK: -PolicyStatus ChangeReq
    func updatePolicy(policyStatus : Int, index : Int) {
        
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let timeZone = TimeZone.current.localizedName(for: .shortStandard, locale: .current) ?? ""  // "GMT-3
        //  Converted to Swift 4 by Swiftify v4.1.6654 - https://objectivec2swift.com/
        var localTimeZoneFormatter = DateFormatter()
        localTimeZoneFormatter.timeZone = NSTimeZone.local
        localTimeZoneFormatter.dateFormat = "Z"
        var localTimeZoneOffset = localTimeZoneFormatter.string(from: Date())
        
        localTimeZoneOffset.insert(":", at: localTimeZoneOffset.index(localTimeZoneOffset.startIndex, offsetBy: 3)) // prints hel!lo
        
        let data : Dictionary<String,AnyObject> = ["type": "update" as AnyObject, "id" : policyControlsList[index].getpolicyId() as AnyObject, "status" : policyStatus as AnyObject, "title": policyControlsList[index].getPolicyTitle() as AnyObject, "s_time" : policyControlsList[index].getPolicyStarttime() as AnyObject, "e_time" : policyControlsList[index].getPolicyEndTime() as AnyObject, "s_time_gmt" : policyControlsList[index].getPolicyStartTimeGMT() as AnyObject, "e_time_gmt" : policyControlsList[index].getPolicyEndTimeGMT() as AnyObject,"days" : policyControlsList[index].getPolicyDays() as AnyObject, "hosts_list" : policyControlsList[index].poicyDevices as AnyObject, "time_zone": localTimeZoneOffset as AnyObject]
//        if policyControlsList[index].poicyDevices == 0 {
//            self.show(self.navi, sender: <#T##Any?#>)
//        }
        
        let params : Dictionary<String,AnyObject> = ["data": data as AnyObject, "request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.CREATE_POLICIES_API, sendData: params, success: { (data) in
            print(params)
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                
                self.changePolicySwitchState(status : policyStatus, index : index)
                
                //policy updated created
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                let msg = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(msg)
                }
            } else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    
    @IBAction func create_policy_pressed(_ sender: Any) {
        
        if isPolicyControlPressed {
            let VC = PolicyPlanVC.policyPlanVc()
            self.navigationController?.pushViewController(VC, animated: true)
        }else if isProtocolManagerPressed {
//            showNetworkPolicyPrompt()
            
            let VC = HostSelectionVC.hostSelectionVC()
            self.navigationController?.pushViewController(VC, animated: true)
        }
        
    }
    
    func showNetworkPolicyPrompt() {
        let alertController = UIAlertController(title: "Prytex", message: "Select a 'policy type' to create a policy for managing protocols.", preferredStyle: .alert)
        
        // Create the actions
        let networkAction = UIAlertAction(title: "Network", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("networkAction pressed")
            let VC = ProtocolManagerVC.protocolManagerVC()
            self.navigationController?.pushViewController(VC, animated: true)
        }
        let deviceAction = UIAlertAction(title: "Device", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("deviceAction pressed")
            let VC = HostSelectionVC.hostSelectionVC()
            self.navigationController?.pushViewController(VC, animated: true)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("cancel Pressed")
        }
        // Add the actions
        alertController.addAction(networkAction)
        alertController.addAction(deviceAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    // MARK: -Session data hide show func
    func showHideSessionData() {
        if self.isSessionDataHiden {
            self.isSessionDataHiden = false
            DispatchQueue.main.async {
                 
                self.policyDataSessionLabel.isHidden = true
                self.table_view.isHidden = true
                self.sesssion_heading_lbl.isHidden = true
              
                
            }
            //hide data
        } else {
            self.isSessionDataHiden = true
            DispatchQueue.main.async {
                           self.table_view.isHidden = false
                           self.sesssion_heading_lbl.isHidden = false
                           self.policyDataSessionLabel.isHidden = false
                       }
        }
    }
    
    
}

extension PauseInternetVC : PolicyDurationHeaderTableViewCellDelegate{
    
    func didSelectUserHeaderTableViewCell(isShowDetails: Bool, UserHeader: PolicyDurationOuterCell) {
        print("Index \(isShowDetails)")
        if isShowDetails{
            
            self.selectedIndex = (UserHeader.tag)
        }else{
            self.selectedIndex = -1
        }
        self.table_view.reloadData()
    }
}

extension NSMutableAttributedString {
    
    public func setAsLink(textToFind:String, linkURL:String) -> Bool {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(.link, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
}

// MARK: -Hard coded Policies

extension PauseInternetVC {
    func populateHardCodedData() {
   
        
        hardCodePolicy.setPolicyTitle("Home Work")
        hardCodePolicy.setPolicyEndTime("21:00")
        hardCodePolicy.setPolicyStartTime("19:00")
        hardCodePolicy.setPolicyStartTimeGMT("19:00")
        hardCodePolicy.setPolicyEndTimeGMT("21:00")
        hardCodePolicy.setPolicyDays(127)
        hardCodePolicy.setPolicyStatus(0)
        hardCodePolicy.setHardcodeStatus(yes: true)
        hardCodePolicy.setPolicyDescription("A pre-populated policy to allows you to set a distraction free time for home work. You can edit and change the start/end time as well as the devices that should be blocked during this time duration. After editing, simply enable or disable this policy.")
        let id1 =  NetworkHelper.sharedInstance.getRandomKeyString()
        hardCodePolicy.setpolicyId(id1)
     
        
       
        hardCodePolicy2.setPolicyTitle("Privacy Mode")
        hardCodePolicy2.setPolicyEndTime("17:00")
        hardCodePolicy2.setPolicyStartTime("09:00")
        hardCodePolicy2.setPolicyStartTimeGMT("09:00")
        hardCodePolicy2.setPolicyEndTimeGMT("17:00")
        hardCodePolicy2.setPolicyDays(127)
        hardCodePolicy2.setPolicyStatus(0)
        hardCodePolicy2.setHardcodeStatus(yes: true)
        hardCodePolicy2.setPolicyDescription("Do you know that many smart speakers and connected gadgets are always on and listening? You can setup a privacy mode to disallow access by these devices. You can change the start/end time")
        let id2 =  NetworkHelper.sharedInstance.getRandomKeyString()
        hardCodePolicy.setpolicyId(id1)
 
        
       
        
    }
}
