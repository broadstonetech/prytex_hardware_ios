//
//  EULAVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 26/07/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class EULAVC: Parent {

    
    var titleText:String = ""
    
    
    override func viewDidLoad() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.titleText = "End User Licence Agreement"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.navigationItem.title = titleText
        openPdfFileFromURL(fileurl: ApiUrl.dmoatEULADocURL)
    }
    
    @IBAction func btn_agree_pressed(_ sender: AnyObject) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
