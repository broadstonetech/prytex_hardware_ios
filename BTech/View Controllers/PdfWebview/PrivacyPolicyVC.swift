//
//  PrivacyPolicyVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 26/07/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class PrivacyPolicyVC: Parent {

    
    var titleText:String = ""
    override func viewDidLoad() {
        
        openPdfFileFromURL(fileurl: ApiUrl.dmoatPrivacyPolicyDocURL)
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.titleText = "Privacy Policy"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.navigationItem.title = titleText
        
    }
    @IBAction func btn_agree_pressed(_ sender: AnyObject) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
