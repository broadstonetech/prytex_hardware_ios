//
//  TeermsOfServices.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 17/07/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import PDFKit

class TeermsOfServices: Parent {

    @IBOutlet weak var text_viewA: UITextView!
 
    
    
    //Mark: - class variables
    var titleText:String = ""
    
    class func TosVC() -> TeermsOfServices {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "terms_of_services") as! TeermsOfServices
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleText = "Terms of Serevices"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.title = titleText
        
        if let link = URL(string: ApiUrl.dmoatTermsServicesDocURL) {
          UIApplication.shared.open(link)
        }

    }
    
//    override func viewWillAppear(_ animated: Bool) {
//
//
//    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_agree_pressed(_ sender: AnyObject) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
