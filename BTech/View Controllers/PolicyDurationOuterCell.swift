//
//  PolicyDurationOuterCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 01/03/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
protocol PolicyDurationHeaderTableViewCellDelegate {
    func didSelectUserHeaderTableViewCell(isShowDetails: Bool, UserHeader: PolicyDurationOuterCell)
}
class PolicyDurationOuterCell: UITableViewCell{//, UITableViewDataSource, UITableViewDelegate  {

    //MARK:- outlets
    @IBOutlet weak var hostName: UILabel!
    @IBOutlet weak var totalDuration: UILabel!
    @IBOutlet weak var headerButtonOutlet: UIButton!

//    @IBOutlet weak var table_view: UITableView!
    var delegate : PolicyDurationHeaderTableViewCellDelegate?
    
    //MARK:- variables
//    var slotDurations = PolicyDurationModel()
    
    
    override func draw(_ rect: CGRect) {
        
//        self.table_view.delegate = self
//        self.table_view.dataSource = self
//        var frame = self.table_view.frame
//        frame.size.height = self.table_view.contentSize.height
//        self.table_view.frame = frame
    }
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func selectedHeader(sender: UIButton) {
        if sender.tag == 0{
            sender.tag = 1
            delegate?.didSelectUserHeaderTableViewCell(isShowDetails: true, UserHeader: self)
        }else{
            sender.tag = 0
            delegate?.didSelectUserHeaderTableViewCell(isShowDetails: false, UserHeader: self)

        }
        
    }
    //Mark: -Tableview delegates
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
//        print("rowSelected\(indexPath.row)")
//        
//        
//    }
//    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    //    {
//    //        return UITableViewAutomaticDimension
//    ////                   return 20  //for pre muted... i have to set it according to no of values in each section later
//    //
//    //    }
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        print("slotsCount-== \(slotDurations.arraySlotsSets.count)")
//        return slotDurations.arraySlotsSets.count
//    
//        
//    }
//    //    func numberOfSections(in tableView: UITableView) -> Int {
//    //
//    //        return 1
//    //    }
//    
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        
//        let policyDuration_inner_cell =  self.table_view.dequeueReusableCell(withIdentifier: "policy_durartion_inner_cell") as! PolicyDurationInnerCell
//        
//        print("policy_durartion_inner_cell== \(indexPath.row)")
//        if (self.slotDurations.arraySlotsSets.count <= indexPath.row){
//            return policyDuration_inner_cell
//        }
//        DispatchQueue.main.async {
//            let item = self.slotDurations.arraySlotsSets[indexPath.row]
//            policyDuration_inner_cell.slotConnectionTime.text = item["time"] as! String
//            policyDuration_inner_cell.slotDuration.text = String(describing: item["interval"]!) + "min"
//        }
//        return policyDuration_inner_cell
//        
//        }
    
}
