//
//  PolicyManagement.swift
//  BTech
//
//  Created by Ahmed Akhtar on 23/12/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
//import MZFormSheetPresentationController
class PolicyManagement: Parent,UITextFieldDelegate,UITextViewDelegate {
    
    var updatePolicy:Bool?
    var idSelectedForUpdate:Int?
    var titleString:String?
    var startTimeString:String?
    var endTimeString:String?
    var daysString:String?
    var protocolsString:String?
    var hostListString:String?
    var targetURLString:String?
    
    var targetURLArray = [String]()
    var hostListArray = [String]()
    var protocolsArray = [String]()
    
    //Outlets
    @IBOutlet var policyManagement: UIScrollView!
    @IBOutlet var targetURLTableView: UITableView!
    @IBOutlet var hostListTableView: UITableView!
    @IBOutlet var heightConstraintForUrlTableView: NSLayoutConstraint!
    @IBOutlet var titleField: UITextField!
    @IBOutlet var startTimeField: UITextField!
    @IBOutlet var endTimeField: UITextField!
    @IBOutlet var daysField: UITextField!
    @IBOutlet var protocolsField: UITextField!
    @IBOutlet var daysTextView: UITextView!
    @IBOutlet var protocolsTextView: UITextView!
    
    
    
    
    
    
    
    //IBAction
    @IBAction func addURLBtnPressed(_ sender: AnyObject) {
        targetURLArray.append("")
        // Update Table Data
        targetURLTableView.beginUpdates()
        targetURLTableView.insertRows(at: [
            IndexPath(row: targetURLArray.count-1, section: 0)
            ], with: .automatic)
        targetURLTableView.endUpdates()
    }
    
    func getWeightFromArray(_ arrayDays : [String]) -> Int
    {
        var weight : Int = 0
        for value in arrayDays
        {
            if(value == "Mon")
            {
                weight = weight + 64
            }
            if(value == "Tue")
            {
                weight = weight + 32
            }
            if(value == "Wed")
            {
                weight = weight + 16
            }
            if(value == "Thu")
            {
                weight = weight + 8
            }
            if(value == "Fri")
            {
                weight = weight + 4
            }
            if(value == "Sat")
            {
                weight = weight + 2
            }
            if(value == "Sun")
            {
                weight = weight + 1
            }
        }
        return weight
        
    }
    
    
    func callCreatePolicyWithModelAndID(_ model: PolicyManagementModel, ID: Int)
    {
        NetworkHelper.sharedInstance.error401 = false
        let daysArr = self.daysField.text!.components(separatedBy: ",")
        let weight = self.getWeightFromArray(daysArr)
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let ID = Int(arc4random_uniform(1000000000) + 1)
        
        
        let data : Dictionary<String,AnyObject> = ["type": "create" as AnyObject, "id" : ID as AnyObject, "title": model.title! as AnyObject, "s_time" : model.startTime! as AnyObject, "e_time" : model.endTime! as AnyObject, "days" : weight as AnyObject, "targets" : model.targetURLArray as AnyObject, "hosts_list" : model.hostListArray as AnyObject, "protocols" : model.protocolsArray as AnyObject]
        let params : Dictionary<String,AnyObject> = ["data": data as AnyObject, "request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: "https://api.dmoat.com/post/policy", sendData: params, success: { (data) in
            print(data)
            print(params)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async {
                    self.showAlert("Success")
                }
                //PolicyManagementRealm().populatePolicyWithModel(model, id: ID)
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                let msg = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(msg)
                }
            }  else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            }else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                     self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    
    func callUpdatePolicyWithModelAndID(_ model: PolicyManagementModel, ID: Int)
    {
        NetworkHelper.sharedInstance.error401 = false
        let daysArr = self.daysField.text!.components(separatedBy: ",")
        let weight = self.getWeightFromArray(daysArr)
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let data : [String : AnyObject] = ["type": "update" as AnyObject, "id" : ID as AnyObject, "title": model.title! as AnyObject, "s_time" : model.startTime! as AnyObject, "e_time" : model.endTime! as AnyObject, "days" : weight as AnyObject, "targets" : model.targetURLArray as AnyObject, "hosts_list" : model.hostListArray as AnyObject, "protocols" : model.protocolsArray as AnyObject]
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "data" : data as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: "https://api.dmoat.com/post/policy", sendData: params, success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async {
                    self.showAlert("Success")
                }
                //                let stringTargetURL = model.targetURLArray.joinWithSeparator(",")
                //                let stringHostList = model.hostListArray.joinWithSeparator(",")
                //                let stringProtocols = model.protocolsArray.joinWithSeparator(",")
                //                PolicyManagementRealm.updateRecordWith(ID, title: model.title!, startTime: model.startTime!, endTime: model.endTime!, days: model.days!, protocols: stringProtocols, url: stringTargetURL, hostList: stringHostList)
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                let msg = data["message"] as! String
                DispatchQueue.main.async {
                     self.showAlert(msg)
                }
            } else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                     self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    
    func checkMissingFields()->Bool
    {
        if(self.titleField.text == "")
        {
            showAlert("Please Enter title")
            return false
        }
        if(self.startTimeField.text == "")
        {
            showAlert("Please Enter start time")
            return false
        }
        if(self.endTimeField.text == "")
        {
            showAlert("Please Enter end time")
            return false
        }
        
        if(self.daysField.text == "")
        {
            showAlert("Please Enter days")
            return false
        }
        
        
        return true
    }
    
    
    
    func checkStartAndEndDates()->Bool
    {
        
        let strTime : String = startTimeField.text!
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm"
        //formatter.dateFormat = "h:mm"
        let startDate : Date = formatter.date(from: strTime)!
        let endTime = endTimeField.text
        let endDate : Date = formatter.date(from: endTime!)!
        
        if((startDate as NSDate) .earlierDate(endDate) == startDate)
        {
            return true
        }
        else{
            showAlert("Your start time should be less than end time")
            return false
        }
        
    }
    
    
    
    
    func checkForEmptyElementsInArrays()
    {
        for element in self.hostListArray
        {
            if(element == "")
            {
                self.hostListArray.remove(at: self.hostListArray.firstIndex(of: element)!)
            }
        }
        for element in self.targetURLArray
        {
            if(element == "")
            {
                self.targetURLArray.remove(at: self.targetURLArray.firstIndex(of: element)!)
            }
        }
        for element in self.protocolsArray
        {
            if(element == "")
            {
                self.protocolsArray.remove(at: self.protocolsArray.firstIndex(of: element)!)
            }
        }
    }
    
    
    
    @IBAction func savePolicyPressed(_ sender: AnyObject) {
        
        
        if(self.checkMissingFields())
        {
            
            if(self.checkStartAndEndDates())
            {
                self.checkForEmptyElementsInArrays()
                let policy = PolicyManagementModel()
                policy.populatePolicyModelWith(titleField.text!, startTime: startTimeField.text!, endTime: endTimeField.text!, days: daysField.text!, hostList: self.hostListArray, targetURL: self.targetURLArray, protocols: self.protocolsArray)
                let count : Int = PolicyManagementRealm.retrievePoliciesCount()
                let ID : Int = count + 1
                if(self.updatePolicy == true)
                {
                    self.callUpdatePolicyWithModelAndID(policy, ID: idSelectedForUpdate!)
                }
                else{
                    self.callCreatePolicyWithModelAndID(policy, ID: ID)
                }
            }
            
        }
    }
    
    class func policyManagementVC() -> PolicyManagement {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "policyManagement") as! PolicyManagement
    }
    
    
    @objc func showDays() {
        
        self.daysField.text = NetworkHelper.sharedInstance.daysSelected
        
    }
    
    
    
    
    @objc func getHostList()
    {
        self.hostListArray.removeAll()
        for model in NetworkHelper.sharedInstance.arrayConnectedDevices
        {
            if(model.checked == true)
            {
                self.hostListArray.append(model.hostname!)
            }
        }
        
        if(self.hostListArray.count == 0)
        {
            hostListArray.append("")
        }
        DispatchQueue.main.async {
            self.hostListTableView.reloadData()
        }
    }
    
    @objc func showProtocols()
    {
        self.protocolsArray.removeAll()
        var protocolString : String = ""
        for model in NetworkHelper.sharedInstance.protocolsMainArray
        {
            if(model.checked == true)
            {
                protocolsArray.append(model.title!)
                protocolString = protocolString + model.title! + ","
            }
        }
        let protocolsStringFinal = String(protocolString.dropLast())
        print(protocolsStringFinal)
        self.protocolsField.text = protocolsStringFinal
        
        
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(showDays),
            name: NSNotification.Name(rawValue: "ShowDaysNotification"),
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(showProtocols),
            name: NSNotification.Name(rawValue: "ShowProtocolsNotification"),
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(getHostList),
            name: NSNotification.Name(rawValue: "getHostListNotification"),
            object: nil)
        self.daysField.text = ""
        policyManagement.isDirectionalLockEnabled = true
        self.title = "Policy Management"
        targetURLArray.append("")
        
        //set initial row for host list
        hostListArray.append("")
        // Update Table Data
        //        hostListTableView.beginUpdates()
        //        hostListTableView.insertRowsAtIndexPaths([
        //            NSIndexPath(forRow: hostListArray.count-1, inSection: 0)
        //            ], withRowAnimation: .Automatic)
        //        hostListTableView.endUpdates()
        
        
        //check if its update policy
        if(updatePolicy == true)
        {
            self.titleField.text = titleString
            self.startTimeField.text = startTimeString
            self.endTimeField.text = endTimeString
            self.daysField.text = daysString
            self.protocolsField.text = protocolsString
            protocolsArray.removeAll()
            protocolsArray = protocolsField.text!.components(separatedBy: ",")
            hostListArray.removeAll()
            hostListArray = hostListString!.components(separatedBy: ",")
            targetURLArray.removeAll()
            targetURLArray = targetURLString!.components(separatedBy: ",")
            
        }
        
        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0
        }
    }
    
    
    //table view delegates
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == self.targetURLTableView)
        {
            return targetURLArray.count
        }
        else
        {
            return hostListArray.count
        }
        
    }
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let targetUrlCell =  self.targetURLTableView.dequeueReusableCell(withIdentifier: "targetURL")! as! TargetURLTableViewCell
        let hostListCell =  self.hostListTableView.dequeueReusableCell(withIdentifier: "hostList")! as! HostListTableViewCell
        
        if(tableView == self.targetURLTableView)
        {
            targetUrlCell.urlField.delegate = self
            targetUrlCell.urlField.text = targetURLArray[indexPath.row]
            targetUrlCell.urlField.tag = 8000 + indexPath.row
            return targetUrlCell
        }
        else
        {
            hostListCell.hostListField.delegate = self
            hostListCell.hostListField.tag = 2000 + indexPath.row
            hostListCell.hostListField.text = hostListArray[indexPath.row]
            
            return hostListCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, commitEditingStyle editingStyle: UITableViewCell.EditingStyle, forRowAtIndexPath indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            targetURLArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
    }
    
    
    
    //text view delegates
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
//        if(textView == self.daysTextView)
//        {
//            self.view.endEditing(true)
//            self.daysTextView.tintColor = UIColor.clear
//            //days view selected
//            let viewController = self.storyboard!.instantiateViewController(withIdentifier: "chooseDays") as! ChooseDays
//            let formSheetController = MZFormSheetPresentationViewController(contentViewController: viewController)
//            formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = true
//            formSheetController.presentationController?.shouldApplyBackgroundBlurEffect = false
//            formSheetController.presentationController?.contentViewSize = CGSize(width: 300, height: 370)
//            self.present(formSheetController, animated: true, completion: nil)
//
//
//        }
            
//        else if (textView == self.protocolsTextView)
//        {
//            self.view.endEditing(true)
//            self.protocolsTextView.tintColor = UIColor.clear
//            let viewController = self.storyboard!.instantiateViewController(withIdentifier: "chooseProtocols") as! ChooseProtocols
//            let formSheetController = MZFormSheetPresentationViewController(contentViewController: viewController)
//            formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = true
//            formSheetController.presentationController?.shouldApplyBackgroundBlurEffect = false
//            formSheetController.presentationController?.contentViewSize = CGSize(width: 300, height: 370)
//            self.present(formSheetController, animated: true, completion: nil)
//        }
        
        
    }
    
    
    //text field delegates
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField.tag >= 8000)
        {
            let aSet = CharacterSet(charactersIn:"0123456789")
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        // Return true so the text field will be changed
        return true
    }
    
    
    
    func verifyUrl (_ urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url  = URL(string: urlString) {
                return UIApplication.shared.canOpenURL(url)
            }
        }
        return false
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //target url fields selected
        if(textField.tag >= 8000)
        {
            if(self.verifyUrl(textField.text!))
            {
                let index : Int = textField.tag - 8000
                self.targetURLArray[index] = textField.text!
            }
            else{
                self.showAlert("Please enter correct URL")
                textField.text = ""
            }
        }
        
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if(textField == self.daysField || textField == self.protocolsField)
        {
            return false
        }
        
        return true
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        
        if(textField == self.daysField)
        {
            //days field selected
            
            //            textField.resignFirstResponder()
            //            let viewController = self.storyboard!.instantiateViewControllerWithIdentifier("chooseDays") as! ChooseDays
            //            let formSheetController = MZFormSheetPresentationViewController(contentViewController: viewController)
            //            formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = true
            //            formSheetController.presentationController?.shouldApplyBackgroundBlurEffect = false
            //            formSheetController.presentationController?.contentViewSize = CGSizeMake(300, 370)
            //            self.presentViewController(formSheetController, animated: true, completion: nil)
            
            
        }
            
            
        else if(textField == self.protocolsField)
        {
            //protocols field selected
            //            textField.resignFirstResponder()
            //            let viewController = self.storyboard!.instantiateViewControllerWithIdentifier("chooseProtocols") as! ChooseProtocols
            //            let formSheetController = MZFormSheetPresentationViewController(contentViewController: viewController)
            //            formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = true
            //            formSheetController.presentationController?.shouldApplyBackgroundBlurEffect = false
            //            formSheetController.presentationController?.contentViewSize = CGSizeMake(300, 370)
            //            self.presentViewController(formSheetController, animated: true, completion: nil)
            
        }
            
            //URL fields selected
        else if(textField.tag >= 8000)
        {
            
        }
            //host list fields selected
        else if(textField.tag >= 2000 && textField.tag < 8000)
        {
            textField.resignFirstResponder()
            //show all and connected host options
            let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
            let all = UIAlertAction(title: "ALL", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.hostListArray.removeAll()
                print(self.hostListArray)
                textField.text = "All"
                
            })
            let connectedHostList = UIAlertAction(title: "Connected Host List", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                textField.resignFirstResponder()
//                let viewController = self.storyboard!.instantiateViewController(withIdentifier: "hostList") as! hostList
//                let formSheetController = MZFormSheetPresentationViewController(contentViewController: viewController)
//                formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = true
//                formSheetController.presentationController?.shouldApplyBackgroundBlurEffect = false
//                formSheetController.presentationController?.contentViewSize = CGSize(width: 300, height: 370)
//                self.present(formSheetController, animated: true, completion: nil)
                
                
            })
            optionMenu.addAction(all)
            optionMenu.addAction(connectedHostList)
            self.present(optionMenu, animated: true, completion: nil)
            
            
        }
            
            
        else if(textField == self.startTimeField)
        {
            let datePickerView:UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePicker.Mode.time
            textField.inputView=datePickerView
            datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForStart), for: UIControl.Event.valueChanged)
            
        }
            
        else if(textField == self.endTimeField)
        {
            let datePickerView:UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePicker.Mode.time
            textField.inputView=datePickerView
            datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForEnd), for: UIControl.Event.valueChanged)
            
        }
        
        
        
    }
    @objc
    func datePickerValueChangedForStart(_ sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.dateFormat = "h:mm"
        //dateFormatter.dateFormat = "h:mm"
        startTimeField.text = dateFormatter.string(from: sender.date)
        
    }
    
    @objc
    func datePickerValueChangedForEnd(_ sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.dateFormat = "h:mm"
        //dateFormatter.dateFormat = "h:mm"
        endTimeField.text = dateFormatter.string(from: sender.date)
        
    }
    
    
}
