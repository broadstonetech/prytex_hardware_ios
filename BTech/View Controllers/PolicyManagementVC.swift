//
//  PolicyManagementVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 21/05/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class PolicyManagementVC: Parent {

    //MARK: -Class outlets
    //MARK: - Class Variables
    var titleText:String = ""
    let rightButtonBar = UIButton()
    var clickIndex = -1
    var currentIndex = -1
    var policyDurationList: [PolicyDurationModel] = [PolicyDurationModel]()
    var selectedIndex = -1
    var policySession:String = ""
    
    //MARK: - Class outlets
    @IBOutlet weak var pause_internet_text_label: UILabel!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var table_view_policy_plans: UITableView!
    
    @IBOutlet weak var policyDataSessionLabel: UILabel!
    @IBOutlet weak var policy_usage_view: UIView!
    @IBOutlet weak var policy_plans_view: UIView!
    @IBOutlet weak var policy_usage_tab_btn: UIButton!
    @IBOutlet weak var parental_plan_tab_btn: UIButton!
    
    //MARK: - Class functions
    class func PolicyManagementVC() -> PolicyManagementVC{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PolicyManagementVC") as! PolicyManagementVC
    }
    
    //MARK: -Class Variables
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: -Class Functions
    
    
    //MARK: -Actions
    
    
    //MARK: -tableView Delegates

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
