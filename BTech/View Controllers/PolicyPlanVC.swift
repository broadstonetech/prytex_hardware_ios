//
//  PolicyPlanVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 09/05/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class PolicyPlanVC: Parent, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: -Class Outlets
    //view A outlets (Policy title)
    @IBOutlet weak var tf_policyTitle: UITextField!
    @IBOutlet weak var btn_policyStatus: UIButton!
    @IBOutlet weak var lable_policyInfo: UILabel!
    //view B outlets (Policy Days)
    @IBOutlet weak var btn_everyDayPolicy: UIButton!
    @IBOutlet weak var stackview_Days: UIStackView!
    @IBOutlet weak var btn_sat: UIButton!
    @IBOutlet weak var btn_sun: UIButton!
    @IBOutlet weak var btn_mon: UIButton!
    @IBOutlet weak var btn_tue: UIButton!
    @IBOutlet weak var btn_wed: UIButton!
    @IBOutlet weak var btn_thu: UIButton!
    @IBOutlet weak var btn_fri: UIButton!
    //view c outlets (Policy time Interval)
    @IBOutlet weak var btn_startTime: UIButton!
    @IBOutlet weak var btn_endTime: UIButton!
    @IBOutlet weak var timePicker: UIDatePicker!
    
    @IBOutlet weak var timePicker_view: UIView!
    //view d outlets (Policy Devices)
    @IBOutlet weak var table_view_devices: UITableView!
    @IBOutlet weak var btn_deletePolicy: UIButton!
    @IBOutlet weak var btn_addAllDevices: UIButton!
    
    //MARK: _ Class Variables
    var titleText:String = ""
    let rightButtonBar = UIButton()
    var isCreateNewPolicy : Bool = true
    let timeFormatter = DateFormatter()
    var isStartTime : Bool = false
    var isEndTime : Bool = false
    var hostsList: [ConnectedDevicesModal] = [ConnectedDevicesModal]()
    var selectedDaysList = [AnyObject]()
    var selectedIndexPathArray = Array<NSIndexPath>()
    
    var policyKey : String = ""
    var policyTitle : String = ""
    var policyStartTime : String = ""
    var policyEndTime : String = ""
    var policyStartTimeGMT : String = ""
    var policyEndTimeGMT : String = ""
    var policyDaysWeight : Int = -1
    var selectedHostsList: [ConnectedDevicesModal] = [ConnectedDevicesModal]()
    var selectedHostsToUpdate =  [[String : AnyObject]]()
    var policyStatus:Int = 1 //0 for disable,inactive and 1 for active/enable
    var policyTimeSelected : String = "hh:mm"
    var isUpdateOldePolicy = false
    var isHardCoded = false
    var isPredefinedPolicy = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table_view_devices.delegate = self
        self.table_view_devices.dataSource = self
        tf_policyTitle.delegate = self
        setUpDatePicker()
        if  isCreateNewPolicy {
            //create new
            btn_deletePolicy.isHidden = true
            DispatchQueue.main.async(execute: {
                self.loadHostsList()
            })
        } else {
            //update policy
            if self.isHardCoded {
                DispatchQueue.main.async(execute: {
                    self.loadHostsList()
                })
                
            }
            if isPredefinedPolicy {
                DispatchQueue.main.async {
                    self.btn_deletePolicy.isHidden = true
                }
            }
            print("PpolicyWeight\(policyDaysWeight)")
            btn_deletePolicy.isHidden = false
            calculateDaysFromWeightsNew(weight: policyDaysWeight)
            setAllDaysSelected()
            tf_policyTitle.text = policyTitle
            btn_startTime.setTitle(policyStartTime, for: .normal)
            btn_endTime.setTitle(policyEndTime, for: .normal)
            if policyStatus == 0 {
                policyStatus = 0
                btn_policyStatus.setTitle("Inactive", for: .normal)
                btn_policyStatus.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            }else{
                policyStatus = 1
                btn_policyStatus.setTitle("Active", for: .normal)
                btn_policyStatus.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            }
            
            for element in selectedHostsToUpdate {
                let macAdres = element["macaddress"] as! String
                let hostName = element["hostname"] as! String
                let ip = element["ip"] as! String
                //add into model class
                let device = ConnectedDevicesModal()
                device.setMacAdress_cd(macAdres)
                device.setHostName_cd(hostName)
                device.setHostIP_cd(ip)
                device.setToAddInPolicy(true)
                hostsList.append(device)
            }
            btn_addAllDevices.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_addAllDevices.isSelected = true
            table_view_devices.reloadData()
            
        }
        timePicker.backgroundColor = CozyLoadingActivity.Settings.DashboardBgColor
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
     //   Analytics.setScreenName(titleText, screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "TitleTxt", AnalyticsParameterScreenClass: screenClass])
    }

    override func viewWillAppear(_ animated: Bool) {
        self.titleText = "Access Control"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.navigationItem.title = titleText
        createRightButtonNavigationBar()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createRightButtonNavigationBar()
    {
        
        rightButtonBar.setImage(UIImage(named: "info"), for: UIControl.State())
        rightButtonBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        rightButtonBar.addTarget(self, action: #selector(topRightButtonBar), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = rightButtonBar
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        //create custom
    }
    @objc
    func topRightButtonBar()
    {
        self.showAlert(ConstantStrings.policy_screen_txt)
    }
    
    func didFinishSelectingDate(selectedDate: NSDate) {
        print("selectedDate==\(selectedDate)")
    }
    // MARK: - Class Functions
    
    class func policyPlanVc() -> PolicyPlanVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "policy_plan") as! PolicyPlanVC
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField === tf_policyTitle)
        {
            tf_policyTitle.resignFirstResponder()
            tf_policyTitle.endEditing(true)
            return true
        }
        return true
    }
    
    private func setUpDatePicker() {
        
        self.timePicker.datePickerMode = UIDatePicker.Mode.time
        self.timePicker.addTarget(self, action: #selector(datePickerChanged(sender:)), for: .valueChanged)
        
    }
    
    @objc func datePickerChanged(sender: UIDatePicker) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let time = formatter.string(from: sender.date)
        policyTimeSelected = time
                print("timeSelected \(policyTimeSelected)")
    }
    
    func setDateAndTime() {
        
        self.timePicker.datePickerMode = UIDatePicker.Mode.time
        self.timePicker.addTarget(self, action: #selector(datePickerChanged(sender:)), for: .valueChanged)
        
    }
    
    // MARK: - Button's Actions
    @IBAction func policyStatus_pressed(_ sender: Any) {
        if (sender as AnyObject).title(for: .normal) == "Active" {
            
            btn_policyStatus.setTitle("Inactive", for: .normal)
            policyStatus = 0
            btn_policyStatus.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
        }else {
            btn_policyStatus.setTitle("Active", for: .normal)
            policyStatus = 1
            btn_policyStatus.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
        }
    }
    
    @IBAction func btn_everyDay_pressed(_ sender: Any) {
        if btn_everyDayPolicy.isSelected == false {
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_sat.isSelected = false
            btn_sun.isSelected = false
            btn_mon.isSelected = false
            btn_tue.isSelected = false
            btn_wed.isSelected = false
            btn_thu.isSelected = false
            btn_fri.isSelected = false
            setAllDaysSelected()
            btn_everyDayPolicy.isSelected = true
        }else{
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_everyDayPolicy.isSelected = false
            btn_sat.isSelected = true
            btn_sun.isSelected = true
            btn_mon.isSelected = true
            btn_tue.isSelected = true
            btn_wed.isSelected = true
            btn_thu.isSelected = true
            btn_fri.isSelected = true
            setAllDaysSelected()
        }
    }
    
    @IBAction func btnSat_pressed(_ sender: Any) {
        if btn_sat.isSelected == false{
            btn_sat.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_sat.isSelected = true
            print("isselected true")
        }else{
            btn_sat.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_sat.isSelected = false
            print("isselected false")
            btn_everyDayPolicy.isSelected = false
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
        }
    }
    
    @IBAction func btnSun_pressed(_ sender: Any) {
        if btn_sun.isSelected == false{
            btn_sun.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_sun.isSelected = true
            print("isselected true")
        }else{
            btn_sun.isSelected = false
            btn_sun.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            print("isselected false")
            btn_everyDayPolicy.isSelected = false
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
        }
    }
    
    @IBAction func btnMon_pressed(_ sender: Any) {
        if btn_mon.isSelected == false {
            btn_mon.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_mon.isSelected = true
        }else{
            btn_mon.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_mon.isSelected = false
            btn_everyDayPolicy.isSelected = false
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
        }
    }
    
    @IBAction func btnTue_pressed(_ sender: Any) {
        if btn_tue.isSelected == false {
            btn_tue.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_tue.isSelected = true
        }else{
            btn_tue.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_tue.isSelected = false
            btn_everyDayPolicy.isSelected = false
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
        }
    }
    
    @IBAction func btnWed_pressed(_ sender: Any) {
        if btn_wed.isSelected == false {
            btn_wed.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_wed.isSelected = true
        }else{
            btn_wed.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_wed.isSelected = false
            btn_everyDayPolicy.isSelected = false
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
        }
    }
    
    @IBAction func btnThu_pressed(_ sender: Any) {
        if btn_thu.isSelected == false {
            btn_thu.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_thu.isSelected = true
        }else{
            btn_thu.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_thu.isSelected = false
            btn_everyDayPolicy.isSelected = false
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
        }
    }
    
    @IBAction func btnFri_pressed(_ sender: Any) {
        if btn_fri.isSelected == false {
            btn_fri.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_fri.isSelected = true
        }else{
            btn_fri.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_fri.isSelected = false
            btn_everyDayPolicy.isSelected = false
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
        }
    }
    
    @IBAction func startTime_pressed(_ sender: Any) {
        timePicker_view.isHidden = false
        isStartTime = true
        isEndTime = false
        setDateAndTime()
    }
    
    @IBAction func endTime_pressed(_ sender: Any) {
        timePicker_view.isHidden = false
        isStartTime = false
        isEndTime = true
        setDateAndTime()
    }
    
    
    
    @IBAction func addAllDevicesInPolicy_pressed(_ sender: Any) {
        if btn_addAllDevices.isSelected == false{
            btn_addAllDevices.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_addAllDevices.isSelected = true
            print("addAll status is set to true")
            for element in hostsList {
                element.setToAddInPolicy(true)
            }
            table_view_devices.reloadData()
        }else{
            btn_addAllDevices.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_addAllDevices.isSelected = false
            for element in hostsList {
                element.setToAddInPolicy(false)
            }
            table_view_devices.reloadData()
            print("addAll status is set to false")
        }
        
    }
    
    
    @IBAction func done_timePicker_pressed(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        if let dt = dateFormatter.date(from: policyTimeSelected) {
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "H:mm"
        
            let timeGmt = dateFormatter.string(from: dt)
         print("Utc conversion is \(timeGmt)")
        
        
        if isStartTime {
            policyStartTimeGMT = timeGmt
            policyStartTime = policyTimeSelected
            btn_startTime.setTitle(policyStartTime, for: .normal)
        }else if isEndTime {
            policyEndTimeGMT = timeGmt
            policyEndTime = policyTimeSelected
            btn_endTime.setTitle(policyEndTime, for: .normal)
        }
        timePicker_view.isHidden = true
        }
    }
    
    @IBAction func cancel_timePicker_pressed(_ sender: Any) {
        timePicker_view.isHidden = true
    }
    
    
    @IBAction func btn_save_policy_pressed(_ sender: Any) {
        addHostsForPolicy()
        if checkMissingFields() {
            policyDaysWeight = getWeightFromArray()
            calculateDaysFromWeight(policyDaysWeight)
            
            var url : String = ""
            var policType : String
            if isCreateNewPolicy  {
                policyKey =  NetworkHelper.sharedInstance.getRandomKeyString()
                url = ApiUrl.CREATE_POLICIES_API
                policType = "create"
                
            }  else {
                // use existing key received for uodate/del
                //set type to update
                
                if isPredefinedPolicy {
                    policyKey =  NetworkHelper.sharedInstance.getRandomKeyString()
                    url = ApiUrl.CREATE_POLICIES_API
                     policType = "create"
                    
                } else {
                    url = ApiUrl.CREATE_POLICIES_API
                    policType = "update"
                }
            }
            
            let hostList = ConnectedDevicesModal.conertToDictionary(selectedHost: selectedHostsList)

            savePolicy(isCreateNew: isCreateNewPolicy, isDelete: false, url: url, policType: policType, hostList: hostList as AnyObject)
        }
        else{
            print("missing required fields")
        }
    }
    
    
    // MARK: - Delete policy action
    @IBAction func btn_delete_policy_pressed(_ sender: Any) {
        addHostsForPolicy()
        if checkMissingFields() {
            showDeletePolicyDialog()
           
        }
        else{
            print("missing required fields")
        }
    }
    
    func showDeletePolicyDialog() {
        let alertController = UIAlertController(title: "Prytex", message: "Do you really want to delete policy?", preferredStyle: .alert)
        
        // Create the actions
        let osAction = UIAlertAction(title: "Delete", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.policyDaysWeight = self.getWeightFromArray()
            self.calculateDaysFromWeight(self.policyDaysWeight)
            let policType = "delete"
            var url : String = ApiUrl.CREATE_POLICIES_API
            let hostList = ConnectedDevicesModal.conertToDictionary(selectedHost: self.selectedHostsList)
            
            self.savePolicy(isCreateNew: false, isDelete: true, url: url, policType: policType, hostList: hostList as AnyObject)
            NSLog("delete pressed")
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("cancel Pressed")
        }
        // Add the actions
        alertController.addAction(osAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    // MARK: - Devices TableView delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return hostsList.count
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        let item = hostsList[indexPath.row]
        selectedIndexPathArray.append(indexPath as NSIndexPath)
        if item.isInPolicy(){
            item.setToAddInPolicy(false)
            btn_addAllDevices.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_addAllDevices.isSelected = false
        }else{
            item.setToAddInPolicy(true)
        }
        table_view_devices.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let policy_device_cell =  self.table_view_devices.dequeueReusableCell(withIdentifier: "policyDevicesCell") as! policyDevicesCell
        
        let item = hostsList[indexPath.row]
        policy_device_cell.device_name.text = item.getHostName_cd()
        policy_device_cell.device_ip.text = item.getHostIP_cd()
        
        
        if item.isInPolicy(){
            policy_device_cell.imgCheckMark.isHidden = false
        }else{
            policy_device_cell.imgCheckMark.isHidden = true
        }
        
        return policy_device_cell
    }
    
    //MARK: -Class functions
    func setAllDaysSelected() {
        if btn_sat.isSelected == false{
            btn_sat.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_sat.isSelected = true
            print("isselected true")
        }else{
            btn_sat.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_sat.isSelected = false
            btn_everyDayPolicy.isSelected = false
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            print("isselected false")
        }
        
        if btn_sun.isSelected == false{
            btn_sun.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_sun.isSelected = true
            print("isselected true")
        }else{
            btn_sun.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_sun.isSelected = false
            btn_everyDayPolicy.isSelected = false
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            print("isselected false")
        }
        
        if btn_mon.isSelected == false{
            btn_mon.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_mon.isSelected = true
            print("isselected true")
        }else{
            btn_mon.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_mon.isSelected = false
            btn_everyDayPolicy.isSelected = false
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            print("isselected false")
        }
        if btn_tue.isSelected == false{
            btn_tue.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_tue.isSelected = true
            print("isselected true")
        }else{
            btn_tue.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_tue.isSelected = false
            btn_everyDayPolicy.isSelected = false
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            print("isselected false")
        }
        if btn_wed.isSelected == false{
            btn_wed.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_wed.isSelected = true
            print("isselected true")
        }else{
            btn_wed.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_wed.isSelected = false
            btn_everyDayPolicy.isSelected = false
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            print("isselected false")
        }
        if btn_thu.isSelected == false{
            btn_thu.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_thu.isSelected = true
            print("isselected true")
        }else{
            btn_thu.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_thu.isSelected = false
            btn_everyDayPolicy.isSelected = false
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            print("isselected false")
        }
        if btn_fri.isSelected == false{
            btn_fri.backgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
            btn_fri.isSelected = true
            print("isselected true")
        }else{
            btn_fri.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            btn_fri.isSelected = false
            btn_everyDayPolicy.isSelected = false
            btn_everyDayPolicy.backgroundColor = CozyLoadingActivity.Settings.BtnDisabledColor
            print("isselected false")
        }
    }
    
    func checkMissingFields() -> Bool {
        if tf_policyTitle.text == "" {
            showAlert(ConstantStrings.policy_title_missing)
            return false
        }
        else if btn_fri.isSelected == false && btn_sat.isSelected == false && btn_sun.isSelected == false
            && btn_mon.isSelected == false && btn_tue.isSelected == false && btn_wed.isSelected == false &&
            btn_thu.isSelected == false {
            showAlert(ConstantStrings.policy_day_missing)
            return false
        } else if policyStartTime == "" || policyEndTime == "" {
           
            showAlert(ConstantStrings.policy_time_missing)
            return false
        }else if !checkStartEndTime() {
            showAlert(ConstantStrings.policy_time_validation)
        }
        else if selectedHostsList.count < 1 {
            print("no host selected")
            showAlert(ConstantStrings.policy_hostsList_missing)
            return false
        }
        else{
            return true
        }
        return true
    }
    
    func checkStartEndTime() -> Bool {
        
        if (policyEndTime.compare(policyStartTime) == ComparisonResult.orderedDescending){
            ///saved date is greater
            NetworkHelper.sharedInstance.isPausedInternet = true
            print("eTime greater")
            return true
        }else{
            print("sTime greater")
            showAlert(ConstantStrings.policy_time_validation)
            return false
        }
        
    }
    
    func addHostsForPolicy() {
        for element in hostsList{
            if element.isInPolicy(){
                selectedHostsList.append(element)
            }
        }
    }
    
    
    //MARK: - webservices calls hostsList
    //MARK: web service calls
    func loadHostsList(){
        
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.GET_CNCTDHOST_API, sendData: params, success: { (data) in
            print(data)
            let message = "Request failed.Please try again"
            
            if(NetworkHelper.sharedInstance.isConnectedDevicesSuccess)
            {
                if let jsonArr = data["message"] as? [[String:AnyObject]] {
                
                    if ((jsonArr.count) > 0) {
                    
                        self.fetchData(jsonArr)
                    
                }else{
                        DispatchQueue.main.async {
                            self.showAlert("No devices are connected. Please refresh screen to process again.")
                        }
                }
            }
            else{
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
            }
            }
            //
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
    }
    
    //MARK: - fetch data
    func fetchData(_ jsonArr : [[String : AnyObject]]){
        
        for element in jsonArr {
            let macAdres = element["macaddress"] as! String
            let hostName = element["hostname"] as! String
            let ip = element["ip"] as! String
            let device_category = element["device_category"] as! String
            //add into model class
            let device = ConnectedDevicesModal()
            device.setMacAdress_cd(macAdres)
            device.setHostName_cd(hostName)
            device.setHostIP_cd(ip)
            if device_category.range(of: "Router") != nil {
                
            }else{
                device.setToAddInPolicy(false)
                hostsList.append(device)
            }
        }
        
        self.hostsList.sort(by: self.sorterForFileIDASC)
        
        DispatchQueue.main.async {
            self.table_view_devices.reloadData()
        }
        
    }
    
    func getWeightFromArray() -> Int
    {
        var weight : Int = 0
        //        for value in arrayDays
        //        {
        if(btn_mon.isSelected == true)
        {
            weight = weight + 64
        }
        if(btn_tue.isSelected == true)
        {
            weight = weight + 32
        }
        if(btn_wed.isSelected == true)
        {
            weight = weight + 16
        }
        if(btn_thu.isSelected == true)
        {
            weight = weight + 8
        }
        if(btn_fri.isSelected == true)
        {
            weight = weight + 4
        }
        if(btn_sat.isSelected == true)
        {
            weight = weight + 2
        }
        if(btn_sun.isSelected == true)
        {
            weight = weight + 1
        }
        //        }
        return weight
        
    }
    //MARK: - Class functins/uer defaults
    
    func calculateDaysFromWeightsNew(weight : Int) {
        var weight = weight
        let dayWeights = [64, 32, 16, 8, 4, 2, 1]
        for e in dayWeights {
           if e  & weight != 0 {
            print("day included for \(e)")
            selectedDaysList.append(e as AnyObject)
            weight = weight - e
           }else{
            print("day removed for \(e)")
            }
        }
        
        btn_sat.isSelected = false
        btn_sun.isSelected = false
        btn_mon.isSelected = false
        btn_tue.isSelected = false
        btn_wed.isSelected = false
        btn_thu.isSelected = false
        btn_fri.isSelected = false
        
        for element in selectedDaysList{
            if element as! Int == 64 {
                btn_mon.isSelected = true
            }else if element as! Int == 32 {
                btn_tue.isSelected = true
            }else if element as! Int == 16 {
                btn_wed.isSelected = true
            }else if element as! Int == 8 {
                btn_thu.isSelected = true
            }else if element as! Int == 4 {
                btn_fri.isSelected = true
            }else if element as! Int == 2 {
                btn_sat.isSelected = true
            }else if element as! Int == 1{
                btn_sun.isSelected = true
            }
        }
        setAllDaysSelected()
    }
    
    func calculateDaysFromWeight(_ weight: Int) -> [Int]
    {
        var weight = weight
//        let binary = UInt8(exactly: policyDaysWeight)
        
        let num = 22
        let str = String(num, radix: 2)
        print(str) // 10110
        print(pad(string: str, toSize: 8))  // 00010110

        var indexes = [Int]()
        indexes = [0,0,0,0,0,0,0]
        var j : Int = 0
        var i : Int = 6
        while(i >= 0)
        {
            let result : Int = Int(2^i)
            if(weight>=result)
            {
                weight = weight - result
                indexes[j] = 1
            }
            j = j + 1
            i = i - 1
        }
        print("Indexlist \(indexes)")
        return indexes
    }
    
    func pad(string : String, toSize: Int) -> String {
        var padded = string
        for _ in 0..<(toSize - string.count) {
            padded = "0" + padded
        }
        return padded
    }

    
    func calculateDaysStringFromIndexexArray(_ indexes : [Int]) -> String
    {
        
        var daysString : String = ""
        
        if(indexes[0] == 0)
        {
            
            btn_mon.isSelected = false
        }
        else{
            daysString = daysString + "Mon,"
            btn_mon.isSelected = true
        }
        
        if(indexes[1] == 0)
        {
            btn_tue.isSelected = false
        }
        else{
            daysString = daysString + "Tue,"
            btn_tue.isSelected = true
            
        }
        
        if(indexes[2] == 0)
        {
            btn_wed.isSelected = false
        }
        else{
            daysString = daysString + "Wed,"
            btn_wed.isSelected = true
        }
        
        if(indexes[3] == 0)
        {
            btn_thu.isSelected = false
        }
        else{
            daysString = daysString + "Thu,"
            btn_thu.isSelected = true
        }
        
        if(indexes[4] == 0)
        {
            btn_fri.isSelected = false
        }
        else{
            daysString = daysString + "Fri,"
            btn_fri.isSelected = true
        }
        
        if(indexes[5] == 0)
        {
            btn_sat.isSelected = false
        }
        else{
            daysString = daysString + "Sat,"
            btn_sat.isSelected = true
        }
        
        if(indexes[6] == 0)
        {
            btn_sun.isSelected = false
        }
        else{
            daysString = daysString + "Sun,"
            btn_sun.isSelected = true
        }
        
        setAllDaysSelected()
        daysString = String(daysString.dropLast())
        return daysString
        
    }
    
   
    
    
    func savePolicy(isCreateNew : Bool, isDelete : Bool, url : String, policType : String, hostList : AnyObject) {
        NetworkHelper.sharedInstance.error401 = false
        let weight = self.getWeightFromArray()
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let timeZone = TimeZone.current.localizedName(for: .shortStandard, locale: .current) ?? ""  // "GMT-3
        
        //  Converted to Swift 4 by Swiftify v4.1.6654 - https://objectivec2swift.com/
        var localTimeZoneFormatter = DateFormatter()
        localTimeZoneFormatter.timeZone = NSTimeZone.local
        localTimeZoneFormatter.dateFormat = "Z"
        var localTimeZoneOffset = localTimeZoneFormatter.string(from: Date())
        
        localTimeZoneOffset.insert(":", at: localTimeZoneOffset.index(localTimeZoneOffset.startIndex, offsetBy: 3)) // prints hel!lo
        if isPredefinedPolicy{
            policyStatus = 1
        }
        
        let data : Dictionary<String,AnyObject> = ["type": policType as AnyObject, "id" : policyKey as AnyObject, "status" : policyStatus as AnyObject, "title": tf_policyTitle.text as AnyObject, "s_time" : policyStartTime as AnyObject, "e_time" : policyEndTime as AnyObject, "s_time_gmt" : policyStartTimeGMT as AnyObject, "e_time_gmt" : policyEndTimeGMT as AnyObject, "days" : weight as AnyObject, "hosts_list" : hostList as AnyObject, "time_zone": localTimeZoneOffset as AnyObject]
        let params : Dictionary<String,AnyObject> = ["data": data as AnyObject, "request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (data) in
            print(params)
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async(execute: {
                self.showPolicySuccessDialog()
                //policy successfully created
                })
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                let msg = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(msg)
                }
            } else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
   
    func sorterForFileIDASC(_ this:ConnectedDevicesModal, that:ConnectedDevicesModal) -> Bool {
        
        let f = this.getHostIP_cd().components(separatedBy: ".")
        let s = that.getHostIP_cd().components(separatedBy: ".")
        return Int(f.last!)! < Int(s.last!)!
        
    }
    
    func showPolicySuccessDialog() {
        let alertController = UIAlertController(title: "Prytex", message: ConstantStrings.policy_created_text, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            self.navigationController?.popViewController(animated: true)
            
        }
       
        // Add the actions
        alertController.addAction(okAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
