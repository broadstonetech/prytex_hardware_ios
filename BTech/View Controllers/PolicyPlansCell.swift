//
//  PolicyPlansCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 04/05/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class PolicyPlansCell: UITableViewCell {

    @IBOutlet weak var description_lbl_width: NSLayoutConstraint!
    @IBOutlet weak var title_view: UIView!
    @IBOutlet weak var duration_view: UIView!
    @IBOutlet weak var days_view: UIView!
    @IBOutlet weak var devices_view: UIView!
    @IBOutlet weak var timestamp_view: UIView!
    
    @IBOutlet weak var title_label: UILabel!
    @IBOutlet weak var duration_heading_label: UILabel!
    @IBOutlet weak var duration_label: UILabel!
    @IBOutlet weak var days_heading_label: UILabel!
    @IBOutlet weak var days_label: UILabel!
    @IBOutlet weak var devices_heading_label: UILabel!
    @IBOutlet weak var devices_label: UILabel!
    @IBOutlet weak var timestamp_label: UILabel!
    @IBOutlet weak var actions_label: UILabel!
    
    @IBOutlet weak var selectPolicyPlanBtn: UIButton!
    
    @IBOutlet weak var policy_switch_btn: UISwitch!
    @IBOutlet weak var policy_switch_btn_width: NSLayoutConstraint!
    
    @IBOutlet weak var policy_delete_btn: UIButton!
    @IBOutlet weak var policy_type_icon: UIImageView!
    @IBOutlet weak var policy_type_icon_width: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
