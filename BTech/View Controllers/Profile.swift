//
//  Profile.swift
//  BTech
//
//  Created by Ahmed on 03/10/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
//import TPKeyboardAvoiding
//import MZFormSheetPresentationController
import FirebaseAnalytics

class Profile: Parent{
    
    var pickerDataSource = ["White", "Red", "Green", "Blue"];
    var pickerView: UIPickerView!
    let rightButtonBar = UIButton()
    var isShowNetConfigScreen:Bool = false
    var isModeShown = false
    var isNotificationHide = false
    var isNotificationViewHidden = false
    
    // MARK: - Outlets
    @IBOutlet weak var passwordView: UITextView!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var expandableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var email: UILabel!
    
    @IBOutlet weak var username_label: UILabel!
    
    @IBOutlet weak var muteSwitch: UISwitch!
    
    @IBOutlet weak var promptSwitch: UISwitch!
    
    @IBOutlet var celciusBtn: UIButton!
    
    @IBOutlet var modeBtn: UIButton!
    
    @IBOutlet var ledManagementBtn: UIButton!
    
    @IBOutlet weak var themeSwitch: UISwitch!
    @IBOutlet weak var reset_dmoat_view: UIView!
    //    @IBOutlet var scrollView: TPKeyboardAvoidingCollectionView!
    //Push notifications
    
    @IBOutlet weak var changePasswordView: UIView!
    @IBOutlet weak var connectedDevice_switch: UISwitch!
    @IBOutlet weak var blockedAlerts_switch: UISwitch!
    @IBOutlet weak var infoAlerts_switch: UISwitch!
    @IBOutlet weak var flowAlerts_switch: UISwitch!
    @IBOutlet weak var sensorAlerts_switch: UISwitch!
    @IBOutlet weak var applyNotificationsbtn: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        scrollView.directionalLockEnabled = true
        //
        //check app settings temperature state
        if NetworkHelper.sharedInstance.getCurrentDisplayMode() == false{
            self.themeSwitch.setOn(false, animated: true)
            if #available(iOS 13.0, *) {
            self.navigationController?.overrideUserInterfaceStyle = .light
            }
        }
        else {
            self.themeSwitch.setOn(true, animated: true)
            if #available(iOS 13.0, *) {
            self.navigationController?.overrideUserInterfaceStyle = .dark
            }
        }
        createRightButtonNavigationBar()
        if(NetworkHelper.sharedInstance.appSettingsTemperatureStatus == "Fahrenheit")
        {
            self.celciusBtn.setTitle("Fahrenheit", for: .normal)
        }
        else{
            self.celciusBtn.setTitle("Celcius", for: .normal)
        }
        if(NetworkHelper.sharedInstance.muteSwitchState == nil)
        {
            NetworkHelper.sharedInstance.muteSwitchState = "off"
        }
        if(NetworkHelper.sharedInstance.promptSwitchState == nil)
        {
            NetworkHelper.sharedInstance.promptSwitchState = "off"
        }
        //check mute switch state
        if(NetworkHelper.sharedInstance.muteSwitchState == "on")
        {
            self.muteSwitch.isOn = true
        }
        else if(NetworkHelper.sharedInstance.muteSwitchState == "off")
        {
            self.muteSwitch.isOn = false
        }
        //check prompt switch state
        if(NetworkHelper.sharedInstance.promptSwitchState == "on")
        {
            self.promptSwitch.isOn = true
        }
        else if(NetworkHelper.sharedInstance.promptSwitchState == "off")
        {
            self.promptSwitch.isOn = false
        }
        
        //check app settings led management
        getLedModeConfig()
        getNotificationSettings()
        let tapchangePasswordView = UITapGestureRecognizer(target: self, action: #selector(self.changePasswordViewPressed(_:)))
        changePasswordView.addGestureRecognizer(tapchangePasswordView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
   //     Analytics.setScreenName("Profile and Setting", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Profile and Setting", AnalyticsParameterScreenClass: screenClass])
    }
    
    
    @IBAction func themeSwitchBtn(_ sender: Any) {
        
        if themeSwitch.isOn {
            if #available(iOS 13.0, *) {
            
            self.navigationController?.overrideUserInterfaceStyle = .dark
                NetworkHelper.sharedInstance.saveCurrentDisplayMode(true)
            }else {
            if #available(iOS 13.0, *) {
                self.navigationController?.overrideUserInterfaceStyle = .light
            } else {
                // Fallback on earlier versions
            }
            
        }
        } else {
            if #available(iOS 13.0, *) {
                self.navigationController?.overrideUserInterfaceStyle = .light
                NetworkHelper.sharedInstance.saveCurrentDisplayMode(false)
            } else {
                // Fallback on earlier versions
            }
        }
        
    }
    
    @objc func changePasswordViewPressed(_ sender: UITapGestureRecognizer? = nil) {
        let changePasswordVC = ChangePassword.ChangePasswordVC()
        self.setNavBar()
        self.navigationController?.pushViewController(changePasswordVC, animated: true)
    }
    
    func createRightButtonNavigationBar()
    {
        rightButtonBar.setImage(UIImage(named: "info"), for: UIControl.State())
        rightButtonBar.frame = CGRect(x: 0, y: 0, width: 48, height: 48)
        rightButtonBar.addTarget(self, action: #selector(topRightButtonBar), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = rightButtonBar
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        //create custom
    }
    
    @objc func topRightButtonBar()
    {
        self.showAlert(ConstantStrings.profile_setting_screen_txt)
    }
    
    //    override func viewWillLayoutSubviews() {
    //
    //        let positionOfButton = reset_dmoat_view.frame.size.height+reset_dmoat_view.frame.origin.y
    //        scrollView.contentSize=CGSize(width: UIScreen.main.bounds.size.width, height: positionOfButton+10)
    //
    //    }
    
    @IBAction func policyManagementPressed(_ sender: AnyObject) {
        //        let policyManagementVC = PolicyManagement.policyManagementVC()
        //        self.setNavBar()
        //        self.navigationController?.pushViewController(policyManagementVC, animated: true)
        
        let viewPolicyVC = ViewPolicy.viewPolicyVC()
        self.setNavBar()
        self.navigationController?.pushViewController(viewPolicyVC, animated: true)
        
    }
    
    
    @IBAction func ResetSettingsPressed(_ sender: AnyObject) {
        
        if NetworkHelper.sharedInstance.isDmoatOnline {
            
            let alertController = UIAlertController(title: "Prytex", message: "Are you sure you want to reset Prytex to its factory default settings?", preferredStyle: .alert)
            
            // Create the actions
            let osAction = UIAlertAction(title: "Reset", style: UIAlertAction.Style.default) {
                UIAlertAction in
                
                DispatchQueue.main.async {
                    //                     self.callClearBaseline()
                    self.callResetDmoatSettings()
                    
                }
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
            }
            // Add the actions
            alertController.addAction(osAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }else{
            showAlert(ConstantStrings.dmoatOffline)
        }
    }
    
    func callResetDmoatSettings() {
        
        NetworkHelper.sharedInstance.error401 = false
        //call service for reset settings
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let params:Dictionary<String,String> = ["request_id": requestID, "app_id" : NetworkHelper.sharedInstance.app_ID!, "token": NetworkHelper.sharedInstance.getCurrentToken(), "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace()]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.RESET_SETTING_API, sendData: params as [String : AnyObject], success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }
                else{
                    DispatchQueue.main.async {
                        self.showAlert("Request forwarded to Prytex.")
                    }
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            } else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                if ((message.range(of: "Please request again after")) != nil){
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    //MARK: -Mode config
    @IBAction func modePressed(_ sender: AnyObject) {
        if NetworkHelper.sharedInstance.isDmoatOnline {
            
            if(NetworkHelper.sharedInstance.isDmoatOnline){
                getModeConfig();
            }else{
                self.showAlert(ConstantStrings.dmoatOffline)
            }
        }else{
            showAlert(ConstantStrings.dmoatOffline)
        }
        
    }
    
    
    //MARK: -webservice calls
    
    func getModeConfig() {
        
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let webService = NetworkHelper ()
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.GET_CONFIG_MODE_API, sendData: params, success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            
            if ((statusCode?.range(of: "200")) != nil){
                let modeType = data["type"] as! String
                //                self.showAlert("Success \(modeType)")
                if(modeType == "P&P" || modeType == "Plug & Play") {
                    NetworkHelper.sharedInstance.saveCurrentUserMode("Plug & Play")
                    NetworkHelper.sharedInstance.appSettingsMode = "Plug & Play"
                }else{ //Advanced
                    NetworkHelper.sharedInstance.saveCurrentUserMode("Advanced")
                    NetworkHelper.sharedInstance.appSettingsMode = "Advanced"
                }
                DispatchQueue.main.async {
                    self.modeDialog()
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                        
                    }
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }  else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            } else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
    }
    
    
    func callPAndPService()
    {
        NetworkHelper.sharedInstance.error401 = false
        //call service for changing password
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let data = ["request": "change_boot_mode", "mode": 0] as [String : Any]
        let params:Dictionary<String,AnyObject> = (["request_id": requestID, "token": NetworkHelper.sharedInstance.getCurrentToken(), "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace(), "data" : data, "app_id" : NetworkHelper.sharedInstance.app_ID!] as NSDictionary) as! Dictionary<String, AnyObject>
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.NET_CONFIG_API, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async {
                    self.showAlert("Prytex is in Plug & Play mode.")
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                        
                    }
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            } else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    //MARK:- led management
    @IBAction func ledManagementPressed(_ sender: Any) {
        if NetworkHelper.sharedInstance.isDmoatOnline {
                   let optionMenu = UIAlertController(title: nil, message: "LED Blinking Interval:", preferredStyle: .actionSheet)
                   let off = UIAlertAction(title: "Turn Led's off", style: .default, handler: {
                       (alert: UIAlertAction!) -> Void in
       
                       self.callLEDManagement(frequency: "OFF")
       
       
                   })
                   let fiveSeconds = UIAlertAction(title: "5 seconds", style: .default, handler: {
                       (alert: UIAlertAction!) -> Void in
                       self.callLEDManagement(frequency: "5")
       
                   })
       
                   let tenSeconds = UIAlertAction(title: "10 seconds", style: .default, handler: {
                       (alert: UIAlertAction!) -> Void in
                       self.callLEDManagement(frequency: "10")
       
       
                   })
       
                   let cancelAction = UIAlertAction(title:"Cancel", style: .cancel, handler: {
                       (alert: UIAlertAction!) -> Void in
       
                   })
       
       
                   optionMenu.addAction(fiveSeconds)
                   optionMenu.addAction(tenSeconds)
                   optionMenu.addAction(off)
                   optionMenu.addAction(cancelAction)
                   self.present(optionMenu, animated: true, completion: nil)
               } else {
                   showAlert(ConstantStrings.dmoatOffline)
               }
    }
    
//    @IBAction func ledManagementPressed(_ sender: AnyObject) {
//        if NetworkHelper.sharedInstance.isDmoatOnline {
//            let optionMenu = UIAlertController(title: nil, message: "LED Blinking Interval:", preferredStyle: .actionSheet)
//            let off = UIAlertAction(title: "Turn Led's off", style: .default, handler: {
//                (alert: UIAlertAction!) -> Void in
//
//                self.callLEDManagement(frequency: "OFF")
//
//
//            })
//            let fiveSeconds = UIAlertAction(title: "5 seconds", style: .default, handler: {
//                (alert: UIAlertAction!) -> Void in
//                self.callLEDManagement(frequency: "5")
//
//            })
//
//            let tenSeconds = UIAlertAction(title: "10 seconds", style: .default, handler: {
//                (alert: UIAlertAction!) -> Void in
//                self.callLEDManagement(frequency: "10")
//
//
//            })
//
//            let cancelAction = UIAlertAction(title:"Cancel", style: .cancel, handler: {
//                (alert: UIAlertAction!) -> Void in
//
//            })
//
//
//            optionMenu.addAction(fiveSeconds)
//            optionMenu.addAction(tenSeconds)
//            optionMenu.addAction(off)
//            optionMenu.addAction(cancelAction)
//            self.present(optionMenu, animated: true, completion: nil)
//        } else {
//            showAlert(ConstantStrings.dmoatOffline)
//        }
//    }
    
    //MARK: -LED management
    
    func callLEDManagement(frequency : String)
    {
        NetworkHelper.sharedInstance.error401 = false
        //call service for changing password
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        var data:Dictionary<String, Any>
        if (frequency == "OFF") {
            data = ["mode": frequency]
            NetworkHelper.sharedInstance.appSettingsLEDManagement = frequency
        }else{
            data = ["mode": "ON", "frequency" : frequency]
            NetworkHelper.sharedInstance.appSettingsLEDManagement = "\(frequency) seconds"
        }
        let params:Dictionary<String,AnyObject> = (["request_id": requestID, "token": NetworkHelper.sharedInstance.getCurrentToken(), "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace(), "data" : data, "app_id" : NetworkHelper.sharedInstance.app_ID!] as NSDictionary) as! Dictionary<String, AnyObject>
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.LED_CONFIG_API, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                //                                self.showAlert("success")
                DispatchQueue.main.async {
                    self.ledManagementBtn.setTitle(NetworkHelper.sharedInstance.appSettingsLEDManagement, for: .normal)
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            } else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    //MARK: -CLASS FUNCTIONS
    
    class func ProfileVC() -> Profile {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Profile") as! Profile
    }
    
    @objc func refreshLEDButtonStatus()
    {
        self.ledManagementBtn.setTitle(NetworkHelper.sharedInstance.appSettingsLEDManagement, for: UIControl.State())
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(refreshLEDButtonStatus),
            name: NSNotification.Name(rawValue: "LEDNotification"),
            object: nil)
        
        
        //        self.modeBtn.isHidden = true
        self.navigationItem.title = "Profile & Setting"
        self.passwordView.isSecureTextEntry = true
        self.name.text = NetworkHelper.sharedInstance.name
        self.email.text = NetworkHelper.sharedInstance.email
        self.username_label.text = NetworkHelper.sharedInstance.username
        if isModeShown {
            isModeShown = false
            if NetworkHelper.sharedInstance.isDmoatOnline {
                
                if(NetworkHelper.sharedInstance.isDmoatOnline){
                    getModeConfig();
                }else{
                    self.showAlert(ConstantStrings.dmoatOffline)
                }
            }else{
                showAlert(ConstantStrings.dmoatOffline)
            }
        }
        //check app settings temperature state
        if(NetworkHelper.sharedInstance.appSettingsTemperatureStatus == "Fahrenheit")
        {
            self.celciusBtn.setTitle("Fahrenheit", for: UIControl.State())
        }
        else{
            self.celciusBtn.setTitle("Celcius", for: UIControl.State())
        }
        
        if(NetworkHelper.sharedInstance.muteSwitchState == nil)
        {
            NetworkHelper.sharedInstance.muteSwitchState = "off"
        }
        if(NetworkHelper.sharedInstance.promptSwitchState == nil)
        {
            NetworkHelper.sharedInstance.promptSwitchState = "off"
        }
        //check mute switch state
        if(NetworkHelper.sharedInstance.muteSwitchState == "on")
        {
            self.muteSwitch.isOn = true
        }
        else
        {
            self.muteSwitch.isOn = false
        }
        //check prompt switch state
        if(NetworkHelper.sharedInstance.promptSwitchState == "on")
        {
            self.promptSwitch.isOn = true
        }
        else
        {
            self.promptSwitch.isOn = false
        }
        
        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0
        }
    }
    
    //MARK: -Temp scale
    
    @IBAction func celciusBtnPressed(_ sender: AnyObject) {
        if NetworkHelper.sharedInstance.isDmoatOnline {
            let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
            let CelciusAction = UIAlertAction(title: "Celcius", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                NetworkHelper.sharedInstance.appSettingsTemperatureStatus = "Celcius"
                NetworkHelper.sharedInstance.saveCurrentUserTemperatureStatus("Celcius")
                self.celciusBtn.setTitle(NetworkHelper.sharedInstance.appSettingsTemperatureStatus, for: UIControl.State())
                
            })
            let FahrenheitAction = UIAlertAction(title: "Fahrenheit", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                NetworkHelper.sharedInstance.appSettingsTemperatureStatus = "Fahrenheit"
                NetworkHelper.sharedInstance.saveCurrentUserTemperatureStatus("Fahrenheit")
                self.celciusBtn.setTitle(NetworkHelper.sharedInstance.appSettingsTemperatureStatus, for: UIControl.State())
                
            })
            let cancelAction = UIAlertAction(title:"cancel", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                
            })
            
            
            optionMenu.addAction(CelciusAction)
            optionMenu.addAction(FahrenheitAction)
            optionMenu.addAction(cancelAction)
            self.present(optionMenu, animated: true, completion: nil)
        }else{
            DispatchQueue.main.async {
                self.showAlert(ConstantStrings.dmoatOffline)
            }
        }
    }
    
    
    
    //MARK: -Invite subscriber
    
    @IBAction func invitePressed(_ sender: AnyObject) {
        if NetworkHelper.sharedInstance.isDmoatOnline {
            
            let invite = Subscribers.SubscribersVC()
            self.setNavBar()
            self.navigationController?.pushViewController(invite, animated: true)
        }else{
            DispatchQueue.main.async {
                self.showAlert(ConstantStrings.dmoatOffline)
            }
        }
        
    }
    //MARK: -Modify password
    
    @IBAction func changeButtonPressed(_ sender: AnyObject) {
        let changePasswordVC = ChangePassword.ChangePasswordVC()
        self.setNavBar()
        self.navigationController?.pushViewController(changePasswordVC, animated: true)
        
    }
    
    
    //MARK: -Mode config
    @IBAction func promptSwitchPressed(_ sender: AnyObject) {
        if NetworkHelper.sharedInstance.isDmoatOnline {
            if (sender.isOn == true) {
                NetworkHelper.sharedInstance.promptSwitchState = "on"
                NetworkHelper.sharedInstance.saveCurrentUserPromptSwitchState("on")
                NetworkHelper.sharedInstance.promptUserAlert = "true"
                
            } else {
                NetworkHelper.sharedInstance.promptSwitchState = "off"
                NetworkHelper.sharedInstance.saveCurrentUserPromptSwitchState("off")
                NetworkHelper.sharedInstance.promptUserAlert = "false"
            }
        }else{
            DispatchQueue.main.async {
                self.showAlert(ConstantStrings.dmoatOffline)
            }
            promptSwitch.setOn(false, animated: false)
        }
        
    }
    
    //MARK: -Mute action
    
    @IBAction func muteSwitchPressed(_ sender: AnyObject) {
        if (sender.isOn == true) {
            NetworkHelper.sharedInstance.muteSwitchState = "on"
            NetworkHelper.sharedInstance.saveCurrentUserMuteAllState("on")
            print("On")
            if(NetworkHelper.sharedInstance.deviceToken != nil)
            {
                (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
            }
        } else {
            NetworkHelper.sharedInstance.muteSwitchState = "off"
            NetworkHelper.sharedInstance.saveCurrentUserMuteAllState("off")
            print("off")
            if(NetworkHelper.sharedInstance.deviceToken != nil)
            {
                (UIApplication.shared.delegate as! AppDelegate).UnmuteNotifications()
            }
        }
        
    }
    
    //MARK: -Relearn behaviour /clearbaseline
    
    @IBAction func clearBaselinePressed(_ sender: Any) {
        if NetworkHelper.sharedInstance.isDmoatOnline {
            
            let alertController = UIAlertController(title: "Prytex", message: "Are you sure you want to clear all stored values?", preferredStyle: .alert)
            
            // Create the actions
            let osAction = UIAlertAction(title: "Relearn", style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.callClearBaseline()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
            }
            // Add the actions
            alertController.addAction(osAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }else{
            DispatchQueue.main.async {
                self.showAlert(ConstantStrings.dmoatOffline)
            }
        }
        
    }
    
    func callClearBaseline() {
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let data : [String : AnyObject] = ["resource" : "clear_baseline" as AnyObject, "request" : "get" as AnyObject ]
        
        let params:Dictionary<String,AnyObject> = ["namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "data" : data as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject,"token" : NetworkHelper.sharedInstance.getCurrentToken() as AnyObject,"request_id" : requestID as AnyObject,"postkey" : NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject]
        print(params)
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.CLEAR_BASELINE, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                
                DispatchQueue.main.async {
                    //  self.showAlert("Baseline Cleared.")
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    
    //MARK: -PUSH NOTIFICATIONS
    
    @IBAction func pushNotifications_pressed(_ sender: Any) {
        
        //        let vc = CustomisePushNotificationsVC.customPushNotificationsVC()
        //        self.navigationController?.pushViewController(vc, animated: true)
        //new screen removed and showing section in same view
        if self.isNotificationViewHidden {
            self.heightConstraint.constant = 276
            isNotificationViewHidden = false
        } else {
            
            self.heightConstraint.constant = 50
            isNotificationViewHidden = true
        }
        
        
    }
    
    //MARK: -class functions
    func modeDialog() {
        //self.pickerVew.hidden = false
        let optionMenu = UIAlertController(title: nil, message: "Prytex Mode", preferredStyle: .actionSheet)
        let pAndPAction = UIAlertAction(title: "Plug & Play", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            // Create the alert controller
            let alertController = UIAlertController(title: "Prytex mode", message: ConstantStrings.alert_guide_pnp, preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "Switch", style: UIAlertAction.Style.default) {
                UIAlertAction in
                if (NetworkHelper.sharedInstance.appSettingsMode == "Plug & Play" || NetworkHelper.sharedInstance.appSettingsMode == "P&P") {
                    DispatchQueue.main.async {
                        self.showPnPAlreadyDialog()
                    }
                } else{
                    DispatchQueue.main.async {
                        self.callPAndPService()
                    }
                }
                NSLog("OK Pressed")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
                print("Mode== \(NetworkHelper.sharedInstance.appSettingsMode)")
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
        })
        
        let pAndPActionRed = UIAlertAction(title: "Plug & Play", style: .destructive, handler: {
            (alert: UIAlertAction!) -> Void in
            // Create the alert controller
            
            if (NetworkHelper.sharedInstance.appSettingsMode == "Plug & Play" || NetworkHelper.sharedInstance.appSettingsMode == "P&P") {
                DispatchQueue.main.async {
                    self.showPnPAlreadyDialog()
                }
            } else{
                
                
                let alertController = UIAlertController(title: "Prytex mode", message: ConstantStrings.alert_guide_pnp, preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "Switch", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    self.callPAndPService()
                    NSLog("OK Pressed")
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                    UIAlertAction in
                    self.modeDialog()
                    NSLog("Cancel Pressed, again open dialog")
                }
                
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
                
            }
            
        })
        //pAndPAction.enabled = false
        let advanceAction = UIAlertAction(title: "Advanced", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            // Create the alert controller
            let alertController = UIAlertController(title: "Prytex mode", message: ConstantStrings.alert_guide_advance, preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "Switch", style: UIAlertAction.Style.default) {
                UIAlertAction in
                //                self.callAdvanceService()
                let advanceModeVC = AdvanceMode.AdvanceModeVC()
                self.navigationController?.pushViewController(advanceModeVC, animated: true)
                
                NSLog("OK Pressed")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
                print("Mode== \(NetworkHelper.sharedInstance.appSettingsMode)")
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
        })
        let advanceActionRed = UIAlertAction(title: "Advanced", style: .destructive, handler: {
            (alert: UIAlertAction!) -> Void in
            //                UIAlertAction in
            let alertController = UIAlertController(title: "Prytex mode", message: ConstantStrings.alert_guide_advance, preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "Switch", style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.callAdvanceService()
                
                NSLog("OK Pressed")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
                print("Mode== \(NetworkHelper.sharedInstance.appSettingsMode)")
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title:"Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        
        if(NetworkHelper.sharedInstance.appSettingsMode == "Plug & Play" || NetworkHelper.sharedInstance.appSettingsMode == "P&P")
        {
            //red p and p
            //pAndPAction.enabled = false
            optionMenu.addAction(pAndPActionRed)
            optionMenu.addAction(advanceAction)
        }
        else{
            //red advanced
            //advanceAction.enabled = false
            optionMenu.addAction(pAndPAction)
            optionMenu.addAction(advanceActionRed)
            
        }
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    func showPnPAlreadyDialog() {
        
        let alertController = UIAlertController(title: "Prytex mode", message: ConstantStrings.already_pnp_text, preferredStyle: .alert)
        
        // Create the actions
        
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            self.modeDialog()
            NSLog("ok Pressed, again open dialog")
        }
        
        // Add the actions
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    func callAdvanceService()
    {
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.GET_NET_CONFIG, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                print("success get/netconfig")
                self.isShowNetConfigScreen = true
                //                NotificationCenter.default.addObserver(
                //                    self,
                //                    selector: #selector(self.getNetConfigData),
                //                    name: NSNotification.Name(rawValue: "AdvanceModeNotification"),
                //                    object: nil)
                let advanceModeVC = AdvanceMode.AdvanceModeVC()
                //                self.setNavBar()
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(advanceModeVC, animated: true)
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            } else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    //    //ger pubnub netconfig data
    //    @objc func getNetConfigData()
    //    {
    //        
    //        if(NetworkHelper.sharedInstance.advanceModeModel?.netmaskString == nil ||
    //            NetworkHelper.sharedInstance.advanceModeModel?.subnetString == nil ||
    //            NetworkHelper.sharedInstance.advanceModeModel?.routerIpString == nil ||
    //            NetworkHelper.sharedInstance.advanceModeModel?.defaultLeaseTimeString == nil ||
    //            NetworkHelper.sharedInstance.advanceModeModel?.maxLeaseTimeString == nil ||
    //            NetworkHelper.sharedInstance.advanceModeModel?.broadcastString == nil){
    //            print("pubnub nill")
    //        }else{
    //            print("pubnub response received for net config")
    //            if isShowNetConfigScreen {
    //                isShowNetConfigScreen = false
    //                let advanceModeVC = AdvanceMode.AdvanceModeVC()
    //                self.setNavBar()
    //                self.navigationController?.pushViewController(advanceModeVC, animated: true)
    //            }else{
    //                print("no request for show net config screen")
    //            }
    //        }
    //        
    //        //self.saveAdvanceMode()
    //    }
    
    //MARK: -GetLedMode
    func getLedModeConfig() {
        
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let data : [String : AnyObject] = ["resource" : "clear_baseline" as AnyObject, "request" : "get" as AnyObject ]
        
        let params:Dictionary<String,AnyObject> = ["namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "data" : data as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject,"token" : NetworkHelper.sharedInstance.getCurrentToken() as AnyObject,"request_id" : requestID as AnyObject,"postkey" : NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject]
        print(params)
        
        NetworkHelper.sharedInstance.postDashboardRequest(serviceName: ApiUrl.Get_Led_Mode, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                
                //                get led mode and frequency
                if data["mode"] != nil{
                    let ledMode = data["mode"] as! String
                    if ledMode == "OFF"{
                        DispatchQueue.main.async {
                            NetworkHelper.sharedInstance.appSettingsLEDManagement = "OFF"
                            self.ledManagementBtn.setTitle(NetworkHelper.sharedInstance.appSettingsLEDManagement, for: .normal)
                        }
                    }
                    else if data["frequency"] != nil {
                        let ledFrequency = data["frequency"] as AnyObject
                        let freq = String(describing: ledFrequency)
                        if freq.range(of: "5") != nil {
                            DispatchQueue.main.async {
                                NetworkHelper.sharedInstance.appSettingsLEDManagement = "5 seconds"
                                self.ledManagementBtn.setTitle(NetworkHelper.sharedInstance.appSettingsLEDManagement, for: .normal)
                            }
                        }else{
                            DispatchQueue.main.async {
                                NetworkHelper.sharedInstance.appSettingsLEDManagement = "10 seconds"
                                self.ledManagementBtn.setTitle(NetworkHelper.sharedInstance.appSettingsLEDManagement, for: .normal)
                            }
                        }
                    }
                }
                
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    //MARK: -IB ACtions
    
    @IBAction func connectedDeviceSwitch_pressed(_ sender: Any) {
        
    }
    @IBAction func blockedAlertSwitch_pressed(_ sender: Any) {
        
    }
    @IBAction func infoAlertSwitch_pressed(_ sender: Any) {
        
    }
    @IBAction func flowAlertSwitch_pressed(_ sender: Any) {
        
    }
    @IBAction func sensorAlertSwitch_pressed(_ sender: Any) {
        
    }
    
    
    // MARK: - IB Actions
    
    @IBAction func applyNotifications_pressed(_ sender: Any) {
        print("apply notification settings")
        setNotificationSettings()
    }
    
    
    @IBAction func rebootDmoat_pressed(_ sender: Any) {
        
        if NetworkHelper.sharedInstance.isDmoatOnline {
            
            let alertController = UIAlertController(title: "Prytex", message: "Are you sure you want to reboot Prytex?", preferredStyle: .alert)
            
            // Create the actions
            let osAction = UIAlertAction(title: "Reboot", style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.callRebootDmoat()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
            }
            // Add the actions
            alertController.addAction(osAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }else{
            showAlert(ConstantStrings.dmoatOffline)
        }
        
    }
    
    //MARK: - API calls
    func getNotificationSettings() {
        
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.GET_NOTIFICATION_SETTING, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                print("success \(data)")
                DispatchQueue.main.async {
                    let dataReceived = data["data"] as? [String : AnyObject]
                    if dataReceived!["blocked"] as! Int == 0 {
                        self.blockedAlerts_switch.setOn(false, animated: true)
                    }
                    if dataReceived!["cnctd_host"] as! Int == 0 {
                        self.connectedDevice_switch.setOn(false, animated: true)
                    }
                    if dataReceived!["devices"] as! Int == 0 {
                        self.flowAlerts_switch.setOn(false, animated: true)
                    }
                    if dataReceived!["info"] as! Int == 0 {
                        self.infoAlerts_switch.setOn(false, animated: true)
                    }
                    if dataReceived!["sensors"] as! Int == 0 {
                        self.sensorAlerts_switch.setOn(false, animated: true)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "503")) != nil) {
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    //                    self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                }else{
                    //                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    //                    self.showAlert(message)
                }else{
                    //                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            } else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                //                self.showAlert(message)
            } else {
                //                self.showAlert(ConstantStrings.ErrorText)
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
    }
    
    //set notifications settings
    func setNotificationSettings() {
        
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        var infoAlert : Int = 0
        var cnctdHostAlert : Int = 0
        var blockedAlert : Int = 0
        var sensorsAlert : Int = 0
        var devicesAlert : Int = 0
        
        if infoAlerts_switch.isOn {
            infoAlert = 1
        }
        if connectedDevice_switch.isOn {
            cnctdHostAlert = 1
        }
        if blockedAlerts_switch.isOn {
            blockedAlert = 1
        }
        if sensorAlerts_switch.isOn {
            sensorsAlert = 1
        }
        if flowAlerts_switch.isOn {
            devicesAlert = 1
        }
        let data : [String : AnyObject] = ["info" : infoAlert as AnyObject, "cnctd_host" : cnctdHostAlert as AnyObject, "blocked" : blockedAlert as AnyObject, "devices" : devicesAlert as AnyObject, "sensors" : sensorsAlert as AnyObject ]
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "data" : data as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.SET_NOTIFICATION_SETTING, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil) {
                print("success \(data)")
                DispatchQueue.main.async {
                    self.showAlert("Settings applied successfully.")
                }
            }
            else if ((statusCode?.range(of: "503")) != nil) {
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            } else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
        
    }
    
    func callRebootDmoat() {
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let params:Dictionary<String,AnyObject> = ["namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject,"token" : NetworkHelper.sharedInstance.getCurrentToken() as AnyObject,"request_id" : requestID as AnyObject,"postkey" : NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject]
        print(params)
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.REBOOT_DMOAT, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                
                DispatchQueue.main.async {
                    self.showAlert("Request forwarded to Prytex.")
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    //Mark: -logout service
     func callLogOutService()
     {
         NetworkHelper.sharedInstance.error401 = false
         let randomNumber = Int(arc4random_uniform(10000) + 1)
         let requestID : String = String(randomNumber)
         
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject, ConstantStrings.fcm_device_token : NetworkHelper.sharedInstance.FCM_DEVICE_TOKEN as AnyObject, ConstantStrings.device_uuid : UIDevice.current.identifierForVendor?.uuidString as AnyObject]
         
         //        call service for log out
         //        success
         
         NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.SIGN_OUT_API, sendData: params as [String : AnyObject], success: { (data) in
             print(data)
              let defaults = UserDefaults.standard
             let statusCode = defaults.string(forKey: "statusCode")
             
             if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async {
                self.logoutUserToLogIn()
                }
                 
             }else if ((statusCode?.range(of: "503")) != nil){
                 if (data["message"] != nil) {
                     let msg = data["message"] as! String
                     self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                 }else{
                     self.showAlert(ConstantStrings.ErrorText)
                     
                 }
             }
             else if ((statusCode?.range(of: "409")) != nil){
                 NetworkHelper.sharedInstance.isDmoatOnline = false
                 if (data["message"] != nil) {
                     let message = data["message"] as! String
                     self.showAlert(message)
                 }else{
                     self.showAlert(ConstantStrings.ErrorText)
                     
                 }
             }
             else{
                 self.showAlert("Failed to Sign out!")
                 self.logoutUserToLogIn()
             }
             
         }, failure: { (data) in
             print(data)
             if(NetworkHelper.sharedInstance.error401)
             {
                 NetworkHelper.sharedInstance.isNotificationLocked = true
                 //mute notifications
                 if(NetworkHelper.sharedInstance.deviceToken != nil)
                 {
                     (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                 }
                 //unsubscribe from all
                 (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                 //clear nsuserdefaults
                 NetworkHelper.sharedInstance.clearUserData()
                 //delete all data
                 //AlertModelRealm.deleteAllRecords()
                 PolicyManagementRealm.deleteAllRecords()
                 let loginVC = Login.loginVC()
                 self.navigationController?.pushViewController(loginVC, animated: true)
             }
             
         })
         
         
     }
    
    @IBAction func about_pressed(_ sender: Any) {
        let aboutVC = About.AboutVC()
        self.setNavBar()
        self.navigationController?.pushViewController(aboutVC, animated: true)
    }
    @IBAction func signOut_pressed(_ sender: Any) {
        
        NetworkHelper.sharedInstance.isDmoatOnline = true
        let alertController = UIAlertController(title: "Signing Out", message: "Are you sure you want to sign out?", preferredStyle: .alert)
        
        // Create the actions
        let osAction = UIAlertAction(title: "Sign Out", style: UIAlertAction.Style.default) {
            UIAlertAction in
            DispatchQueue.main.async {
            if self.isInternetAvailable() {
                self.callLogOutService()
            } else {
            self.logoutUserToLogIn()
            }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(osAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    //MARK: - GO to Login-- logout
    public func logoutUserToLogIn() {
        NetworkHelper.sharedInstance.isNotificationLocked = true
        //unregister for apns
        UIApplication.shared.unregisterForRemoteNotifications()
        //mute notifications
        //        if(NetworkHelper.sharedInstance.deviceToken != nil)
        //        {
        //        }
        if(self.isInternetAvailable()){
            //unsubscribe from all
            //            (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
            (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
            (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
            (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
            //clear nsuserdefaults
            NetworkHelper.sharedInstance.clearUserData()
            //delete all data
            AlertModelRealm.deleteAllRecords()
            PolicyManagementRealm.deleteAllRecords()
            NetworkHelper.sharedInstance.deviceToken = nil
            NetworkHelper.sharedInstance.uuidToken = ""
            let loginVC = Login.loginVC()
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        
        //        if(self.checkPausedTimeAsAndroid()) {
        //
        //            let profileVC = Profile.ProfileVC()
        //            self.setNavBar()
        //            self.navigationController?.pushViewController(profileVC, animated: true)
        //        }
        //        else{
        //            self.showAlert(ConstantStrings.pausedIntenettext)
        //        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
        
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
}
