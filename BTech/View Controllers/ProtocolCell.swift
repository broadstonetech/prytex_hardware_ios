//
//  ProtocolCell.swift
//  BTech
//
//  Created by Ahmed Akhtar on 06/01/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class ProtocolCell: UITableViewCell {

    var checked = false
    
    //iboutlet
    @IBOutlet var protocolName: UILabel!
    @IBOutlet var checkedBtn: UIButton!
    
    //ibaction
    
    @IBAction func checkedPressed(_ sender: UIButton) {
        if checked {
            var model = ProtocolModel()
            model = NetworkHelper.sharedInstance.protocolsMainArray[sender.tag]
            model.checked = false
            NetworkHelper.sharedInstance.protocolsMainArray[sender.tag] = model
            checkedBtn.setImage( UIImage(named:"uncheckbox"), for: UIControl.State())
            checked = false
        } else {
            var model = ProtocolModel()
            model = NetworkHelper.sharedInstance.protocolsMainArray[sender.tag]
            model.checked = true
            NetworkHelper.sharedInstance.protocolsMainArray[sender.tag] = model
            checkedBtn.setImage(UIImage(named:"checkbox"), for: UIControl.State())
            checked = true
        }

        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
