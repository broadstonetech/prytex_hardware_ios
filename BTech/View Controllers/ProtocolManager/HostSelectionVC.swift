//
//  HostSelectionVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 18/09/2018.
//  Copyright © 2018 Broadstone Technologies. All rights reserved.
//

import UIKit

class HostSelectionVC: Parent, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var createPolicyForNetwork_btn: UIButton!
    @IBOutlet weak var applyToNetwork_view: UIView!
    
    @IBOutlet weak var errorMsg_lbl: UILabel!
    var titleText:String = ""
    let rightButtonBar = UIButton()
    var hostsList: [ConnectedDevicesModal] = [ConnectedDevicesModal]()
    var selectedHostsList: [ConnectedDevicesModal] = [ConnectedDevicesModal]()
    
    class func hostSelectionVC() -> HostSelectionVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "host_selection_vc") as! HostSelectionVC
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table_view.dataSource = self
        self.table_view.delegate = self
        DispatchQueue.main.async {
            self.loadHostsList()
        }
        self.createPolicyForNetwork_btn.addTarget(self, action: #selector(createPolicyForNetwork), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.titleText = "Create Policy"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.navigationItem.title = titleText
        //        createRightButtonNavigationBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createRightButtonNavigationBar()
    {
        
        rightButtonBar.setImage(UIImage(named: "info"), for: UIControl.State())
        rightButtonBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        rightButtonBar.addTarget(self, action: #selector(topRightButtonBar), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = rightButtonBar
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        //create custom
    }
    
    @objc func createPolicyForNetwork() {

        let VC = ProtocolManagerVC.protocolManagerVC()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    @objc func topRightButtonBar()
    {
        self.showAlert("ConstantStrings.policy_screen_txt)")
    }
    
    // MARK: - Devices TableView delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return hostsList.count
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let policy_device_cell =  self.table_view.dequeueReusableCell(withIdentifier: "policyDevicesCell") as! policyDevicesCell
        
        policy_device_cell.imgCheckMark.isHidden = false
        
        let item = hostsList[indexPath.row]
        //        selectedIndexPathArray.append(indexPath as NSIndexPath)
        //            if item.isInPolicy(){
        //                item.setToAddInPolicy(false)
        ////                for var i in (0..<selectedHostsList.count){
        ////                    if selectedHostsList[i].getHostId() == item.getHostId(){
        ////                        selectedHostsList.remove(at: i)
        ////                        break
        ////                    }
        ////                }
        //            }else{
        //                for var i in (0..<hostsList.count){
        //                    hostsList[i].setToAddInPolicy(false)
        //                }
        //                selectedHostsList.removeAll()
        //                item.setToAddInPolicy(true)
        //                let device = ConnectedDevicesModal()
        //                device.setHostId_cd(item.getHostId())
        //                device.setHostIP_cd(item.getHostIP_cd())
        //                device.setHostName_cd(item.getHostName_cd())
        //                device.setMacAdress_cd(item.getMacAdress_cd())
        //                selectedHostsList.append(device)
        //            }
        //            table_view.reloadData()
        let vc = ProtocolManagerVC.protocolManagerVC()
        vc.hostId = item.getHostId()
        vc.hostName = item.getHostName_cd()
        vc.macAdress = item.getMacAdress_cd()
        vc.hostIp = item.getHostIP_cd()
        vc.isForDevice = true
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let policy_device_cell =  self.table_view.dequeueReusableCell(withIdentifier: "policyDevicesCell") as! policyDevicesCell
        
        
        let item = hostsList[indexPath.row]
        policy_device_cell.device_name.text = item.getHostName_cd()
        //            policy_device_cell.host_ip_view.isHidden = false
        policy_device_cell.device_ip.text = item.getHostIP_cd()
        if  item.isInPolicy(){
            policy_device_cell.imgCheckMark.isHidden = false
        }else{
            policy_device_cell.imgCheckMark.isHidden = true
        }
        
        
        
        return policy_device_cell
    }
    
    //MARK: web service calls
    func loadHostsList(){
        
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.GET_CNCTDHOST_API, sendData: params, success: { (data) in
            print(data)
            let message = "Request failed.Please try again"
            
            if(NetworkHelper.sharedInstance.isConnectedDevicesSuccess)
            {
                if let jsonArr = data["message"] as? [[String:AnyObject]] {
                    
                    if ((jsonArr.count) > 0) {
                        DispatchQueue.main.async {
                            self.table_view.isHidden = false
                            self.applyToNetwork_view.isHidden = false
                            self.fetchData(jsonArr)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.hideViews()
                            self.errorMsg_lbl.isHidden = false
                            //                        self.showAlert("No devices are connected. Please make sure that your d.moat is powered on and connected to the network.")
//                            self.navigationController?.popViewController(animated: true)
                            
                        }
                    }
                }
                else{
                    self.hideViews()
                    self.errorMsg_lbl.isHidden = false
                    self.showAlert(message)
                }
            }
            //
        }, failure: { (data) in
            print(data)
            self.hideViews()
            self.errorMsg_lbl.isHidden = false
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
    }
    
    func hideViews() {
        applyToNetwork_view.isHidden = true
        table_view.isHidden = true
    }
    //MARK: - fetch data
    func fetchData(_ jsonArr : [[String : AnyObject]]){
        
        for element in jsonArr {
            let macAdres = element["macaddress"] as! String
            let hostName = element["hostname"] as! String
            let hostID = element["HOSTID"] as! String
            let ip = element["ip"] as! String
            let device_category = element["device_category"] as! String
            //add into model class
            let device = ConnectedDevicesModal()
            device.setMacAdress_cd(macAdres)
            device.setHostName_cd(hostName)
            device.setHostId_cd(hostID)
            device.setHostIP_cd(ip)
            if device_category.range(of: "Router") != nil {
                
            }else{
                device.setToAddInPolicy(false)
                hostsList.append(device)
            }
        }
        
        self.hostsList.sort(by: self.sorterForFileIDASC)
        
        DispatchQueue.main.async {
            self.table_view.reloadData()
        }
        
    }
    
    @IBAction func proceed_btn_pressed(_ sender: Any) {
        print("move to protocol manager screen and pass hostname, id and macaddress")
        if selectedHostsList.count > 1 || selectedHostsList.isEmpty {
            print("add a max of one device")
            showAlert("Please select a device to proceed.")
        }else{
            print("everything is ok")
            //            self.navigationController?.popViewController(animated: false)
            let vc = ProtocolManagerVC.protocolManagerVC()
            vc.hostId = selectedHostsList[0].getHostId()
            vc.hostName = selectedHostsList[0].getHostName_cd()
            vc.macAdress = selectedHostsList[0].getMacAdress_cd()
            vc.hostIp = selectedHostsList[0].getHostIP_cd()
            vc.isForDevice = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    func sorterForFileIDASC(_ this:ConnectedDevicesModal, that:ConnectedDevicesModal) -> Bool {
        
        let f = this.getHostIP_cd().components(separatedBy: ".")
        let s = that.getHostIP_cd().components(separatedBy: ".")
        return Int(f.last!)! < Int(s.last!)!
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
