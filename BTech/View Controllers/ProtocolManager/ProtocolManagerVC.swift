//
//  ProtocolManagerVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 03/09/2018.
//  Copyright © 2018 Broadstone Technologies. All rights reserved.
//

import UIKit

class ProtocolManagerVC: Parent, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var table_view_frequent_protocols: UITableView!
    
    @IBOutlet weak var tf_policyTitle: UITextField!
    @IBOutlet weak var tf_available_for: UILabel!
    @IBOutlet weak var addition_protocols_btn: UIButton!
    @IBOutlet weak var additional_protocols_lbl: UILabel!
    @IBOutlet weak var collapse_icon: UIImageView!
    
    var titleText:String = ""
    let rightButtonBar = UIButton()
    var frequentProtocolsListRcvd : [PolicyPlanModel] = [PolicyPlanModel]()
    var additionalProtocolsList : [PolicyPlanModel] = [PolicyPlanModel]()
    var allProtocolsList : [PolicyPlanModel] = [PolicyPlanModel]()
    var hostsList: [ConnectedDevicesModal] = [ConnectedDevicesModal]()
    var listOfSelectedProtocolsKeys : [PolicyPlanModel] = [PolicyPlanModel]()
    var listOfSelectedProtocols = [String]()
    var frequentProtocolUiNames = [String]()
    var protocolsCount : Int = -1
    var isForDevice : Bool = false
    var policyType : String = ""
    var policyKey : String = ""
    var hostName : String = ""
    var hostIp : String = ""
    var hostId : String = ""
    var macAdress : String = ""
    
    
    class func protocolManagerVC() -> ProtocolManagerVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "protocol_manager_vc") as! ProtocolManagerVC
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table_view_frequent_protocols.delegate = self
        self.table_view_frequent_protocols.dataSource = self
        tf_policyTitle.delegate = self
        policyKey =  NetworkHelper.sharedInstance.getRandomKeyString()
        if isForDevice {
            policyType = "device"
            tf_available_for.text = hostName
        }else{
            policyType = "network"
            tf_available_for.text = "Network"
        }
        NetworkHelper.sharedInstance.BlockAppProtocolsKeyValues() //prepare protocol key values
        //create new
        DispatchQueue.main.async(execute: {
            self.loadFrequentProtocols()
        })
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.titleText = "Protocol Manager"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.navigationItem.title = titleText
        createRightButtonNavigationBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func createRightButtonNavigationBar()
    {
        
        rightButtonBar.setImage(UIImage(named: "info"), for: UIControl.State())
        rightButtonBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        rightButtonBar.addTarget(self, action: #selector(topRightButtonBar), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = rightButtonBar
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        //create custom
    }
    
    @objc func topRightButtonBar()
    {
        self.showAlert(ConstantStrings.protocol_policy_screen_txt)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField === tf_policyTitle)
        {
            tf_policyTitle.resignFirstResponder()
            tf_policyTitle.endEditing(true)
            return true
        }
        return true
    }
    
    // MARK: - Devices TableView delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return protocolsCount
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 40
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 40 //UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = table_view_frequent_protocols.cellForRow(at: indexPath)
        let item = allProtocolsList[indexPath.row]
        //        selectedIndexPathArray.append(indexPath as NSIndexPath)
        if item.isInPolicy(){
            item.setToAddInPolicy(false)
            
            for var i in (0..<listOfSelectedProtocols.count){
                if listOfSelectedProtocols[i] == item.getProtocolName(){
                    listOfSelectedProtocols.remove(at: i)
                    break
                }
            }
        }else{
            item.setToAddInPolicy(true)
            listOfSelectedProtocols.append(item.getProtocolName())
        }
        table_view_frequent_protocols.reloadData()
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let policy_device_cell =  self.table_view_frequent_protocols.dequeueReusableCell(withIdentifier: "policyDevicesCell") as! policyDevicesCell
        
        let item = allProtocolsList[indexPath.row]
        let protocolName = item.getProtocolName()
        policy_device_cell.device_name.text = protocolName
        policy_device_cell.host_ip_view.isHidden = true
        policy_device_cell.protocol_img.isHidden = false
        policy_device_cell.protocol_img_width.constant = 24
        
        //        //image finding
        var protocolKey : String = ""
        for (key, value) in NetworkHelper.sharedInstance.protocolsValuesDictForBlocking {
            if protocolName == value {
                protocolKey = key
                break
            }
        }
        if (UIImage(named: protocolKey) != nil) {
            //            let image = UIImage(named: protocolName)
            policy_device_cell.protocol_img.image = UIImage(named: protocolKey)
        }
        else {
            policy_device_cell.protocol_img.image=UIImage(named: "ip_default")
        }
        
        if item.isInPolicy(){
            policy_device_cell.imgCheckMark.isHidden = false
        }else{
            policy_device_cell.imgCheckMark.isHidden = true
        }
        
        
        
        return policy_device_cell
    }
    
    
    
    //MARK: - Load Frequent Protocols
    func loadFrequentProtocols() {
        frequentProtocolsListRcvd.removeAll()
        additionalProtocolsList.removeAll()
        listOfSelectedProtocols.removeAll()
        frequentProtocolUiNames.removeAll()
        NetworkHelper.sharedInstance.arrayBlockedAlerts.removeAll()
        let timeZone = TimeZone.current.localizedName(for: .shortStandard, locale: .current) ?? ""  // "GMT-3
        
        //  Converted to Swift 4 by Swiftify v4.1.6654 - https://objectivec2swift.com/
        var localTimeZoneFormatter = DateFormatter()
        localTimeZoneFormatter.timeZone = NSTimeZone.local
        localTimeZoneFormatter.dateFormat = "Z"
        var localTimeZoneOffset = localTimeZoneFormatter.string(from: Date())
        
        localTimeZoneOffset.insert(":", at: localTimeZoneOffset.index(localTimeZoneOffset.startIndex, offsetBy: 3)) // prints hel!lo
        
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        NetworkHelper.sharedInstance.error401 = false
        
        var data : Dictionary <String, AnyObject> = [:]
        data = ["time_zone": localTimeZoneOffset as AnyObject]
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject, "data" : data as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.GET_FREQUENT_PROTOCOLS, sendData: params as [String : AnyObject], success: { (data) in
            print(data)
            let message = ConstantStrings.ErrorText
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil)
            {
                
                let jsonArr = data["message"] as? [String]
                
                print("json array received for frequent protocols \(jsonArr!)")
                
                for element in jsonArr!{
                    let protocolName = element
                    print(protocolName)
                    var protocolUIName : String
                    let policyItem = PolicyPlanModel()
                    //
                    //                            policyItem.frequentProtocolsList = jsonArr as! [String]//(hostsList as? [[String:AnyObject]])!
                    //                            self.frequentProtocolsListRcvd.append(policyItem)
                    let keyValueExists = NetworkHelper.sharedInstance.protocolsValuesDictForBlocking[protocolName] != nil
                    if keyValueExists {
                        protocolUIName = NetworkHelper.sharedInstance.protocolsValuesDictForBlocking[protocolName]!
                        
                    }else{
                        protocolUIName = protocolName
                    }
                    policyItem.setProtocolName(protocolUIName)
                    policyItem.setToAddInPolicy(false)
                    self.frequentProtocolsListRcvd.append(policyItem)
                    
                }
                DispatchQueue.main.async {
                    self.frequentProtocolsListRcvd.sort(by: self.sorterForFileIDASC)
                    self.protocolsCount = self.frequentProtocolsListRcvd.count
                    self.loadAdditionalProtocols()
                    if self.frequentProtocolsListRcvd.count == 0 {
                        self.protocolsCount = self.allProtocolsList.count
                    }
                    self.table_view_frequent_protocols.reloadData()
                }
            }
                
            else if ((statusCode?.range(of: "503")) != nil) {
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            } else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            }
            else{
                self.showAlert(message)
            }
            
        }, failure: { (data) in
            print("failure error is \(data)")
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
    }
    
    func loadAdditionalProtocols() {
        //        additionalProtocolsList//
        for (key, value) in NetworkHelper.sharedInstance.protocolsValuesDictForBlocking {
            let policyItem = PolicyPlanModel()
            if value == "" {
                
            }else{
                policyItem.setProtocolName(value)
                additionalProtocolsList.append(policyItem)
            }
        }
        for e in frequentProtocolsListRcvd {
            let frequentPname = e.getProtocolName()
            for var i in (0..<additionalProtocolsList.count){
                //            for e2 in additionalProtocolsList {
                let allPnames = additionalProtocolsList[i].getProtocolName()
                if frequentPname == allPnames {
                    additionalProtocolsList.remove(at: i)
                    break
                    //remove that protocol from all wali list/additional wali list
                }else{
                    print("protocol not exist")
                }
            }
        }
        additionalProtocolsList.sort(by: sorterForFileIDASC)
        
        allProtocolsList = frequentProtocolsListRcvd + additionalProtocolsList
    }
    //MARK: - Class actions
    @IBAction func load_additional_protocols_pressed(_ sender: Any) {
        addition_protocols_btn.isEnabled = false
        additional_protocols_lbl.textColor = UIColor.gray
        collapse_icon.image = UIImage(named: "minimize")
        protocolsCount = allProtocolsList.count
        
        table_view_frequent_protocols.reloadData()
        self.table_view_frequent_protocols.scrollToRow(at: IndexPath.init(row: frequentProtocolsListRcvd.count, section: 0), at: .bottom, animated: true)
        
    }
    
    
    @IBAction func save_policy_pressed(_ sender: Any) {
        if (tf_policyTitle.text?.count)! < 1{
            print("protocols selected \(listOfSelectedProtocols)")
            showAlert("please input policy title")
            
        }
        else if !(listOfSelectedProtocols.isEmpty) && !(listOfSelectedProtocols.count > 5) {
            print("everything is ok")
            for var i in (0..<listOfSelectedProtocols.count){
                let model = PolicyPlanModel()
                let protocolName = listOfSelectedProtocols[i]
                
                for (key, value) in NetworkHelper.sharedInstance.protocolsValuesDictForBlocking {
                    if protocolName == value {
                        let model = PolicyPlanModel()
                        model.setProtocolName(key)
                        listOfSelectedProtocolsKeys.append(model)
                        break
                    }
                }
                
            }
            if NetworkHelper.sharedInstance.isDmoatOnline {
                DispatchQueue.main.async {
                    self.createPolicyPrompt()
                }
            }else{
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.dmoatOffline)
                }
            }
            
        }else{
            DispatchQueue.main.async {
                self.showAlert("Please select max of 5 protocols.")
            }
        }
        
        
    }
    
    func createPolicyPrompt() {
        let alertController = UIAlertController(title: "Prytex", message: ConstantStrings.create_protoclo_policy_text, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Confirm", style: UIAlertAction.Style.default) {
            UIAlertAction in
            DispatchQueue.main.async {
            self.saveProtocolPolicy()
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    //MARK: - Call SavePolicy Api
    func saveProtocolPolicy() {
        
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        var data : Dictionary<String,AnyObject>
        
        
        let protocolsList = PolicyPlanModel.conertToDictionary(selectedHost: listOfSelectedProtocolsKeys)//PolicyPlanModel.conertToDictionary(selectedHost: listOfSelectedProtocols)
        if isForDevice {

            let polciyTitle = "Protocols blocked for \(hostName)"
            data = ["type": policyType as AnyObject, "id" : policyKey as AnyObject, "procedure" : "create" as AnyObject, "title": polciyTitle as AnyObject, "hostname" : hostName as AnyObject, "hostid" : hostId as AnyObject, "ip" : hostIp as AnyObject, "protocols" : protocolsList as AnyObject]
        }else{
            let polciyTitle = "Protocols blocked for Network"
            
            data = ["type": policyType as AnyObject, "id" : policyKey as AnyObject, "procedure" : "create" as AnyObject, "title": polciyTitle as AnyObject, "protocols" : protocolsList as AnyObject]
        }
        let params : Dictionary<String,AnyObject> = ["data": data as AnyObject, "request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.CREATE_PROTOCOL_POLICIES_API, sendData: params, success: { (data) in
            print(params)
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                //                self.showProtocolPolicySuccessDialog()
                DispatchQueue.main.async {
                    let vc = PauseInternetVC.PauseInternetVC()
                    vc.isProtocolManagerPressed = true
                    
                    self.navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
                        if vc.isKind(of: PauseInternetVC.self) || vc.isKind(of: MainMenu.self) {
                            return false
                        } else {
                            return true
                        }
                    })
                }
                print("policy successfully created")
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                let msg = data["message"] as! String
                DispatchQueue.main.async {
                     self.showAlert(msg)
                }
            } else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                     self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
        
    }
    
    func showProtocolPolicySuccessDialog() {
        let alertController = UIAlertController(title: "Prytex", message: ConstantStrings.policy_created_text, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.navigationController?.popViewController(animated: true)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func sorterForFileIDASC(_ this : PolicyPlanModel, that : PolicyPlanModel ) -> Bool {
        return that.getProtocolName() > this.getProtocolName()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
