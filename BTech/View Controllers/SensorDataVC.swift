 //
 //  SensorDataVC.swift
 //  BTech
 //
 //  Created by Mac Book Pro 13 on 03/10/2017.
 //  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
 //
 
 import UIKit
 import Charts
 import FirebaseAnalytics
 
 class SensorDataVC: Parent {
    
    //MARK: - CLASS outlets
    
    @IBOutlet var main_root_view: UIView!
    
    @IBOutlet weak var charts_view_main: UIView!
    @IBOutlet weak var tab_bar_view: UIView!
    @IBOutlet weak var load_current_data_btn: UIButton!
    @IBOutlet weak var humidity_view_main: UIView!
    @IBOutlet weak var load_all_data_btn: UIButton!
    @IBOutlet weak var temp_view_main: UIView!
    @IBOutlet weak var air_view_main: UIView!
    @IBOutlet weak var air_quality_label: UILabel!
    
    @IBOutlet weak var line_chart_humidity: LineChartView!
    @IBOutlet weak var line_chart_temp: LineChartView!
    @IBOutlet weak var line_chart_airQuality: LineChartView!
    
    @IBOutlet weak var scroll_view: UIScrollView!
    
    @IBOutlet weak var last_sync_label: UILabel!
    @IBOutlet weak var next_sync_label: UILabel!
    
    @IBOutlet weak var humidity_avg_value: UILabel!
    @IBOutlet weak var temp_avg_value: UILabel!
    @IBOutlet weak var air_avg_value: UILabel!
    
    @IBOutlet weak var humidity_min_value: UILabel!
    @IBOutlet weak var humidity_max_value: UILabel!
    
    @IBOutlet weak var temp_min_value: UILabel!
    @IBOutlet weak var temp_max_value: UILabel!
    
    @IBOutlet weak var air_quality_min_value: UILabel!
    @IBOutlet weak var air_quality_max_value: UILabel!
    @IBOutlet weak var air_quality_behaviour: UILabel!
    @IBOutlet weak var currentHumidityValue_lbl: UILabel!
    @IBOutlet weak var currentTemperatureValue_lbl: UILabel!
    @IBOutlet weak var currentAirQualityValue_lbl: UILabel!
    
    //MARK: - CLASS VARIABLES
    var titleText:String = ""
    var temperatureUnit : String = ""
    var humidityAvgVal:Float = 0
    var tempAvgValue:Float = 0
    var airAvgVal:Int32 = 0
    var sensorDataList: [SensorDataModel] = [SensorDataModel]()
    let refreshControl = UIRefreshControl()
    let rightButtonBar = UIButton()
    
    //Line charts declarations
    var lineChartHumidityEntry = [ChartDataEntry]()
    var lineChartTempEntry = [ChartDataEntry]()
    var lineChartAirQualityEntry = [ChartDataEntry]()
    //booleans
    var is24HourSelected : Bool = false
    
    var currentTemperatureValue : String = ""
    var currentHumidityValue : String = ""
    var currentAirQualityValue : String = ""
    
    //MARK: -Class functions
    class func sensorDataVC() -> SensorDataVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SensorData") as! SensorDataVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkTemperatureUnit()
        
        DispatchQueue.main.async {

                self.loadSensorData()

                }
        main_root_view.setNeedsDisplay()
        line_chart_humidity.noDataText = "No humdity data found."
        line_chart_temp.noDataText = "No temperature data found."
        line_chart_airQuality.noDataText = "No air quality data found."
        refreshControl.addTarget(self, action: #selector(refreshSensorData), for: .valueChanged)
        //        charts_view_main.refreshControl = refreshControl
        
        
        // Do any additional setup after loading the view.
        
        self.load_current_data_btn.backgroundColor = UIColor(named: "UpperTabSelected")
        self.load_all_data_btn.backgroundColor = UIColor(named: "ViewPatchB")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let screenClass = classForCoder.description()
      //  Analytics.setScreenName("Sensor Data Current", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Sensor Data Current", AnalyticsParameterScreenClass: screenClass])
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.navigationBar.topItem?.title = ""
        self.navigationItem.title = titleText
        createRightButtonNavigationBar()
        
        currentHumidityValue_lbl.text = "Current: " + currentHumidityValue + "%"
        currentTemperatureValue_lbl.text = "Current: " + currentTemperatureValue + "°F"
        currentAirQualityValue_lbl.text = "Current: " + currentAirQualityValue + "PPM"
    }
    func createRightButtonNavigationBar()
    {
        
        rightButtonBar.setImage(UIImage(named: "info"), for: UIControl.State())
        rightButtonBar.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        rightButtonBar.addTarget(self, action: #selector(topRightButtonBar), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = rightButtonBar
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        //create custom
    }
    
    @objc func topRightButtonBar()
    {
        self.showAlert(ConstantStrings.sensordata_screen_txt)
    }
    //MARK: - TopNavBar actions
    @IBAction func load_current_data_pressed(_ sender: Any) {
        let screenClass = classForCoder.description()
       // Analytics.setScreenName("Sensor Data current", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Sensor Data current", AnalyticsParameterScreenClass: screenClass])
        
        DispatchQueue.main.async {
            self.currentHumidityValue_lbl.isHidden = false
            self.currentTemperatureValue_lbl.isHidden = false
            self.currentAirQualityValue_lbl.isHidden = false
            self.is24HourSelected = false
            self.humidity_max_value.isHidden = true
            self.humidity_min_value.isHidden = true
            self.temp_max_value.isHidden = true
            self.temp_min_value.isHidden = true
            self.air_quality_max_value.isHidden = true
            self.air_quality_min_value.isHidden = true
            
            self.load_current_data_btn.setTitleColor(CozyLoadingActivity.Settings.WhiteColor, for: .normal)
            //            self.load_current_data_btn.backgroundColor = CozyLoadingActivity.Settings.selectedButonColor
            self.load_all_data_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
            //            self.load_all_data_btn.backgroundColor = CozyLoadingActivity.Settings.UnselectedButonColor
            
            //self.load_all_data_btn.backgroundColor = CozyLoadingActivity.Settings.UnselectedTabBgColor
            //self.load_current_data_btn.backgroundColor = CozyLoadingActivity.Settings.SelectedTabBgColor
            self.load_current_data_btn.backgroundColor = UIColor(named: "UpperTabSelected")
            self.load_all_data_btn.backgroundColor = UIColor(named: "ViewPatchB")
            
            
            if(self.sensorDataList.count > 0){
                self.lineChartAirQualityEntry.removeAll(keepingCapacity: true)
                self.lineChartTempEntry.removeAll(keepingCapacity: true)
                self.lineChartHumidityEntry.removeAll(keepingCapacity: true)
                self.humidityAvgVal = 0
                self.tempAvgValue = 0
                self.airAvgVal = 0
                var countVal = 0
                if(self.sensorDataList.count>15){
                    countVal = 15;
                    self.populateHumdityChart(count: 15)
                    self.populateTempChart(count: 15)
                    self.populateAirQualityChart(count: 15)
                }else{
                    let totalValues = self.sensorDataList.count
                    countVal = totalValues
                    self.populateHumdityChart(count: totalValues)
                    self.populateTempChart(count: totalValues)
                    self.populateAirQualityChart(count: totalValues)
                }
                let totalValues = countVal
                let tempAvgCelciusCalculated = Float(self.tempAvgValue) / Float(totalValues)
                let fahrnhtVal = ((tempAvgCelciusCalculated * 9) / 5 + 32);
                self.temp_avg_value.text = "\(String(format: "%.1f",tempAvgCelciusCalculated))" + "\u{00B0}" + "C" + "\n" + "\(String(format: "%.1f",fahrnhtVal))" + "\u{00B0}" + "F"
                
                let humidityAvgCalculated = Float(self.humidityAvgVal) / Float(totalValues)
                self.humidity_avg_value.text = "\(String(format: "%.1f",humidityAvgCalculated))" + "%"
                let airAvgCalculated = Int(self.airAvgVal) / totalValues
                self.air_avg_value.text = "\(airAvgCalculated)"+"\nCO2PPM"
                
                if (airAvgCalculated >= 1000){
                    self.air_quality_label.text = "Air Quality - Hazardous"
                }else  if (airAvgCalculated >= 600 && airAvgCalculated <= 999 ){
                    self.air_quality_label.text = "Air Quality - Bad"
                }else  if (airAvgCalculated >= 450 && airAvgCalculated >= 599 ){
                    self.air_quality_label.text = "Air Quality - Fair"
                }else  if (airAvgCalculated < 450){
                    self.air_quality_label.text = "Air Quality - Good"
                }else{
                    self.air_quality_label.text = "Air Quality"
                }
            }
        }
    }
    
    @IBAction func load_all_data_pressed(_ sender: Any) {
        let screenClass = classForCoder.description()
    //    Analytics.setScreenName("Sensor Data 24hour", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Sensor Data 24hour", AnalyticsParameterScreenClass: screenClass])
        
        DispatchQueue.main.async {
            self.currentHumidityValue_lbl.isHidden = true
            self.currentTemperatureValue_lbl.isHidden = true
            self.currentAirQualityValue_lbl.isHidden = true
            self.is24HourSelected = true
            self.humidity_max_value.isHidden = false
            self.humidity_min_value.isHidden = false
            self.temp_max_value.isHidden = false
            self.temp_min_value.isHidden = false
            self.air_quality_max_value.isHidden = false
            self.air_quality_min_value.isHidden = false
            self.load_all_data_btn.setTitleColor(CozyLoadingActivity.Settings.WhiteColor, for: .normal)
            //            self.load_all_data_btn.backgroundColor = CozyLoadingActivity.Settings.selectedButonColor
            self.load_current_data_btn.setTitleColor(CozyLoadingActivity.Settings.UnselectedButonTextColor, for: .normal)
            //            self.load_current_data_btn.backgroundColor = CozyLoadingActivity.Settings.UnselectedButonColor
            
//            self.load_current_data_btn.backgroundColor = CozyLoadingActivity.Settings.UnselectedTabBgColor
//            self.load_all_data_btn.backgroundColor = CozyLoadingActivity.Settings.SelectedTabBgColor
//
            self.load_current_data_btn.backgroundColor = UIColor(named: "ViewPatchB")
            self.load_all_data_btn.backgroundColor = UIColor(named: "UpperTabSelected")
            
            
            if(self.sensorDataList.count > 0){
                self.lineChartAirQualityEntry.removeAll(keepingCapacity: true)
                self.lineChartTempEntry.removeAll(keepingCapacity: true)
                self.lineChartHumidityEntry.removeAll(keepingCapacity: true)
                self.humidityAvgVal = 0
                self.tempAvgValue = 0
                self.airAvgVal = 0
                let totalValues = self.sensorDataList.count
                self.populateHumdityChart(count: totalValues)
                self.populateTempChart(count: totalValues)
                self.populateAirQualityChart(count: totalValues)
                
                let tempAvgCelciusCalculated = Float(self.tempAvgValue) / Float(totalValues)
                let fahrnhtVal = ((tempAvgCelciusCalculated * 9) / 5 + 32);
                self.temp_avg_value.text = "\(String(format: "%.1f",tempAvgCelciusCalculated))" + "\u{00B0}" + "C" + "\n" + "\(String(format: "%.1f",fahrnhtVal))" + "\u{00B0}" + "F"
                
                let humidityAvgCalculated = Float(self.humidityAvgVal) / Float(totalValues)
                self.humidity_avg_value.text = "\(String(format: "%.1f",humidityAvgCalculated))" + "%"
                let airAvgCalculated = Int(self.airAvgVal) / totalValues
                self.air_avg_value.text = "\(airAvgCalculated)"+"\nCO2PPM"
                
                if (airAvgCalculated >= 1000){
                    self.air_quality_label.text = "Air Quality - Hazardous"
                }else  if (airAvgCalculated >= 600 && airAvgCalculated <= 999 ){
                    self.air_quality_label.text = "Air Quality - Bad"
                }else  if (airAvgCalculated >= 450 && airAvgCalculated >= 599 ){
                    self.air_quality_label.text = "Air Quality - Fair"
                }else  if (airAvgCalculated < 450){
                    self.air_quality_label.text = "Air Quality - Good"
                }else{
                    self.air_quality_label.text = "Air Quality"
                }
            }
            
        }
    }
    
    @objc func refreshSensorData(refreshControl: UIRefreshControl) {
        print("refreshing!")
        sensorDataList.removeAll()
        self.loadSensorData()
        
        // somewhere in your code you might need to call:
        //        refreshControl.endRefreshing()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    // MARK: - Actions
    // MARK: - Init Sensors
    @IBAction func init_sensor_pressed(_ sender: Any) {
        
        //init sensor web service call
        if(NetworkHelper.sharedInstance.isDmoatOnline){
            self.showInitSensorDialog()
            
        }else{
            self.showAlert(ConstantStrings.dmoatOffline)
        }
        
    }
    
    @IBAction func screen_info_pressed(_ sender: Any) {
    }
    
    
    func showInitSensorDialog() {
        let alertController = UIAlertController(title: "Prytex", message: ConstantStrings.initialize_sensor_text, preferredStyle: .alert)
        
        // Create the actions
        let initAction = UIAlertAction(title: "Initialize", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.callInitSensor()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(initAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    //MARK: -webservice calls
    func loadSensorData(){
        NetworkHelper.sharedInstance.arraySensorData.removeAll()
        NetworkHelper.sharedInstance.arrayTempReadings.removeAll()
        NetworkHelper.sharedInstance.arrayHumidityReadings.removeAll()
        NetworkHelper.sharedInstance.arrayAirPressureReadings.removeAll()
        NetworkHelper.sharedInstance.arrayTimeReadings.removeAll()
        //call service for getting sensor data
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let params:Dictionary<String,String> = ["request_id": requestID, "token": NetworkHelper.sharedInstance.getCurrentToken(), "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace(), "app_id": NetworkHelper.sharedInstance.app_ID!]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.GET_SENSOR_DATA_API, sendData: params as [String : AnyObject], success: { (data) in
            print(data)
            let message = ConstantStrings.ErrorText
            
            if(NetworkHelper.sharedInstance.isConnectedDevicesSuccess)
            {
                let jsonArr = data["message"] as? [[String:AnyObject]]
                //                print(jsonArr?.count)
                if((jsonArr?.count)! > 0){
                    self.self.fetchData(jsonArr!)
                    
                }else{
                    self.showAlert(ConstantStrings.no_sensorData_detected)
                }
            }
            else{
                self.showAlert(message)
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
    }
    
    func fetchData(_ jsonArr : [[String : AnyObject]]){
        
        for element in jsonArr {
            
            let coppm = element["coppm"]
            
            let airQualitValue = Double((coppm! ) as! Double)
            let humidity = element["humidity"]
            let humiValue = Double((humidity! ) as! Double)
            let temp = element["temperature"]
            let tempval = Double((temp! ) as! Double)
            
            let eveSec = element["eve_sec"] as! Int
            //            let meanAdc = element["mean_adc"] as! Int
            //            let meanCorrectedRes = element["mean_corrected_res"] as! Int
            //            let meanRes = element["mean_res"] as! Int
            //            let validInterval = element["validInterval"] as! Int
            //add into model class
            let sensorData = SensorDataModel()
            //            sensorData.setBaseRes(baseRes as! String)
            //            sensorData.setAirQuality(airQuality as! String)
            //            sensorData.setMeanAdc(meanAdc)
            sensorData.setTemperature(Double(tempval))
            sensorData.setEveSec(eveSec)
            sensorData.setCoppm(Double(airQualitValue))
            sensorData.setHumidity(Double(humiValue))
            //            sensorData.setValidInterval(validInterval)
            //            sensorData.setMeanRes(meanRes)
            //            sensorData.setMeanCorrectedRes(meanCorrectedRes)
            
            sensorDataList.append(sensorData)
        }
        
        sensorDataList.sort(by: sorterForFileIDASC)
        
        DispatchQueue.main.async {
            
            self.sensorDataList.reverse()
            self.currentHumidityValue_lbl.isHidden = false
            self.currentTemperatureValue_lbl.isHidden = false
            self.currentAirQualityValue_lbl.isHidden = false
            self.currentHumidityValue_lbl.text = " Current: " + String(format:"%.0f", self.sensorDataList.last!.getHumidity()) + "% "
            self.currentTemperatureValue_lbl.text = " Current: " + String(format:"%.0f", self.sensorDataList.last!.getTemperature()) + "°F "
            self.currentAirQualityValue_lbl.text = " Current: " + String(format:"%.0f", self.sensorDataList.last!.getCoppm()) + "PPM "
        }
        
        DispatchQueue.main.async {
            self.last_sync_label.isHidden = false
            //            self.next_sync_label.isHidden = false
            let sync_date = self.convertEveSecToDateTime(eveSec: self.sensorDataList[0].getEveSec())
            let nextDate = self.convertEveSecToDateTime(eveSec: self.sensorDataList[0].getEveSec() + 908)
            self.last_sync_label.padding = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 0)
            //            label.padding = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
            
            self.last_sync_label.text = " Data refreshed at:" + sync_date;
            var chartValues = 0
            if(self.sensorDataList.count>15){
                chartValues = 15;
                self.populateHumdityChart(count: 15)
                self.populateTempChart(count: 15)
                self.populateAirQualityChart(count: 15)
            }else{
                let totalValuesToShow = self.sensorDataList.count
                chartValues = totalValuesToShow
                self.populateHumdityChart(count: totalValuesToShow)
                self.populateTempChart(count: totalValuesToShow)
                self.populateAirQualityChart(count: totalValuesToShow)
                
            }
            let totalValues = chartValues
            let tempAvgCelciusCalculated = Float(self.tempAvgValue) / Float(totalValues)
            let fahrnhtVal = ((tempAvgCelciusCalculated * 9) / 5 + 32);
            self.temp_avg_value.text = "\(String(format: "%.1f",tempAvgCelciusCalculated))" + "\u{00B0}" + "C" + "\n" + "\(String(format: "%.1f",fahrnhtVal))" + "\u{00B0}" + "F"
            
            let humidityAvgCalculated = Float(self.humidityAvgVal) / Float(totalValues)
            self.humidity_avg_value.text = "\(String(format: "%.1f",humidityAvgCalculated))" + "%"
            let airAvgCalculated = Int(self.airAvgVal) / totalValues
            self.air_avg_value.text = "\(airAvgCalculated)"+"\nCO2PPM"
            
            if (airAvgCalculated >= 1000){
                self.air_quality_behaviour.text = "- Hazardous"
            }else  if (airAvgCalculated >= 600 && airAvgCalculated <= 999 ){
                self.air_quality_behaviour.text = "- Bad"
            }else  if (airAvgCalculated >= 450 && airAvgCalculated >= 599 ){
                self.air_quality_behaviour.text = "- Fair"
            }else  if (airAvgCalculated < 450){
                self.air_quality_behaviour.text = "- Good"
            }else{
                self.air_quality_behaviour.text = "- Good"
            }
            
            self.humidity_max_value.layer.cornerRadius = 5.0
            self.humidity_max_value.clipsToBounds = true
            self.humidity_min_value.layer.cornerRadius = 5.0
            self.humidity_min_value.clipsToBounds = true
            
            self.temp_max_value.layer.cornerRadius = 5.0
            self.temp_max_value.clipsToBounds = true
            self.temp_min_value.layer.cornerRadius = 5.0
            self.temp_min_value.clipsToBounds = true
            
            self.air_quality_max_value.layer.cornerRadius = 5.0
            self.air_quality_max_value.clipsToBounds = true
            self.air_quality_min_value.layer.cornerRadius = 5.0
            self.air_quality_min_value.clipsToBounds = true
        }
    }
    
    func sorterForFileIDASC(_ this:SensorDataModel, that:SensorDataModel) -> Bool {
        return this.getEveSec() > that.getEveSec()
    }
    
    //Mark :- web service init sensor
    func callInitSensor(){
        
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let data : [String : AnyObject] = ["resource" : "initialize_sensor" as AnyObject, "request" : "get" as AnyObject ]
        
        let params:Dictionary<String,AnyObject> = ["namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "data" : data as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject,"token" : NetworkHelper.sharedInstance.getCurrentToken() as AnyObject,"request_id" : requestID as AnyObject,"postkey" : NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject]
        print(params)
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.INIT_SENSOR_API, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                
                DispatchQueue.main.async {
                    self.showAlert("Sensors Initialized")
                    self.title = "Prytex"
                }
                
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            } else if ((statusCode?.range(of: "401")) != nil) {
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                    (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                }
                //unsubscribe from all
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                }
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
                
            } else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    
    //MARK: -Temperature unit conversion
    func checkTemperatureUnit()
    {
        if(NetworkHelper.sharedInstance.appSettingsTemperatureStatus == "Fahrenheit")
        {
            temperatureUnit=" F"
        }
        else{
            temperatureUnit="\u{00B0}" + "C"
        }
        
    }
    
    //MARK: - Update Chart
    
    //line chart humidity population
    func populateHumdityChart(count : Int) {
        
        var timeArray = [String]()
        for i in 0..<count {
            let date = convertEveSecToDate(eveSec: sensorDataList[i].getEveSec())
            timeArray.append(date)
        }
        
        for i in 0..<count {
            let yVal = sensorDataList[i].getHumidity()
            line_chart_airQuality.xAxis.valueFormatter = IndexAxisValueFormatter(values: timeArray)
            let values = ChartDataEntry(x: Double(i), y: Double(yVal))
            lineChartHumidityEntry.append(values)
            humidityAvgVal = humidityAvgVal + Float(yVal)
        }
        let line1 = LineChartDataSet(entries: lineChartHumidityEntry, label: "")
        line1.colors = [NSUIColor.clear]
        line1.circleRadius = 0;
        line1.drawCirclesEnabled = false
        line1.fillColor =  UIColor.darkGray
        line1.drawCircleHoleEnabled = false
        line1.drawFilledEnabled = true
        line1.cubicIntensity = 12
        
        let data = LineChartData()
        data.addDataSet(line1)
        if is24HourSelected {
            data.setDrawValues(false)
        }
        line_chart_humidity.data = data
        line_chart_humidity.backgroundColor = UIColor.init(red: 207/255, green: 240/255, blue: 255/255, alpha: 1.0)
        line_chart_humidity.rightAxis.drawLabelsEnabled = false
        line_chart_humidity.leftAxis.drawLabelsEnabled = false
        line_chart_humidity.xAxis.drawLabelsEnabled = false
        line_chart_humidity.xAxis.labelPosition = .bottom
        line_chart_humidity.leftAxis.drawGridLinesEnabled = false
        line_chart_humidity.rightAxis.drawGridLinesEnabled = false
        line_chart_humidity.xAxis.drawGridLinesEnabled = false
        line_chart_humidity.dragDecelerationEnabled = true
        line_chart_humidity.chartDescription?.text = ""
        
        line_chart_humidity.rightAxis.enabled = false
        line_chart_humidity.leftAxis.enabled = false
        line_chart_humidity.xAxis.drawAxisLineEnabled = false
        
        //set min and max Y values for 24 hour data chart
        let maxY:String = String(format:"%.2f", (line_chart_humidity.lineData?.getYMax())!)
        humidity_max_value.text = maxY
        let minY:String = String(format:"%.2f", (line_chart_humidity.lineData?.getYMin())!)
        humidity_min_value.text = minY
    }
    
    func populateTempChart(count : Int) {
        var timeArray = [String]()
        for i in 0..<count {
            let date = convertEveSecToDate(eveSec: sensorDataList[i].getEveSec())
            timeArray.append(date)
        }
        
        for i in 0..<count {
            let yVal = sensorDataList[i].getTemperature()
            line_chart_airQuality.xAxis.valueFormatter = IndexAxisValueFormatter(values: timeArray)
            let values = ChartDataEntry(x: Double(i), y: Double(yVal))
            lineChartTempEntry.append(values)
            tempAvgValue = tempAvgValue + Float(yVal)
            
            
        }
        let line1 = LineChartDataSet(entries: lineChartTempEntry, label: "")
        line1.colors = [NSUIColor.clear]
        line1.circleRadius = 0;
        line1.drawCirclesEnabled = false
        line1.fillColor =  UIColor.brown
        line1.drawCircleHoleEnabled = false
        line1.drawFilledEnabled = true
        line1.cubicIntensity = 12
        let data = LineChartData()
        data.addDataSet(line1)
        if is24HourSelected {
            data.setDrawValues(false)
        }
        line_chart_temp.data = data
        line_chart_temp.backgroundColor = UIColor.init(red: 235/255, green: 237/255, blue: 212/255, alpha: 1.0)
        line_chart_temp.rightAxis.drawLabelsEnabled = false
        line_chart_temp.rightAxis.drawLabelsEnabled = false
        line_chart_temp.leftAxis.drawLabelsEnabled = false
        line_chart_temp.xAxis.drawLabelsEnabled = false
        line_chart_temp.xAxis.labelPosition = .bottom
        line_chart_temp.leftAxis.drawGridLinesEnabled = false
        line_chart_temp.rightAxis.drawGridLinesEnabled = false
        line_chart_temp.xAxis.drawGridLinesEnabled = false
        line_chart_temp.dragDecelerationEnabled = true
        line_chart_temp.chartDescription?.text = ""
        
        line_chart_temp.rightAxis.enabled = false
        line_chart_temp.leftAxis.enabled = false
        line_chart_temp.xAxis.drawAxisLineEnabled = false
        //set min and max Y values for 24 hour data chart
        let maxY:String = String(format:"%.2f", (line_chart_temp.lineData?.getYMax())!)
        temp_max_value.text = maxY
        let minY:String = String(format:"%.2f", (line_chart_temp.lineData?.getYMin())!)
        temp_min_value.text = minY
    }
    
    func populateAirQualityChart(count : Int) {
        
        var timeArray = [String]()
        for i in 0..<count {
            let date = convertEveSecToDate(eveSec: sensorDataList[i].getEveSec())
            timeArray.append(date)
        }
        
        for i in 0..<count {
            let yVal = sensorDataList[i].getCoppm()
            line_chart_airQuality.xAxis.valueFormatter = IndexAxisValueFormatter(values: timeArray)
            let values = ChartDataEntry(x: Double(i), y: Double(yVal))
            lineChartAirQualityEntry.append(values)
            airAvgVal += Int32(yVal)
        }
        let line1 = LineChartDataSet(entries: lineChartAirQualityEntry, label: "")
        line1.colors = [NSUIColor.clear]
        line1.circleRadius = 0;
        line1.drawCirclesEnabled = false
        line1.fillColor =  UIColor.gray
        line1.drawCircleHoleEnabled = false
        line1.drawFilledEnabled = true
        line1.cubicIntensity = 12
        let data = LineChartData()
        data.addDataSet(line1)
        if is24HourSelected {
            data.setDrawValues(false)
        }
        line_chart_airQuality.data = data
        line_chart_airQuality.backgroundColor = UIColor.init(red: 221/255, green: 252/255, blue: 220/255, alpha: 1.0)
        line_chart_airQuality.xAxis.granularity = 1
        line_chart_airQuality.rightAxis.drawLabelsEnabled = false
        line_chart_airQuality.rightAxis.drawLabelsEnabled = false
        line_chart_airQuality.leftAxis.drawLabelsEnabled = false
        line_chart_airQuality.xAxis.drawLabelsEnabled = false
        line_chart_airQuality.xAxis.labelPosition = .bottom
        line_chart_airQuality.leftAxis.drawGridLinesEnabled = false
        line_chart_airQuality.rightAxis.drawGridLinesEnabled = false
        line_chart_airQuality.xAxis.drawGridLinesEnabled = false
        line_chart_airQuality.dragDecelerationEnabled = true
        line_chart_airQuality.chartDescription?.text = ""
        
        line_chart_airQuality.rightAxis.enabled = false
        line_chart_airQuality.leftAxis.enabled = false
        line_chart_airQuality.xAxis.drawAxisLineEnabled = false
        //set min and max Y values for 24 hour data chart
        let maxY:String = String(format:"%.0f", (line_chart_airQuality.lineData?.getYMax())!)
        air_quality_max_value.text = maxY
        let minY:String = String(format:"%.0f", (line_chart_airQuality.lineData?.getYMin())!)
        air_quality_min_value.text = minY
    }
    
    
    func convertEveSecToDate(eveSec:Int) -> String {
        let time = String(eveSec)
        let timeInterval  = TimeInterval(time) // as NSTimeInterval
        
        //Convert to Date
        let date = Date(timeIntervalSince1970: timeInterval!)
        
        //Date formatting
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm:a"
        dateFormatter.timeZone = TimeZone.current //name: "UTC")
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    //convert unix to dd-mm-yyy hh:mm:a
    func convertEveSecToDateTime(eveSec:Int) -> String {
        let timeInterval  = TimeInterval(eveSec) // as NSTimeInterval
        
        //Convert to Date
        let date = Date(timeIntervalSince1970: timeInterval)
        
        //Date formatting
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM-dd-yyyy h:mm:a"
        dateFormatter.timeZone = TimeZone.current //name: "UTC")
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        
        //        account_label_tabbar.textColor = UIColor(red: 0/255, green: 174/255, blue: 239/255, alpha: 1.0)
        //        var image: UIImage = UIImage(named: "tab_home_selected")!
        //        account_img_tabbar.image = image
        
        if(self.checkPausedTimeAsAndroid()){
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
        
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
    }
 }
