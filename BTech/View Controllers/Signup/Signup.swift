//
//  Signup.swift
//  BTech
//
//  Created by Ahmad Waqas on 22/07/2016..
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
//import QRCodeReader
import PDFKit

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}


class Signup: Parent,UITextFieldDelegate ,AVCaptureMetadataOutputObjectsDelegate, UIDocumentInteractionControllerDelegate {
    
    @IBOutlet weak var scrollContainer: UIScrollView!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet var tfName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfMobileNum: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfRepass: UITextField!
    @IBOutlet var deviceID: UITextField!
    @IBOutlet weak var btnShowHidePassword: UIButton!
    @IBOutlet weak var btnShowHideRepass: UIButton!
    
    @IBOutlet weak var acceptTerms_toggle: UISwitch!
    @IBOutlet weak var rejectTerms_toggle: UISwitch!
    
    var isProfileShowing:Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setAppearance()
        showWatchVideoAlert()
        tfMobileNum.delegate = self
        deviceID.delegate = self
        // Do any additional setup after loading the view.
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfMobileNum {
        return string.rangeOfCharacter(from: CharacterSet(charactersIn: "§±!@#$%^&*+()-= []{};:/?.>,<")) == nil
        }
        return string.rangeOfCharacter(from: CharacterSet(charactersIn: "")) == nil

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if ConstantStrings.productIdentifierScanner == "Product Identifier"{
            deviceID.placeholder = ConstantStrings.productIdentifierScanner
        }else{
            deviceID.text = ConstantStrings.productIdentifierScanner
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField === tfName)
        {
            tfMobileNum.becomeFirstResponder()
        }else if (textField == tfMobileNum){
            tfEmail.becomeFirstResponder()
        }else if (textField == tfEmail){
            tfPassword.becomeFirstResponder()
        }else if (textField == tfPassword){
            tfRepass.becomeFirstResponder()
        }else if (textField == tfRepass){
            deviceID.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
            textField.endEditing(true)
        }
        return true
    }
    
    // MARK: - Appearance Function
    func setAppearance(){
        
        //        self.navigationController!.navigationBar.barTintColor = UIColor.whiteColor()
        //
        //
        //        UINavigationBar.appearance().setBackgroundImage(UIImage(), forBarPosition: .Any, barMetrics: .Default)
        //        UINavigationBar.appearance().shadowImage = UIImage()
        
        if((self.isProfileShowing) != nil){
            self.setAppearanceForProfile()
        }else{
            self.title = "Sign Up"
        }
    }
    
    func setAppearanceForProfile(){
        
        self.title = "My Profile"
        self.btnLogin.isHidden = true
        self.tfEmail.text = DGlobals.emailAddress()
        self.tfPassword.text = DGlobals.password()
        self.tfRepass.text = DGlobals.password()
        self.tfMobileNum.text = "Not Given"
        DispatchQueue.main.async {
            self.btnSignup.isHidden = true
        }
        
    }
    
    @IBAction func rejectBtn_pressd(_ sender: AnyObject) {
        if rejectTerms_toggle.isOn {
            acceptTerms_toggle.setOn(false, animated: true)
        }else{
            
        }
    }
    
    // MARK: - Actions
    
    @IBAction func acceptTerms_Pressed(_ sender: AnyObject) {
        if acceptTerms_toggle.isOn {
            rejectTerms_toggle.setOn(false, animated: true)
        }else{
            
        }
    }
    
    @IBAction func btnQrScannerPressed(_ sender: AnyObject) {
        print("barcode scanning")
        
        self.showAlert(ConstantStrings.qr_code_scanning_error)
    }
    
    
    @IBAction func btnSignUp_tapped(_ sender: AnyObject) {
        
        
        
        if(self.tfEmail.text=="" || self.tfPassword.text=="" || self.tfMobileNum.text=="" || self.tfRepass.text=="" || self.deviceID.text=="" || self.tfName.text=="")
        {
            self.showAlert(ConstantStrings.fill_all_fields_text)
        }
            
        else if(self.isFormValid()){
            //check if user have accepted the terms or not , then proceed
            if acceptTerms_toggle.isOn {
                if isValidEmail(tfEmail.text!) {
                    self.createUserWithCredentials()
                    ConstantStrings.productIdentifierScanner = "Product Identifier"
                }else{
                    showAlert("Please enter valid email address.")
                }
            }else{
                showAlert(ConstantStrings.terms_conditions_text)
            }
        }
    }
    @IBAction func btnSignIn_tapped(_ sender: AnyObject) {
        ConstantStrings.productIdentifierScanner = "Product Identifier"
        let loginVC = Login.loginVC()
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @IBAction func btnCloseTApped(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - API Call
    func createUserWithCredentials(){
        
        UserDefaults.standard.set(deviceID.text!, forKey: "prodId")
        NetworkHelper.sharedInstance.email = tfEmail.text 
        UserDefaults.standard.synchronize()
        
        self.view.endEditing(true)
        
        //call sign up service
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let appID = UIDevice.current.identifierForVendor?.uuidString
        NetworkHelper.sharedInstance.app_ID = appID
        
        //convert to base64
        let plainPass = (tfRepass.text! as NSString).data(using: String.Encoding.utf8.rawValue)
        
        let base64Pass = plainPass!.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        print("bas364-pass== \(base64Pass)")
        
        print(NetworkHelper.sharedInstance.app_ID!)
        let formattedUserName = tfMobileNum.text!.replacingOccurrences(of: " ", with: "")
        
        let data : [String : AnyObject] = ["prod_id": deviceID.text! as AnyObject, "user_name" : tfMobileNum.text! as AnyObject, "name": tfName.text! as AnyObject, "email" : tfEmail.text! as AnyObject, "password": base64Pass as AnyObject]
        print(data)
        //         let params:Dictionary<String,AnyObject> = ["uikey": "ui111", "data" : data]
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "uikey": "ui111" as AnyObject, "data" : data as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.ACCOUNT_SETUP_API, sendData: params, success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                //go to discovery page
                
                let DiscoveryMod = DiscoveryMode.dmodeVC()
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(DiscoveryMod, animated: true)
                }
                
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            } else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Validation
 
    func atleastOneCapitalAlphabetWithString(_ phrase : String)->Bool
    {
        let letters = CharacterSet.uppercaseLetters
        
        let phrase = phrase
        let range = phrase.rangeOfCharacter(from: letters)
        
        // range will be nil if no letters is found
        if range != nil {
            return false
        }
        else {
            return true
        }
    }
    
    func atleastOneSmallAlphabetWithString(_ phrase : String)->Bool
    {
        let letters = CharacterSet.lowercaseLetters
        
        let phrase = phrase
        let range = phrase.rangeOfCharacter(from: letters)
        
        // range will be nil if no letters is found
        if range != nil {
            return false
        }
        else {
            return true
        }
    }
    
    func atleastOneDigitWithString(_ phrase : String)->Bool
    {
        let letters = CharacterSet.decimalDigits
        
        let phrase = phrase
        let range = phrase.rangeOfCharacter(from: letters)
        
        // range will be nil if no letters is found
        if range != nil {
            return false
        }
        else {
            return true
        }
    }
    
    func atleastOneSpecialCharacterWithString(_ phrase : String)->Bool
    {
        //        let letters = CharacterSet.punctuationCharacters
        //        let phrase = phrase
        //        let range = phrase.rangeOfCharacter(from: letters)
        //
        //        // range will be nil if no letters is found
        //        if range != nil {
        //            return false
        //        }
        //        else {
        //            return true
        //        }
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if phrase.rangeOfCharacter(from: characterset.inverted) != nil {
            print("string contains special characters")
            return false
        }
        return true
    }
    
    func isFormValid()->Bool{
        
        if(!DGlobals.validateEmail(tfEmail.text)){
            self.showAlertWithTitle("Validation Error", message: "Please enter valid email address.", sender: self)
            return false
        }
        
        if(self.tfRepass.text?.count < 8){
            self.showAlertWithTitle("Validation Error", message: ConstantStrings.password_validation_text, sender: self)
            return false
        }
        
        if(self.atleastOneCapitalAlphabetWithString((tfRepass.text)!))
        {
            self.showAlertWithTitle("Validation Error", message: ConstantStrings.password_validation_text, sender: self)
            return false
        }
        if(self.atleastOneSmallAlphabetWithString((tfRepass.text)!))
        {
            self.showAlertWithTitle("Validation Error", message: ConstantStrings.password_validation_text, sender: self)
            return false
        }
        if(self.atleastOneDigitWithString((tfRepass.text)!))
        {
            self.showAlertWithTitle("Validation Error", message: ConstantStrings.password_validation_text, sender: self)
            return false
        }
        if(self.atleastOneSpecialCharacterWithString((tfRepass.text)!))
        {
            self.showAlertWithTitle("Validation Error", message: ConstantStrings.password_validation_text, sender: self)
            return false
        }
        
        if(tfRepass.text != tfPassword.text)
        {
            self.showAlertWithTitle("Validation Error", message: ConstantStrings.password_mismatch_text, sender: self)
            return false
        }
        let prod_count = deviceID.text?.count
        if (prod_count < 8) {
            self.showAlertWithTitle("Validation Error", message: ConstantStrings.qr_code_mismatch_text, sender: self)
            return false
        }
        
        return true
    }
    
    func showAlertWithTitle(_ title:String,message:String,sender:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        sender.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Watch video alert dialog
    //Let's get started. // Let us help you to setup an accoount. // Help for creating an account and setting up d.moat. // Need help for setting up an account/d.maot?
    func showWatchVideoAlert() {
        let alertController = UIAlertController(title: "Prytex", message: "Let’s get started with account setup.", preferredStyle: .alert)
        
        // Create the actions
        let initAction = UIAlertAction(title: "View", style: UIAlertAction.Style.default) {
            UIAlertAction in
            let videoURL = URL(string: ApiUrl.video_account_setup_url)
            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
        let cancelAction = UIAlertAction(title: "Skip", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(initAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    // MARK: - Buttons listener
    
    @IBAction func showHidePassword_pressed(_ sender: Any) {
        if tfPassword.isSecureTextEntry {
            tfPassword.isSecureTextEntry = false
            //            btn_showHide_pwd.setBackgroundImage(UIImage(named: "hidepass.png"), for: UIControlState.normal)
            btnShowHidePassword.setBackgroundImage(UIImage(named: "hide_pwd"), for: UIControl.State.normal)
        } else {
            tfPassword.isSecureTextEntry = true
            //            btn_showHide_pwd.setBackgroundImage(UIImage(named: "Showpass.png"), for: UIControlState.normal)
            btnShowHidePassword.setBackgroundImage(UIImage(named: "show_pwd"), for: UIControl.State.normal)
        }
    }
    
    @IBAction func showHideRepeatPassword_pressed(_ sender: Any) {
        if tfRepass.isSecureTextEntry {
            tfRepass.isSecureTextEntry = false
            //            btn_showHide_pwd.setBackgroundImage(UIImage(named: "hidepass.png"), for: UIControlState.normal)
            btnShowHideRepass.setBackgroundImage(UIImage(named: "hide_pwd"), for: UIControl.State.normal)
        } else {
            tfRepass.isSecureTextEntry = true
            //            btn_showHide_pwd.setBackgroundImage(UIImage(named: "Showpass.png"), for: UIControlState.normal)
            btnShowHideRepass.setBackgroundImage(UIImage(named: "show_pwd"), for: UIControl.State.normal)
        }
    }
    @IBAction func btn_termsServices_pressed(_ sender: AnyObject) {
        print("signup terms of services") //
//        let vc = TeermsOfServices.TosVC()
//             self.navigationController?.pushViewController(vc, animated: true)
        if let link = URL(string: ApiUrl.dmoatTermsServicesDocURL) {
                UIApplication.shared.open(link)
              }
    }
    @IBAction func btn_privacyPolicy_pressed(_ sender: AnyObject) {
        print("signup privacy policy pressed")
     
        if let link = URL(string: ApiUrl.dmoatEULADocURL) {
                           UIApplication.shared.open(link)
                         }
    }
    @IBAction func btn_ensUserAgreement_pressed(_ sender: AnyObject) {
        print("signup end user agreement pressed")
      if let link = URL(string: ApiUrl.dmoatPrivacyPolicyDocURL) {
                        UIApplication.shared.open(link)
                      }
    }
    
    
}
