//
//  Subscribe.swift
//  BTech
//
//  Created by Ahmed Akhtar on 21/11/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class Subscribe: Parent {
    
    //Outlets
    @IBOutlet weak var emailField: UITextField!
    class func SubscribeVC() -> Subscribe {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Subscribe") as! Subscribe
    }
    //Actions
    @IBAction func AddSubscriberPressed(_ sender: AnyObject) {
        
        if(self.emailField.text=="")
        {
            self.showAlert("Please enter a valid email to subscribe.")
        }
            
        else if(self.isValidEmail(self.emailField.text!)==false)
        {
            self.showAlert("Please enter a valid email address.")
        }
            
        else
        {
            NetworkHelper.sharedInstance.error401 = false
            NetworkHelper.sharedInstance.error403 = false
            self.view.endEditing(true)
            //call service for adding subscriber
            let randomNumber = Int(arc4random_uniform(10000) + 1)
            let requestID : String = String(randomNumber)
            let data : [String : String] = ["username": NetworkHelper.sharedInstance.username!,"subscriber_id": String(emailField.text!)]
            let params : [String : AnyObject] = ["app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject, "request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "namespace":String(NetworkHelper.sharedInstance.getCurrentUserNamespace()) as AnyObject, "postkey":String(NetworkHelper.sharedInstance.getCurrentUserPostKey()) as AnyObject, "data":data as AnyObject]
            
            NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.ADD_SUBSCRIBER, sendData: params, success: { (data) in
                print(data)
                let defaults = UserDefaults.standard
                let statusCode = defaults.string(forKey: "statusCode")
                
                if ((statusCode?.range(of: "200")) != nil){
                    self.showOkAlert()
                    
                    //self.loginToUpdateSubscribers()
                }
                else if ((statusCode?.range(of: "503")) != nil){
                    if (data["message"] != nil) {
                        let msg = data["message"] as! String
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }else{
                        self.showAlert(ConstantStrings.ErrorText)
                        
                    }
                }
                else if ((statusCode?.range(of: "409")) != nil){
                    NetworkHelper.sharedInstance.isDmoatOnline = false
                    if (data["message"] != nil) {
                        let message = data["message"] as! String
                        self.showAlert(message)
                    }else{
                        self.showAlert(ConstantStrings.ErrorText)
                        
                    }
                }  else if (data["message"] != nil) {
                    let message = data["message"] as! String
                    self.showAlert(message)
                } else {
                    self.showAlert("Subscriber could not be added. Please try again.")
                }
                
            }, failure: { (data) in
                print(data)
                if(NetworkHelper.sharedInstance.error401)
                {
                    NetworkHelper.sharedInstance.isNotificationLocked =  true
                    //mute notifications
                    if(NetworkHelper.sharedInstance.deviceToken != nil)
                    {
                        (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    }
                    //unsubscribe from all
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                    //clear nsuserdefaults
                    NetworkHelper.sharedInstance.clearUserData()
                    //delete all data
                    //AlertModelRealm.deleteAllRecords()
                    PolicyManagementRealm.deleteAllRecords()
                    let loginVC = Login.loginVC()
                    self.navigationController?.pushViewController(loginVC, animated: true)
                }
                
            })
            
        }
        
        func loginToUpdateSubscribers()
        {
            NetworkHelper.sharedInstance.error401 = false
            let randomNumber = Int(arc4random_uniform(10000) + 1)
            let requestID : String = String(randomNumber)
            let url = ApiUrl.USER_LOGIN_API
            let data = [ConstantStrings.user_name: NetworkHelper.sharedInstance.username! , ConstantStrings.password:NetworkHelper.sharedInstance.password!]
            let params:Dictionary<String,AnyObject> = ["app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject, "request_id" : requestID as AnyObject, "data" : data as AnyObject]
            
            NetworkHelper.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (data) in
                print(data)
                
                UserDefaults.standard.removeObject(forKey: "subscribers")
                NetworkHelper.sharedInstance.arraySubscribers.removeAll()
                let subscribers = data["subscriber"] as! [String]
                for user in subscribers{
                    NetworkHelper.sharedInstance.arraySubscribers.append(user)
                }
                NetworkHelper.sharedInstance.saveSubscribers(NetworkHelper.sharedInstance.arraySubscribers)
                self.navigationController?.popViewController(animated: true)
                //
                
                
            }, failure: { (data) in
                print(data)
                if(NetworkHelper.sharedInstance.error401)
                {
                    NetworkHelper.sharedInstance.isNotificationLocked =  true //
                    //mute notifications
                    if(NetworkHelper.sharedInstance.deviceToken != nil)
                    {
                        (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    }
                    //unsubscribe from all
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                    //clear nsuserdefaults
                    NetworkHelper.sharedInstance.clearUserData()
                    //delete all data
                    //AlertModelRealm.deleteAllRecords()
                    PolicyManagementRealm.deleteAllRecords()
                    let loginVC = Login.loginVC()
                    self.navigationController?.pushViewController(loginVC, animated: true)
                }
                
            })
            
        }
        
     
        
        func viewWillAppear(_ animated: Bool) {
            self.navigationItem.title = "Add Subscriber"
            
        }
    }
        func showOkAlert(){
            let alertController = UIAlertController(title: "Prytex", message: "Successfully Subscribed", preferredStyle: .alert)
            
            // Create the actions
            let osAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.dismiss(animated: true, completion: {});
                self.navigationController?.popViewController(animated: true);
            }
            
            // Add the actions
            alertController.addAction(osAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }
        

}
