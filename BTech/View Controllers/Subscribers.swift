//
//  Subscribers.swift
//  BTech
//
//  Created by Ahmed Akhtar on 25/11/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class Subscribers: Parent, UIAlertViewDelegate {
    
    //Outlets
    @IBOutlet weak var subscriberOne: UILabel!
    
    @IBOutlet weak var subscriberTwo: UILabel!
    
    @IBOutlet weak var subscriberThree: UILabel!
    
    @IBOutlet weak var removeSubscriberOneBtn: UIButton!
    
    @IBOutlet weak var removeSubscriberTwoBtn: UIButton!
    
    @IBOutlet weak var removeSubscriberThreeBtn: UIButton!
    
    
    
    //class variables
    
    var textField : UITextField!
    let alert = UIAlertView()
    
    //Action
    
    
    @IBAction func removeSubscriberOnePressed(_ sender: UIButton) {
        self.removeSubscriberWithSubscriberID(subscriberOne.text!)
    }
    
    
    func removeSubscriberWithSubscriberID(_ subscriberID : String)
    {
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        //call service for adding subscriber
        let data : [String : String] = ["username": NetworkHelper.sharedInstance.username!,"subscriber_id": String(subscriberID)]
        let params : [String : AnyObject] = ["app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject, "request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "namespace":String(NetworkHelper.sharedInstance.getCurrentUserNamespace()) as AnyObject, "postkey":String(NetworkHelper.sharedInstance.getCurrentUserPostKey()) as AnyObject, "data":data as AnyObject]
        
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.REMOVE_SUBSCRIBER_API, sendData: params, success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                self.showAlert("Subscriber Unsubscribed successfully ")
                //self.loginToUpdateSubscribers()
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    self.showAlert(message)
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            } else if (data["message"] != nil) {
                let message = data["message"] as! String
                self.showAlert(message)
            }
            else{
                self.showAlert("Subscriber could not be unsubscribed. Please try again.")
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    
    
    
    @IBAction func removeSubscriberTwoPressed(_ sender: UIButton) {
        //call service for adding subscriber
        self.removeSubscriberWithSubscriberID(subscriberTwo.text!)
    }
    
    @IBAction func removeSubscriberThreePressed(_ sender: UIButton) {
        self.removeSubscriberWithSubscriberID(subscriberThree.text!)
    }
    
    func loginToUpdateSubscribers()
    {
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let url = ApiUrl.USER_LOGIN_API
        let data = [ConstantStrings.user_name: NetworkHelper.sharedInstance.username! , ConstantStrings.password:NetworkHelper.sharedInstance.password!]
        let params:Dictionary<String,AnyObject> = ["app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject, "request_id" : requestID as AnyObject, "data" : data as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (data) in
            print(data)
            
            UserDefaults.standard.removeObject(forKey: "subscribers")
            NetworkHelper.sharedInstance.arraySubscribers.removeAll()
            let subscribers = data["subscriber"] as! [String]
            for user in subscribers{
                NetworkHelper.sharedInstance.arraySubscribers.append(user)
            }
            NetworkHelper.sharedInstance.saveSubscribers(NetworkHelper.sharedInstance.arraySubscribers)
            self.checkSubscribers()
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    class func SubscribersVC() -> Subscribers {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Subscribers") as! Subscribers
    }
    
    @objc func checkSubscribers()
    {
        //check no of subscribers
        if(NetworkHelper.sharedInstance.arraySubscribers.count==3)
        {
            
            removeSubscriberOneBtn.isHidden = false
            removeSubscriberTwoBtn.isHidden = false
            removeSubscriberThreeBtn.isHidden = false
            subscriberOne.isHidden = false
            subscriberTwo.isHidden = false
            subscriberThree.isHidden = false
            subscriberOne.text = NetworkHelper.sharedInstance.arraySubscribers[0]
            subscriberTwo.text = NetworkHelper.sharedInstance.arraySubscribers[1]
            subscriberThree.text = NetworkHelper.sharedInstance.arraySubscribers[2]
        }
        else if(NetworkHelper.sharedInstance.arraySubscribers.count==2)
        {
            removeSubscriberOneBtn.isHidden = false
            removeSubscriberTwoBtn.isHidden = false
            subscriberOne.isHidden = false
            subscriberTwo.isHidden = false
            subscriberOne.text = NetworkHelper.sharedInstance.arraySubscribers[0]
            subscriberTwo.text = NetworkHelper.sharedInstance.arraySubscribers[1]
            subscriberThree.isHidden = true
            removeSubscriberThreeBtn.isHidden = true
        }
        else if(NetworkHelper.sharedInstance.arraySubscribers.count==1)
        {
            removeSubscriberOneBtn.isHidden = false
            subscriberOne.isHidden = false
            subscriberOne.text = NetworkHelper.sharedInstance.arraySubscribers[0]
            subscriberTwo.isHidden = true
            subscriberThree.isHidden = true
            removeSubscriberThreeBtn.isHidden = true
            removeSubscriberTwoBtn.isHidden = true
        }
        else {
            subscriberOne.isHidden = true
            subscriberTwo.isHidden = true
            subscriberThree.isHidden = true
            removeSubscriberOneBtn.isHidden = true
            removeSubscriberTwoBtn.isHidden = true
            removeSubscriberThreeBtn.isHidden = true
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(checkSubscribers),
            name: NSNotification.Name(rawValue: "SubscribersNotification"),
            object: nil)
        
        self.navigationItem.title = "Subscribers"
        removeSubscriberOneBtn.setImage(UIImage(named: "cross"), for: UIControl.State())
        removeSubscriberTwoBtn.setImage(UIImage(named: "cross"), for: UIControl.State())
        removeSubscriberThreeBtn.setImage(UIImage(named: "cross"), for: UIControl.State())
        //check no of subscribers
        self.checkSubscribers()
        
    }
    
    
    @IBAction func invitePressed(_ sender: AnyObject) {
        //                let invite = Subscribe.SubscribeVC()
        //                self.setNavBar()
        //                self.navigationController?.pushViewController(invite, animated: true)
        print("show invitation dialog")
        showTextFieldAlert()
    }
    
    func showTextFieldAlert() {
        
        let alert = UIAlertView()
        alert.delegate = self
        alert.title = "Subscriber"
        alert.addButton(withTitle: "Subscribe")
        alert.alertViewStyle = UIAlertViewStyle.plainTextInput
        alert.addButton(withTitle: "Cancel")
        alert.show()
        
        self.textField = alert.textField(at: 0)
        self.textField!.placeholder = "Enter email address"
        
    }
    //MARK: - UIviewALert delegate
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 0{
            let subscriberEmail = self.textField.text!
            if(!DGlobals.validateEmail(subscriberEmail)){
                self.showAlertWithTitle("Validation Error", message: "Please enter valid email address.", sender: self)
            }else {
                addSubscriberService(inviteEmail: subscriberEmail)
            }
            
            print("subscriber email==\(subscriberEmail)")
            
        }else{
        }
    }
    
    func showAlertWithTitle(_ title:String,message:String,sender:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        sender.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK: - Add Subscriber
    
    
    func addSubscriberService(inviteEmail:String) {
        NetworkHelper.sharedInstance.error401 = false
        NetworkHelper.sharedInstance.error403 = false
        self.view.endEditing(true)
        //call service for adding subscriber
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let data : [String : String] = ["username": NetworkHelper.sharedInstance.username!,"subscriber_id": String(inviteEmail)]
        let params : [String : AnyObject] = ["app_id": NetworkHelper.sharedInstance.app_ID! as AnyObject, "request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "namespace":String(NetworkHelper.sharedInstance.getCurrentUserNamespace()) as AnyObject, "postkey":String(NetworkHelper.sharedInstance.getCurrentUserPostKey()) as AnyObject, "data":data as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.ADD_SUBSCRIBER, sendData: params, success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                //                    self.showAlert()
                //self.loginToUpdateSubscribers()
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    self.showAlert(message)
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                self.showAlert(message)
            } else {
                self.showAlert("Subscriber could not be added. Please try again. ")
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
    }
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        
        //        account_label_tabbar.textColor = UIColor(red: 0/255, green: 174/255, blue: 239/255, alpha: 1.0)
        //        var image: UIImage = UIImage(named: "tab_home_selected")!
        //        account_img_tabbar.image = image
        
        if(self.checkPausedTimeAsAndroid()){
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else {
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
      
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
}


