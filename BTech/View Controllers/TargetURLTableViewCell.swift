//
//  TargetURLTableViewCell.swift
//  BTech
//
//  Created by Ahmed Akhtar on 23/12/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class TargetURLTableViewCell: UITableViewCell {

    //IBOutlets
    @IBOutlet var urlField: UITextField!
    @IBAction func deleteURLPressed(_ sender: AnyObject) {
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
