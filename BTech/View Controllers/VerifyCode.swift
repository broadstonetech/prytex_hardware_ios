//
//  VerifyCode.swift
//  BTech
//
//  Created by Ahmed Akhtar on 10/01/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class VerifyCode: Parent, UITextFieldDelegate {
    
    
    var isresetSettings:Bool?
    
    //iboutlet
    
    @IBOutlet var codeField: UITextField!
    
    @IBOutlet var codeDescription: UILabel!
    
    @IBOutlet var backBtn: UIButton!
    let isResetDevice : Bool = false
    
    var codeDescriptionText = ""
    var titleText:String = ""
    
    class func verifyCodeVC() -> VerifyCode {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "verifyCode") as! VerifyCode
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        codeDescription.text = codeDescriptionText
        if(isresetSettings == true)
        {
            self.backBtn.isHidden = true
        }
        codeField.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.titleText = "Verify Code"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.navigationItem.title = titleText
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField === codeField)
        {
            codeField.resignFirstResponder()
            codeField.endEditing(true)
            return true
        }
        return true
    }
    
    @IBAction func backBtnPressed(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func submitPressed(_ sender: AnyObject) {
        
        if(isresetSettings == true)
        {
            self.callVerifyCodeServiceForResetSettings()
        }
        else{
            self.callVerifyCodeService()
        }
    }
    
    func callVerifyCodeServiceForResetSettings(){
        self.view.endEditing(true)
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let data:Dictionary<String,AnyObject>  = ["code": codeField.text! as AnyObject]
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "data": data as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.VERIFY_SETTING_CODE_API, sendData: params, success: { (data) in
            print(data)
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                if self.isresetSettings! {
                
                    DispatchQueue.main.async {
                        self.showAlert("Prytex restored to default settings.")
                    }
                    
                }else{
                }
                self.navigationController?.popViewController(animated: true)
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }  else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
            
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    
    
    func callVerifyCodeService(){
        self.view.endEditing(true)
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let data:Dictionary<String,AnyObject>  = ["username": NetworkHelper.sharedInstance.usernameSavedForVerifyCode! as AnyObject, "code": codeField.text! as AnyObject]
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "data": data as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.VERIFY_PASSWORD_CODE_API, sendData: params, success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                let token = data["token"] as! String
            if(token != nil)
            {
                NetworkHelper.sharedInstance.tokenSavedForSettingNewPassword = token
                //self.showAlert("User will be redirected to set new password page.")
                let changePasswordVC = ChangePassword.ChangePasswordVC()
                changePasswordVC.setNewPassword = true
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(changePasswordVC, animated: true)
                }
                
            }
                
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                    
                }
            } else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
        
    }
    
    
}
