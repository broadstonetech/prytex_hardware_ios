//
//  AlertViewCell.swift
//  BTech
//
//  Created by Talha Ejaz on 06/09/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class AlertViewCell: UITableViewCell {
    
    var alertType : String? = nil
    var modelTemp : AlertModel? = nil
    var checkedUnMute = false
    var checkedViewAlerts = false
    var blockUnBlockButtonPressed : ((UIButton) -> Void)?
    var muteUnMuteButtonPressed : ((UIButton) -> Void)?
    
    //OutLets
    @IBOutlet var separater: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var categoryOneLabel: UILabel!
    @IBOutlet weak var categoryOneIP: UILabel!
    @IBOutlet weak var categoryTwoLabel: UILabel!
    @IBOutlet weak var categoryTwoIP: UILabel!
    @IBOutlet weak var protocolLabel: UILabel!
    @IBOutlet weak var protocolSSH: UILabel!
    @IBOutlet weak var protocolLabelTwo: UILabel!
    @IBOutlet weak var protocolSSHTwo: UILabel!
    @IBOutlet weak var muteButton: UIButton!
    @IBOutlet weak var blockButton: UIButton!
    @IBOutlet weak var backgroundViewButtons: UIView!
    @IBOutlet weak var eve_sec: UILabel!
    @IBOutlet weak var seenView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func blockUnBlockBtnPressed(_ sender: UIButton) {
        //        if checkedUnMute {
        //            sender.setImage( UIImage(named:"unmute"), forState: .Normal)
        //            checkedUnMute = false
        //        } else {
        //            sender.setImage(UIImage(named:"blocked"), forState: .Normal)
        //            checkedUnMute = true
        //        }
        if checkedUnMute {
            sender.setImage( UIImage(named:"unmute_blue"), for: UIControl.State())
            checkedUnMute = false
        } else {
            sender.setImage(UIImage(named:"block_blue"), for: UIControl.State())
            checkedUnMute = true
        }
        if(self.blockUnBlockButtonPressed != nil) {
            self.blockUnBlockButtonPressed!(sender)
        }
    }
    
    
    func showPrompt(_ message:String, sender: UIButton)
    {
        let alert = UIAlertController(title: "dmoat", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            self.performBlockAction(sender)
        }))
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    
    
    func performBlockAction(_ sender: UIButton)
    {
        self.transferModelData(self.modelTemp!, indexModel:sender.tag, alertType: self.alertType!)
        if checkedViewAlerts {
            sender.setImage( UIImage(named:"ViewAlerts"), for: UIControl.State())
            checkedViewAlerts = false
        } else {
            sender.setImage(UIImage(named:"mute"), for: UIControl.State())
            checkedViewAlerts = true
        }
        
        if(self.muteUnMuteButtonPressed != nil) {
            self.muteUnMuteButtonPressed!(sender)
        }
        
    }
    
    
    @IBAction func muteUnMuteBtnPressed(_ sender: UIButton) {
        if(NetworkHelper.sharedInstance.promptUserAlert=="true")
        {
            showPrompt("Are you sure?", sender: sender)
        }
        else{
            self.performBlockAction(sender)
        }
    }
    
    
}
extension AlertViewCell : SavingModelDelegate{
    
    func transferModelData(_ model: AlertModel, indexModel: Int, alertType: String) {
        
        print("Data fetched")
        var type : String? = nil
        if(alertType=="blocked")
        {
            type="Unblock"
        }
        else if(alertType=="muted")
        {
            type="Unmute"
        }
        NetworkHelper.sharedInstance.error401 = false
        //call service for changing password
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let alertId = model.alert_id! as AnyObject
        let desc = model.desc! as AnyObject
        let destIp =  model.dest_ip! as AnyObject
        let destPort = model.dest_port! as AnyObject
        let srcIP = model.src_ip! as AnyObject
        let srcPort = model.src_port! as AnyObject
        let messag = model.msg! as AnyObject
        let nameSpace = model.namespace! as AnyObject
        let recordId = model.record_id! as AnyObject
        let inputSrc = model.input_src! as AnyObject
        
        let response : Dictionary <String, AnyObject> = ["interval": "" as AnyObject, "mode": "" as AnyObject, "type": type! as AnyObject]
        
        let dataVal : Dictionary <String, AnyObject> = ["alert_id" : alertId, "description" : desc, "dest_ip" : destIp, "dest_port" : destPort, "input_src": inputSrc, "message": messag, "namespace": nameSpace, "record_id": recordId, "src_ip": srcIP, "src_port": srcPort, "response": response as AnyObject]
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "data" : dataVal as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: "https://api.dmoat.com/response/blockedalert", sendData: params, success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                if(alertType=="blocked")
                {
                    self.showAlert("Unblocked")
                }
                else{
                    self.showAlert("Unmuted")
                }
                
                NetworkHelper.sharedInstance.arrayBlockedAlerts.remove(at: indexModel)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "AlertNotification"), object: nil);
            }
            else{
                if(alertType=="blocked")
                {
                    self.showAlert("Failed to Unblock")
                }
                else{
                    self.showAlert("Failed to Unmute")
                }
            }
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                loginVC.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
        
    }
    
    func showAlert(_ message:String)
    {
        
        let alert = UIAlertController(title: "dmoat", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
    
}


