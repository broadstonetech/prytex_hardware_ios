//
//  SensorAlertCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 16/01/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class SensorAlertCell: UITableViewCell {

    @IBOutlet weak var description_label: UILabel!

    @IBOutlet weak var sensorValueRange_label: UILabel!
    
    @IBOutlet weak var sensorValue_label: UILabel!
    @IBOutlet weak var sensorValueHeading_label: UILabel!
 
    @IBOutlet weak var seenView: UIView!
    @IBOutlet weak var timestamp_label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
