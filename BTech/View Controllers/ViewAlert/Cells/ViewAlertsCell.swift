//
//  ViewAlertsCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 20/03/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class ViewAlertsCell: UITableViewCell {

    //MARK: -views outlets
    @IBOutlet weak var view_a: UIView! //description
    @IBOutlet weak var view_b: UIView! //hostname/sensor value/ block ip // source port view
    @IBOutlet weak var view_c: UIView! //ip // good bad sensor value// dest port
    @IBOutlet weak var view_d: UIView! //category
    @IBOutlet weak var view_e: UIView! //os
    @IBOutlet weak var view_f: UIView! //time stamp, action dots ...
    @IBOutlet weak var seenView: UIView!
    
    
    //MARK: -labels outlets
    @IBOutlet weak var description_label_a: UILabel!
    @IBOutlet weak var label_heading_b: UILabel!
    @IBOutlet weak var label_value_b: UILabel!
    @IBOutlet weak var label_heading_c: UILabel!
    @IBOutlet weak var label_value_c: UILabel!
    @IBOutlet weak var label_heading_d: UILabel!
    @IBOutlet weak var label_value_d: UILabel!
    @IBOutlet weak var label_heading_e: UILabel!
    @IBOutlet weak var label_value_e: UILabel!
    @IBOutlet weak var timestamp_label: UILabel!
    @IBOutlet weak var actions_label: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
