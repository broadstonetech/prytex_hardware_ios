//
//  ViewAlerts.swift
//  BTech
//
//  Created by Talha Ejaz on 05/09/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved. hello
//

import UIKit
import DropDown
import UserNotifications
import FirebaseAnalytics
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}

protocol SavingModelDelegate {
    func transferModelData(_ model : AlertModel, indexModel: Int, alertType: String)->Void
}
class ViewAlerts: Parent ,UIActionSheetDelegate, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - CLASS VARIABLES
    var titleText:String = ""
    let dropDown = DropDown()
    var alertType:String = ""
    var clickIndex = -1
    var currentIndex = -1
    var selectedIndex = -1
    
    var delegate : SavingModelDelegate? = nil
    //    var arrayViewAlertsRead = [AlertModel]()
    let refreshControl = UIRefreshControl()
    let rightButtonBar = UIButton()
    var viewAlertsFilteredList: [BlockedMutedAlertsModel] = [BlockedMutedAlertsModel]()
    var blockedFilteredList: [BlockedMutedAlertsModel] = [BlockedMutedAlertsModel]()
    var mutedFilteredList: [BlockedMutedAlertsModel] = [BlockedMutedAlertsModel]()
    
    var isFirstViewAlertsCall : Bool = true;
    var isFirstBlockedAlertsCall : Bool = true;
    var isFirtMutedAlertsCall : Bool = true;
    var isFirsPreMutedAlertsCall : Bool = true;
    let defaults = UserDefaults.standard
    var remainingheight : Float = 400.0
    
    //MARK: - CLASS OUTLETS
    
    
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectionBtn: UIButton!
    @IBOutlet weak var selectionBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var dropDown_img: UIImageView!
    @IBOutlet weak var dropDown_img_height: NSLayoutConstraint!
    
    @IBOutlet weak var error_label: UILabel!
    
    // pre-muted/observed events outlets
    @IBOutlet weak var preMutedInnerView: UIView!
    
    @IBOutlet weak var snort_label: UILabel!
    @IBOutlet weak var btn_snort: UIButton!
    @IBOutlet weak var tableView_preMuted: UITableView!
    
    @IBOutlet weak var flow_label: UILabel!
    @IBOutlet weak var btn_flow: UIButton!
    @IBOutlet weak var snort_view_height: NSLayoutConstraint!
    
    @IBOutlet weak var table_view_snort_height: NSLayoutConstraint!
    @IBOutlet weak var flow_view_height: NSLayoutConstraint!
    
    @IBOutlet weak var table_view_flow_height: NSLayoutConstraint!
    
    @IBOutlet weak var table_view_flow: UITableView!
    
    //MARK: - NAv TabBar OUTLETS
    
    @IBOutlet weak var current_label: UILabel!
    @IBOutlet weak var current_btn: UIButton!
    @IBOutlet weak var blocked_label: UILabel!
    @IBOutlet weak var blocked_btn: UIButton!
    @IBOutlet weak var muted_label: UILabel!
    @IBOutlet weak var muted_btn: UIButton!
    @IBOutlet weak var pre_muted_label: UILabel!
    @IBOutlet weak var pre_muted_btn: UIButton!
    
    @IBOutlet weak var current_btn_tab_view: UIView!
    @IBOutlet weak var blocked_btn_tab_view: UIView!
    @IBOutlet weak var muted_btn_tab_view: UIView!
    @IBOutlet weak var premuted_btn_tab_view: UIView!
    
    class func viewAlerts() -> ViewAlerts{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewAlerts") as! ViewAlerts
    }
    
    func customizeDropDown(_ sender: AnyObject) {
        let appearance = DropDown.appearance()
        appearance.cellHeight = self.selectionBtn.bounds.size.height
       // appearance.backgroundColor = UIColor(named: "AllAlertsBg")
       // appearance.selectionBackgroundColor = CozyLoadingActivity.Settings.CLAppThemeColor
     
        if #available(iOS 13.0, *) {
            if self.navigationController?.overrideUserInterfaceStyle == .dark {
                appearance.selectionBackgroundColor = UIColor(named: "AllAlertsBg")!
                appearance.backgroundColor = UIColor(named: "AllAlertsBg")
                appearance.textColor = .white
            }else {
                appearance.selectionBackgroundColor = .white
                appearance.backgroundColor = .white
                appearance.textColor = .black
            }
        } else {
            // Fallback on earlier versions
        }
        appearance.separatorColor = UIColor.lightGray
        appearance.animationduration = 0.25
        appearance.textFont = UIFont(name: "Avenir-Book", size: 14)!
    }
    
    //MARK: -Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView_preMuted.delegate = self
        tableView_preMuted.dataSource = self
        if(self.titleText == "")
        {
            self.titleText = "Prytex Alerts"
        }
        refreshControl.addTarget(self, action: #selector(refreshAlerts), for: .valueChanged)
        if #available(iOS 10.0, *) {
            //            tableView.refreshControl = refreshControl
        } else {
            // Fallback on earlier versions
        }
        // add notification obeserver
        self.selectionBtn.setTitle("All Alerts", for: UIControl.State())
        
        //        NotificationCenter.default.addObserver(
        //            self,
        //            selector: #selector(reloadAlertTable),
        //            name: NSNotification.Name(rawValue: "AlertNotification"),
        //            object: nil)
        //
        self.navigationController!.navigationBar.topItem?.title = ""
        self.title = titleText
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.dropDown.direction = .bottom
        
        dropDown.anchorView = self.selectionBtn
        // Will set a custom with instead of anchor view width
        //        dropDown.width = 100
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        dropDown.bottomOffset = CGPoint(x: 0, y: self.selectionBtn.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        dropDown.dataSource = CozyLoadingActivity.DropDownOtions.viewAlertFilterValues
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index, item) in
            //self.blackView.hidden = true
            self.selectionBtn.setTitle(item, for: UIControl.State())
            //apply filter selected
            self.applyFilterWithItem(item)
            
        }
        self.customizeDropDown(dropDown as AnyObject)
        
        self.tableView_preMuted.rowHeight = UITableView.automaticDimension
        self.tableView_preMuted.sectionHeaderHeight = UITableView.automaticDimension
        tableView_preMuted.estimatedSectionHeaderHeight = 1000
        tableView_preMuted.estimatedRowHeight = 1000
        
        self.tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 1000
        
    }
  
    override func viewWillAppear(_ animated: Bool) {
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.removeAllPendingNotificationRequests() // To remove all pending notifications which are not delivered yet but scheduled.
            center.removeAllDeliveredNotifications() // To remove all delivered notifications
        } else {
            UIApplication.shared.cancelAllLocalNotifications()
        }
        
        if(NetworkHelper.sharedInstance.deviceToken != nil)
        {
            (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
        }
        if(self.titleText == "")
        {
            self.titleText = "Prytex Alerts"
        }
        self.navigationController!.navigationBar.topItem?.title = ""
        self.navigationItem.title = titleText
        
        createRightButtonNavigationBar()
        setNavBarTitleColors(isCurrent : true, isBlocked : false, isMuted : false, isPreMuted : false)
        alertType = "view_alerts"
        self.selectionBtnHeight.constant = 44
        self.dropDown_img_height.constant = 15
        self.selectionBtn.setTitle("All Alerts", for: UIControl.State())
        if isFirstViewAlertsCall {
            loadAlertsData(ApiUrl.GET_ALL_VIEW_ALERTS_API, viewAlerts: true, blocked: false, muted: false, preMuted: false)
        }else{
            //add filterd list
        }
        
        self.current_label.text = "New Alerts" + " (\(String(self.viewAlertsFilteredList.count)))"
        self.reloadAlertTable()
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
            let screenClass = classForCoder.description()
            //Analytics.setScreenName("Alerts View", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Alerts View", AnalyticsParameterScreenClass: screenClass])
        
        let height = self.preMutedInnerView.frame.size.height
        print("premutedHeightAppear=== \(height)")
        
        snort_view_height.constant = 40
        flow_view_height.constant = 40
        remainingheight = Float(height - 100)
        print("premutedremainingheightAppear=== \(remainingheight)")
        table_view_snort_height.constant = CGFloat(remainingheight)
    }
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    func createRightButtonNavigationBar()
    {
        
        rightButtonBar.setImage(UIImage(named: "info"), for: UIControl.State())
        rightButtonBar.frame = CGRect(x: 0, y: 0, width: 48, height: 48)
        rightButtonBar.addTarget(self, action: #selector(topRightButtonBar), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = rightButtonBar
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        //create custom
    }
    
    @objc func topRightButtonBar()
    {
        self.showAlert(ConstantStrings.view_alerts_screen_txt)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(NetworkHelper.sharedInstance.deviceToken != nil)
        {
            (UIApplication.shared.delegate as! AppDelegate).UnmuteNotifications()
        }
        //NetworkHelper.sharedInstance.pushNotification = true
        
    }
    
    // MARK: - Actions
    
    @IBAction func Screen_info_pressed(_ sender: Any) {
    }
    
    
    @objc func refreshAlerts() {
        print("refreshing!")
        if alertType == "view_alerts" {
            
            alertType = "view_alerts"
            error_label.isHidden = true
            self.selectionBtn.setTitle("All Alerts", for: UIControl.State())
            
            loadAlertsData(ApiUrl.GET_ALL_VIEW_ALERTS_API, viewAlerts: true, blocked: false, muted: false, preMuted: false)
            self.refreshControl.endRefreshing()
            
            self.reloadAlertTable()
            
        }else if alertType == "blocked" {
            
            alertType = "blocked"
            error_label.isHidden = true
            self.selectionBtn.setTitle("All Alerts", for: UIControl.State())
            
            
            NetworkHelper.sharedInstance.blockedAlertsList.removeAll()
            loadAlertsData(ApiUrl.GET_BLOCKED_ALERTS_API, viewAlerts: false, blocked: true, muted: false, preMuted: false)
            filterBlockedMutedAlertsForUser()
            self.refreshControl.endRefreshing()
            
            self.reloadAlertTable()
            
        }
        else if alertType == "pre_muted"{
            
            alertType = "pre_muted"
            error_label.isHidden = true
            
            NetworkHelper.sharedInstance.preMutedAlertsList.removeAll()
            NetworkHelper.sharedInstance.preMutedFlowList.removeAll()
            
            loadAlertsData(ApiUrl.GET_PRE_MUTED_ALERTS_API, viewAlerts: false, blocked: false, muted: false, preMuted: true)
            
            self.refreshControl.endRefreshing()
            
            self.reloadAlertTable()
            
        }
        
    }
    
    // MARK: - Nav TabBar Controls
    
    @IBAction func current_btn_pressed(_ sender: Any) {
            let screenClass = classForCoder.description()
        //    Analytics.setScreenName("Alerts View", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Alerts View", AnalyticsParameterScreenClass: screenClass])
        
        print("cuurent presed")
        setNavBarTitleColors(isCurrent : true, isBlocked : false, isMuted : false, isPreMuted : false)
        alertType = "view_alerts"
        error_label.isHidden = true
        self.selectionBtnHeight.constant = 44
        self.dropDown_img_height.constant = 15
        self.selectionBtn.setTitle("All Alerts", for: UIControl.State())
        if isFirstViewAlertsCall {
            loadAlertsData(ApiUrl.GET_ALL_VIEW_ALERTS_API, viewAlerts: true, blocked: false, muted: false, preMuted: false)
        }else{
            //add filterd list
        }
        
        self.current_label.text = "New Alerts" + " (\(String(self.viewAlertsFilteredList.count)))"
        self.reloadAlertTable()
        
        self.tableView.isHidden = false
        self.preMutedInnerView.isHidden = true
    }
    
    @IBAction func blocked_btn_pressed(_ sender: Any) {
            let screenClass = classForCoder.description()
          //  Analytics.setScreenName("Blocked ALerts", screenClass: screenClass)
        
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Blocked Alerts", AnalyticsParameterScreenClass: screenClass])
        
        print("blocked pressed")
        setNavBarTitleColors(isCurrent : false, isBlocked : true, isMuted : false, isPreMuted : false)
        alertType = "blocked"
        error_label.isHidden = true
        self.selectionBtnHeight.constant = 44
        self.dropDown_img_height.constant = 15
        self.selectionBtn.setTitle("All Alerts", for: UIControl.State())
        if isFirstBlockedAlertsCall {
            NetworkHelper.sharedInstance.blockedAlertsList.removeAll()
            loadAlertsData(ApiUrl.GET_BLOCKED_ALERTS_API, viewAlerts: false, blocked: true, muted: false, preMuted: false)
            filterBlockedMutedAlertsForUser()
            
        }else{
            filterBlockedMutedAlertsForUser()
        }
        DispatchQueue.main.async {
            
            self.blocked_label.text = "Blocked Events" + " (\(String(self.blockedFilteredList.count)))"
        }
        self.reloadAlertTable()
        
        self.tableView.isHidden = false
        self.preMutedInnerView.isHidden = true
    }
    
    @IBAction func muted_btn_pressed(_ sender: Any) {
        //        print("muted pressed")
    }
    
    @IBAction func pre_muted_btn_pressed(_ sender: Any) {
        print("pre muted pressed")
            let screenClass = classForCoder.description()
           // Analytics.setScreenName("Muted Alerts", screenClass: screenClass)
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: "Muted Alerts", AnalyticsParameterScreenClass: screenClass])
        
        setNavBarTitleColors(isCurrent : false, isBlocked : false, isMuted : false, isPreMuted : true)
        alertType = "pre_muted"
        error_label.isHidden = true
        self.selectionBtnHeight.constant = 0
        self.dropDown_img_height.constant = 0
        self.selectionBtn.setTitle("", for: UIControl.State())
        if isFirsPreMutedAlertsCall {
            NetworkHelper.sharedInstance.preMutedAlertsList.removeAll()
            NetworkHelper.sharedInstance.preMutedFlowList.removeAll()
            
            loadAlertsData(ApiUrl.GET_PRE_MUTED_ALERTS_API, viewAlerts: false, blocked: false, muted: false, preMuted: true)
        }else{
            
        }
        let observedCount = NetworkHelper.sharedInstance.preMutedAlertsList.count + NetworkHelper.sharedInstance.preMutedFlowList.count
                   self.pre_muted_label.text = "Observed Events" + " (\(String(observedCount)))"
        
        self.refreshControl.endRefreshing()
        self.tableView.isHidden = true
        self.preMutedInnerView.isHidden = false
        self.reloadAlertTable()
    }
    
    // MARK: - Filter Alerts Actions
    
    func applyFilterWithItem(_ item : String)
    {
        if alertType == "view_alerts" {
            DispatchQueue.main.async {
                self.filterViewAlerts(item: item)
            }
        }
            //blocked muted
        else if alertType == "blocked"{
            self.filterBlockedAlerts(item: item)
        }
        
    }
    
    func filterViewAlerts (item : String){
        
        var filterItem = "All"
        if item == "New Host" {
            filterItem = "cnctdhost"
        }
        else if item == "Prytex Info" {
            filterItem = "info"
        }else if item == "Blocked IP Addresses"{
            filterItem = "blockips"
        }else if item == "Network Alert"{
            filterItem = "ids"
        }else if item == "Flow Alert"{
            filterItem = "flow"
        }else if item == "Sensor"{
            filterItem = "sensor"
        }else{}
        
        let alertsCount = NetworkHelper.sharedInstance.viewAlertsList.count
        if filterItem == "All"{
            self.viewAlertsFilteredList.removeAll()
            for i in 0 ..< alertsCount {
                let id = NetworkHelper.sharedInstance.viewAlertsList[i].getNameSpace()
                print("item== \(filterItem) inputSrc == all" )
                self.viewAlertsFilteredList.append(NetworkHelper.sharedInstance.viewAlertsList[i])
            }
            
            DispatchQueue.main.async {
                self.reloadAlertTable()
            }
        }else{
            self.viewAlertsFilteredList.removeAll()
            for i in 0 ..< alertsCount {
                let inputSrc = NetworkHelper.sharedInstance.viewAlertsList[i].getInputSrc()
                print("item== \(filterItem) inputSrc == \(inputSrc)" )
                if  inputSrc == filterItem{
                    self.viewAlertsFilteredList.append(NetworkHelper.sharedInstance.viewAlertsList[i])
                }
            }
            
            DispatchQueue.main.async {
                self.reloadAlertTable()
            }
        }
    }
    
    func filterBlockedAlerts(item : String) {
        
        var filterItem = "All"
        if item == "New Host" {
            filterItem = "cnctdhost"
        }
        else if item == "Prytex Info" {
            filterItem = "info"
        }else if item == "Blocked IP Addresses"{
            filterItem = "blockips"
        }else if item == "Network Alert"{
            filterItem = "ids"
        }else if item == "Flow Alert"{
            filterItem = "flow"
        }else if item == "Sensor"{
            filterItem = "sensor"
        }else{}
        
        
        let alertsCount = NetworkHelper.sharedInstance.blockedAlertsList.count
        let userNameSpace = NetworkHelper.sharedInstance.getCurrentUserNamespace()
        if filterItem == "All"{
            self.blockedFilteredList.removeAll()
            for i in 0 ..< alertsCount {
                let id = NetworkHelper.sharedInstance.blockedAlertsList[i].getNameSpace()
                let inputSrc = NetworkHelper.sharedInstance.blockedAlertsList[i].getInputSrc()
                print("item== \(filterItem) inputSrc == for all" )
                if id == userNameSpace{
                    self.blockedFilteredList.append(NetworkHelper.sharedInstance.blockedAlertsList[i])
                }
            }
            
            DispatchQueue.main.async {
                self.reloadAlertTable()
            }
        }else{
            self.blockedFilteredList.removeAll()
            for i in 0 ..< alertsCount {
                let id = NetworkHelper.sharedInstance.blockedAlertsList[i].getNameSpace()
                let inputSrc = NetworkHelper.sharedInstance.blockedAlertsList[i].getInputSrc()
                print("item== \(filterItem) inputSrc == \(inputSrc)" )
                if id == userNameSpace && inputSrc == filterItem{
                    self.blockedFilteredList.append(NetworkHelper.sharedInstance.blockedAlertsList[i])
                }
            }
            
            DispatchQueue.main.async {
                self.reloadAlertTable()
            }
        }
    }
    
    func filterMutedAlerts(item : String) {
        
        var filterItem = "All"
        if item == "New Host" {
            filterItem = "cnctdhost"
        }
        else if item == "Prytex Info" {
            filterItem = "info"
        }else if item == "Blocked IP Addresses"{
            filterItem = "blockips"
        }else if item == "Network Alert"{
            filterItem = "ids"
        }else if item == "Flow Alert"{
            filterItem = "flow"
        }else if item == "Sensor"{
            filterItem = "sensor"
        }else{}
        
        
        let alertsCount = NetworkHelper.sharedInstance.MutedAlertsList.count
        let userNameSpace = NetworkHelper.sharedInstance.getCurrentUserNamespace()
        if filterItem == "All"{
            self.mutedFilteredList.removeAll()
            for i in 0 ..< alertsCount {
                let id = NetworkHelper.sharedInstance.MutedAlertsList[i].getNameSpace()
                let inputSrc = NetworkHelper.sharedInstance.MutedAlertsList[i].getInputSrc()
                print("item== \(filterItem) inputSrc == for all" )
                if id == userNameSpace{
                    self.mutedFilteredList.append(NetworkHelper.sharedInstance.MutedAlertsList[i])
                }
            }
            
            DispatchQueue.main.async {
                self.reloadAlertTable()
            }
        }else{
            self.mutedFilteredList.removeAll()
            for i in 0 ..< alertsCount {
                let id = NetworkHelper.sharedInstance.MutedAlertsList[i].getNameSpace()
                let inputSrc = NetworkHelper.sharedInstance.MutedAlertsList[i].getInputSrc()
                print("item== \(filterItem) inputSrc == \(inputSrc)" )
                if id == userNameSpace && inputSrc == filterItem{
                    self.mutedFilteredList.append(NetworkHelper.sharedInstance.MutedAlertsList[i])
                }
            }
            
            DispatchQueue.main.async {
                self.reloadAlertTable()
            }
        }
    }
    func reloadAlertTable()
    {
        self.tableView.reloadData()
        self.tableView_preMuted.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func dropDownBtnClicked(_ sender: UIButton) {
        
        //self.blackView.hidden = !self.blackView.hidden here
        if alertType == "view_alerts" {
            dropDown.dataSource = CozyLoadingActivity.DropDownOtions.viewAlertFilterValues
        }else if alertType == "blocked" {
            dropDown.dataSource = CozyLoadingActivity.DropDownOtions.blockedFilterValues
        } else {
            dropDown.dataSource = CozyLoadingActivity.DropDownOtions.viewAlertFilterValues
        }
        
        self.selectionBtn.setTitle("Choose Alert Type", for: UIControl.State())
        self.dropDown.show()
    }
    
    // MARK: - Alerts Action sheet
    
    func updateAckCall(model : BlockedMutedAlertsModel, position: Int) {
        
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let type = model.getInfoType()
        var url = ""
        if type == "app" {
            url = ApiUrl.UPDATE_APP_ALERTS_ACK_API
        }else if type == "firmware" {
            url = ApiUrl.UPDATE_FIRMWARE_ALERTS_ACK_API
        }
        
        //call service
        let response : Dictionary <String, AnyObject> =  ["type": type as AnyObject]
        var data : Dictionary <String, AnyObject> = [:]
        let inputSrc = model.getInputSrc()
        let recordId = model.getRecordId() as AnyObject
    
            data = ["record_id": recordId, "response": response as AnyObject]
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "data" : data as AnyObject]
        
        
        NetworkHelper.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                //self.showAlert("Success")
                //now remove that record from database
                print("device unblocked\(model.getHostName())")
                if self.alertType == "view_alerts" {
                    
                    if let index = NetworkHelper.sharedInstance.viewAlertsList.firstIndex(of: model) {
                        NetworkHelper.sharedInstance.viewAlertsList.remove(at: index)
                    }
                    if model.getInfoType() == "app"{
                        DispatchQueue.main.async {
                        UIApplication.shared.openURL(NSURL(string: ApiUrl.APPSTORE_DMOAT_URL)! as URL)
                    }
                    }else {
                        self.showAlert("Request forwarded to Prytex.")
                    }
                    
                } else{}
                //                self.tableView.reloadData()
                DispatchQueue.main.async {
                    self.applyFilterWithItem((self.selectionBtn.titleLabel?.text)!)
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    self.showAlert(message)
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            } else if ((statusCode?.range(of: "400")) != nil){
                let message = data["message"] as! String
                if (message.range(of: "Alert does not exist") != nil){
                    //now remove that record from database
                    
                        self.viewAlertsFilteredList.remove(at: position)
                        let alertId = model.getalertId()
                        NetworkHelper.sharedInstance.viewAlertsList.remove(at: alertId)
                   
                    //self.tableView.reloadData()
                    self.applyFilterWithItem((self.selectionBtn.titleLabel?.text)!)
                }else{
                    self.showAlert(message)
                }
            }
            else{
                self.showAlert(ConstantStrings.ErrorText)
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    func sendBlockSheetResponse(_ model:BlockedMutedAlertsModel, mode : String, type : String, interval : String, position : Int)
    {
        print("blocked selected sheetresponse\(model) and position \(position)")
        
        NetworkHelper.sharedInstance.error401 = false
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        var url = ""
        if alertType == "view_alerts" {
            url = ApiUrl.VIEW_ALERTS_RESPONSE_API + model.getInputSrc()
        }else if alertType == "blocked" {
            url = ApiUrl.RESPONSE_BLOCKED_ALERT_API
        }
        //call service
        let response : Dictionary <String, AnyObject> = ["interval": interval as AnyObject, "mode": mode as AnyObject, "type": type as AnyObject]
        var data : Dictionary <String, AnyObject> = [:]
        let inputSrc = model.getInputSrc()
        let recordId = model.getRecordId() as AnyObject
        
        if model.getInputSrc() == "flow"{
            data = ["record_ids_list": model.arrayFlowRecordIdsList as AnyObject, "response": response as AnyObject]
        }else{
            data = ["record_id": recordId, "response": response as AnyObject]
        }
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "data" : data as AnyObject]
        
        
        NetworkHelper.sharedInstance.postRequest(serviceName: url, sendData: params, success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                //self.showAlert("Success")
                //now remove that record from database
                print("device unblocked\(model.getHostName())")
                if self.alertType == "view_alerts" {
                    
                    if let index = NetworkHelper.sharedInstance.viewAlertsList.firstIndex(of: model) {
                        NetworkHelper.sharedInstance.viewAlertsList.remove(at: index)
                    }
                    
                } else if self.alertType == "blocked" {
                    
                    if let index = NetworkHelper.sharedInstance.blockedAlertsList.firstIndex(of: model) {
                        NetworkHelper.sharedInstance.blockedAlertsList.remove(at: index)
                }
                }else{}
                //                self.tableView.reloadData()
                DispatchQueue.main.async {
                    self.applyFilterWithItem((self.selectionBtn.titleLabel?.text)!)
                }
            }
            else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    self.showAlert(message)
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            } else if ((statusCode?.range(of: "400")) != nil){
                let message = data["message"] as! String
                if (message.range(of: "Alert does not exist") != nil){
                    //now remove that record from database
                    
                    if self.alertType == "view_alerts"{
                        self.viewAlertsFilteredList.remove(at: position)
                        let alertId = model.getalertId()
                        NetworkHelper.sharedInstance.viewAlertsList.remove(at: alertId)
                    }else if self.alertType == "blocked" {
                        self.blockedFilteredList.remove(at: position)
                        let alertId = model.getalertId()
                        NetworkHelper.sharedInstance.blockedAlertsList.remove(at: alertId)
                    }
                    //self.tableView.reloadData()
                    self.applyFilterWithItem((self.selectionBtn.titleLabel?.text)!)
                }else{
                    self.showAlert(message)
                }
            }
            else{
                self.showAlert(ConstantStrings.ErrorText)
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    
    func showBlockSheet(_ model:BlockedMutedAlertsModel, position : Int) -> Void {
        
        
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        
        let allowAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.Allow.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.Allow.rawValue)
            self.sendBlockSheetResponse(model, mode: "", type: "Allow", interval: "", position : position)
            
        })
        
        let trustOneHourAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.TrustOneHour.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.Allow.rawValue)
            self.sendBlockSheetResponse(model, mode: "", type: "Allow", interval: "", position: position)
            
            
        })
        let trustDeviceAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.TrustDevice.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.Allow.rawValue)
            self.sendBlockSheetResponse(model, mode: "Forever", type: "Allow", interval: "", position: position)
            
            
        })
        let allowForeverAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.Allow.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.Allow.rawValue)
            self.sendBlockSheetResponse(model, mode: "Forever", type: "Allow", interval: "", position: position)
            
        })
        let blockForOneMinAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.BlockForOneMint.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.BlockForOneMint.rawValue)
            self.sendBlockSheetResponse(model, mode: "Interval", type: "Block", interval: "900", position: position)
        })
        
        let blockForeverAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.BlockForever.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.BlockForever.rawValue)
            self.sendBlockSheetResponse(model, mode: "Forever", type: "Block", interval: "", position: position)
        })
        
        let muteAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.Mute.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.Mute.rawValue)
            self.sendBlockSheetResponse(model, mode: "", type: "Mute", interval: "", position: position)
        })
        
        let unblockAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.UnBlock.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.UnBlock.rawValue)
            self.sendBlockSheetResponse(model, mode: "", type: "Unblock", interval: "", position: position)
        })
        
        let unmuteAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.Unmute.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.Unmute.rawValue)
            self.sendBlockSheetResponse(model, mode: "", type: "Unmute", interval: "", position: position)
        })
        
        
        let keepBlockAction = UIAlertAction(title: CozyLoadingActivity.BlockOptions.KeepBlock.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.KeepBlock.rawValue)
            self.sendBlockSheetResponse(model, mode: "", type: "Keepblock", interval: "", position: position)
        })
        
        let updateNow = UIAlertAction(title: CozyLoadingActivity.BlockOptions.UpdateNow.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.KeepBlock.rawValue)
            self.updateAckCall(model: model, position: position)
        })
        
        let updateLater = UIAlertAction(title: CozyLoadingActivity.BlockOptions.UpdateLater.rawValue, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print(CozyLoadingActivity.BlockOptions.KeepBlock.rawValue)
        })
        
        let cancelAction = UIAlertAction(title:CozyLoadingActivity.BlockOptions.Cancel.rawValue, style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        
        //check if input source is connected host
        let inputSrc = model.getInputSrc()

        if alertType == "view_alerts"{
            if(inputSrc=="cnctdhost")
            {
                
                if (model.getDeviceCategory() == "Router") {
                    return
                }else{
                    
                    optionMenu.addAction(allowAction)
                    //                    optionMenu.addAction(muteAction)
                    optionMenu.addAction(blockForeverAction)
                    optionMenu.addAction(blockForOneMinAction)
                    optionMenu.addAction(cancelAction)
                    
                }
            }
            else if(inputSrc=="ids")
            {
                
                if(model.getIdsBlockType()=="post")
                {
                    optionMenu.addAction(allowAction)
                    optionMenu.addAction(blockForeverAction)
                    optionMenu.addAction(blockForOneMinAction)
                    optionMenu.addAction(cancelAction)
                }
                    
                else if(model.getIdsBlockType()=="pre")
                {
                    optionMenu.addAction(unblockAction)
                    //                    optionMenu.addAction(muteAction)
                    optionMenu.addAction(keepBlockAction)
                    optionMenu.addAction(blockForOneMinAction)
                    optionMenu.addAction(cancelAction)
                }else{
                    
                    optionMenu.addAction(cancelAction)
                }
            }
            else if(inputSrc=="blockips"){
                
                optionMenu.addAction(allowAction)
                optionMenu.addAction(keepBlockAction)
                optionMenu.addAction(cancelAction)
            }
            else if inputSrc == "update" {
                let type = model.getInfoType()
            if type == "app" {
                
                optionMenu.addAction(updateNow)
                optionMenu.addAction(updateLater)
                optionMenu.addAction(cancelAction)
                
            } else if type == "firmware" {
                
                optionMenu.addAction(updateNow)
                optionMenu.addAction(updateLater)
                optionMenu.addAction(cancelAction)
                
            }
        }
        }
        else if alertType == "blocked" {
            
            optionMenu.addAction(allowAction)
            optionMenu.addAction(unblockAction)
            optionMenu.addAction(cancelAction)
            
        }
            
       
        else {
            return
        }
        
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func showIDSAlert(_ alertCell:ViewAlertsCell, alertModel:BlockedMutedAlertsModel)
    {
        //Convert to Date jan-dd-yyyy
        let dates = newDateFormat(alertModel.getEveSec())
        
        alertCell.description_label_a.text = alertModel.getDescription()
        alertCell.label_heading_b.text = "Source:"
        alertCell.label_value_b.text = alertModel.getSrcHostName()
        alertCell.label_heading_c.text = "Destination:"
        alertCell.label_value_c.text = alertModel.getDestIp()
        alertCell.timestamp_label.text = dates
        
        alertCell.view_a.isHidden = false
        alertCell.view_b.isHidden = false
        alertCell.view_c.isHidden = false
        
        alertCell.actions_label.isHidden = false
        alertCell.timestamp_label.isHidden = false
        
        alertCell.view_d.isHidden = true
        alertCell.view_e.isHidden = true
        
    }
    
    func showFlowAlert(_ alertCell:ViewAlertsCell, alertModel:BlockedMutedAlertsModel)
    {
        alertCell.view_a.isHidden = false
        var descriptionList = ""
        for element in alertModel.arrayFlowAlerts  {
            descriptionList += element["description"] as! String + ".\n";
        }
        
        var slotId = alertModel.getSlotId()
        
        let hour = slotId % 24;
        let day = slotId / 24;
        let dayTitle = getDayFromSlot(slot : day);
        let hourTitle = gethourFromSlot(slot : hour);
        let startingHour = getDayHourTimeStamp(alertModel.getEveSec(), hour: true, day: false, format: false)
        let alertDay = getDayHourTimeStamp(alertModel.getEveSec(), hour: false, day: true, format: false)
        var timeFormatEnd = getDayHourTimeStamp(alertModel.getEveSec(), hour: false, day: false, format: true)
        var timeformatStart = ""
        var endingHour:Int! = Int(startingHour)
        //        let a:Int?
        endingHour = endingHour + 1
        if endingHour == 0 {
            endingHour = 1
        }
        else if endingHour == 13 {
            endingHour = 1
        }
        if (timeFormatEnd.range(of: "AM") != nil) {
            timeFormatEnd = "AM"
        }else{
            timeFormatEnd = "PM"
        }
        
        alertCell.description_label_a.text = descriptionList
        alertCell.timestamp_label.text = "\(alertDay) between \(String(endingHour))-\(startingHour)\(timeFormatEnd)"
        
        alertCell.actions_label.isHidden = false
        alertCell.timestamp_label.isHidden = false
        alertCell.view_b.isHidden = true
        alertCell.view_c.isHidden = true
        alertCell.view_d.isHidden = true
        alertCell.view_e.isHidden = true
        
    }
    
    func getDayFromSlot(slot : Int) -> String {
        if (slot == 0){
            return "Monday";
        }else if (slot == 1){
            return "Tuesday";
        }else if (slot == 2){
            return "Wednesday";
        }else if (slot == 3){
            return "Thursday";
        }else if (slot == 4){
            return "Friday";
        }else if (slot == 5){
            return "Saturday";
        }else if (slot == 6){
            return "Sunday";
        }
        return "";
    }
    
    func gethourFromSlot(slot : Int) -> String {
        if (slot == 0){
            return "12am-1am";
        }else if (slot == 1){
            return "1am-2am";
        }else if (slot == 2){
            return "2am-3am";
        }else if (slot == 3){
            return "3am-4am";
        }else if (slot == 4){
            return "4am-5am";
        }else if (slot == 5){
            return "5am-6am";
        }else if (slot == 6){
            return "6am-7am";
        }else if (slot == 7){
            return "7am-8am";
        }else if (slot == 8){
            return "8am-9am";
        }else if (slot == 9){
            return "9am-10am";
        }else if (slot == 10){
            return "10am-11am";
        }else if (slot == 11){
            return "11am-12Pm";
        }else if (slot == 12){
            return "12Pm-1Pm";
        }else if (slot == 13){
            return "1Pm-2Pm";
        }else if (slot == 14){
            return "2Pm-3Pm";
        }else if (slot == 15){
            return "3Pm-4Pm";
        }else if (slot == 16){
            return "4Pm-5Pm";
        }else if (slot == 17){
            return "5Pm-6Pm";
        }else if (slot == 18){
            return "6Pm-7Pm";
        }else if (slot == 19){
            return "7Pm-8Pm";
        }else if (slot == 20){
            return "8Pm-9Pm";
        }else if (slot == 21){
            return "9Pm-10Pm";
        }else if (slot == 22){
            return "10Pm-11Pm";
        }else if (slot == 23){
            return "11Pm-12Am";
        }
        return "";
    }
    
    func showConnectedHostViewAlert(_ alertCell:ViewAlertsCell, alertModel:BlockedMutedAlertsModel)
    {
        
        print("cnctd host alert")
        
        let dates = newDateFormat(alertModel.getEveSec())
        
        alertCell.description_label_a.text = alertModel.getDescription()
        
        alertCell.label_heading_b.text = "Hostname:"
        alertCell.label_value_b.text = alertModel.getHostName()
        
        alertCell.label_heading_c.text = "IP:"
        alertCell.label_value_c.text = alertModel.getIP()
        
        alertCell.label_heading_d.text = "Operating System:"
        alertCell.label_value_d.text = alertModel.getOS()
        
        if alertModel.getDeviceCategory() == "Unknown" {
            alertCell.view_e.isHidden = true
        }else{
            alertCell.view_e.isHidden = false
            alertCell.label_heading_e.text = "Category:"
            alertCell.label_value_e.text=alertModel.getDeviceCategory()
        }
        
        alertCell.timestamp_label.text = dates
        
        alertCell.actions_label.isHidden = false
        alertCell.timestamp_label.isHidden = false
        
        alertCell.view_a.isHidden = false
        alertCell.view_b.isHidden = true
        alertCell.view_c.isHidden = false
        alertCell.label_heading_c.isHidden = false
        alertCell.view_d.isHidden = false
        alertCell.view_f.isHidden = false
        
    }
    
    
    
    func mapSensorAlert(_ alertCell:ViewAlertsCell, alert:BlockedMutedAlertsModel)
    {
        let dates = newDateFormat(alert.getEveSec())
        
        alertCell.description_label_a.text = alert.getDescription()
        alertCell.label_heading_b.text = "Sensor value:"
        if alert.getCategory() == "temperature" {
            alertCell.label_value_b.text = String(alert.getSensorReading()) + "°C"
            alertCell.label_value_c.text = "Good temperature value is: 5-31°C."
        }else if alert.getCategory() == "humidity"{
            alertCell.label_value_b.text = String(alert.getSensorReading()) + "%"
            alertCell.label_value_c.text = "Good humidity value is: 0-60%."
        }else{
            alertCell.label_value_b.text = String(alert.getSensorReading()) + "CO2PPM"
            alertCell.label_value_c.text = "Good air quality value is: 0-450 CO2PPM."
        }
        
        alertCell.timestamp_label.text = dates
        
        alertCell.view_a.isHidden = false
        alertCell.view_b.isHidden = false
        alertCell.view_c.isHidden = false
        
        alertCell.label_heading_c.isHidden = true
        alertCell.view_d.isHidden = true
        alertCell.view_e.isHidden  = true
        alertCell.actions_label.isHidden = true
    }
    
    
    func mapBlockIpsAlert(_ alertCell:ViewAlertsCell, alert:BlockedMutedAlertsModel)
    {
        let dates = newDateFormat(alert.getEveSec())
        
        alertCell.description_label_a.text = alert.getDescription()
        //        alertCell.ipLabel_heading.text = "Reading:"
        
        alertCell.timestamp_label.text = dates
        
        alertCell.actions_label.isHidden = false
        alertCell.timestamp_label.isHidden = false
        
        alertCell.view_a.isHidden = false
        alertCell.view_b.isHidden = true
        alertCell.view_c.isHidden = true
        alertCell.view_d.isHidden = true
        alertCell.view_e.isHidden  = true
        alertCell.actions_label.isHidden = true
    }
    
    func mapInfoAlert(_ alertCell:ViewAlertsCell, alert:BlockedMutedAlertsModel)
    {
        let dates = newDateFormat(alert.getEveSec())
        
        if alert.getInfoType() == "internet_restarted"{
            
            let timeON = alert.getTimeOn()
            let timeOFF = alert.getTimeOff()
            let timeONstring = newDateFormat(timeON)
            let timeOFFstring = newDateFormat(timeOFF)
            
            alertCell.description_label_a.text = "Internet access was unavailable from \(timeOFFstring) to \(timeONstring)."
        }else {
            alertCell.description_label_a.text = alert.getDescription()
        }
        
        alertCell.timestamp_label.text = dates
        
        alertCell.view_a.isHidden = false
        alertCell.view_b.isHidden = true
        alertCell.view_c.isHidden = true
        alertCell.view_d.isHidden = true
        alertCell.view_e.isHidden  = true
        alertCell.actions_label.isHidden = true
    }
    
    func mapUpdateAlert(_ alertCell:ViewAlertsCell, alert:BlockedMutedAlertsModel)
    {
        let dates = newDateFormat(alert.getEveSec())
        
      
            alertCell.description_label_a.text = alert.getDescription()
        
        alertCell.timestamp_label.text = dates
        
        alertCell.view_a.isHidden = false
        alertCell.view_b.isHidden = true
        alertCell.view_c.isHidden = true
        alertCell.view_d.isHidden = true
        alertCell.view_e.isHidden  = true
        alertCell.actions_label.isHidden = true
    }
    
    //MARK: -Tableview delegates
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.tableView {
            return 1
        } else if tableView == self.tableView_preMuted {
            return NetworkHelper.sharedInstance.preMutedAlertsList.count
        }else if tableView == self.table_view_flow {
            return NetworkHelper.sharedInstance.preMutedFlowList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  tableView == self.tableView {
            
            if alertType == "view_alerts" {
                return viewAlertsFilteredList.count
            }else if alertType == "blocked"{
                let rowCount = blockedFilteredList.count
                return rowCount
            }
            else if alertType == "pre_muted" {
                return NetworkHelper.sharedInstance.preMutedAlertsList.count
            }
            return 0
        }else if tableView == self.tableView_preMuted {
            if self.selectedIndex >= 0 && section == self.selectedIndex {
                return NetworkHelper.sharedInstance.preMutedAlertsList[section].arraySets.count
            }
            //            return NetworkHelper.sharedInstance.preMutedAlertsList.count
        }else if tableView == self.table_view_flow {
            return NetworkHelper.sharedInstance.preMutedFlowList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.tableView  || tableView == self.table_view_flow {
        } else if tableView == self.tableView_preMuted {
            return UITableView.automaticDimension//80 //55
        }
        return UITableView.automaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == self.tableView_preMuted {
            return UITableView.automaticDimension //50
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let preMuted_cell_outer = self.tableView_preMuted.dequeueReusableCell(withIdentifier: "pre_muted_outer_cell") as! PreMutedOuterCell
        //        let preMuted_cell_Flow = self.table_view_flow.dequeueReusableCell(withIdentifier: "pre_muted_flow_cell") as! PreMutedFlowCell
        
        if tableView == self.tableView_preMuted {
            let items = NetworkHelper.sharedInstance.preMutedAlertsList[section]
            
            let dates = newDateFormat(items.getEveSec())
            print(items.getinputSrc())
            if items.getinputSrc() == "ids" {
                preMuted_cell_outer.descLabel.text = items.getDescription()
                preMuted_cell_outer.timestamp.text = dates
                preMuted_cell_outer.tag = section
                preMuted_cell_outer.headerButtonOutlet.tag = 0
                if section == self.selectedIndex{
                    preMuted_cell_outer.headerButtonOutlet.tag = 1
                }
                preMuted_cell_outer.backgroundColor = UIColor.white
                preMuted_cell_outer.delegate = self
                preMuted_cell_outer.actions_label.isHidden = false
                //        policyDurationOuterCell.slotDurations = items
                
                return preMuted_cell_outer
                
            } else if items.getinputSrc() == "flow"{
                
                var descriptionList = ""
                for element in items.arrayFlowAlerts  {
                    
                    descriptionList += element["description"] as! String + ".\n";
                    
                }
                
                let startingHour = getDayHourTimeStamp(items.getEveSec(), hour: true, day: false, format: false)
                let alertDay = getDayHourTimeStamp(items.getEveSec(), hour: false, day: true, format: false)
                var timeFormatEnd = getDayHourTimeStamp(items.getEveSec(), hour: false, day: false, format: true)
                var endingHour:Int! = Int(startingHour)
                //        let a:Int?
                endingHour = endingHour + 1
                if endingHour == 0 {
                    endingHour = 1
                }
                else if endingHour == 13 {
                    endingHour = 1
                }
                if (timeFormatEnd.range(of: "AM") != nil) {
                    timeFormatEnd = "AM"
                }else{
                    timeFormatEnd = "PM"
                }
                
                preMuted_cell_outer.descLabel.text = descriptionList
                let timeStamptext : String = "\(alertDay) between \(startingHour)-\(String(endingHour)) \(timeFormatEnd)"
                preMuted_cell_outer.timestamp.text = timeStamptext
                preMuted_cell_outer.tag = section
                preMuted_cell_outer.headerButtonOutlet.tag = 0
                if section == self.selectedIndex{
                    preMuted_cell_outer.headerButtonOutlet.tag = 1
                }
                preMuted_cell_outer.backgroundColor = UIColor.white
                preMuted_cell_outer.delegate = self
                preMuted_cell_outer.actions_label.isHidden = true
                //        policyDurationOuterCell.slotDurations = items
                
                return preMuted_cell_outer
                
            }else{}
            return preMuted_cell_outer
        }
        //        else if tableView == self.table_view_flow {
        //                        preMuted_cell_Flow.description_label.text = "sdsdsddsds"
        //                        preMuted_cell_Flow.timestamp_label.text = "11-11-2011"
        //                        return preMuted_cell_Flow
        //        }
        return preMuted_cell_outer
    }
    
    //
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            //        tableView.deselectRow(at: indexPath, animated: true)
            if tableView == self.tableView {
                
                if NetworkHelper.sharedInstance.isDmoatOnline {
                    if(self.alertType=="view_alerts")
                    {

                        
                        let item = self.viewAlertsFilteredList[indexPath.row]
                        DispatchQueue.main.async {
                        if item.getSeenStatus(){
                            print("alert already seen")
                        }else{

                            DispatchQueue.main.async {
                            self.setAlertSeen(index: indexPath.row, item: item)
                            }
                        }
                        }
                        let inputSrc = item.getInputSrc()
                        
                        //check if alert is seen or not with input source types
                        if(inputSrc=="cnctdhost" || inputSrc=="ids" || inputSrc=="flow" || inputSrc=="blockips" || inputSrc == "update")
                        {
                            DispatchQueue.main.async {
                                self.showBlockSheet(item, position: indexPath.row)
                            }
                            //update cell seen view after alert is seen
//                            if(inputSrc=="flow")
//                            {}
//                            else if(inputSrc=="ids")
//                            {}
//                            else if(inputSrc=="cnctdhost")
//                            {
//                                if (item.getDeviceCategory() == "Router") {
//                                    self.showAlert(ConstantStrings.router_no_action)
//                                }else{
//
//                                }
//                            }
                            
                        }
                        else if(inputSrc=="sensor")
                        {
                        }
                        else {
                           
                        
                    }
                    }
                    else if self.alertType == "blocked"{
                        let item = NetworkHelper.sharedInstance.blockedAlertsList[indexPath.row]
                        let inputSrc = item.getInputSrc()
                        print("blocked selected\(item) and position \(indexPath.row) and alertID \(item.getalertId())")
                        if inputSrc=="cnctdhost" {
                            
                            self.showBlockSheet(item, position: indexPath.row)
                            
                        }
                        else if inputSrc=="ids" {
                            self.showBlockSheet(item, position: indexPath.row)
                        }
                        else if inputSrc=="flow" {
                            self.showBlockSheet(item, position: indexPath.row)
                            
                        }else if inputSrc=="blockips"{
                            self.showBlockSheet(item, position: indexPath.row)
                        }
                        
                    }
                }
                else {
                    self.showAlert(ConstantStrings.dmoatOffline)
                }
            }else if tableView == self.tableView_preMuted {
                DispatchQueue.main.async {
                    
                    print("clieckndex==\(self.clickIndex) didselect row = \(indexPath.row)")
                    if self.clickIndex == indexPath.row{
                        
                        self.clickIndex = -1
                    }else{
                        self.clickIndex = indexPath.row
                    }
                    
                    self.reloadAlertTable()
                }
            }else if tableView == self.table_view_flow {
                print("observend ents for flow pressed")
            }
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let viewAlertsCell =  self.tableView.dequeueReusableCell(withIdentifier: "view_alerts_cell")! as! ViewAlertsCell
        
        let preMuted_cell_blockedMuted = self.tableView_preMuted.dequeueReusableCell(withIdentifier: "pre_muted_outer_cell") as! PreMutedOuterCell
        
        let preMuted_cell_Flow = self.table_view_flow.dequeueReusableCell(withIdentifier: "pre_muted_flow_cell") as! PreMutedFlowCell
        
        
        if tableView == self.tableView {
            
            //check alert model type
            if(alertType=="view_alerts")
            {
                let item = viewAlertsFilteredList[indexPath.row]
                let inputSrc = item.getInputSrc()
                let seenStatus = item.getSeenStatus()
                let eveSec = item.getEveSec()
                
                //check category type
                if(inputSrc == "sensor")
                {
                    if(seenStatus)
                    {
                        viewAlertsCell.seenView.isHidden = true
                    }
                    else{
                        viewAlertsCell.seenView.isHidden = false
                    }
                    //map sensor alert
                    
                    self.mapSensorAlert(viewAlertsCell, alert: item)
                    return viewAlertsCell
                    
                }
                else if(inputSrc != ""){
                    //check input source type
                    if(inputSrc=="cnctdhost")
                    {
                        if(seenStatus)
                        {
                            viewAlertsCell.seenView.isHidden = true
                        }
                        else{
                            viewAlertsCell.seenView.isHidden = false
                        }
                        
                        showConnectedHostViewAlert(viewAlertsCell, alertModel: item)
                        //                    alertConnectedDevicesView.separater.isHidden = true
                        return viewAlertsCell
                    }
                        
                    else if(inputSrc == "flow")
                    {
                        if(seenStatus)
                        {
                            viewAlertsCell.seenView.isHidden = true
                        }
                        else{
                            viewAlertsCell.seenView.isHidden = false
                        }
                        showFlowAlert(viewAlertsCell, alertModel: item)
                        //Convert to Date jan-dd-yyyy flow
                        //                    let dates = newDateFormat(eveSec)
                        //
                        return viewAlertsCell
                    }
                    else if(inputSrc == "ids")
                    {
                        if(seenStatus)
                        {
                            viewAlertsCell.seenView.isHidden = true
                        }
                        else{
                            viewAlertsCell.seenView.isHidden = false
                        }
                        
                    }
                    else if(inputSrc == "blockips")
                    {
                        if(seenStatus)
                        {
                            viewAlertsCell.seenView.isHidden = true
                        }
                        else{
                            viewAlertsCell.seenView.isHidden = false
                        }
                        mapBlockIpsAlert(viewAlertsCell, alert: item)
                        return viewAlertsCell
                    }
                    else if(inputSrc == "update")
                    {
                        if(seenStatus)
                        {
                            viewAlertsCell.seenView.isHidden = true
                        }
                        else{
                            viewAlertsCell.seenView.isHidden = false
                        }
                        mapUpdateAlert(viewAlertsCell, alert: item)
                        return viewAlertsCell
                    }
                    if (inputSrc == "info") {
                        
                        if(seenStatus)
                        {
                            viewAlertsCell.seenView.isHidden = true
                        }
                        else{
                            viewAlertsCell.seenView.isHidden = false
                        }
                        if(item.getInfoType() == "online" || item.getInfoType() == "offline"){
                            //Convert to Date jan-dd-yyyy flow
                            mapInfoAlert(viewAlertsCell, alert: item)
                            return viewAlertsCell
                        }
                        else if(item.getInfoType() == "internet_restarted"){
                            mapInfoAlert(viewAlertsCell, alert: item)
                            return viewAlertsCell
                        }else{
                            mapInfoAlert(viewAlertsCell, alert: item)
                        }
                    }
                    
                }
                else{
                    print("some other alert in tableview")
                    print(item.getInputSrc())
                    //don't return anything
                    return viewAlertsCell
                    
                }
            } //blocked/muted/pre muted
            else if alertType == "blocked"{
                
                viewAlertsCell.seenView.isHidden = true
                if blockedFilteredList.count > 0 {
                    
                    let item = blockedFilteredList[indexPath.row]
                    //convert evesec to time,date
                    
                    
                    //
                    let inputSrc = item.getInputSrc()
                    if (inputSrc == "flow") {
                        
                        showFlowAlert(viewAlertsCell, alertModel: item)
                        return viewAlertsCell
                        
                    } else if (inputSrc == "cnctdhost") {
                        
                        showConnectedHostViewAlert(viewAlertsCell, alertModel: item)
                        viewAlertsCell.view_b.isHidden = true
                        viewAlertsCell.view_c.isHidden = true
                        viewAlertsCell.view_d.isHidden = true
                        viewAlertsCell.view_e.isHidden = true
                        return viewAlertsCell
                        
                    }else if (inputSrc == "blockips") {
                        
                        mapBlockIpsAlert(viewAlertsCell, alert: item)
                        return viewAlertsCell
                        
                    }
                    else if (inputSrc == "ids") {
                        
                        showIDSAlert(viewAlertsCell, alertModel: item)
                        return viewAlertsCell
                        
                    }else{
                        
                        mapInfoAlert(viewAlertsCell, alert: item)
                        return viewAlertsCell
                        
                    }
                }
                
            }
            return viewAlertsCell
        }
        else if tableView == self.tableView_preMuted {
            if NetworkHelper.sharedInstance.preMutedAlertsList.count > 0 {
                //                preMuted_cell.preMutedAlertsListIPs = preMutedAlertsList
                
                //                let items = NetworkHelper.sharedInstance.preMutedAlertsList[indexPath.row]
                //convert evesec to time,date
                let pre_muted_inner_cell =  self.tableView_preMuted.dequeueReusableCell(withIdentifier: "pre_muted_inner_cell") as! PreMutedInnerCell
                
                let item = NetworkHelper.sharedInstance.preMutedAlertsList[indexPath.section].arraySets[indexPath.row]
                pre_muted_inner_cell.src_ip.text = item["src_ip"] as! String
                pre_muted_inner_cell.dest_ip.text = item["dest_ip"] as! String
                
                return pre_muted_inner_cell
            }
        }
        else if tableView == self.table_view_flow {
            let items = NetworkHelper.sharedInstance.preMutedFlowList[indexPath.row]
            
            var descriptionList = ""
            for element in items.arrayFlowAlerts  {
                descriptionList += element["description"] as! String + ".\n";
            }
            
            let startingHour = getDayHourTimeStamp(items.getEveSec(), hour: true, day: false, format: false)
            let alertDay = getDayHourTimeStamp(items.getEveSec(), hour: false, day: true, format: false)
            var timeFormatEnd = getDayHourTimeStamp(items.getEveSec(), hour: false, day: false, format: true)
            var endingHour:Int! = Int(startingHour)
            //        let a:Int?
            endingHour = endingHour + 1
            if endingHour == 0 {
                endingHour = 1
            }
            else if endingHour == 13 {
                endingHour = 1
            }
            if (timeFormatEnd.range(of: "AM") != nil) {
                timeFormatEnd = "AM"
            }else{
                timeFormatEnd = "PM"
            }
            
            let timeStamptext : String = "\(alertDay) between \(startingHour)-\(String(endingHour)) \(timeFormatEnd)"
            
            preMuted_cell_Flow.description_label.text = descriptionList
            preMuted_cell_Flow.timestamp_label.text = timeStamptext
            return preMuted_cell_Flow
        }
        
        return viewAlertsCell
        
    }
    //MARK: -UnblockUnmuteActions
    
    @IBAction func UnblockUnmuteCnctdDevice(_ sender: Any) {
        let  cnctdHstBtn = sender as! UIButton
        print(cnctdHstBtn.tag)
        print("unblockunmute ids alert")
        currentIndex = cnctdHstBtn.tag
        if alertType == "blocked" {
            let item = NetworkHelper.sharedInstance.blockedAlertsList[currentIndex]
            self.showBlockSheet(item, position: currentIndex)
        }
    }
    
    @IBAction func UnblockUnmuteIdsAlert(_ sender: Any) {
        let  idsBtn = sender as! UIButton
        print(idsBtn.tag)
        print("unblockunmuteidsalert")
        currentIndex = idsBtn.tag
        
        if alertType == "blocked" {
            let item = NetworkHelper.sharedInstance.blockedAlertsList[currentIndex]
            self.showBlockSheet(item, position: currentIndex)
        }
    }
    
    @IBAction func UnblockUnmuteFlowAlert(_ sender: Any) {
        let  flowBtn = sender as! UIButton
        print("unblockunmuteflowalert")
        currentIndex = flowBtn.tag
        if alertType == "blocked" {
            let item = NetworkHelper.sharedInstance.blockedAlertsList[currentIndex]
            self.showBlockSheet(item, position: currentIndex)
        }
    }
    
    //MARK: -dateToNewFormat
    func newDateFormat(_ oldDate: Int) -> String {
        
        let timeInterval  = TimeInterval(oldDate) // as NSTimeInterval
        //Convert to Date
        let date = Date(timeIntervalSince1970: timeInterval)
        //Date formatting
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM-dd-yyyy h:mm:a"
        dateFormatter.timeZone = TimeZone.current //name: "UTC")
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    //MARK: -InternetOnOffTime
    func internetOnOffFormat(_ oldDate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-DD h:mm:ss"
        
        let date = dateFormatter.date(from: oldDate)
        
        dateFormatter.dateFormat = "MMM-DD-YYYY h:mm:ss"
        let newFormatDate = dateFormatter.string(from: date!)
        return oldDate
    }
    //MARK: -GetHourDayFormat
    func getDayHourTimeStamp(_ oldDate: Int, hour: Bool, day: Bool, format: Bool) -> String {
        
        
        let time = Int(oldDate)
        let timeInterval  = TimeInterval(time) // as NSTimeInterval
        //Convert to Date
        let date = Date(timeIntervalSince1970: timeInterval)
        //Date formatting
        let dateFormatter = DateFormatter()
        
        if hour{
            dateFormatter.dateFormat = "h"
            
        }else if day{
            dateFormatter.dateFormat = "EEEE"
            
        }else{
            dateFormatter.dateFormat = "h:mm:ss a"
            dateFormatter.amSymbol = "AM"
            dateFormatter.pmSymbol = "PM"
            
        }
        
        dateFormatter.timeZone = TimeZone.current //name: "UTC")
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    //MARK: -ViewAlerts webservice
    func loadAlertsData(_ url : String, viewAlerts : Bool, blocked : Bool, muted : Bool, preMuted : Bool) {
        //        NetworkHelper.sharedInstance.arrayBlockedAlerts.removeAll()
        //call service for getting blocked alerts data
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        NetworkHelper.sharedInstance.error401 = false
        let params:Dictionary<String,String> = ["request_id": requestID, "token": NetworkHelper.sharedInstance.getCurrentToken(), "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace(), "app_id": NetworkHelper.sharedInstance.app_ID!]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: url, sendData: params as [String : AnyObject], success: { (data) in
            print(data)
            let message = ConstantStrings.ErrorText
            
            if(NetworkHelper.sharedInstance.isConnectedDevicesSuccess)
            {
                if let jsonArr = data["message"] as? [[String:AnyObject]] {
                    //                print(jsonArr?.count)
                    if(jsonArr.count > 0){
                        self.error_label.isHidden = true
                        print("json array received for alerts")
                        if viewAlerts{
                            self.viewAlertsFilteredList.removeAll()
                            self.fetchAlertsData(jsonArr, viewAlerts: true, blocked: false, muted: false)
                        }else if muted {
                            self.mutedFilteredList.removeAll()
                            self.fetchMutedBlockedAlertsData(jsonArr)
                            
                        }else if preMuted {
                            self.fetchPreMutedAlertsData(jsonArr)
                            
                        }else { //if blocked{
                            
                            self.blockedFilteredList.removeAll()
                            self.fetchMutedBlockedAlertsData(jsonArr)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.error_label.isHidden = false
                            if self.alertType == "view_alerts"{
                                self.error_label.text = "No alerts were detected."
                            }else if self.alertType == "blocked"{
                                self.error_label.text = "No malicious traffic was detected or blocked."
                            }else if self.alertType == "muted"{
                                self.error_label.text = "No alerts were detected."
                            }else{
                                self.hideSnortView()
                                self.error_label.text = "No alerts were detected."
                            }
                        }
                    }
                }
            }
            else{
                self.showAlert(message)
                
            }
            
        }, failure: { (data) in
            print("failure error is \(data)")
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
            
        })
        
    }
    
    //MARK: -Fetch data
    func fetchAlertsData(_ jsonArr : [[String : AnyObject]], viewAlerts : Bool, blocked : Bool, muted : Bool) {
        let nameSpace = NetworkHelper.sharedInstance.getCurrentUserNamespace()
        NetworkHelper.sharedInstance.viewAlertsList.removeAll()
        self.viewAlertsFilteredList.removeAll()
        var viewAlertsCount = 0
        var blockedAlertsCount = 0
        //retreive from userdefaults
        
        for element in jsonArr {
            let blockedAlrt = BlockedMutedAlertsModel()
            let inputSrc = element["input_src"] as! String
            //if record id exist skip else add
            
            //            isAlertNotExist = checkAlertExistence(recordId: recordId)
            var seenStatus = element["seen"] as AnyObject
            seenStatus = String(describing: seenStatus) as AnyObject
            
            if (inputSrc == "ids") {
                let recordId = String(describing: element["record_id"])  as AnyObject
                let description = String(describing: element["description"])
                let eveSec = element["eve_sec"] as! Int
                //                let destHostname = element["dest_hostname"] as? String
                let blockType = String(describing: element["blocktype"]) as AnyObject
                let srcIp = String(describing: element["src_ip"]) as AnyObject
                let destPort = String(describing: element["dest_port"]) as AnyObject
                let srcPort = String(describing: element["src_port"]) as AnyObject
                let destIp = String(describing: element["dest_ip"])  as AnyObject
                
                blockedAlrt.setDescription(description)
                blockedAlrt.setEveSec(eveSec)
                //                blockedAlrt.setDestHostName(destHostname!)
                //                blockedAlrt.setSrcName(srcHostName!)
                blockedAlrt.setSrcIp(srcIp as! String)
                blockedAlrt.setRecordId(recordId as! String)
                blockedAlrt.setDestIp(destIp as! String)
                blockedAlrt.setInputSrc(inputSrc )
                blockedAlrt.setIdsBlockType(blockType as! String)
                let sourcePort = srcPort as! String
                let destinationPort = destPort as! String
                if(sourcePort.range(of: "") != nil || sourcePort.count < 1) {
                    blockedAlrt.setSrcPort("Unknown")
                }else{
                    blockedAlrt.setSrcPort(sourcePort )
                }
                if (destinationPort.range(of: "") != nil || destinationPort.count < 1) {
                    blockedAlrt.setDestPort("Unknown")
                }else{
                    blockedAlrt.setDestPort(destinationPort )
                }
            }
            else if (inputSrc == "flow") {
                
                let slotId = element["slot_id"] as! Int
                let eveSec = element["eve_sec"] as! Int
                let alertsJsonArr = element["alerts"]  as? [[String:AnyObject]]
                let alertsRecordIdsJsonArr = element["record_ids"]
                
                blockedAlrt.setInputSrc(inputSrc)
                blockedAlrt.setSlotId(slotId)
                blockedAlrt.setEveSec(eveSec)
                blockedAlrt.arrayFlowAlerts = alertsJsonArr as! [[String : AnyObject]]
                blockedAlrt.arrayFlowRecordIdsList = alertsRecordIdsJsonArr as! [String]
                
            }
            else if (inputSrc == "blockips") {
                //                    let seenStatus = element["seen"] as! String
                let description = element["description"] as! String
                let eveSec = element["eve_sec"] as! Int
                let recordId = element["record_id"] as! String
                let ip = element["ip"] as! String
                blockedAlrt.setInputSrc(inputSrc)
                blockedAlrt.setDescription(description)
                blockedAlrt.setEveSec(eveSec)
                blockedAlrt.setRecordId(recordId)
                blockedAlrt.setIp(ip)
                
            }
            else if(inputSrc == "cnctdhost") {
                //                    let seenStatus = element["seen"] as! String
                let description = element["description"] as! String
                let eveSec = element["eve_sec"] as! Int
                let recordId = element["record_id"] as! String
                let category = element["device_category"] as! String
                //                let macAdres = element["macaddress"] as! String
                let hostName = element["hostname"] as! String
                //                let hostID = element["HOSTID"] as! String
                let ip = element["ip"] as! String
                let os = element["OS"] as! String
                
                blockedAlrt.setDescription(description)
                blockedAlrt.setEveSec(eveSec)
                blockedAlrt.setInputSrc(inputSrc)
                blockedAlrt.setRecordId(recordId)
                blockedAlrt.setDeviceCategory(category)
                //                blockedAlrt.setMacAddress(macAdres)
                blockedAlrt.setHostName(hostName)
                //                blockedAlrt.sethostID(hostID)
                blockedAlrt.setIp(ip)
                blockedAlrt.setOS(os)
                
            }
            else if(inputSrc == "info"){
                //                    let seenStatus = element["seen"] as! String
                let description = element["description"] as! String
                let eveSec = element["eve_sec"] as! Int
                let recordId = element["record_id"] as! String
                let type = element["type"] as! String
                if type == "internet_restarted" {
                    let timeOn = element["on"] as! Int
                    let timeOff = element["off"] as! Int
                    blockedAlrt.setTimeOn(timeOn)
                    blockedAlrt.setTimeOff(timeOff)
                }
                
                blockedAlrt.setDescription(description)
                blockedAlrt.setEveSec(eveSec)
                blockedAlrt.setInputSrc(inputSrc)
                blockedAlrt.setRecordId(recordId)
                blockedAlrt.setInfoType(type)
                
            }  else if(inputSrc == "sensor"){
                //                    let seenStatus = element["seen"] as! String
                let description = element["description"] as! String
                let eveSec = element["eve_sec"] as! Int
                let recordId = element["record_id"] as! String
                let reading = element["reading"] as! Double
                let category = element["category"] as! String
                blockedAlrt.setDescription(description)
                blockedAlrt.setEveSec(eveSec)
                blockedAlrt.setInputSrc(inputSrc)
                blockedAlrt.setRecordId(recordId)
                blockedAlrt.setSensorReading(reading)
                blockedAlrt.setCategory(category)
                
            } else if(inputSrc == "update"){
                //                    let seenStatus = element["seen"] as! String
                let description = element["description"] as! String
                let eveSec = element["eve_sec"] as! Int
                let recordId = element["record_id"] as! String
                let type = element["type"] as! String
                blockedAlrt.setDescription(description)
                blockedAlrt.setEveSec(eveSec)
                blockedAlrt.setInputSrc(inputSrc)
                blockedAlrt.setRecordId(recordId)
                blockedAlrt.setInfoType(type)
                
            }else{
                print("some other alert")
            }
            //if bool true add in that list
            if viewAlerts {
                blockedAlrt.setNameSpace(nameSpace)
                if seenStatus as! String == "0" {
                    blockedAlrt.setSeenStatus(false)
                }else{
                    blockedAlrt.setSeenStatus(true)
                }
                blockedAlrt.setAlertId(viewAlertsCount)
                NetworkHelper.sharedInstance.viewAlertsList.append(blockedAlrt)
                viewAlertsCount += 1
            }else if blocked {
                blockedAlrt.setAlertId(blockedAlertsCount)
                NetworkHelper.sharedInstance.blockedAlertsList.append(blockedAlrt)
                blockedAlertsCount += 1
            }else if muted {
                NetworkHelper.sharedInstance.MutedAlertsList.append(blockedAlrt)
            }
            
        }
        
        //save view alerts in userdefault
        
        //reload table view/decorate chart view
        if viewAlerts {
            isFirstViewAlertsCall = false
            NetworkHelper.sharedInstance.viewAlertsList.sort(by: self.sorterForFileIDASC)
        }else if blocked {
            isFirstBlockedAlertsCall = false
            NetworkHelper.sharedInstance.blockedAlertsList.sort(by: sorterForFileIDASC)
        }else if muted {
            isFirtMutedAlertsCall = false
            NetworkHelper.sharedInstance.MutedAlertsList.sort(by: sorterForFileIDASC)
        }
        
        self.filterViewAlertsForUser()
        
        DispatchQueue.main.async {
            self.current_label.text = "New Alerts" + " (\(String(self.viewAlertsFilteredList.count)))"
        }
        DispatchQueue.main.async {
            self.reloadAlertTable()
        }
    }
    
    //MARK: -Fetch muted alerets
    
    func fetchMutedBlockedAlertsData(_ jsonArr : [[String : AnyObject]]){
        //        MutedAlertsList.removeAll()
        var alertsCount = 0
        for element in jsonArr {
            alertsCount += 1
            let blockedAlrt = BlockedMutedAlertsModel()
            let inputSrc = element["input_src"] as! String
            if (inputSrc == "ids") {
                let description = element["description"] as! String
                let eveSec = element["eve_sec"] as! Int
                //                let destHostname = element["dest_hostname"] as? String
                let srcHostName = element["src_hostname"] as? String
                let srcIp = element["src_ip"] as! String
                let destIp = element["dest_ip"] as! String
                let recordId = element["record_id"] as! String
                let destPort = element["dest_port"] as! String
                let srcPort = element["src_port"] as! String
                
                blockedAlrt.setDescription(description)
                blockedAlrt.setEveSec(eveSec)
                //                blockedAlrt.setDestHostName(destHostname!)
                //                blockedAlrt.setSrcName(srcHostName!)
                blockedAlrt.setSrcIp(srcIp)
                blockedAlrt.setRecordId(recordId)
                blockedAlrt.setDestIp(destIp)
                blockedAlrt.setInputSrc(inputSrc)
                
                if(srcHostName?.range(of: "") != nil || srcHostName?.count < 1) {
                    blockedAlrt.setSrcPort("Unknown")
                }else{
                    blockedAlrt.setSrcName(srcHostName!)
                }
                if(srcPort.range(of: "") != nil || srcPort.count < 1) {
                    blockedAlrt.setSrcPort("Unknown")
                }else{
                    blockedAlrt.setSrcPort(srcPort)
                }
                if (destPort.range(of: "") != nil || destPort.count < 1) {
                    blockedAlrt.setDestPort("Unknown")
                }else{
                    blockedAlrt.setDestPort(destPort)
                }
            }
            else if (inputSrc == "flow") {
                let slotId = element["slot_id"] as! Int
                //                    let recordId = element["record_id"] as! String
                let alertsJsonArr = element["alerts"]  as? [[String:AnyObject]]
                let alertsRecordIdsJsonArr = element["record_ids"]
                
                blockedAlrt.setInputSrc(inputSrc)
                blockedAlrt.setSlotId(slotId)
                blockedAlrt.arrayFlowAlerts = alertsJsonArr as! [[String : AnyObject]]
                blockedAlrt.arrayFlowRecordIdsList = alertsRecordIdsJsonArr as! [String]
                
            }
            else if (inputSrc == "blockips") {
                let description = element["description"] as! String
                let eveSec = element["eve_sec"] as! Int
                let recordId = element["record_id"] as! String
                let ip = element["ip"] as! String
                blockedAlrt.setInputSrc(inputSrc)
                blockedAlrt.setDescription(description)
                blockedAlrt.setEveSec(eveSec)
                blockedAlrt.setRecordId(recordId)
                blockedAlrt.setIp(ip)
                
            }
            else if(inputSrc == "cnctdhost") {
                let description = element["description"] as! String
                let eveSec = element["eve_sec"] as! Int
                let recordId = element["record_id"] as! String
                let parameter = element["parameters"] as! String
                let macAdres = element["macaddress"] as! String
                let hostName = element["hostname"] as! String
                let hostID = element["HOSTID"] as! String
                let ip = element["ip"] as! String
                let os = element["OS"] as! String
                
                blockedAlrt.setDescription(description)
                blockedAlrt.setEveSec(eveSec)
                blockedAlrt.setInputSrc(inputSrc)
                blockedAlrt.setRecordId(recordId)
                blockedAlrt.setParameters(parameter)
                blockedAlrt.setMacAddress(macAdres)
                blockedAlrt.setHostName(hostName)
                blockedAlrt.sethostID(hostID)
                blockedAlrt.setIp(ip)
                blockedAlrt.setOS(os)
                
            }
            else{
                print("some other alert")
            }
            if alertType == "blocked" {
                blockedAlrt.setNameSpace(NetworkHelper.sharedInstance.getCurrentUserNamespace())
                blockedAlrt.setAlertId(alertsCount)
                NetworkHelper.sharedInstance.blockedAlertsList.append(blockedAlrt)
            }
            else if alertType == "muted" {
            }
        }
        //sort lists
        if alertType == "blocked" {
            isFirstBlockedAlertsCall = false
            NetworkHelper.sharedInstance.blockedAlertsList.sort(by: sorterForFileIDASC)
            filterBlockedMutedAlertsForUser()
            DispatchQueue.main.async {
                self.blocked_label.text = "Blocked Events" + " (\(String(NetworkHelper.sharedInstance.blockedAlertsList.count)))"
            }
            
        }
        DispatchQueue.main.async {
            self.reloadAlertTable()
        }
    }
    
    //MARK: -Fetch pre-muted alerets
    func fetchPreMutedAlertsData(_ jsonArr : [[String : AnyObject]]){
        NetworkHelper.sharedInstance.preMutedAlertsList.removeAll()
        NetworkHelper.sharedInstance.preMutedFlowList.removeAll()
        
        for element in jsonArr {
            let preMuteAlrt = PreMutedAlertModel()
            let inputSrc = element["input_src"] as! String
            if (inputSrc == "ids") {
                let description = element["description"] as! String
                let eveSec = element["eve_sec"] as! Int
                
                preMuteAlrt.setDescription(description)
                preMuteAlrt.setinputSrc(inputSrc)
                preMuteAlrt.setEveSec(eveSec)
                
                let jsonArrIPs = element["sets"] as? [[String:AnyObject]]
                preMuteAlrt.arraySets = jsonArrIPs! as! [[String : String]]
                
                NetworkHelper.sharedInstance.preMutedAlertsList.append(preMuteAlrt)
                
            }else if (inputSrc == "flow") {
                let eveSec = element["eve_sec"] as! Int
                let alertsJsonArr = element["alerts"]  as? [[String:AnyObject]]
                
                preMuteAlrt.setinputSrc(inputSrc)
                preMuteAlrt.setEveSec(eveSec)
                preMuteAlrt.arrayFlowAlerts = alertsJsonArr as! [[String : AnyObject]]
                NetworkHelper.sharedInstance.preMutedFlowList.append(preMuteAlrt)
                
            }else{}
            
        }
        //reload table view/decorate chart view
        isFirsPreMutedAlertsCall = false
        NetworkHelper.sharedInstance.preMutedAlertsList.sort(by: sorterForPreMuted)
        NetworkHelper.sharedInstance.preMutedFlowList.sort(by: sorterForPreMuted)
        DispatchQueue.main.async {
            self.tableView_preMuted.reloadData()
        }
        DispatchQueue.main.async {
            self.table_view_flow.reloadData()
        }
        DispatchQueue.main.async {
            let observedCount = NetworkHelper.sharedInstance.preMutedAlertsList.count + NetworkHelper.sharedInstance.preMutedFlowList.count
            self.pre_muted_label.text = "Observed Events" + " (\(String(observedCount)))"
            self.flow_label.text = "Devices (" + String(NetworkHelper.sharedInstance.preMutedFlowList.count) + ")"
            self.snort_label.text = "Network (" + String(NetworkHelper.sharedInstance.preMutedAlertsList.count) + ")"
            if NetworkHelper.sharedInstance.preMutedAlertsList.count > 0 {
                self.showSnortView()
            }else{
                self.hideSnortView()
            }
            //            if NetworkHelper.sharedInstance.preMutedFlowList.count > 0 {
            //                self.showFlowView()
            //            }else{
            ////                self.hideFlowView()
            //            }
            
        }
        //
    }
    //MARK: -Sort alerts
    func sorterForFileIDASC(_ this:BlockedMutedAlertsModel, that:BlockedMutedAlertsModel) -> Bool {
        return this.getEveSec() > that.getEveSec()
    }
    func sorterForPreMuted(_ this:PreMutedAlertModel, that:PreMutedAlertModel) -> Bool {
        return this.getEveSec() > that.getEveSec()
    }
    
    //MARK: -List for Filters
    func filterViewAlertsForUser() {
        viewAlertsFilteredList.removeAll()
        var alertsCount:Int = 0
        if NetworkHelper.sharedInstance.viewAlertsList.count > 0 {
            alertsCount = NetworkHelper.sharedInstance.viewAlertsList.count
        }else{
            alertsCount = 0
        }
        
        for i in 0 ..< alertsCount {
            viewAlertsFilteredList.append(NetworkHelper.sharedInstance.viewAlertsList[i])
        }
    }
    
    func filterBlockedMutedAlertsForUser() {
        var alertsCount = -1
        if alertType == "blocked" {
            blockedFilteredList.removeAll()
            alertsCount = NetworkHelper.sharedInstance.blockedAlertsList.count
        }
        for i in 0 ..< alertsCount {
            if alertType == "blocked" {
                blockedFilteredList.append(NetworkHelper.sharedInstance.blockedAlertsList[i])
            }
        }
    }
    
    //delete older alerts
    func deleteOlderAlert(index : Int) -> Bool {
        let dates = NetworkHelper.sharedInstance.viewAlertsList[index].getEveSec()
        
        let timeInterval  = TimeInterval(dates) // as NSTimeInterval
        //Convert to Date
        let date = Date(timeIntervalSince1970: timeInterval)
        //Date formatting
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM-dd-yyyy h:mm:a"
        dateFormatter.timeZone = TimeZone.current //name: "UTC")
        //            let dateString = dateFormatter.(from: date) //(from: date)
        
        let timeIntervals : TimeInterval = NSDate().timeIntervalSince(date)
        let ti = NSInteger(timeIntervals)
        let hours = (ti / 3600)
        print("TimeHours == \(hours)")
        if(hours>=24)
        {
            return true
        }else{
            return false
        }
    }
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        if(self.checkPausedTimeAsAndroid()){
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
       
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    // MARK: - top NavBAr title colors
    func setNavBarTitleColors(isCurrent : Bool, isBlocked : Bool, isMuted : Bool, isPreMuted : Bool)  {
        
        
        self.current_label.textColor =  CozyLoadingActivity.Settings.UnselectedButonTextColor
        self.blocked_label.textColor =  CozyLoadingActivity.Settings.UnselectedButonTextColor
        self.muted_label.textColor = CozyLoadingActivity.Settings.UnselectedButonTextColor
        self.pre_muted_label.textColor = CozyLoadingActivity.Settings.UnselectedButonTextColor
        
        self.current_btn_tab_view.backgroundColor = UIColor(named: "UpperTabSelected")
        self.blocked_btn_tab_view.backgroundColor = UIColor(named: "ViewPatchB")
        self.muted_btn_tab_view.backgroundColor = UIColor(named: "ViewPatchB")
        self.premuted_btn_tab_view.backgroundColor = UIColor(named: "ViewPatchB")
        
        if isCurrent {
            self.current_label.textColor =  CozyLoadingActivity.Settings.WhiteColor
            
        
        }
        else if isBlocked {
            self.blocked_label.textColor =  CozyLoadingActivity.Settings.WhiteColor
            self.current_btn_tab_view.backgroundColor = UIColor(named: "ViewPatchB")
            self.blocked_btn_tab_view.backgroundColor = UIColor(named: "UpperTabSelected")
            self.muted_btn_tab_view.backgroundColor = UIColor(named: "ViewPatchB")
            self.premuted_btn_tab_view.backgroundColor = UIColor(named: "ViewPatchB")
        }
        else if isMuted {
            self.muted_label.textColor = CozyLoadingActivity.Settings.WhiteColor
            self.current_btn_tab_view.backgroundColor = UIColor(named: "ViewPatchB")
            self.blocked_btn_tab_view.backgroundColor = UIColor(named: "ViewPatchB")
            self.muted_btn_tab_view.backgroundColor = UIColor(named: "UpperTabSelected")
            self.premuted_btn_tab_view.backgroundColor = UIColor(named: "ViewPatchB")
        }
        else if isPreMuted {
            self.pre_muted_label.textColor = CozyLoadingActivity.Settings.WhiteColor
            self.current_btn_tab_view.backgroundColor = UIColor(named: "ViewPatchB")
            self.blocked_btn_tab_view.backgroundColor = UIColor(named: "ViewPatchB")
            self.muted_btn_tab_view.backgroundColor = UIColor(named: "ViewPatchB")
            self.premuted_btn_tab_view.backgroundColor = UIColor(named: "UpperTabSelected")
        }else{
            
        }
    }
    
    // MARK: - Alert seen Webservice call
    
    
    func setAlertSeen(index : Int, item : BlockedMutedAlertsModel) {
        
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        var data : Dictionary <String, AnyObject> = [:]
        if item.getInputSrc() == "flow" {
            data = ["record_ids_list" : item.arrayFlowRecordIdsList as AnyObject]
        }else{
            data = ["record_id": item.getRecordId() as AnyObject]
        }
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "data" : data as AnyObject]
        
        NetworkHelper.sharedInstance.postDashboardRequest(serviceName: ApiUrl.VIEW_ALERTS_SEEN_API, sendData: params, success: { (data) in
            print(data)
            let message = "Request failed.Please try again"
            
            let statusCode = self.defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                
                DispatchQueue.main.async {
                    
                self.viewAlertsFilteredList[index].setSeenStatus(true)
                let alertId = NetworkHelper.sharedInstance.viewAlertsList[index].getalertId()
                NetworkHelper.sharedInstance.viewAlertsList[alertId].setSeenStatus(true)
                self.tableView.reloadData()
                }
            } else if ((statusCode?.range(of: "503")) != nil){
                if (data["message"] != nil) {
                    let msg = data["message"] as! String
                    self.showAlert("\(ConstantStrings.ERROR_TEXT_503) \(msg)")
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                if (data["message"] != nil) {
                    let message = data["message"] as! String
                    self.showAlert(message)
                }else{
                    self.showAlert(ConstantStrings.ErrorText)
                    
                }
            }
            else{
                //                self.showAlert(message)
            }
            //
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    //MARK: -Observed events listeners
    
    @IBAction func snort_observed_btn_pressed(_ sender: Any) {
        DispatchQueue.main.async {
            
            if !self.tableView_preMuted.isHidden {
                self.hideSnortView()
                
            }else{
                self.showSnortView()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
                self.table_view_flow.reloadData()
            }
            
        }
    }
    
    
    
    @IBAction func flow_observed_btn_pressed(_ sender: Any) {
        DispatchQueue.main.async {
            
            if !self.table_view_flow.isHidden {
                self.hideFlowView()
            }else{
                self.showFlowView()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // change 1 to desired number of seconds
                self.table_view_flow.reloadData()
            }
        }
    }
    
    func hideSnortView() {
        self.tableView_preMuted.isHidden = true
        self.table_view_flow.isHidden = false
        
        self.table_view_snort_height.constant = 0
        self.table_view_flow_height.constant = CGFloat(self.remainingheight)
    }
    
    func showSnortView() {
        self.table_view_snort_height.constant = 0
        self.table_view_flow_height.constant = CGFloat(self.remainingheight)
        
        self.tableView_preMuted.isHidden = false
        self.table_view_flow.isHidden = true
        
        self.table_view_flow_height.constant = 0
        self.table_view_snort_height.constant = CGFloat(self.remainingheight)
    }
    
    func hideFlowView() {
        self.table_view_snort_height.constant = 0
        self.table_view_flow_height.constant = CGFloat(self.remainingheight)
        
        self.tableView_preMuted.isHidden = false
        self.table_view_flow.isHidden = true
        
        self.table_view_flow_height.constant = 0
        self.table_view_snort_height.constant = CGFloat(self.remainingheight)
    }
    func showFlowView() {
        self.tableView_preMuted.isHidden = true
        self.table_view_flow.isHidden = false
        
        self.table_view_snort_height.constant = 0
        self.table_view_flow_height.constant = CGFloat(self.remainingheight)
    }
}


extension ViewAlerts : PreMutedHeaderTableViewCellDelegate {
    
    func didSelectUserHeaderTableViewCell(isShowDetails: Bool, UserHeader: PreMutedOuterCell) {
        print("Index \(isShowDetails)")
        if isShowDetails{
            
            self.selectedIndex = (UserHeader.tag)
        }else{
            self.selectedIndex = -1
        }
        self.tableView_preMuted.reloadData()
    }
}
