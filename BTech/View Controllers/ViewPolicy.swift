//
//  ViewPolicy.swift
//  BTech
//
//  Created by Ahmed Akhtar on 04/01/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class ViewPolicy: Parent {
    
    
    var protocolsNameArray = [String]()
    var policies = [String]()
    var policyModelArray = [PolicyModel]()
    
    //iboutlet
    
    @IBOutlet var viewPolicyTableView: UITableView!
    
    //ibaction
    
    @IBAction func createPolicyPressed(_ sender: AnyObject) {
        self.resetPolicyProtocols()
        self.refreshDays()
        let policyManagementVC = PolicyManagement.policyManagementVC()
        self.setNavBar()
        
        self.navigationController?.pushViewController(policyManagementVC, animated: true)    }
    
    class func viewPolicyVC() -> ViewPolicy {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "viewPolicy") as! ViewPolicy
    }
    
    func refreshDays()
    {
        NetworkHelper.sharedInstance.mondayChecked = false
        NetworkHelper.sharedInstance.tuesdayChecked = false
        NetworkHelper.sharedInstance.wednesdayChecked = false
        NetworkHelper.sharedInstance.thursdayChecked = false
        NetworkHelper.sharedInstance.fridayChecked = false
        NetworkHelper.sharedInstance.saturdayChecked = false
        NetworkHelper.sharedInstance.sundayChecked = false
        
    }
    
    func getSelectedProtocolsWithString(_ protocols : String)
    {
        let protocolsArray = protocols.components(separatedBy: ",")
        let mapperProtocols = ProtocolsMapper()
        let protocolsArr = mapperProtocols.mapProtocolsWithArray(protocolsArray)
        
        for model in NetworkHelper.sharedInstance.protocolsMainArray
        {
            for value in protocolsArr
            {
                if(value == model.title)
                {
                    let index : Int = NetworkHelper.sharedInstance.protocolsMainArray.firstIndex(of: model)!
                    model.checked = true
                    NetworkHelper.sharedInstance.protocolsMainArray[index] = model
                }
            }
        }
        
    }
    
    
    
    func resetPolicyProtocols()
    {
        
        protocolsNameArray.removeAll()
        protocolsNameArray.append("FTP Control")
        protocolsNameArray.append("IPP")
        protocolsNameArray.append("Hotmail")
        protocolsNameArray.append("Xbox")
        protocolsNameArray.append("HTTP Download")
        protocolsNameArray.append("SSl NO CERT")
        protocolsNameArray.append("IRC")
        protocolsNameArray.append("Jabber")
        protocolsNameArray.append("MSN")
        protocolsNameArray.append("Yahoo")
        protocolsNameArray.append("Battlefield")
        protocolsNameArray.append("Quake")
        protocolsNameArray.append("World Of War Craft")
        protocolsNameArray.append("Telnet")
        protocolsNameArray.append("ICMP")
        protocolsNameArray.append("RDP")
        protocolsNameArray.append("VNC")
        protocolsNameArray.append("Pc Anywhere")
        protocolsNameArray.append("SSL")
        protocolsNameArray.append("SSH")
        protocolsNameArray.append("SIP")
        protocolsNameArray.append("Dofus")
        protocolsNameArray.append("Fiesta")
        protocolsNameArray.append("Guildwars")
        protocolsNameArray.append("HTTP Application Active Sync")
        protocolsNameArray.append("Warcraft3")
        protocolsNameArray.append("World Of Kung Fu")
        protocolsNameArray.append("Slack")
        protocolsNameArray.append("Facebook")
        protocolsNameArray.append("Twitter")
        protocolsNameArray.append("Dropbox")
        protocolsNameArray.append("Gmail")
        protocolsNameArray.append("Google Maps")
        protocolsNameArray.append("Youtube")
        protocolsNameArray.append("Skype")
        protocolsNameArray.append("Google")
        protocolsNameArray.append("HTTP Connect")
        protocolsNameArray.append("HTTP Proxy")
        protocolsNameArray.append("Citrix")
        protocolsNameArray.append("Netflix")
        protocolsNameArray.append("Lastfm")
        protocolsNameArray.append("Waze")
        protocolsNameArray.append("Citrix Online")
        protocolsNameArray.append("Apple")
        protocolsNameArray.append("Webex")
        protocolsNameArray.append("Whatsapp")
        protocolsNameArray.append("icloud")
        protocolsNameArray.append("Viber")
        protocolsNameArray.append("itunes")
        protocolsNameArray.append("Windows Update")
        protocolsNameArray.append("Remotescan")
        protocolsNameArray.append("Spotify")
        protocolsNameArray.append("h323")
        protocolsNameArray.append("Openvpn")
        protocolsNameArray.append("Ciscovpn")
        protocolsNameArray.append("Tor")
        protocolsNameArray.append("Ubuntuone")
        protocolsNameArray.append("Ftpdata")
        protocolsNameArray.append("Wikipedia")
        protocolsNameArray.append("Ebay")
        protocolsNameArray.append("CNN")
        protocolsNameArray.append("Telegram")
        protocolsNameArray.append("Vevo")
        protocolsNameArray.append("Pandora")
        protocolsNameArray.append("Whatsapp Voice")
        protocolsNameArray.append("kakaotalk")
        protocolsNameArray.append("Kakaotalk Voice")
        protocolsNameArray.append("Twitch")
        protocolsNameArray.append("Quickplay")
        protocolsNameArray.append("Snapchat")
        protocolsNameArray.append("Deezer")
        protocolsNameArray.append("Instagram")
        protocolsNameArray.append("Microsoft")
        protocolsNameArray.append("Office 365")
        protocolsNameArray.append("Cloudflare")
        protocolsNameArray.append("One drive")
        protocolsNameArray.append("MQTT")
        protocolsNameArray.append("Starcraft")
        
        NetworkHelper.sharedInstance.protocolsMainArray.removeAll()
        
        for value in protocolsNameArray
        {
            let model = ProtocolModel()
            model.title = value
            model.checked = false
            NetworkHelper.sharedInstance.protocolsMainArray.append(model)
            print(model.title)
            print(model.checked)
        }
        
    }
    
    func refreshPolicies()
    {
//        let result = PolicyManagementRealm.retrievePolicies()
//        policies.removeAll()
//        policyModelArray.removeAll()
//        for object in result
//        {
//            let policyObject = PolicyModel()
//            policyObject.title = object.title
//            policyObject.ID = object.id
//            policyObject.startTime = object.startTime
//            policyObject.endTime = object.endTime
//            policyObject.days = object.days
//            policyObject.protocols = object.protocols
//            policyObject.targetURL = object.targetURL
//            policyObject.hostList = object.hostList
//            policies.append(object.title!)
//            policyModelArray.append(policyObject)
//
//        }
//        self.viewPolicyTableView.reloadData()
        
    }
    
    
    
    @objc func reloadTable()
    {
        self.refreshPolicies()
        self.viewPolicyTableView.reloadData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        let result = PolicyManagementRealm.retrievePolicies()
        //        for object in result
        //        {
        //            policies.append(object.title!)
        //        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "POLICIES"
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(reloadTable),
            name: NSNotification.Name(rawValue: "ViewPolicyNotification"),
            object: nil)
        
        
        self.refreshPolicies()
        self.viewPolicyTableView.reloadData()
        
    }
    
    func checkDaysSelected(_ daysString : String)
    {
        NetworkHelper.sharedInstance.mondayChecked = false
        NetworkHelper.sharedInstance.tuesdayChecked = false
        NetworkHelper.sharedInstance.wednesdayChecked = false
        NetworkHelper.sharedInstance.thursdayChecked = false
        NetworkHelper.sharedInstance.fridayChecked = false
        NetworkHelper.sharedInstance.saturdayChecked = false
        NetworkHelper.sharedInstance.sundayChecked = false
        
        if daysString.range(of: "Mon") != nil{
            NetworkHelper.sharedInstance.mondayChecked = true
        }
        if daysString.range(of: "Tue") != nil{
            NetworkHelper.sharedInstance.tuesdayChecked = true
        }
        if daysString.range(of: "Wed") != nil{
            NetworkHelper.sharedInstance.wednesdayChecked = true
        }
        if daysString.range(of: "Thu") != nil{
            NetworkHelper.sharedInstance.thursdayChecked = true
        }
        if daysString.range(of: "Fri") != nil{
            NetworkHelper.sharedInstance.fridayChecked = true
        }
        if daysString.range(of: "Sat") != nil{
            NetworkHelper.sharedInstance.saturdayChecked = true
        }
        if daysString.range(of: "Sun") != nil{
            NetworkHelper.sharedInstance.sundayChecked = true
        }
    }
    
    //table view delegates
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
//        self.resetPolicyProtocols()
//        var policyObject = PolicyModel()
//        policyObject = policyModelArray[indexPath.row]
//        self.getSelectedProtocolsWithString(policyObject.protocols!)
//        let result = PolicyManagementRealm.retrievePoliciesWithID(policyObject.ID!)
//        let object = result.first
//        let policyManagementVC = PolicyManagement.policyManagementVC()
//        self.setNavBar()
//        policyManagementVC.updatePolicy = true
//        policyManagementVC.idSelectedForUpdate = policyObject.ID
//        policyManagementVC.titleString=object?.title
//        policyManagementVC.startTimeString=object?.startTime
//        policyManagementVC.endTimeString=object?.endTime
//        policyManagementVC.daysString=object?.days
//        self.checkDaysSelected((object?.days)!)
//        policyManagementVC.hostListString=object?.hostList
//        policyManagementVC.targetURLString=object?.targetURL
//        let policyArrProtocols = object?.protocols?.components(separatedBy: ",") //object!.protocols?.componentsSeparated(by: ",")
//        let mapperProtocols = ProtocolsMapper()
//        let protocolsArr = mapperProtocols.mapProtocolsWithArray(policyArrProtocols!)
//        let protocolsString : String? = protocolsArr.joined(separator: ",")//protocolsArr.joinWithSeparator(",")
//        policyManagementVC.protocolsString=protocolsString
//
//        self.navigationController?.pushViewController(policyManagementVC, animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0//policies.count
    }
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, commitEditingStyle editingStyle: UITableViewCell.EditingStyle, forRowAtIndexPath indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            
            let title : String = policies[indexPath.row]
            var policyObject = PolicyModel()
            policyObject = policyModelArray[indexPath.row]
            //call delete service
            NetworkHelper.sharedInstance.error401 = false
            let randomNumber = Int(arc4random_uniform(10000) + 1)
            let requestID : String = String(randomNumber)
            let data : [String : AnyObject] = ["type": "delete" as AnyObject, "id" : policyObject.ID! as AnyObject]
            let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject, "data" : data as AnyObject]
            
            NetworkHelper.sharedInstance.postRequest(serviceName: "https://api.dmoat.com/post/policy", sendData: params, success: { (data) in
                print(data)
                
                //                                let success = data["success"]?.boolValue
                //                                let message = data["message"].string
                let defaults = UserDefaults.standard
                let statusCode = defaults.string(forKey: "statusCode")
                
                if ((statusCode?.range(of: "200")) != nil){
                    
                    DispatchQueue.main.async {
                         self.showAlert("Success")
                                           self.policies.remove(at: indexPath.row)
                                           tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
                    }
                    //PolicyManagementRealm.deleteRecordWith(policyObject.ID!)
                    
                }
                else if ((statusCode?.range(of: "409")) != nil){
                    NetworkHelper.sharedInstance.isDmoatOnline = false
                    let msg = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(msg)
                    }
                } else if ((statusCode?.range(of: "401")) != nil) {
                    if(NetworkHelper.sharedInstance.deviceToken != nil)
                    {
                        (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                        (UIApplication.shared.delegate as! AppDelegate).unsubPubnubPushNotifications()
                        (UIApplication.shared.delegate as! AppDelegate).UnsubUserForNotifications()
                    }
                    //unsubscribe from all
                    DispatchQueue.main.async {
                        (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                    }
                    //clear nsuserdefaults
                    NetworkHelper.sharedInstance.clearUserData()
                    //delete all data
                    AlertModelRealm.deleteAllRecords()
                    PolicyManagementRealm.deleteAllRecords()
                    let loginVC = Login.loginVC()
                    self.navigationController?.pushViewController(loginVC, animated: true)
                    
                } else if (data["message"] != nil) {
                    let message = data["message"] as! String
                    DispatchQueue.main.async {
                        self.showAlert(message)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.showAlert(ConstantStrings.ErrorText)
                    }
                }
                
                
            }, failure: { (data) in
                print(data)
                if(NetworkHelper.sharedInstance.error401)
                {
                    NetworkHelper.sharedInstance.isNotificationLocked =  true
                    //mute notifications
                    if(NetworkHelper.sharedInstance.deviceToken != nil)
                    {
                        (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                    }
                    //unsubscribe from all
                    (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                    //clear nsuserdefaults
                    NetworkHelper.sharedInstance.clearUserData()
                    //delete all data
                    //AlertModelRealm.deleteAllRecords()
                    PolicyManagementRealm.deleteAllRecords()
                    let loginVC = Login.loginVC()
                    self.navigationController?.pushViewController(loginVC, animated: true)
                }
                
            })
            
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        var rowHeightDeduct : Int = 0
        self.viewPolicyTableView.rowHeight = 182
        let viewPolicyCell =  self.viewPolicyTableView.dequeueReusableCell(withIdentifier: "viewPolicyCell") as! ViewPolicyCell
        viewPolicyCell.heightConstraintProtocolsList.constant = 21.0
        viewPolicyCell.heightConstraintHostList.constant = 21.0
        viewPolicyCell.heightConstraintURL.constant = 23.0
        let policyModel = policyModelArray[indexPath.row]
        let policyNumberInteger : Int = indexPath.row + 1
        viewPolicyCell.policyTitle.text = String(policyNumberInteger) + ". " + policies[indexPath.row]
        viewPolicyCell.startTime.text = "Start Time: " + policyModel.startTime!
        viewPolicyCell.endTime.text = "End Time: " + policyModel.endTime!
        viewPolicyCell.days.text = "Week Days: " + policyModel.days!
        
        let policyArrProtocols = policyModel.protocols!.components(separatedBy: ",")
        let mapperProtocols = ProtocolsMapper()
        let protocolsArr = mapperProtocols.mapProtocolsWithArray(policyArrProtocols)
        policyModel.protocols = protocolsArr.joined(separator: ",")
        if(policyModel.protocols != "")
        {
            viewPolicyCell.protocols.text = "Protocols List: " + policyModel.protocols!
        }
        else{
            viewPolicyCell.heightConstraintProtocolsList.constant = 0.0
            rowHeightDeduct = rowHeightDeduct + 23
        }
        if(policyModel.hostList != "")
        {
            viewPolicyCell.hostList.text = "Host List: " + policyModel.hostList!
        }
        else{
            viewPolicyCell.heightConstraintHostList.constant = 0.0
            rowHeightDeduct = rowHeightDeduct + 23
        }
        if(policyModel.targetURL != "")
        {
            viewPolicyCell.url.text = "Target URL: " + policyModel.targetURL!
        }
        else{
            viewPolicyCell.heightConstraintURL.constant = 0.0
            rowHeightDeduct = rowHeightDeduct + 23
        }
        
        self.viewPolicyTableView.rowHeight = 182 - CGFloat(rowHeightDeduct)
        return viewPolicyCell
        
    }
    
    
    
    
    
}
