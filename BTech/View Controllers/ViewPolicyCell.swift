//
//  ViewPolicyCell.swift
//  BTech
//
//  Created by Ahmed Akhtar on 06/01/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class ViewPolicyCell: UITableViewCell {

    //iboutlet
    
    @IBOutlet var heightConstraintProtocolsList: NSLayoutConstraint!
    
    @IBOutlet var heightConstraintURL: NSLayoutConstraint!
    
    @IBOutlet var heightConstraintHostList: NSLayoutConstraint!
    
    @IBOutlet var policyTitle: UILabel!
    
    @IBOutlet var startTime: UILabel!
    
    @IBOutlet var endTime: UILabel!
    
    @IBOutlet var days: UILabel!
    
    @IBOutlet var protocols: UILabel!
    
    @IBOutlet var url: UILabel!
    
    @IBOutlet var hostList: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
