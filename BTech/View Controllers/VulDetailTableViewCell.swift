//
//  VulDetailTableViewCell.swift
//  BTech
//
//  Created by Ahmed Akhtar on 18/11/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class VulDetailTableViewCell: UITableViewCell {

    //Outlets
    
    @IBOutlet weak var bulletMarkLabel: UILabel!
    @IBOutlet weak var cellTextView: UITextView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellTextView.textContainerInset = UIEdgeInsets.zero
        cellTextView.textContainer.lineFragmentPadding = 0
        //cellTextView.backgroundColor = UIColor.orangeColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
