//
//  VulnDetailReferenceVC.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 12/07/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class VulnDetailReferenceVC: Parent {
    
    //Mark: - class outlets
    @IBOutlet weak var table_view: UITableView!
    
    @IBOutlet weak var vuln_desc: UITextView!
    
    @IBOutlet weak var vuln_pub_date: UILabel!
    
    class func VulDetailRefVC() -> VulnDetailReferenceVC {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VulDetailRef") as! VulnDetailReferenceVC
    }
    
    //Mark: - class variables
    var titleText:String = ""
    var refArr = [String]()
//    var refArrJson = String()
    var listVulnerability = VulnerabilityModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleText = "Vulnerability Detail"
        self.navigationController!.navigationBar.topItem?.title = ""
        self.title = titleText
        //Mark: -Defaults and string split
        let defaults = UserDefaults.standard
        let desc = defaults.string(forKey: "desc")
        let pDate =  defaults.string(forKey: "date")
        var refernceList = defaults.string(forKey: "references")
        refernceList = refernceList!.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range: nil)
        refernceList = refernceList!.replacingOccurrences(of: "(", with: "", options: NSString.CompareOptions.literal, range: nil)
        refernceList = refernceList!.replacingOccurrences(of: ")", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        refArr = refernceList!.components(separatedBy: ",")
        
        vuln_desc.text = desc
        vuln_pub_date.text = pDate
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark: - Tableview delegates
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("rowSelected\(indexPath.row)")
        
        var a = listVulnerability.jsonArrRef[indexPath.row] /// as? String
////        a = a.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)
//        print(a)
//        UIApplication.shared.openURL(URL(fileURLWithPath: a))
        
        guard let url = URL(string: listVulnerability.jsonArrRef[indexPath.row]) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listVulnerability.jsonArrRef.count
        
    }
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  self.table_view.dequeueReusableCell(withIdentifier: "vuln_ref_cell") as! VulnerabilityRefCell
        var a = listVulnerability.jsonArrRef[indexPath.row] as? String
        a = a!.replacingOccurrences(of: "\n", with: "", options: NSString.CompareOptions.literal, range: nil)
        cell.ref_url.text = a
        
        return cell
        
    }
   
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {

        self.navigationController?.popToRootViewController(animated: true)

    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        
        //        account_label_tabbar.textColor = UIColor(red: 0/255, green: 174/255, blue: 239/255, alpha: 1.0)
        //        var image: UIImage = UIImage(named: "tab_home_selected")!
        //        account_img_tabbar.image = image
        
        if(self.checkPausedTimeAsAndroid()){
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
      
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
}
