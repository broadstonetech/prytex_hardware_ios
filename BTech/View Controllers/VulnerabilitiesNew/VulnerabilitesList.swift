//
//  VulnerabilitesList.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 11/07/2017.
//  Copyright © 2017 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class VulnerabilitesList: Parent {
    
    
    class func VulDetailListVC() -> VulnerabilitesList {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VulDetailList") as! VulnerabilitesList
    }
    
    //Mark: - class outlets
    @IBOutlet weak var table_view: UITableView!
    
    //Mark: - class variables
    var titleText:String = ""
    var vulnerabilitiesList: [VulnerabilityModel] = [VulnerabilityModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadVulnerabilities()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.titleText = "Vulnerabilities"
        self.navigationController!.navigationBar.topItem?.title = ""
        
        self.navigationItem.title = titleText
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Mark: - Tableview delegates
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("rowSelected\(indexPath.row)")
        
        let defaults = UserDefaults.standard
        defaults.set(vulnerabilitiesList[indexPath.row].getDescription(), forKey: "desc")
        defaults.set(vulnerabilitiesList[indexPath.row].getPubDate(), forKey: "date")
        defaults.set(vulnerabilitiesList[indexPath.row].getReferences(), forKey: "references")
        
        let alertVC = VulnDetailReferenceVC.VulDetailRefVC()
        alertVC.listVulnerability = vulnerabilitiesList[indexPath.row]
        self.navigationController?.pushViewController(alertVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vulnerabilitiesList.count
        
    }
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  self.table_view.dequeueReusableCell(withIdentifier: "Vulnerabiltiy_cell") as! VulnerabilityListCell
        
        let item = self.vulnerabilitiesList[indexPath.row]
        
        
        cell.host_name.text = item.getTitle()
        cell.attack_complexity.text = item.getAttackComplexity()
        cell.attack_vector.text = item.getAttackVector()
        cell.vuln_risk.text = item.getRisk()
        cell.vul_description.text = item.getDescription()
        cell.date.text = item.getPubDate()
        
        return cell
        
    }
    // MARK: - Actions
    
    @IBAction func screen_info_pressed(_ sender: Any) {
    }
    
    //Mark: -WebService call
    func loadVulnerabilities() {
        
        let defaults = UserDefaults.standard
        let name = defaults.string(forKey: "device_id")
        let hostName =  defaults.string(forKey: "hostName")
        let hostOs = defaults.string(forKey: "hostOs")
        
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "hostname" : hostName! as AnyObject, "OS" : hostOs! as AnyObject, "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: ApiUrl.GET_VULNERABILITIES, sendData: params, success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            let message = "Request failed. Please try again."
            
            if(NetworkHelper.sharedInstance.isConnectedDevicesSuccess)
            {
                let jsonArr = data["message"] as? [[String:AnyObject]]
                
                print(jsonArr?.count)
                //                            print(jsonArr!)
                if((jsonArr?.count)! > 0){
                    print("json array received for vulnerabilities")
                    self.fetchData(jsonArr!)
                    
                }else{
                    self.showAlert("No vulnerability found.")
                }
            }
            else{
                self.showAlert(message)
            }
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked =  true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
    }
    
    //MARK: - fetch data
    func fetchData(_ jsonArr : [[String : AnyObject]]){
        
        for element in jsonArr {
            let title = element["title"] as! String
            let attackVector = element["attack_vector"] as! String
            let risk = element["risk"] as! String
            let attackComplexity = element["attack_complexity"] as! String
            let references = element["references"]
            let description = element["description"] as! String
            let publishedDate = element["published_date"] as! String
            //add into model class
            let vuln = VulnerabilityModel()
            vuln.setTitle(title)
            vuln.setRisk(risk)
            vuln.setAttackVector(attackVector)
            vuln.setAttackComplexity(attackComplexity)
            vuln.setPubDate(publishedDate)
            vuln.setReferences(references!)
            vuln.jsonArrRef = references as! [String]
            vuln.setDescription(description)
            
            vulnerabilitiesList.append(vuln)
        }
        
        DispatchQueue.main.async {
            self.table_view.reloadData()
        }
        
    }
    
    
    //MARK: -Tabbar actions
    @IBAction func home_btn_dashboard_pressed(_ sender: Any) {

        self.navigationController?.popToRootViewController(animated: true)

    }
    
    @IBAction func policy_btn_dashboard_pressed(_ sender: Any) {
        
        let vcc = PauseInternetVC.PauseInternetVC()
        vcc.titleText="Policy"
        //            alertVC.alertType="connected_devices"
        self.navigationController?.pushViewController(vcc, animated: true)
    }
    
    @IBAction func account_btn_dashboard_pressed(_ sender: Any) {
        
        //        account_label_tabbar.textColor = UIColor(red: 0/255, green: 174/255, blue: 239/255, alpha: 1.0)
        //        var image: UIImage = UIImage(named: "tab_home_selected")!
        //        account_img_tabbar.image = image
        
        if(self.checkPausedTimeAsAndroid()){
            
            let profileVC = Profile.ProfileVC()
            self.setNavBar()
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            self.showAlert(ConstantStrings.pausedIntenettext)
        }
    }
    
    @IBAction func help_btn_dashboard_pressed(_ sender: Any) {
       
        let VC = HelpTutorialsVC.helpTutorialsVC()
        self.setNavBar()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
}
