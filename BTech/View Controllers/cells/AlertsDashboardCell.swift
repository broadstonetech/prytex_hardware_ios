//
//  AlertsDashboardCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 12/02/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class AlertsDashboardCell: UITableViewCell {

    @IBOutlet weak var alert_description: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
