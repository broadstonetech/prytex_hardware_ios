//
//  BandwidthDashboardCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 12/02/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class BandwidthDashboardCell: UITableViewCell {

    @IBOutlet weak var hostname_label: UILabel!
    @IBOutlet weak var legend_clr_label: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
