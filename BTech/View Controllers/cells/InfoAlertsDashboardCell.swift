//
//  InfoAlertsDashboardCell.swift
//  BTech
//
//  Created by MacBook-Mubashar on 10/10/2019.
//  Copyright © 2019 Broadstone Technologies. All rights reserved.
//

import UIKit

class InfoAlertsDashboardCell: UITableViewCell {

    @IBOutlet weak var parent_view: UIView!
    
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    @IBOutlet weak var statusIcon_img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
