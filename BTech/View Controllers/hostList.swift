//
//  hostList.swift
//  BTech
//
//  Created by Ahmed Akhtar on 27/12/2016.
//  Copyright © 2016 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class hostList: Parent {
    
    //iboutlet
    
    @IBOutlet var hostListTableView: UITableView!
    
    @IBOutlet var okBtn: UIButton!
    
    
    //ibaction
    
    @IBAction func okPressed(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "getHostListNotification"), object: nil);
        })
        
    }
    
    
    class func hostListVC() -> hostList {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "hostList") as! hostList
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(reloadTable),
            name: NSNotification.Name(rawValue: "HostListNotification"),
            object: nil)
        NetworkHelper.sharedInstance.connectedHostList = true
        self.callHostListService()
    }
    
    @objc func reloadTable()
    {
        self.hostListTableView.reloadData()
    }
    
    
    func callHostListService()
    {
        NetworkHelper.sharedInstance.error401 = false
        NetworkHelper.sharedInstance.arrayConnectedDevices.removeAll()
        let randomNumber = Int(arc4random_uniform(10000) + 1)
        let requestID : String = String(randomNumber)
        let params:Dictionary<String,AnyObject> = ["request_id": requestID as AnyObject, "token": NetworkHelper.sharedInstance.getCurrentToken() as AnyObject, "postkey": NetworkHelper.sharedInstance.getCurrentUserPostKey() as AnyObject , "namespace": NetworkHelper.sharedInstance.getCurrentUserNamespace() as AnyObject, "app_id" : NetworkHelper.sharedInstance.app_ID! as AnyObject]
        
        NetworkHelper.sharedInstance.postRequest(serviceName: "https://api.dmoat.com/get/cnctdhosts", sendData: params, success: { (data) in
            print(data)
            
            
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            
            if ((statusCode?.range(of: "200")) != nil){
                
            }
            else if ((statusCode?.range(of: "409")) != nil){
                NetworkHelper.sharedInstance.isDmoatOnline = false
                let msg = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(msg)
                }
            } else if (data["message"] != nil) {
                let message = data["message"] as! String
                DispatchQueue.main.async {
                    self.showAlert(message)
                }
            } else {
                DispatchQueue.main.async {
                    self.showAlert(ConstantStrings.ErrorText)
                }
            }
            //
            
            
        }, failure: { (data) in
            print(data)
            if(NetworkHelper.sharedInstance.error401)
            {
                NetworkHelper.sharedInstance.isNotificationLocked = true
                //mute notifications
                if(NetworkHelper.sharedInstance.deviceToken != nil)
                {
                    (UIApplication.shared.delegate as! AppDelegate).muteNotifications()
                }
                //unsubscribe from all
                (UIApplication.shared.delegate as! AppDelegate).unsubscribeFromAll()
                //clear nsuserdefaults
                NetworkHelper.sharedInstance.clearUserData()
                //delete all data
                //AlertModelRealm.deleteAllRecords()
                PolicyManagementRealm.deleteAllRecords()
                let loginVC = Login.loginVC()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
            
        })
        
    }
    
    
    //table view delegates
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NetworkHelper.sharedInstance.arrayConnectedDevices.count
    }
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let alertHostList =  self.hostListTableView.dequeueReusableCell(withIdentifier: "hostListDetailCell")! as! HostListDetailCell
        var model = AlertModel()
        model = NetworkHelper.sharedInstance.arrayConnectedDevices[indexPath.row]
        alertHostList.checkBox.tag = indexPath.row
        alertHostList.osName.text = model.OS
        alertHostList.ipAddress.text = model.ip
        alertHostList.hostName.text = model.hostname
        alertHostList.macAddress.text = model.macaddress
        if(model.checked == true)
        {
            alertHostList.checkBox.setImage( UIImage(named:"checkbox"), for: UIControl.State())
        }
        else{
            alertHostList.checkBox.setImage( UIImage(named:"uncheckbox"), for: UIControl.State())
        }
        
        return alertHostList
        
        
    }
    
    
}
