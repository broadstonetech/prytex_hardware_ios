//
//  policyDevicesCell.swift
//  BTech
//
//  Created by Mac Book Pro 13 on 09/05/2018.
//  Copyright © 2018 NextBridge Pvt Ltd. All rights reserved.
//

import UIKit

class policyDevicesCell: UITableViewCell {

    @IBOutlet weak var device_name: UILabel!
    
    @IBOutlet weak var device_ip: UILabel!
    
    @IBOutlet weak var host_ip_view: UIView!
    
    @IBOutlet weak var imgCheckMark: UIImageView!
    
    @IBOutlet weak var protocol_img: UIImageView!
    
    @IBOutlet weak var protocol_img_width: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
